
// Check for each action that field is filled or not

function validateEachBasic()
{   			
	var alfanumeric = /^[a-zA-Z0-9]*$/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
    var fullName = document.getElementById("fullNameId").value;
    var emailId = document.getElementById("EmailsId").value;
    var mobileNo = document.getElementById("mobilNoId").value;
    var address = document.getElementById("addressId").value;
    var password = document.getElementById("pwId").value;
    var confirmPassword = document.getElementById("cnPwId").value;
    
    var agree = document.getElementById("agreeId");
  
    if(fullName!="")
	{
		if(!fullName.match(nameWithSpace))  
        {  			
			$("#nameError").show();
	        $("#nameError").html("Only alphabets are allowed");	
	        return false;       
        }
		else
		{
			$("#nameError").hide();
		}
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			$("#emailError").show();
	        $("#emailError").html("Please enter a valid email id.");	
	        return false;       
        }
		else
		{
			$("#emailError").hide();
		}
	}
       
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			flag='false';
			$("#mobileError").show();
	        $("#mobileError").html("Please enter a valid mobile no.");	
	        return false;       
        }
		else
		{
			$("#mobileError").hide();
		}
	}
    
    if(address!="")
	{
		$("#addressError").hide();
	}
    
    if(password!="")
	{
		$("#pwError").hide();
	}
   
    if(confirmPassword!="")
	{
		$("#cnpwError").hide();	 
	}
   
    if(agree.checked==true)
    {
		$("#checkError").hide();	
    }     
}


//  Save basic details of User 

function saveBasicUserDetails()
{   		
    var flag='true';
	var alfanumeric = /^.*[a-zA-z].*\d.*/;        	
	var nameWithOutSpace = /^[a-zA-Z]+$/;
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	var poi= /^[a-zA-Z0-9]{6,20}$/;
	
    var fullName = document.getElementById("fullNameId").value;
    var emailId = document.getElementById("EmailsId").value;
    var mobileNo = document.getElementById("mobilNoId").value;
    var address = document.getElementById("addressId").value;
    var password = document.getElementById("pwId").value;
    var confirmPassword = document.getElementById("cnPwId").value;
    
    var agree = document.getElementById("agreeId");
      
    if(fullName=="")
	{
    	flag='false';
		$("#nameError").show();
        $("#nameError").html("Please enter your full name.");	
        return false; 
	}
    
    if(fullName!="")
	{
		if(!fullName.match(nameWithSpace))  
        {  			
			flag='false';
			$("#nameError").show();
	        $("#nameError").html("Only alphabets with space are allowed");	
	        return false;       
        }		
	}
    
    if(emailId=="")
	{
    	flag='false';
		$("#emailError").show();
        $("#emailError").html("Please enter a email id.");	
        return false; 
	}
    
    if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag='false';
			$("#emailError").show();
	        $("#emailError").html("Please enter a valid email id.");	
	        return false;       
        }		
	}
    
    if(mobileNo=="")
	{
    	flag='false';
		$("#mobileError").show();
        $("#mobileError").html("Please enter a mobile no.");	
        return false; 
	}
    
    if(mobileNo!="")
	{
		if(!mobileNo.match(phNo))  
        {  			
			flag='false';
			$("#mobileError").show();
	        $("#mobileError").html("Please enter a valid mobile no.");	
	        return false;       
        }		
	}
    
    if(address=="")
	{
    	flag='false';
		$("#addressError").show();
        $("#addressError").html("Please enter your shipping address with pincode.");	
        return false; 
	}
    
    if(password=="")
	{
    	flag='false';
		$("#pwError").show();
        $("#pwError").html("Please enter a password.");	
        return false; 
	}
    
    
    if(password.length<8)
	{
    	flag='false';
		$("#pwError").show();
        $("#pwError").html("Your password atleast 8 character.");	
        return false; 
	}
    
    if(password.length>=8)
	{   	
    	if(!password.match(alfanumeric))  
        {  			
    		flag='false';
    		$("#pwError").show();
            $("#pwError").html("Your password should be alphanumeric.");	
            return false;       
        }   	
	}
    
    if(confirmPassword=="")
	{
    	flag='false';
		$("#cnpwError").show();
        $("#cnpwError").html("Please confirm your entered password.");	
        return false; 
	}
    
    if(password!=confirmPassword)
	{
    	flag='false';
    	document.getElementById("cnPwId").value = "";
		$("#cnpwError").show();
        $("#cnpwError").html("Password and confirm password not matched.");	
        return false; 
	}
    
    if(agree.checked==false)
    {
    	flag='false';
		$("#checkError").show();
        $("#checkError").html("Please accept that you agree with our terms & conditions.");	
        return false; 	
    }
    
    if(flag=='true')
    {
    	$("#progressBar").show();
    	
    	$.ajax({  
			
		     type : "post",   
		     url : "regValidateEmailId.html", 
		     data :$('#basicProfileForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	 
		    	 if(response!="exist")
		    		 {
					    // Save Script Part
						    	 
						    	 $.ajax({  
						 			
								     type : "post",   
								     url : "saveUserRegister.html", 
								     data :$('#basicProfileForm').serialize(),	     	     	     
								     success : function(response) 
								     {  		
								    	 $("#progressBar").hide();
								    	 
										 if(response=="success")
											 {			
											     $("#popUpheading").html("Success Message");
											     $("#popUpMessage").html("Your basic information have been saved successfully.");	
											     $("#popUpDiv").show();
											 }
										 else
											 {
											     alert("Please Try again later!");
											 }
								     },  
							     });  
						    	 
						// End of Script Part
		    		 }
		    	 else
		    		 {
		    		     $("#progressBar").hide(); 
		    		 
		    		 	 $("#emailError").show();
		    		 	 $("#emailError").html("Given email id is already exist !");	
		 	             return false;       
		    		 }				    	 
		     },  
	     });  
    	
    }
}

// End of save basic details in Db


// Products under Category 

function showProducts(type)
{	
	var currentValue = "";
	
	if(type=="Agriculture")
		{
			document.getElementById("hmCarId").checked = false;
			document.getElementById("herId").checked = false;
			
			currentValue = document.getElementById("agriProdId").value;
		}
	else if(type=="Herbal")
		{
		    document.getElementById("agriId").checked = false;
		    document.getElementById("hmCarId").checked = false;
		    
		    currentValue = document.getElementById("herbalProdId").value;
		}
	else
		{
			document.getElementById("agriId").checked = false;
			document.getElementById("herId").checked = false;
			
			currentValue = document.getElementById("hmCareProdId").value;
		}
	
	$("#progressBar").show();
	
	$.ajax({  
			
	     type : "post",   
	     url : "getProductAsPerType.html", 
	     data : "type="+type+"&existValue="+currentValue,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 $("#featurezone").html(response);			
	     },  
    });  
	
	
}

// End of Search products under category


/////////////////////////////////////////////////



//  Show more about your product in a pop up

function viewMore(productId,productName)
{
    $("#progressBar").show();
	
	$.ajax({  
			
	     type : "post",   
	     url : "showMoreAboutProduct.html", 
	     data : "productId="+productId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 $("#popUPHeading").html("About "+productName);
	    	 $("#prductDetails").html(response);
	    	 $("#productDetailsPopUp").slideDown();
	    	 
	     },  
    });  
}

// End of show more about the product in the pop up



/////////////////////////////////////////////////


//  capture product Id when user click on the select checkbox

function captureSelectedProduct(selectId)
{	
	var currentSelectValue = document.getElementById(selectId).value;
	
	if(document.getElementById("agriId").checked == true)
	{   						
		if(document.getElementById(selectId).checked == true)
		{			
			document.getElementById("agriProdId").value += currentSelectValue + ",";			
		}
		else
		{
			var agriExtvalue = document.getElementById("agriProdId").value;	
			agriExtvalue = agriExtvalue.substring(0,agriExtvalue.length-1);
			
			var arrayValue = agriExtvalue.split(",");
			var temp = "";
			
			for(var i=0;i<arrayValue.length;i++)
				{
					if(currentSelectValue!=arrayValue[i])
						{
						   temp += arrayValue[i] + ",";
						}					
				}
			document.getElementById("agriProdId").value = temp;
		}
	}
	else if(document.getElementById("herId").checked == true)
	{		
		if(document.getElementById(selectId).checked == true)
		{
			document.getElementById("herbalProdId").value += currentSelectValue + ",";	
		}
		else
		{
			var herbalExtvalue = document.getElementById("herbalProdId").value;	
			herbalExtvalue = herbalExtvalue.substring(0,herbalExtvalue.length-1);
			
			var arrayValue = herbalExtvalue.split(",");
			var temp = "";
			
			for(var i=0;i<arrayValue.length;i++)
				{
					if(currentSelectValue!=arrayValue[i])
						{
						   temp += arrayValue[i] + ",";
						}					
				}

			document.getElementById("herbalProdId").value = temp;
		}
	}
	else
	{				
		if(document.getElementById(selectId).checked == true)
		{
			document.getElementById("hmCareProdId").value += currentSelectValue + ",";	
		}
		else
		{
			var hmCareExtvalue = document.getElementById("hmCareProdId").value;
			hmCareExtvalue = hmCareExtvalue.substring(0,hmCareExtvalue.length-1);
			
			var arrayValue = hmCareExtvalue.split(",");
			var temp = "";
			
			for(var i=0;i<arrayValue.length;i++)
				{
					if(currentSelectValue!=arrayValue[i])
						{
						   temp += arrayValue[i] + ",";
						}					
				}
			
			document.getElementById("hmCareProdId").value = temp;
		}
	}	
	
	var hmCare = document.getElementById("hmCareProdId").value;
	var herBal = document.getElementById("herbalProdId").value;
	var agriPro = document.getElementById("agriProdId").value;
		
	var noOfProducts =  (hmCare.split(",").length-1) + (herBal.split(",").length-1) + (agriPro.split(",").length-1);
	
	$("#noOfProId").html(noOfProducts);
}


function showCartProducts()
{
	var hmCare = document.getElementById("hmCareProdId").value;
	var herBal = document.getElementById("herbalProdId").value;
	var agriPro = document.getElementById("agriProdId").value;
	
	$("#progressBar").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "getProductDetailsById.html", 
	     data :"agriProId="+agriPro+"&herBalId="+herBal+"&hmCareId="+hmCare,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 
	    	 $("#cartDetailsPopUp").slideDown();
			 $("#cartDetailsPopUp").html(response);				 	
			 
	     },  
    });  
	
	
	
}


//  End of capture product Id when user click on the select checkbox



/////////////////////////////////////////////////



//  Save selected product and move to refer friend

function saveProductSelection()
{
	var agri = document.getElementById("agriProdId").value
	var herbal = document.getElementById("herbalProdId").value
	var homecare = document.getElementById("hmCareProdId").value
	
	var totalLength = 0;
	var agriLength = agri.split(",");
	var herbalLength = herbal.split(",");
	var homecareLength = homecare.split(",");
	
	if(agriLength.length>0)
	{
		totalLength  += (agriLength.length-1)
	}
	
	if(herbalLength.length>0)
	{
		totalLength  += (herbalLength.length-1)
	}
	
	if(homecareLength.length>0)
	{
		totalLength  += (homecareLength.length-1)
	}
	
	var flag = 'true';
	
	/*if(agri=="")
	{
		flag = 'false';
		alert("Select atleast some product to know your interest.");
		return false;
	}
	if(herbal=="")
	{
		flag = 'false';
		alert("Select atleast some product to know your interest.");
		return false;
	}
	if(homecare=="")
	{
		flag = 'false';
		alert("Select atleast some product to know your interest.");
		return false;
	}*/
	
	
	if(totalLength==0)
	{
		flag = 'false';
		$("#notifypopUpheading").html("Notification");
		$("#notifypopUpMessage").html("Please select some products to know your interest.");
		$("#NotifypopUpDiv").show();
		
		return false;
	}
	
	if(totalLength>10)
	{
		flag = 'false';
		$("#notifypopUpheading").html("Notification");
		$("#notifypopUpMessage").html("Please select minimum 10 products to know your interest.");
		$("#NotifypopUpDiv").show();
		
		return false;
	}
	
	
	if(flag=='true')
		{
		    $("#progressBar").show();
		
			$.ajax({  
				
			     type : "post",   
			     url : "saveProductSelection.html", 
			     data :$('#productSelectionForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#progressBar").hide();
			    	 
					 if(response=="success")
						 {	
						 	$("#popUpheading").html("Success Message");
						 	$("#popUpMessage").html("Your product selection have been saved successfully.");	
						 	$("#popUpDiv").show();
						 }
					 else
						 {
						     alert("Please Try again later!");
						 }
			     },  
		    });  
		}
}

//  End of save product selection and move to refer friend


///////////////////////



// Save REference Details

function checkReferenceDetails()
{
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var referName = document.getElementById("referNameId").value
	var referEmail = document.getElementById("referEmailId").value
	var referMobile = document.getElementById("referMobileNoId").value
	
	if(referName!="")
	{
			if(!referName.match(nameWithSpace))  
	        {  			
				$("#nameError").show();
		        $("#nameError").html("Only alphabets are allowed.");	
		        return false;       
	        }
			else
			{
				$("#nameError").hide();
			}
	 }
	 
	 if(referEmail!="")
	 {
		 	if(!referEmail.match(email))  
	        {  			
				$("#emailIdError").show();
		        $("#emailIdError").html("Enter a valid email id.");	
		        return false;       
	        }
			else
			{
				$("#emailIdError").hide();
			}		 	
	 }
	 
	 if(referMobile!="")
	 {
		 	if(!referMobile.match(phNo))  
	        {  			
				$("#mobileNoError").show();
		        $("#mobileNoError").html("Enter a valid mobile no.");	
		        return false;       
	        }
			else
			{
				$("#mobileNoError").hide();
			}		 	
	 } 
}


function saveReferenceDetails()
{	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var referName = document.getElementById("referNameId").value
	var referEmail = document.getElementById("referEmailId").value
	var referMobile = document.getElementById("referMobileNoId").value
	
	var flag = 'true';
	
	if(referName=="")
	{
		flag = 'false';
		$("#nameError").show();
        $("#nameError").html("Please enter the reference name.");	
        return false;  
	}
	
	 if(referName!="")
	 {
			if(!referName.match(nameWithSpace))  
	        {  		
				flag = 'false';
				$("#nameError").show();
		        $("#nameError").html("Only alphabets are allowed.");	
		        return false;       
	        }
			else
			{
				$("#nameError").hide();
			}
	 }
	 
	 if(referEmail=="")
	 {
		flag = 'false';
		$("#emailIdError").show();
	    $("#emailIdError").html("Please enter the reference email id.");	
	    return false;  
	 }
		
	 if(referEmail!="")
	 {
		 	if(!referEmail.match(email))  
	        {  		
		 		flag = 'false';
				$("#emailIdError").show();
		        $("#emailIdError").html("Enter a valid email id.");	
		        return false;       
	        }
			else
			{
				$("#emailIdError").hide();
			}
		 	
	 }
	 
	 if(referMobile=="")
	 {
		flag = 'false';
		$("#mobileNoError").show();
	    $("#mobileNoError").html("Please enter the reference mobile no.");	
	    return false;  
	 }
		
	 if(referMobile!="")
	 {
		 	if(!referMobile.match(phNo))  
	        {  
		 		flag = 'false';		
				$("#mobileNoError").show();
		        $("#mobileNoError").html("Enter a valid mobile no.");	
		        return false;       
	        }
			else
			{
				$("#mobileNoError").hide();
			}
		 	
	 }
	 
	if(flag=='true')
		{
		    $("#progressBar").show();
		
			$.ajax({  
				
			     type : "post",   
			     url : "saveReferenceDetails.html", 
			     data :$('#referenceForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#progressBar").hide();
			    	 
					 if(response=="success")
						 {	
						 	$("#popUpheading").html("Success Message");
						 	$("#popUpMessage").html("Your reference details have been saved successfully.");	
						 	$("#popUpDiv").show();
						 }
					 else
						 {
						     alert("Please Try again later!");
						 }
			     },  
		    });  
		}
}

// Save reference Details 



// Show Plan Details

function showPlanDetails(planType)
{
	$("#progressBar").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "showPlanDetails.html", 
	     data : "planType="+planType,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 $("#popUPHeading").html("All the Features of "+planType);
	    	 $("#planDetails").html(response);	
	    	 $("#infoPopUpDiv").show();	
	     },  
    });  
	
}

// End of Show Plan details


// capture plan selection 

function capturePlanSelect(planName,divId)
{
   	document.getElementById("planValueId").value = planName;
   	
   	if(divId=="personal")
   		{
   		    document.getElementById(divId).className += ' select'
   		    document.getElementById("professional").className = 'block professional fl'
   		    document.getElementById("business").className = 'block business fl'
   		}
   	else if(divId=="professional")
   		{
   		    document.getElementById("personal").className = 'block personal fl'
   		    document.getElementById(divId).className += ' select'
   		    document.getElementById("business").className = 'block business fl'
   		}
   	else
   		{
   		    document.getElementById("personal").className = 'block personal fl'
   		    document.getElementById("professional").className = 'block professional fl'
   		    document.getElementById(divId).className += ' select'
   		} 	
}

// End of plan Selection


// Save plan Selection 

function savePlanDetails()
{
	var planValue = document.getElementById("planValueId").value;
	
	if(planValue=="")
		{
			alert("Please select plan before proceed !");
		}
	else
		{
			$("#progressBar").show();
			
			$.ajax({  
				
			     type : "post",   
			     url : "saveUserPlanDetails.html", 
			     data :$('#planForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#progressBar").hide();
			    	 
					 if(response=="success")
						 {	
						 	$("#popUpheading").html("Success Message");
						 	$("#popUpMessage").html("Your selected plan have been saved successfully.");	
						 	$("#popUpDiv").show();
						 }
					 else
						 {
						     alert("Please Try again later!");
						 }
			     },  
		    });  
		}
}



// Choose Payment Option

function choosePaymentOption()
{
    	
}

// End of Choose Payment Option



//  Capture Payment Mode

function capturePaymentMode(checkId)
{
	if(checkId=="roundedTwo")
	{
		document.getElementById("roundedThree").checked = false;
	}
	else
	{
		document.getElementById("roundedTwo").checked = false;
	}
}

function proceedToCheckOut()
{
	if(document.getElementById("roundedTwo").checked==true)
	{
		$("#infoPopUpDiv").hide();
		
		$("#progressBar").show();
		
		var mode = "Online";
		var userId = document.getElementById("userRegId").value;
		
		$.ajax({  
			
		     type : "post",   
		     url : "capturePaymentMode.html", 
		     data :"mode="+mode+"&userId="+userId,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#progressBar").hide();
		    	 
				 if(response=="success")
					 {	
					 	window.location = "registrationCheckOut.html";
					 }
				 else
					 {
					     alert("Please Try again later!");
					 }
		     },  
	    });  
	}
	else if(document.getElementById("roundedThree").checked==true)
	{
		$("#infoPopUpDiv").hide();
		
		var mode = "Offline";
		$("#progressBar").show();
		var userId = document.getElementById("userRegId").value;
		
		$.ajax({  
			
		     type : "post",   
		     url : "capturePaymentMode.html", 
		     data :"mode="+mode+"&userId="+userId,	      	     	     
		     success : function(response) 
		     {  		
		    	 $("#progressBar").hide();
		    	 
				 if(response=="success")
					 {	
					 	 window.location = "registrationCheckOut.html";
					 }
				 else
					 {
					     alert("Please Try again later!");
					 }
		     },  
	    });  
	}
	else
	{
		alert("Please select a Payment mode before you proceed!")
	}
	
}


//  save record for offline order

function proceedForOfflinePayment()
{
	$("#progressBar").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "saveAndCompleteRegistration.html", 
	     data :$('#orderDetailForm').serialize(),	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 
			 if(response=="success")
				 {	
				 	$("#popUpheading").html("Success Message");
				 	$("#popUpMessage").html("Thank you ! You have been successfully registered .");	
				 	$("#popUpDiv").show();
				 }
			 else
				 {
				    alert("Please Try again later!");
				 }
	     },  
    });  
}

//  end of script for save record for offline order


function saveReturnBackProduct()
{
    $("#progressBar").show();
	$.ajax({  
		
	     type : "post",   
	     url : "saveReturnBackProduct.html", 
	     data :$('#returnDetailForm').serialize(),	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 
			 if(response=="success")
				 {	
				 	$("#popUpheading").html("Success Message");
				 	$("#popUpMessage").html("Thank you for agreed with our policies. Go to Login and enjoy the service.");	
				 	$("#popUpDiv").show();
				 }
			 else
				 {
				    alert("Please Try again later!");
				 }
	     },  
    }); 
}




function ProceedToLogin()
{
	 $("#progressBar").show();
		
	 $("#popUpheading").html("Success Message");
	 $("#popUpMessage").html("Thank you to being a member of us. Go to Login and enjoy the service.");	
	 $("#popUpDiv").show();	 
}


function forgetPassword()
{
	var emailId = document.getElementById("inputEmail").value;
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    var flag = 'true';
    
	if(emailId=="")
	{
		flag = 'false';
		$("#emailError").show();
        $("#emailError").html("Please enter a email id to reset your password.");	
        return false;  
	}
	
	if(emailId!="")
	{
		if(!emailId.match(email))  
        {  			
			flag = 'false';
			$("#emailError").show();
	        $("#emailError").html("Please enter a valid email id.");	
	        return false;       
        }
		else
		{
			$("#emailError").hide();
		}
	}
       
    if(flag=='true')
    	{
			    	$.ajax({  
			    		
			   	     type : "post",   
			   	     url : "regValidateEmailId.html", 
			   	     data : "emailId="+emailId,	         	     
			   	     success : function(response) 
			   	     {  		
			   	    	 
			   	    	 if(response=="exist")
			   	    	 	 {
			   						$("#progressBar").show();
			   						
			   							$.ajax({  
			   								
			   							     type : "post",   
			   							     url : "forgetPasswordRequest.html", 
			   							     data : "emailId="+emailId,	     	     	     
			   							     success : function(response) 
			   							     {  		
			   							    	 $("#progressBar").hide();
			   							    	 
			   									 if(response=="success")
			   										 {	
			   										 	$("#popUpheading").html("Success Message");
			   										 	$("#popUpMessage").html("For further process follow the link sent your email id.");	
			   										 	$("#popUpDiv").show();
			   										 }
			   									 else
			   										 {
			   										    alert("Please Try again later!");
			   										 }
			   							     },  
			   						    }); 
			   	    		 }
			   	    	 else
			   	    		 {
			   	    		     $("#progressBar").hide();	    		     
			   	    		     $("#popUpMessage").html("given email id is not registered yet!");	
			   					 $("#popUpDiv").show();
			   	    		 }
			   	     },  
			   	     
			       }); 
    	}
	
	
}


function resetUserPassword()
{
	var flag = 'true';
	
	var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirmPassword").value;
    var alfanumeric = /^.*[a-zA-z].*\d.*/;  
    
    var emailId = document.getElementById("emailId").value;
       
    if(password=="")
    {
    	flag = 'false';
    	$("#pwError").html("Please enter the password.");
    	$("#pwError").show();
    	return false;
    }
    
    if(password.length<8)
	{
    	flag='false';
		$("#pwError").show();
        $("#pwError").html("Your password atleast 8 character.");	
        return false; 
	}
    
    if(password.length>=8)
	{   	
    	if(!password.match(alfanumeric))  
        {  			
    		flag='false';
    		$("#pwError").show();
            $("#pwError").html("Your password should be alphanumeric.");	
            return false;       
        }   	
	}
      
    if(confirmPassword=="")
    {
    	flag = 'false';
    	$("#cnpwError").html("Please confirm the password.");
    	$("#cnpwError").show();
    	return false;
    }
    
    if(confirmPassword!="")
	{
		$("#cnpwError").hide();	 
	}
    
    if(password!=confirmPassword)
    {
    	flag = 'false';
    	$("#cnpwError").html("Confirm Password not matched with password.");
    	$("#cnpwError").show();
    	document.getElementById("confirmPassword").value = "";
    	return false;
    }
    
    if(flag=='true')
    {
	    	 $.ajax({  
	    			
	    	     type : "post",   
	    	     url : "saveResetPassword.html", 
	    	     data : $('#resetForm').serialize(),
	    	     success : function(response) 
	    	     {  		
	    	    	 $("#progressBar").hide();
	    	    	 
	    			 if(response=="success")
	    				 {	
	    				 	$("#popUpheading").html("Success Message");
	    				 	$("#popUpMessage").html("Your password has been changed successfully.");	
	    				 	$("#popUpDiv").show();
	    				 }
	    			 else
	    				 {
	    				    alert("Please Try again later!");
	    				 }
	    	     },  
	        }); 
    }
   
    
}





function authenticateUser()
{
	$("#progressBar").show();
	
    var flag = 'true';
	
    var emailId = document.getElementById("inputEmail").value;
	var password = document.getElementById("inputPassword").value;
     
    if(emailId=="")
    {
    	$("#progressBar").hide();
    	flag = 'false';
    	$("#emailError").html("Please enter your registered email id.");
    	$("#emailError").show();
    	return false;
    }
   
    if(emailId!="")
	{
		$("#emailError").hide();
	}
    
    if(password=="")
    {
    	$("#progressBar").hide();
    	flag = 'false';
    	$("#pwError").html("Please enter the password.");
    	$("#pwError").show();
    	return false;
    }
    if(password!="")
	{
		$("#pwError").hide();	 
	}
    
    if(flag=='true')
    {   
    	
    	$("#progressBar").show();
    	
	    	 $.ajax({  
	    			
	    	     type : "post",   
	    	     url : "authenticationUser.html", 
	    	     data : $('#userAuthForm').serialize(),
	    	     success : function(response) 
	    	     {  		
	    	    	    	    	 
	    			 if(response!="false")
	    				 {	
	    				 	window.location = "checkRegistrationStage.html";
	    				 }
	    			 else
	    				 {
	    				    $("#progressBar").hide();
	    				 
	    				    document.getElementById("inputEmail").value = "";
	    					document.getElementById("inputPassword").value = "";
	    					$("#sessionStatus").html("");
	    				    $("#authStatus").html("Authentication failed due to invalid Credential !");
	    				    $("#authStatus").show();
	    				 }
	    	     },  
	        }); 
    }
   
    

}




	    		

