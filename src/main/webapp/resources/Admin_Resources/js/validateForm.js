
function actimeMenu(menuId)
{
	document.getElementById(menuId).className = "active";
}

function logoutAdmin()
{
	window.location = "supAdminLogOut.html";
}

function createAdminProfileOnChange()
{
	var adminType = document.getElementById("adminType").value;
	if(adminType!="Select Admin Type")
	{		
		$("#errAdminType").hide();		
	}
}

function createAdminProfileOnKeyUp()
{
	
	var fullName = document.getElementById("fullName").value;
	var email = document.getElementById("email").value;
	var mobileno = document.getElementById("mobileno").value;
	var password1 = document.getElementById("password1").value;
	var password2 = document.getElementById("password2").value;
	
	var name = /^[a-zA-Z\s]*$/;	
	var phno = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	
	if(fullName!="")
	{
		if(fullName.match(name))  
        {			
			$("#errfullName").hide();
			$("#errfullName").html("");
        }
	}
	
	if(email!="")
	{
		if(email.match(emailId))  
        {	
			$("#erremail").hide();			
        }	
	}
	
	
	if(mobileno!="")
	{
		if(mobileno.match(phno))  
        {
			$("#errmobileno").hide();			
        }
	}
	
	if(password1!="")
	{
		$("#errpassword1").hide();
	}
	
	if(password2!="")
	{
		$("#errpassword2").hide();
	}
	
	
	if(password1==password2)
	{	
		$("#errpassword1").hide();		
	}
		
}


function createAdminProfile()
{
	var flag = 'true';
	
	var adminType = document.getElementById("adminType").value;
	var fullName = document.getElementById("fullName").value;
	var email = document.getElementById("email").value;
	var mobileno = document.getElementById("mobileno").value;
	var password1 = document.getElementById("password1").value;
	var password2 = document.getElementById("password2").value;
	
	var name = /^[a-zA-Z\s]*$/;	
	var phno = /^[0-9]{10}$/;	
	var emailId = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	if(adminType=="Select Admin Type")
	{
		flag = 'false';
		$("#errAdminType").show();
		$("#errAdminType").html("Please choose Admin type.");
		return false;
	}
	
	if(fullName=="")
	{
		flag = 'false';
		$("#errfullName").show();
		$("#errfullName").html("Please enter full name of Admin.");
		return false;
	}
	
	if(fullName!="")
	{
		if(!fullName.match(name))  
        {
			flag = 'false';
			$("#errfullName").show();
			$("#errfullName").html("Please enter a valid name.");
		    return false;
        }
	}
	
	if(email=="")
	{
		flag = 'false';
		$("#erremail").show();
		$("#erremail").html("Please enter the email id.");
		return false;
	}
	
	if(email!="")
	{
		if(!email.match(emailId))  
        {
			flag = 'false';
			$("#erremail").show();
			$("#erremail").html("Please enter a valid email id.");
			return false;
        }	
	}
	
	if(mobileno=="")
	{
		flag = 'false';
		$("#errmobileno").show();
		$("#errmobileno").html("Please enter the mobile no.");
		return false;
	}
	
	if(mobileno!="")
	{
		if(!mobileno.match(phno))  
        {
			flag = 'false';
			$("#errmobileno").show();
			$("#errmobileno").html("Please enter a valid mobile no.");
			return false;
        }
	}
	
	if(password1=="")
	{
		flag = 'false';
		$("#errpassword1").show();
		$("#errpassword1").html("Please enter the password.");
		return false;
	}
	
	if(password2=="")
	{
		flag = 'false';
		$("#errpassword2").show();
		$("#errpassword2").html("Please enter the confirm password.");
		return false;
	}
	
	
	if(password1!=password2)
	{
		flag = 'false';
		$("#errpassword1").show();
		$("#errpassword1").html("Password and confirm password are not matched.");
		document.getElementById("password1").value = "";
		document.getElementById("password2").value = "";
		return false;
	}
	
		
	if(flag=='true')
	{			
		$("#progressLight").show();
		$("#progressDark").show();
		
		$.ajax({  
			
			     type : "post",   
			     url : "createBusinessAdmin.html", 
			     data :$('#profileForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
					 if(response=="success")
						 {
						     alert("Admin created successfully.");
						     window.location="myAdmin.html";
						 }
					 else
						 {
						     alert("Please Try again later!");
						 }
			     },  
		     });  	
     }
	
		
}



// Manage Admin Function 

function showAdminDetails()
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "manageBusinessAdmin.html", 	        	     
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			 $("#adminDetailsDiv").html(response);
	     },  
    });  
}


// remove admin from the business 

function removeAdminDetails(adminId)
{
	
	var x=window.confirm("Are you sure you want to remove the Admin ?")
	if (x)
		{
				$("#progressLight").show();
				$("#progressDark").show();
					
				$.ajax({  
					
				     type : "post",   
				     url : "removeBusinessAdmin.html",
				     data : "adminId="+adminId,
				     success : function(response) 
				     {  		    	
				    	$("#progressLight").hide();
				 		$("#progressDark").hide();
				 		if(response=="success")
				 			{
				 			     alert("Admin has been removes from the business successfully.");
				 			     showAdminDetails();
				 			}
				 		else
				 			{
				 				alert("Please try again later!");
				 			}
				 		
				     },  
			    });  
		}
	else
		{
		    window.alert("It's ok mistake happens !");
		}
	
}



// New Product entry



function newProductEntry()
{		
	$("#progressLight").show();
	$("#progressDark").show();
	
	var flag = 'true';
	
	var productType = document.getElementById("productTypeId").value;
	var productName = document.getElementById("productNameId").value;
	var productPrice = document.getElementById("productPriceId").value;
	var productDesc = document.getElementById("productDescId").value;
	var productfile = document.getElementById("productImg").value;
	
	
	if(productType=="Select Admin Type")
	{
	   $("#progressLight").hide();
	   $("#progressDark").hide();
	   
	   flag = 'false';	 
	   $("#errproductType").show();
	   $("#errproductType").html("Please select a product type.");
	   return false;
	}
	if(productName=="")
	{
		$("#progressLight").hide();
		$("#progressDark").hide();
		   
		flag = 'false';	 
		$("#errproductName").show();
		$("#errproductName").html("Please enter the product name.");
	    return false;
	}
	 	
	if(productPrice=="")
	{
		$("#progressLight").hide();
 		$("#progressDark").hide();
 		
 		flag = 'false';
 		$("#errproductPrice").show();
		$("#errproductPrice").html("Please enter the product price.");
	    return false;
	}
	
	if(productDesc=="")
	{
		$("#progressLight").hide();
 		$("#progressDark").hide();		
		
 		flag = 'false';
 		$("#errproductDesc").show();
		$("#errproductDesc").html("Please enter description about product.");
	    return false;
	}
	
	if(productfile.value=="")
	{
		$("#progressLight").hide();
 		$("#progressDark").hide();
	    
 		flag = 'false';	  
 		$("#errproductImg").show();
		$("#errproductImg").html("Please browse a file.");
	    return false;
	}
	
	
	
	if(productfile!="")
		{		
		   var fileDetails = productfile;
		   $("#errproductImg").hide();
		    var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png");						    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
			if(condition=="NotGranted")
				{
					$("#progressLight").hide();
			 		$("#progressDark").hide();
				    flag = 'false';	
				    $("#errproductImg").show();
				    $("#errproductImg").html("Only image files are allowed.");
  				    return false;				  
				}
			
			 var fileDetails1 = document.getElementById("productImg");
			 var fileSize = fileDetails1.files[0];
			 var fileSizeinBytes = fileSize.size;
			 var sizeinKB = +fileSizeinBytes / 1024;
	  		 var sizeinMB = +sizeinKB / 1024;
	  		 
	  		 if(sizeinMB>2)
	  			 {
		  			$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
	  				flag = 'false';
	  				$("#errproductImg").show();
	  				$("#errproductImg").html("choose a file less then 2 MB.");
    				return false;	 		   		 
	  			 }
	  		 
		}
			
	
	  		 
			if (flag == 'true') 
			{
											
				var formData = new FormData($("#productdtailsForm")[0]);
	            
					$.ajax({
						type : "POST",
						url : "saveProductDetails.html",
						data : formData,					
						success : function(response) {
		                    
							$("#progressLight").hide();
					 		$("#progressDark").hide();
					 		
							if(response=="success")
							{
								alert("Product details had saved successfully.");
								window.location  = "myProduct.html";
							}
							else
							{
								alert("Please try again later !");
							}
						},
						cache: false,
			 	        contentType: false,
			 	        processData: false,
						
					});
						
			}
}

// get All Product details 

function getProductdetails()
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "getProducts.html", 	        	     
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			$("#productDetailsDiv").html(response);
			$("#editproductDetailsDiv").html("");
	     },  
    });  
}


// Edit the product details 

function showsearchDiv(actionType)
{
    if(actionType=="Product Id")
    	{
    	    $("#categoryDiv").hide();
    	    $("#nameDiv").hide();
    	    $("#idDiv").show();
    	}
    else if(actionType=="Product Name")
    	{
    	 	$("#categoryDiv").hide();
    	 	$("#nameDiv").show();
    	 	$("#idDiv").hide();
    	}
    else
    	{
    	 	$("#categoryDiv").show();
    	 	$("#nameDiv").hide();
    	 	$("#idDiv").hide();
    	}
}


function searchProducts(type)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	var value = "";
	
	if(type=="Product Id")
		{
			value = document.getElementById("searchproductId").value;
		}
	else if(type=="Product Name")
		{
			value = document.getElementById("searchproductNameId").value;
		}
	else
		{
			value = document.getElementById("searchproductTypeId").value;
		}
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchProducts.html", 	
	     data : "value="+value+"&type="+type,
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 			 		
			$("#productDetailsDiv").html(response);
			
			$("#resultHeading").html("Search result As per the "+type);
			
			//$("#editproductDetailsDiv").slideDown();
			$("#editproductDetailsDiv").html("");
			
	     },  
    });
	
}


function editProductDetails(productId)
{	
	
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "editProducts.html",
	     data : "productId="+productId,
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();	 	
	 		
	 		//$("#editproductDetailsDiv").slideUp();
	 		$("#editproductDetailsDiv").html(response);
	 		
	     },  
    });  
	
}


// Remove the product from the business

function removeProductDetails(productId)
{
	
	var x=window.confirm("Are you sure you want to remove the product ?")
	if (x)
		{
				$("#progressLight").show();
				$("#progressDark").show();
					
				$.ajax({  
					
				     type : "post",   
				     url : "removeProducts.html",
				     data : "productId="+productId,
				     success : function(response) 
				     {  		    	
				    	$("#progressLight").hide();
				 		$("#progressDark").hide();
				 		if(response=="success")
				 			{
				 			     alert("Product has been removes from the business successfully.");
				 			     getProductdetails();
				 			}
				 		else
				 			{
				 				alert("Please try again later!");
				 			}
				 		
				     },  
			    });  
		}
	else
		{
		    window.alert("It's ok mistake happens !");
		}
	
}

function updateProduct()
{
	
	$("#progressLight").show();
	$("#progressDark").show();
	
	var flag = 'true';
	
	var productType = document.getElementById("updproductTypeId").value;
	var productName = document.getElementById("updproductNameId").value;
	var productPrice = document.getElementById("updproductPriceId").value;
	var productDesc = document.getElementById("updproductDescId").value;
	var productfile = document.getElementById("updproductImg").value;
	
	if(productType=="Select Admin Type")
	{
	   $("#progressLight").hide();
	   $("#progressDark").hide();
	   
	   flag = 'false';	 
	   $("#errproductType").show();
	   $("#errproductType").html("Please select a product type.");
	   return false;
	}
	if(productName=="")
	{
		$("#progressLight").hide();
		$("#progressDark").hide();
		   
		flag = 'false';	 
		$("#errproductName").show();
		$("#errproductName").html("Please enter the product name.");
	    return false;
	}
	 	
	if(productPrice=="")
	{
		$("#progressLight").hide();
 		$("#progressDark").hide();
 		
 		flag = 'false';
 		$("#errproductPrice").show();
		$("#errproductPrice").html("Please enter the product price.");
	    return false;
	}
	
	if(productDesc=="")
	{
		$("#progressLight").hide();
 		$("#progressDark").hide();		
		
 		flag = 'false';
 		$("#errproductDesc").show();
		$("#errproductDesc").html("Please enter description about product.");
	    return false;
	}
	
	
	if(productfile!="")
	{	
		   var fileDetails = productfile;
		   $("#errproductImg").hide();
		    var ext=fileDetails.substring(fileDetails.lastIndexOf('.')+1);
		    var extension = new Array("jpg","jpeg","gif","png");						    
		    var condition = "NotGranted";
		    for(var m=0;m<extension.length;m++)
				{
				    if(ext==extension[m])
				    	{				    	    
				    	   condition="Granted";				    	    
				    	}				    
				}
			if(condition=="NotGranted")
				{
					$("#progressLight").hide();
			 		$("#progressDark").hide();
				    flag = 'false';	
				    $("#errproductImg").show();
				    $("#errproductImg").html("Only image files are allowed.");
  				    return false;				  
				}
			
			
			 var fileDetails1 = document.getElementById("updproductImg");
			 var fileSize = fileDetails1.files[0];
			 var fileSizeinBytes = fileSize.size;
			 var sizeinKB = +fileSizeinBytes / 1024;
	  		 var sizeinMB = +sizeinKB / 1024;
	  		 
	  		 if(sizeinMB>2)
	  			 {
		  			$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
	  				flag = 'false';
	  				$("#errproductImg").show();
	  				$("#errproductImg").html("choose a file less then 2 MB.");
    				return false;	 		   		 
	  			 }
	
		}
	
			if (flag == 'true') 
			{
				
				
				var formData = new FormData($("#updateproductdtailsForm")[0]);
	            
					$.ajax({
						type : "POST",
						url : "updateProductDetails.html",
						data : formData,					
						success : function(response) {
		                    
							$("#progressLight").hide();
					 		$("#progressDark").hide();
					 		
							if(response=="success")
							{
								alert("Product details had updated successfully.");
								getProductdetails();
							}
							else
							{
								alert("Please try again later !");
							}
						},
						cache: false,
			 	        contentType: false,
			 	        processData: false,
						
					});
						
			}
}


// Save cost Values from the business


function saveCostDetails(costName,costValueId,buttonId)
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	var costValue = document.getElementById(costValueId).value;	
	var buttonStatus = document.getElementById(buttonId).value;
	
	if(buttonStatus=="Edit")
	{
		$("#progressLight").hide();
		$("#progressDark").hide();
		
		document.getElementById(buttonId).value="Save";
		x=document.getElementById(costValueId)
	    x.disabled = !x.disabled;	
	}
	
	if(costValue=="")
	{
		$("#progressLight").hide();
		$("#progressDark").hide();
		alert("Please enter the "+costName+" value.");
		return false;
	}

	if(buttonStatus=="Save")
	{
		$.ajax({  				
			     type : "post",   
			     url : "saveCostDetails.html",
			     data : "costName="+costName+"&costValue="+costValue,
			     success : function(response) 
			     {  		    	
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		if(response=="success")
			 		{
			 			alert(costName+" cost values has been saved successfully.");
			 			window.location = "costmanage.html";
			 		}
			 		else
			 		{
			 			alert("Please try again later!");
			 		}	 		
			     },  
		   });  
	}
		
}


//get search vendor place holder.

function searchVendor(searchType)
{
	if(searchType=="Id")
		{
		   document.getElementById("vendorId").placeholder = "Enter the vendor id";
		}
	else if(searchType=="Name")
		{
			document.getElementById("vendorId").placeholder = "Enter the vendor full name";
		}
	else if(searchType=="emailid")
		{
			document.getElementById("vendorId").placeholder = "Enter the vendor email id";
		}
	else
		{
		    document.getElementById("vendorId").placeholder = "";
		}
	
}


// get search vendor list.

function getVendorSearchResult()
{
	var searchValue = document.getElementById("vendorId").value;
	var searchType = document.getElementById("searchTypeId").value;
	
		$("#progressLight").show();
		$("#progressDark").show();
			
		$.ajax({  
			
		     type : "post",   
		     url : "searchVendor.html",
		     data : "searchType="+searchType+"&searchValue="+searchValue,
		     success : function(response) 
		     {  		    	
		    	$("#progressLight").hide();
		 		$("#progressDark").hide();	 	
		 		
		 		$("#vendorDetailsDiv").html(response);
		 		
		     },  
	    });  
}

// get Sms Details

function getSMSDetails()
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "smsStatus.html",
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();	 	
	 		
	 		$("#smsDetailsDiv").html(response);
	 		
	     },  
    });  
}

// sent email Again

function sentMailAgain(primeId)
{
	
}


//sent sms Again

function sentSmsAgain(primeId)
{
	
}


// Save plandetails 


function savePlanDetails(formId,planName,buttonId,type)
{	
	var formData = new FormData($("#"+formId)[0]);
	var buttonValue = document.getElementById(buttonId).value;
	
	if(buttonValue=="Edit")
	{
		document.getElementById(buttonId).value = "Save";
		
		x=document.getElementById("planValId1"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId2"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId3"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId4"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId5"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId6"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId7"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId8"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId9"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId10"+type);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId11"+type);
	    x.disabled = !x.disabled;	
	}

	if(buttonValue=="Save")
	{
			$.ajax({
				type : "POST",
				url : "savePlanDetails.html",
				data : formData,					
				success : function(response) {
		            
					$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
					if(response=="success")
					{
						alert("Plan details has been saved successfully.");
						
						if(planName=='Plan A')
						{
							window.location = "planmanage.html";
						}
						else if(planName=='Plan B')
						{
							showPlanB();
						}
						else
						{
							showPlanC();
						}
						
					}
					else
					{
						alert("Please try again later !");
					}
				},
				cache: false,
			    contentType: false,
			    processData: false,
				
			});
	}
	
	
}

// Show Plan B to manage

function gotoHome()
{
	window.location ="planmanage.html";
}
function showPlanB()
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "showPlanB.html",
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();	 	
	 		
	 		$("#formcontrols").html("");
	 		$("#planBDiv").html(response);
	 		
	     },  
    });  
	
}

// Show plan C to manage

function showPlanC()
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "showPlanC.html",
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();	 	
	 		
	 		$("#formcontrols").html("");
	 		$("#planBDiv").html("");
	 		
	 		$("#planCDiv").html(response);	 		
	     },  
    });  
	
}


// view Vendor details from supadmin

function viewVendorDetails(vendorId,vendorName)
{
	$("#progressLight").show();
	$("#progressDark").show();
		
	$.ajax({  
		
	     type : "post",   
	     url : "showAllvendorDetails.html",
	     data : "vendorId="+vendorId,
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();	 	
	 		
	 		$("#vendorViewDetailsDiv").html(response);	 		 		
	 		$("#resultHeading").html("Profile details of "+vendorName);
	     },  
    });  	
}





// Save Plan Values


function saveBusinessPlan(formId,planName,planIndex,buttonId)
{
	var formData = new FormData($("#"+formId)[0]);
	var buttonValue = document.getElementById(buttonId).value;
	
	if(buttonValue=="Edit")
	{
		document.getElementById(buttonId).value = "Save";
		
		x=document.getElementById("planValId1"+planIndex);
	    x.disabled = !x.disabled;	
	    
	    x=document.getElementById("planValId2"+planIndex);
	    x.disabled = !x.disabled;		   	    
	}

	if(buttonValue=="Save")
	{
		var flag = 'true';
		
		var onlyNumber = '^[0-9]+$';
		
		
		var planAmt1 = document.getElementById("planValId1"+planIndex).value;
		var planAmt2 = document.getElementById("planValId2"+planIndex).value;
		
		if(planAmt1=="")
			{
				flag = 'false';
				$("#error1"+planIndex).html("Please enter the policy amount.");
				$("#error1"+planIndex).show();
			}
		
		if(planAmt1!="")
		{
			if(planAmt1.match(onlyNumber))  
		    {			
				$("#error1"+planIndex).hide();			
		    }
			else
			{
				flag = 'false';
				$("#error1"+planIndex).html("Only Integer value is allowed.");
				$("#error1"+planIndex).show();
			}
		}
		
		
		if(planAmt2=="")
			{
				flag = 'false';
				$("#error2"+planIndex).html("Please enter the refer amount.");
				$("#error2"+planIndex).show();
			}
		
		if(planAmt2!="")
		{
			if(planAmt2.match(onlyNumber))  
		    {			
				$("#error2"+planIndex).hide();			
		    }
			else
			{
				flag = 'false';
				$("#error2"+planIndex).html("Only Integer value is allowed.");
				$("#error2"+planIndex).show();
			}
		}
		
		
		
		if(flag=='true')
		{			
			$("#progressLight").show();
	 		$("#progressDark").show();
	 		
				$.ajax({
					type : "POST",
					url : "saveBusinessPlanDetails.html",
					data : formData,					
					success : function(response) {
			            
						$("#progressLight").hide();
				 		$("#progressDark").hide();
				 		
						if(response=="success")
						{
							alert("Plan details has been saved successfully.");
							
							if(planName=='Plan A')
							{
								window.location = "planmanage.html";
							}
							else if(planName=='Plan B')
							{
								showPlanB();
							}
							else
							{
								showPlanC();
							}
							
						}
						else
						{
							alert("Please try again later !");
						}
					},
					cache: false,
				    contentType: false,
				    processData: false,
					
				});
				
		}
		
	}

}


function showFeature()
{
	if(document.getElementById("featureFormDiv").style.display=="none")
		{
		     $("#featureFormDiv").slideDown("slow");
		     document.getElementById("addFeatureId").value = "Close Form";
		}
	else
		{
		     $("#featureFormDiv").slideUp("slow");
		     document.getElementById("addFeatureId").value = "Add Feature";
		}
}


function addFeatures(planName)
{
	var onlyNumber = '^[0-9]+$';
	
	
	var flag = 'true';
	
	var featVal1 = document.getElementById("dayId").value;
	var featVal2 = document.getElementById("friendId").value;
	var featVal3 = document.getElementById("bonusAmountId").value;
	var featVal4 = document.getElementById("compAmountId").value;
	
	if(featVal1=="")
	{
			flag = 'false';
			$("#errday").html("Please enter the no of days.");
			$("#errday").show();			
	}
	
	if(featVal1!="")
	{
		if(featVal1.match(onlyNumber))  
        {			
			$("#errday").hide();			
        }
		else
		{
			$("#errday").html("Only Integer value is allowed.");
			$("#errday").show();
		}		
	}
	
	if(featVal2=="")
	{
			flag = 'false';
			$("#errfriend").html("Please enter the no.of friends.");
			$("#errfriend").show();
	}
	
	if(featVal2!="")
	{
		if(featVal2.match(onlyNumber))  
        {			
			$("#errfriend").hide();			
        }
		else
		{
			flag = 'false';
			$("#errfriend").html("Only Integer value is allowed.");
			$("#errfriend").show();
		}	
	}
	
	if(featVal3=="")
	{
		flag = 'false';
		$("#errBnAmt").html("Please enter the bonus amount.");
		$("#errBnAmt").show();
	}
	
	if(featVal3!="")
	{
		if(featVal3.match(onlyNumber))  
        {			
			$("#errBnAmt").hide();			
        }
		else
		{
			flag = 'false';
			$("#errBnAmt").html("Only Integer value is allowed.");
			$("#errBnAmt").show();
		}
	}
	
	if(featVal4=="")
	{
		flag = 'false';
		$("#errCmpAmt").html("Please enter the complement amount.");
		$("#errCmpAmt").show();
	}
	

	if(featVal4!="")
	{
		if(featVal4.match(onlyNumber))  
        {			
			$("#errCmpAmt").hide();			
        }
		else
		{
			flag = 'false';
			$("#errCmpAmt").html("Only Integer value is allowed.");
			$("#errCmpAmt").show();
		}
	}
	
	if(flag=='true')
	{	
		
		var formData = new FormData($("#featureForm")[0]);
		
		$("#progressLight").show();
 		$("#progressDark").show();
 		
			$.ajax({
				type : "POST",
				url : "saveBusinessPlanfeature.html",
				data : formData,					
				success : function(response) {
		            
					$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
					if(response=="success")
					{
						alert("Feature details has been saved successfully.");
						
						if(planName=='Plan A')
						{
							window.location = "planmanage.html";
						}
						else if(planName=='Plan B')
						{
							showPlanB();
						}
						else
						{
							showPlanC();
						}
						
					}
					else
					{
						alert("Please try again later !");
					}
				},
				cache: false,
			    contentType: false,
			    processData: false,
				
			});
			
	}
}



function deleteFeature(featureId,planName)
{
	var x=window.confirm("Are you sure you want to remove the feature ?")
	if (x)
		{				
			$("#progressLight").show();
			$("#progressDark").show();
				
			$.ajax({  
				
			     type : "post",   
			     url : "deleteFeature.html",
			     data : "featureId="+featureId,
			     success : function(response) 
			     {  		    	
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();	 	
			 		
			 		if(response=="success")
					{
						alert("Feature details has been deleted successfully.");
						
						if(planName=='Plan A')
						{
							window.location = "planmanage.html";
						}
						else if(planName=='Plan B')
						{
							showPlanB();
						}
						else
						{
							showPlanC();
						}
						
					}
					else
					{
						alert("Please try again later !");
					}
			     },  
		    }); 
		
		}
	else
		{
		      alert("Its ok mistake happen.");
		}
	 
	
}



