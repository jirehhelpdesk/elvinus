
function actimeMenu(menuId)
{
	document.getElementById(menuId).className = "active";
}


function logoutActAdmin()
{
	window.location = "actadminsignout.html";
}

function logoutProAdmin()
{
	window.location = "proadminsignout.html";
}

function logoutCheAdmin()
{
	window.location = "cheadminsignout.html";
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* -----------------------------------  ----------    START OF SCRIPT FOR PRODUCT ADMIN      --------------------------------------------------------*/




// Search Order by the search Value 

function searchOrder(type)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	if(type=='OrderId')
	{
			var searchValue = document.getElementById("searchValue").value;
			
			$.ajax({  
				
			     type : "post",   
			     url : "getorderdetails.html",
			     data : "searchValue="+searchValue+"&searchType="+type,
			     success : function(response) 
			     {  		    	
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
					 $("#orderDetailsDiv").html(response);
			     },  
		    });  
	}
	else
	{
		var startDate = document.getElementById("some_class_1").value;
		var endDate = document.getElementById("some_class_2").value;
				
		$.ajax({  
			
		     type : "post",   
		     url : "getorderdetails.html",
		     data : "startDate="+startDate+"&endDate="+endDate+"&searchType="+type,
		     success : function(response) 
		     {  		    	
		    	$("#progressLight").hide();
		 		$("#progressDark").hide();
		 		
				$("#orderDetailsDiv").html(response);
		     },  
	    });  
	}
	
}

function OrderPage(url)
{
	window.location = url;
}

function showSearchOrderPanel()
{
	$("#formcontrols").html("");
}

function searchOrderDetails(type)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	if(type=='OrderId')
	{
			var searchValue = document.getElementById("searchValue").value;
			
			$.ajax({  
				
			     type : "post",   
			     url : "getorderdetails.html",
			     data : "searchValue="+searchValue+"&searchType="+type,
			     success : function(response) 
			     {  		    	
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		$("#formcontrols").html("");
					$("#orderDetailsDiv").html(response);
			     },  
		    });  
	}
	else
	{
		var startDate = document.getElementById("some_class_1").value;
		var endDate = document.getElementById("some_class_2").value;
				
		$.ajax({  
			
		     type : "post",   
		     url : "getorderdetails.html",
		     data : "startDate="+startDate+"&endDate="+endDate+"&searchType="+type,
		     success : function(response) 
		     {  		    	
		    	$("#progressLight").hide();
		 		$("#progressDark").hide();
		 		
		 		$("#formcontrols").html("");
				$("#orderDetailsDiv").html(response);
		     },  
	    });  
	}
	
}


// order back otion

function bactoOrderDetails(redirectType)
{
	$("#updateorderDetailsDiv").html("");
	if(redirectType=="MainPage")
	{
		window.location = "productAdmin.html";
	}
	else
		{
		
		}
	
}

//  Print Order Ticket

function printOrderDetails(ordUnqId)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "getorderdetailsForPrint.html",
	     data : "ordUnqId="+ordUnqId+"&OrderType=OrderId",
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 			 		
            $("#printOrderDetails").html(response);		
	 		
	 		var contents = document.getElementById("printOrderDetails").innerHTML;
	 	    
	 		var frame1 = document.createElement('iframe');
	 	    frame1.name = "frame1";
	 	    frame1.style.position = "absolute";
	 	    frame1.style.top = "-1000000px";
	 	    document.body.appendChild(frame1);
	 	    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
	 	    frameDoc.document.open();
	 	    frameDoc.document.write(contents);	    
	 	    frameDoc.document.close();
	 	    setTimeout(function () {
	 	        window.frames["frame1"].focus();
	 	        window.frames["frame1"].print();
	 	        document.body.removeChild(frame1);
	 	        }, 500);
	 	     		
	     },  
    });  
}

// Print the Order in Script

function PrintDiv() {
	
	// One Way
	
		$("#printOrderDetails").html(response);		
		
		var printContents = document.getElementById('printOrderDetails').innerHTML;
	    var originalContents = document.body.innerHTML;
	    document.body.innerHTML = "";	 	
	    document.body.innerHTML = printContents;	 	   
	    window.print();
	    
	    $("#printOrderDetails").html("");		
	    
	    document.body.innerHTML = originalContents;	 	  
	    window.load();
	
	//  Another Way
	
	    var contents = document.getElementById("dvContents").innerHTML;
	    var frame1 = document.createElement('iframe');
	    frame1.name = "frame1";
	    frame1.style.position = "absolute";
	    frame1.style.top = "-1000000px";
	    document.body.appendChild(frame1);
	    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
	    frameDoc.document.open();
	    frameDoc.document.write('<html><head><title>DIV Contents</title>');
	    frameDoc.document.write('</head><body>');
	    frameDoc.document.write(contents);
	    frameDoc.document.write('</body></html>');
	    frameDoc.document.close();
	    setTimeout(function () {
	        window.frames["frame1"].focus();
	        window.frames["frame1"].print();
	        document.body.removeChild(frame1);
	    }, 500);
	    return false;
    
}








// click on update button to update order status


function updateOrderDetails(ordUnqId)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "getorderdetailsForUpdate.html",
	     data : "ordUnqId="+ordUnqId+"&OrderType=OrderId",
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			$("#updateorderDetailsDiv").html(response);			 
			$("#resultHeading").html("Update order details of "+ordUnqId);		
			
			
	     },  
    });  
}


// update order details

/*var formData = new FormData($("#updateOrderForm")[0]);*/


function saveUpdatedOrderValues()
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  		
		     type : "post",   
		     url : "updateOrderDetails.html", 
		     data : $('#updateOrderForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	$("#progressLight").hide();
		 		$("#progressDark").hide();
		 		
				 if(response=="success")
					 {
					     alert("Order details successfully updated.");	
					     bactoOrderDetails("MainPage");
					 }
				 else
					 {
					     alert("Please Try again later!");
					 }
		     },  
	     });
	
}


function showOrderSearchDiv(type)
{
	if(type=="Duration")
		{
		   $("#orderIdDiv").slideUp('slow');
		   $("#durationDiv").slideDown('slow');
		}
	else
		{
		   $("#orderIdDiv").slideDown('slow');
		   $("#durationDiv").slideUp('slow');
		}
}




function createOrderReport()
{
   var flag="true";
	
	var reportType = document.getElementById("reportTypeId").value;
	var reportFrom = document.getElementById("some_class_1").value;
	var reportTo = document.getElementById("some_class_2").value;
	
	    
	
	if(reportType=="Select a Category")
	{
		flag="false";
		$("#errReportType").html("Please select report Type.");
		$("#errReportType").show();
		return false;
	}
	
	if(reportType!="Select a Category")
	{
		$("#errReportType").hide();
	}
	
    if(reportFrom=="")
	{
		flag="false";
		$("#errDuration").html("Please select a date from.");
		$("#errDuration").show();
		return false;
	}
   
    if(reportFrom!="")
	{
		$("#errDuration").hide();
	}
	  
    if(reportTo=="")
	{
		flag="false";
		$("#errDuration").html("Please select a date to.");
		$("#errDuration").show();
		return false;
	}
   
    if(reportTo!="")
	{
		$("#errDuration").hide();
	}
	
	if(flag=="true")
	{
		$("#progressLight").show();
		$("#progressDark").show();
		
		$.ajax({  		
			     type : "post",   
			     url : "generateOrderReport.html", 
			     data : $('#orderReportForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		alert("Report generate successfully . Please download from the list.");
					window.location = "orderReport.html";
			     },  
		     });
	}
}



function deleteOrderReport(reportName,ReportCategory,reportId)
{
	var x=window.confirm("Are you sure you want to delete the reort ?")
	if (x)
		{
			$("#progressLight").show();
			$("#progressDark").show();
			
			$.ajax({  		
				     type : "post",   
				     url : "deleteOrderReport.html", 
				     data : "reportName="+reportName+"&ReportCategory="+ReportCategory+"&reportId="+reportId,	     	     	     
				     success : function(response) 
				     {  		
				    	$("#progressLight").hide();
				 		$("#progressDark").hide();
				 		
				 		if(response=="Done")
				 			{
				 				alert(reportName+" report successfully deleted.");
				 				window.location="orderReport.html";
				 			}
				 		else
				 			{
				 				alert("Due to certain some problem arise , Please try again later.");
				 			}
				 		
				     },  
			     });
			
		}
	else
		{
		   alert("It's ok mistake happens.")
		}
}
// End of Product scripts



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* -----------------------------------  ----------    END OF SCRIPT FOR PRODUCT ADMIN      --------------------------------------------------------*/





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* -----------------------------------  ----------    Start OF SCRIPT FOR ACCOUNT ADMIN      --------------------------------------------------------*/




// Start Transaction Scripts

// Search transaction details as per the transaction order


function searchTransactionDetails()
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	var tranId = document.getElementById("tranId").value;
	
	$.ajax({  
		
	     type : "post",   
	     url : "searchTransactionDetails.html",
	     data : "tranId="+tranId,
	     success : function(response) 
	     {  		    	
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			$("#transactionListDiv").html(response);			 
			$("#resultHeading").html("Transaction details of "+tranId);			 
	     },  
    });  
}


//  update the transaction status 

function showTransDetails(transactionId)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "showTransDetails.html",
	     data : "transactionId="+transactionId,
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			$("#updateTransactionDiv").html(response);			 
			$("#resultHeading1").html("Update transaction details of "+transactionId);				
	     },  
    });  
}

function bactoTranDetails()
{
	$("#updateTransactionDiv").html("");
}


// update the transaction details from account admin



function saveUpdatedTransaction()
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  		
		     type : "post",   
		     url : "updateTranDetails.html", 
		     data : $('#updateTranForm').serialize(),	     	     	     
		     success : function(response) 
		     {  		
		    	$("#progressLight").hide();
		 		$("#progressDark").hide();
		 		
				 if(response=="success")
					 {
					     alert("Transaction details has been successfully updated.");	
					     bactoTranDetails();
					 }
				 else
					 {
					     alert("Please Try again later!");
					 }
		     },  
	     });	
	
}


//Show amount received in the last week



function totalAmtRecLstWeek()
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "amountReceivedWeekly.html",
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			$("#WeeklyRecordDiv").html(response);			 	
	     },  
    });  
}



// Generate Weekly Record for amount received


function genWeeklyRecRecord()
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "downloadAmtReceivedRecord.html",
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
			$("#WeeklyRecordDiv").html(response);			 	
	     },  
    });  
}




// Download VAT Reports


function downloadVatReport(reportName)
{
	$("#progressLight").show();
	$("#progressDark").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "downloadVatReport.html",
	     data : "reprtName="+reportName,
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 							 	
	     },  
    });  
}




function createAccountReport()
{
var flag="true";
	
	var reportType = document.getElementById("reportTypeId").value;
	var reportFor = document.getElementById("reportForId").value;
	var reportFrom = document.getElementById("some_class_1").value;
	var reportTo = document.getElementById("some_class_2").value;
	
	    
	if(reportFor=="Select a Category")
	{
		flag="false";
		$("#errReportFor").html("Please select report For.");
		$("#errReportFor").show();
		return false;
	}
	
	if(reportFor!="Select a Category")
	{
		$("#errReportFor").hide();
	}
	
	if(reportType=="Select a Category")
	{
		flag="false";
		$("#errReportType").html("Please select report Type.");
		$("#errReportType").show();
		return false;
	}
	
	if(reportType!="Select a Category")
	{
		$("#errReportType").hide();
	}
	 
    if(reportFrom=="")
	{
		flag="false";
		$("#errDuration").html("Please select a date from.");
		$("#errDuration").show();
		return false;
	}
   
    if(reportFrom!="")
	{
		$("#errDuration").hide();
	}
	  
    if(reportTo=="")
	{
		flag="false";
		$("#errDuration").html("Please select a date to.");
		$("#errDuration").show();
		return false;
	}
   
    if(reportTo!="")
	{
		$("#errDuration").hide();
	}
	
	if(flag=="true")
	{
		$("#progressLight").show();
		$("#progressDark").show();
		
		$.ajax({  		
			     type : "post",   
			     url : "generateAccountReport.html", 
			     data : $('#accountReportForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		alert("Report generate successfully . Please download from the list.");
					window.location = "accountReports.html";
			     },  
		     });
	}
	
}

function deleteAccountReport(reportName,ReportCategory,reportId)
{
	var x=window.confirm("Are you sure you want to delete the reort ?")
	if (x)
		{
			$("#progressLight").show();
			$("#progressDark").show();
			
			$.ajax({  		
				     type : "post",   
				     url : "deleteAccountReport.html", 
				     data : "reportName="+reportName+"&ReportCategory="+ReportCategory+"&reportId="+reportId,	     	     	     
				     success : function(response) 
				     {  		
				    	$("#progressLight").hide();
				 		$("#progressDark").hide();
				 		
				 		if(response=="Done")
				 			{
				 				alert(reportName+" report successfully deleted.");
				 				window.location="accountReports.html";
				 			}
				 		else
				 			{
				 				alert("Due to certain some problem arise , Please try again later.");
				 			}
				 		
				     },  
			     });
			
		}
	else
		{
		   alert("It's ok mistake happens.")
		}
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* -----------------------------------  ----------    END OF SCRIPT FOR ACCOUNT ADMIN      --------------------------------------------------------*/

















/* -----------------------------------  ----------     SCRIPT FOR CHECK ADMIN      --------------------------------------------------------*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//  Reference Check Operation

function editStatus(refId)
{
	
	var className = document.getElementById("iconId"+refId).className;
	if(className=="btn-icon-only icon-edit")
		{
			x=document.getElementById("checkStatus"+refId)
			x.disabled = !x.disabled;		
			
			document.getElementById("iconId"+refId).className = "btn-icon-only icon-save";
		}
	else
		{
		
		var status = document.getElementById("checkStatus"+refId).value;
		
			$("#progressLight").show();
			$("#progressDark").show();
					
			$.ajax({  
				
			     type : "post",   
			     url : "updateRefCheckStatus.html",
			     data : "referId="+refId+"&status="+status,
			     success : function(response) 
			     {  
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		if(response=="success")
			 			{
			 				 alert("Check details successfully updated.");	
			 				 window.location = "referFriendCheck.html";
			 			}
			 		else
			 			{
			 				alert("Request failed ! Please try again later.")
			 			}
						 	
			     },  
		    }); 
		}
	
}


function showRespectedDiv(value)
{
      if(value=="By Name")
    	  {
    	  		$("#DateDiv").slideUp("slow");
    	  		$("#NameDiv").slideDown("slow");
    	  }
      else
    	  {
    	  		$("#NameDiv").slideUp("slow");
    	  		$("#DateDiv").slideDown("slow");
    	  }
}



function searchCheckVia(searchType)
{
	 var name = '';
	 var startDate = '';
	 var endDate = '';
	 
	 
	$("#progressLight").show();
	$("#progressDark").show();
		
		
	if(searchType=="Name")
		{
			name += document.getElementById("fullName").value
		}
	else
		{		
			startDate += document.getElementById("some_class_1").value
			endDate += document.getElementById("some_class_2").value		    
		}	
		
	$.ajax({  
		
	     type : "post",   
	     url : "searchRefCheckStatus.html",
	     data : "searchType="+searchType+"&name="+name+"&startDate="+startDate+"&endDate="+endDate,
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
	 		$("#todaysCheckDiv").html("");
	 		$("#searchCheckStatusDiv").html(response);
	 		
	     },  
    }); 
	
	
}


// End of Reference Check Bonus



// Offline payment Check Status

function editOfflineCheckStatus(tranId)
{
	
	var className = document.getElementById("iconId"+tranId).className;
	if(className=="btn-icon-only icon-edit")
		{
			x=document.getElementById("checkStatus"+tranId)
			x.disabled = !x.disabled;		
			
			document.getElementById("iconId"+tranId).className = "btn-icon-only icon-save";
		}
	else
		{
		
		var status = document.getElementById("checkStatus"+tranId).value;
		
			$("#progressLight").show();
			$("#progressDark").show();
					
			$.ajax({  
				
			     type : "post",   
			     url : "updateTransactionCheckStatus.html",
			     data : "uniqueId="+tranId+"&status="+status,
			     success : function(response) 
			     {  
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		if(response=="success")
			 			{
			 				 alert("Check details successfully updated.");	
			 				 window.location = "checkAdmin.html";
			 			}
			 		else
			 			{
			 				alert("Request failed ! Please try again later.")
			 			}
						 	
			     },  
		    }); 
		}
	
}


function searchCheckForOfflinePayment(searchType)
{
	 var name = '';
	 var startDate = '';
	 var endDate = '';
	 
	 
	$("#progressLight").show();
	$("#progressDark").show();
		
		
	if(searchType=="Name")
		{
			name += document.getElementById("fullName").value
		}
	else
		{		
			startDate += document.getElementById("some_class_1").value
			endDate += document.getElementById("some_class_2").value		    
		}	
		
	$.ajax({  
		
	     type : "post",   
	     url : "searchTranCheckStatus.html",
	     data : "searchType="+searchType+"&name="+name+"&startDate="+startDate+"&endDate="+endDate,
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
	 		$("#todaysCheckDiv").html("");
	 		$("#searchCheckStatusDiv").html(response);
	 		
	     },  
    }); 
	
	
}




// End of offlike payment check status






// Refer Bonus Check Functionality


function editRefBonusCheckStatus(id)
{	
	var className = document.getElementById("iconId"+id).className;
	if(className=="btn-icon-only icon-edit")
		{
			x=document.getElementById("checkStatus"+id)
			x.disabled = !x.disabled;		
			
			document.getElementById("iconId"+id).className = "btn-icon-only icon-save";
		}
	else
		{		
		    var status = document.getElementById("checkStatus"+id).value;
		
			$("#progressLight").show();
			$("#progressDark").show();
					
			$.ajax({  
				
			     type : "post",   
			     url : "updateRefBonusCheckStatus.html",
			     data : "uniqueId="+id+"&status="+status,
			     success : function(response) 
			     {  
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		if(response=="success")
			 			{
			 				 alert("Check details successfully updated.");	
			 				 window.location = "referFriendBonusCheck.html";
			 			}
			 		else
			 			{
			 				alert("Request failed ! Please try again later.")
			 			}
						 	
			     },  
		    }); 
		}
}



function searchCheckForBonusPayment(searchType)
{
	 var name = '';
	 var startDate = '';
	 var endDate = '';
	 
	 
	$("#progressLight").show();
	$("#progressDark").show();
		
		
	if(searchType=="Name")
		{
			name += document.getElementById("fullName").value
		}
	else
		{		
			startDate += document.getElementById("some_class_1").value
			endDate += document.getElementById("some_class_2").value		    
		}	
		
	$.ajax({  
		
	     type : "post",   
	     url : "searchRefBonusCheckStatus.html",
	     data : "searchType="+searchType+"&name="+name+"&startDate="+startDate+"&endDate="+endDate,
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
	 		$("#todaysCheckDiv").html("");
	 		$("#searchCheckStatusDiv").html(response);	 		
	     },  
    }); 

}

// End of refer Bonus Check Status






//Refer Bonus Check Functionality


function editPolicyBonusCheckStatus(id)
{	
	var className = document.getElementById("iconId"+id).className;
	if(className=="btn-icon-only icon-edit")
		{
			x=document.getElementById("checkStatus"+id)
			x.disabled = !x.disabled;		
			
			document.getElementById("iconId"+id).className = "btn-icon-only icon-save";
		}
	else
		{		
		    var status = document.getElementById("checkStatus"+id).value;
		
			$("#progressLight").show();
			$("#progressDark").show();
					
			$.ajax({  
				
			     type : "post",   
			     url : "updatePolicyBonusCheckStatus.html",
			     data : "uniqueId="+id+"&status="+status,
			     success : function(response) 
			     {  
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		if(response=="success")
			 			{
			 				 alert("Check details successfully updated.");	
			 				 window.location = "userPlanPolicyCheck.html";
			 			}
			 		else
			 			{
			 				alert("Request failed ! Please try again later.")
			 			}
						 	
			     },  
		    }); 
		}
}



function searchCheckForPolicyBonusPayment(searchType)
{
	 var name = '';
	 var startDate = '';
	 var endDate = '';
	 
	 
	$("#progressLight").show();
	$("#progressDark").show();
		
		
	if(searchType=="Name")
		{
			name += document.getElementById("fullName").value
		}
	else
		{		
			startDate += document.getElementById("some_class_1").value
			endDate += document.getElementById("some_class_2").value		    
		}	
		
	$.ajax({  
		
	     type : "post",   
	     url : "searchPolicyBonusCheckStatus.html",
	     data : "searchType="+searchType+"&name="+name+"&startDate="+startDate+"&endDate="+endDate,
	     success : function(response) 
	     {  
	    	$("#progressLight").hide();
	 		$("#progressDark").hide();
	 		
	 		$("#todaysCheckDiv").html("");
	 		$("#searchCheckStatusDiv").html(response);	 		
	     },  
 }); 

}



function createCheckReport()
{	
	var flag="true";
	
	var reportType = document.getElementById("reportTypeId").value;
	var reportFor = document.getElementById("reportForId").value;
	var reportFrom = document.getElementById("some_class_1").value;
	var reportTo = document.getElementById("some_class_2").value;
	
	    
	
	if(reportType=="Select a Category")
	{
		flag="false";
		$("#errReportType").html("Please select report Type.");
		$("#errReportType").show();
		return false;
	}
	
	if(reportType!="Select a Category")
	{
		$("#errReportType").hide();
	}
	  
	if(reportFor=="Select a Category")
	{
		flag="false";
		$("#errReportFor").html("Please select report For.");
		$("#errReportFor").show();
		return false;
	}
	
	if(reportFor!="Select a Category")
	{
		$("#errReportFor").hide();
	}
	  
    if(reportFrom=="")
	{
		flag="false";
		$("#errDuration").html("Please select a date from.");
		$("#errDuration").show();
		return false;
	}
   
    if(reportFrom!="")
	{
		$("#errDuration").hide();
	}
	  
    if(reportTo=="")
	{
		flag="false";
		$("#errDuration").html("Please select a date to.");
		$("#errDuration").show();
		return false;
	}
   
    if(reportTo!="")
	{
		$("#errDuration").hide();
	}
	
	if(flag=="true")
	{
		$("#progressLight").show();
		$("#progressDark").show();
		
		$.ajax({  		
			     type : "post",   
			     url : "generateCheckReport.html", 
			     data : $('#checkReportForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	$("#progressLight").hide();
			 		$("#progressDark").hide();
			 		
			 		alert("Report generate successfully . Please download from the list.");
					window.location = "checkReports.html";
			     },  
		     });
	}
	
}


function deleteCheckReport(reportName,ReportCategory,reportId)
{
	var x=window.confirm("Are you sure you want to delete the reort ?")
	if (x)
		{
			$("#progressLight").show();
			$("#progressDark").show();
			
			$.ajax({  		
				     type : "post",   
				     url : "deleteCheckReport.html", 
				     data : "reportName="+reportName+"&ReportCategory="+ReportCategory+"&reportId="+reportId,	     	     	     
				     success : function(response) 
				     {  		
				    	$("#progressLight").hide();
				 		$("#progressDark").hide();
				 		
				 		if(response=="Done")
				 			{
				 				alert(reportName+" report successfully deleted.");
				 				window.location="checkReports.html";
				 			}
				 		else
				 			{
				 				alert("Due to certain some problem arise , Please try again later.");
				 			}
				 		
				     },  
			     });
			
		}
	else
		{
		   alert("It's ok mistake happens.")
		}
}


//End of refer Bonus Check Status






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* -----------------------------------  ----------    END OF SCRIPT FOR CHECK ADMIN      --------------------------------------------------------*/



