
function saveReferenceDetails()
{
	var flag = 'true';
	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var fullName = document.getElementById("fullNameId").value;
	var emailId = document.getElementById("EmailsId").value;
	var mobileNo = document.getElementById("mobilNoId").value;
	
	if(fullName=="")
	{
		flag = 'false';
		$("#nameError").show();
        $("#nameError").html("Please enter reference full name.");	
        return false;
	}
	
	 if(fullName!="")
		{
			if(!fullName.match(nameWithSpace))  
	        {  		
				flag = 'false';
				$("#nameError").show();
		        $("#nameError").html("Only alphabets are allowed");	
		        return false;       
	        }
			else
			{
				$("#nameError").hide();
			}
		}
	 
	    if(emailId=="")
		{
		    flag = 'false';
			$("#emailError").show();
	        $("#emailError").html("Please enter a email id.");	
	        return false;    
		}
	 
	    if(emailId!="")
		{
			if(!emailId.match(email))  
	        {  			
				flag = 'false';
				$("#emailError").show();
		        $("#emailError").html("Please enter a valid email id.");	
		        return false;       
	        }
			else
			{
				$("#emailError").hide();
			}
		}
	     
	    if(mobileNo=="")
		{
	    	flag='false';
			$("#mobileError").show();
	        $("#mobileError").html("Please enter a  mobile no.");	
	        return false;   
		}
	    
	    if(mobileNo!="")
		{
			if(!mobileNo.match(phNo))  
	        {  			
				flag='false';
				$("#mobileError").show();
		        $("#mobileError").html("Please enter a valid mobile no.");	
		        return false;       
	        }
			else
			{
				$("#mobileError").hide();
			}
		}
	    
	    
	    if(flag=='true')
	    {
	    	$("#progressBar").show();
	    	
	    	$.ajax({  
				
			     type : "post",   
			     url : "regValidateEmailId.html", 
			     data :$('#referForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 
			    	 if(response!="exist")
			    		 {						   	 
							    	 $.ajax({  
							 			
									     type : "post",   
									     url : "saveAddMoreReference.html", 
									     data :$('#referForm').serialize(),	     	     	     
									     success : function(response) 
									     {  		
									    	 $("#progressBar").hide();
									    	 
											 if(response=="success")
												 {			
												     $("#popUpheading").html("Success Message");
												     $("#popUpMessage").html("Your reference information have been saved successfully.");	
												     $("#popUpDiv").show();
												 }
											 else
												 {
												     alert("Please Try again later!");
												 }
									     },  
								     });  
							    	 
							// End of Script Part
			    		 }
			    	 else
			    		 {
			    		     $("#progressBar").hide(); 		    		 
			    		 	 $("#emailError").show();
			    		 	 $("#emailError").html("Given email id is already registered in the portal !");	
			 	             return false;       
			    		 }				    	 
			     },  
		     });  
	    	
	    }
}



// Update Basic Information

function validateOnEachUpdateBasicInfo()
{
	var emailId = document.getElementById("basicEmailId").value;
	var shippingAddress = document.getElementById("shipAddress").value;
	var mobileNo = document.getElementById("mobileNoId").value;
	
	if(emailId!="")
	{
		$("#basicemailError").hide();  
	}
	
	if(mobileNo!="")
	{
		$("#basicMobileError").hide();  
	}
	
	if(shippingAddress!="")
	{
		$("#shippAddressError").hide();        
	}
	
} 

function updateBasicInfo()
{
	var flag = 'true';
	
	var buttonValue = document.getElementById("basicButtonId").value;
	if(buttonValue=="Edit")
		{
		        document.getElementById("basicButtonId").value = "Save";
				x=document.getElementById("basicEmailId")
			    x.disabled = !x.disabled;	
				x=document.getElementById("shipAddress")
			    x.disabled = !x.disabled;	
				x=document.getElementById("mobileNoId")
			    x.disabled = !x.disabled;	
		}
	else
		{
				var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
				var phNo = /^[0-9]{10}$/;	
				
				var emailId = document.getElementById("basicEmailId").value;
				var shippingAddress = document.getElementById("shipAddress").value;
				var mobileNo = document.getElementById("mobileNoId").value;
				
				if(emailId=="")
				{
				    flag = 'false';
					$("#basicemailError").show();
			        $("#basicemailError").html("Please enter a email id.");	
			        return false;    
				}
			 
			    if(emailId!="")
				{
					if(!emailId.match(email))  
			        {  			
						flag = 'false';
						$("#basicemailError").show();
				        $("#basicemailError").html("Please enter a valid email id.");	
				        return false;       
			        }
					else
					{
						$("#basicemailError").hide();
					}
				}
			    
			    if(mobileNo=="")
				{
				    flag = 'false';
					$("#basicMobileError").show();
			        $("#basicMobileError").html("Please enter your mobile no.");	
			        return false;    
				}
			 
			    if(mobileNo!="")
				{
					if(!mobileNo.match(phNo))  
			        {  			
						flag = 'false';
						$("#basicMobileError").show();
				        $("#basicMobileError").html("Please enter a valid mobile no.");	
				        return false;       
			        }
					else
					{
						$("#basicMobileError").hide();
					}
				}
			        
			    if(shippingAddress=="")
				{
				    flag = 'false';
					$("#shippAddressError").show();
			        $("#shippAddressError").html("Please enter your shipping address.");	
			        return false;    
				}
			    
			    if(shippingAddress!="")
				{
					$("#shippAddressError").hide();    
				}
			 
			    if(flag=="true")
			    {
			     	
			    	$("#progressBar").show();
			    	
			    	$.ajax({  
						
					     type : "post",   
					     url : "validateEmailIdOfUser.html", 
					     data : "emailId="+emailId,	     	     	     
					     success : function(response) 
					     {  		
					    	 
					    	 if(response!="exist")
					    		 {						   	 
						    		 $.ajax({  
						    	 			
						    		     type : "post",   
						    		     url : "updateBasicInfo.html", 
						    		     data :$('#basicProfileForm').serialize(),	     	     	     
						    		     success : function(response) 
						    		     {  		
						    		    	 $("#progressBar").hide();
						    		    	 
						    				 if(response=="success")
						    					 {			
						    					     $("#popUpheading").html("Success Message");
						    					     $("#popUpMessage").html("Your basic information have been updated successfully.");	
						    					     $("#popUpDiv").show();
						    					 }
						    				 else
						    					 {
						    					     alert("Please Try again later!");
						    					 }
						    		     },  
						    	     });  
									    	 
					    		 }
					    	 else
					    		 {
					    		     $("#progressBar").hide(); 		    		 
					    		 	 $("#basicemailError").show();
					    		 	 $("#basicemailError").html("Given email id is already registered in the portal !");	
					 	             return false;       
					    		 }				    	 
					     },  
				     });  
			    	
			    }
		}
	
	
}



//var poi= /^[a-zA-Z0-9]{1,30}$/;
//var poi= /^[a-zA-Z0-9]{6,20}$/;

// Save user Other details

function validateOnEachProfileOtherDetails()
{
	var emailId = document.getElementById("altEmailId").value;
	var perAddress = document.getElementById("perAddress").value;
	var poiType = document.getElementById("poiTypeId").value;
	var poiId = document.getElementById("poiId").value;
	
	if(emailId!="")
	{
		$("#emailError").hide();   
	}
	
	if(poiId=="")
	{
		$("#poiIdError").hide();    
	}
}


function saveProfileOtherDetails()
{
    var flag = 'true';

    var buttonValue = document.getElementById("othButtonId").value;
    
    if(buttonValue=="Edit")
    	{
    	    document.getElementById("othButtonId").value = "Save";
    	    x=document.getElementById("fileId")
    	    x.disabled = !x.disabled;	
    	    x=document.getElementById("datetimepicker")
    	    x.disabled = !x.disabled;	
    	    x=document.getElementById("altEmailId")
    	    x.disabled = !x.disabled;	
    	    x=document.getElementById("perAddress")
    	    x.disabled = !x.disabled;	
    	    x=document.getElementById("poiTypeId")
    	    x.disabled = !x.disabled;	
    	    x=document.getElementById("poiId")
    	    x.disabled = !x.disabled;	
    	}
    else
    	{
 
			    	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
			    	var poi= /^[A-Za-z0-9]*$/;
			    	
			    	var file = document.getElementById("fileId").value;
			    	var emailId = document.getElementById("altEmailId").value;
			    	var perAddress = document.getElementById("perAddress").value;
			    	var poiType = document.getElementById("poiTypeId").value;
			    	var poiId = document.getElementById("poiId").value;
			    	
			    	
			    	if(file!="")
			    	{
			    	    var ext=file.substring(file.lastIndexOf('.')+1);
			    	    var extension = new Array("jpg","jpeg","gif","png");						    
			    	    var condition = "NotGranted";
			    	    for(var m=0;m<extension.length;m++)
			    			{
			    			    if(ext==extension[m])
			    			    	{				    	    
			    			    	   condition="Granted";				    	    
			    			    	}				    
			    			}		    
			    		if(condition=="NotGranted")
			    			{				 
			    			   flag = 'false';
			    			   $("#fileError").html("Only Image files are allowed.");
			    			   $("#fileError").show();
			    			   return false;
			    			}
			    		
			    		var fileDetails = document.getElementById("fileId");
			    		var fileSize = fileDetails.files[0];
			    		var fileSizeinBytes = fileSize.size;
			    		var sizeinKB = +fileSizeinBytes / 1024;
			    		var sizeinMB = +sizeinKB / 1024;
			    		
			    		if(sizeinMB>2)
			    		{		 
			    		   flag = 'false';
			    		   $("#fileError").html("File size should not more then 2 MB.");
			    		   $("#fileError").show();
			    		   return false;
			    		}
			    		
			    	}
			    	
			    	if(emailId=="")
			    	{
			    	    flag = 'false';
			    		$("#emailError").show();
			            $("#emailError").html("Please enter a email id.");	
			            return false;    
			    	}
			     
			        if(emailId!="")
			    	{
			    		if(!emailId.match(email))  
			            {  			
			    			flag = 'false';
			    			$("#emailError").show();
			    	        $("#emailError").html("Please enter a valid email id.");	
			    	        return false;       
			            }
			    		else
			    		{
			    			$("#emailError").hide();
			    		}
			    	}
			        
			        if(poiType!="Select a POI Type")
			        {
			        	if(poiId=="")
			        	{
			        	    flag = 'false';
			        		$("#poiIdError").show();
			                $("#poiIdError").html("Please enter your proof of identity.");	
			                return false;    
			        	}
			         
			            if(poiId!="")
			        	{
			        		if(!poiId.match(poi))  
			                {  			
			        			flag = 'false';
			        			$("#poiIdError").show();
			        	        $("#poiIdError").html("Please enter a valid proof of identity.");	
			        	        return false;       
			                }
			        		else
			        		{
			        			$("#poiIdError").hide();
			        		}
			        	}	
			        }
			        
			        
			    		if(flag=='true')
			    		{
			    			$("#progressBar").show();
			    			
			    			var formData = new FormData($("#otherProfileForm")[0]);
			                
			    			$.ajax({
			    				type : "POST",
			    				url : "saveProfileOtherDetails.html",
			    				data : formData,					
			    				success : function(response) {
			                        
			    					$("#progressBar").hide();
			    					
			    					if(response=="success")
			    					 {			
			    					     $("#popUpheading").html("Success Message");
			    					     $("#popUpMessage").html("Your Other Detail information have been saved successfully.");	
			    					     $("#popUpDiv").show();
			    					 }
			    				 else
			    					 {
			    					     alert("Please Try again later!");
			    					 }
			    				},
			    				cache: false,
			    	 	        contentType: false,
			    	 	        processData: false,
			    				
			    			});
			    		}
    	}
    
	
	
}



// Update Password from password setting 

function validateOneachOfupdatePassword()
{
	var curPassword = document.getElementById("curPasswordId").value;
	var newPassword = document.getElementById("nwPasswordId").value;
    var confirmPassword = document.getElementById("cnfPsId").value;
    
    if(curPassword!="")
	{
		$("#curPasswordError").hide();     
	}
    
	if(newPassword!="")
	{
		$("#nwPasswordError").hide();
	}
    
    if(confirmPassword!="")
	{
		$("#cnfPasswordError").hide();
	}
}

function updatePassword()
{
	var flag = 'true';
	
	var alfanumeric = /^.*[a-zA-z].*\d.*/;    
	
	var curPassword = document.getElementById("curPasswordId").value;
	var newPassword = document.getElementById("nwPasswordId").value;
    var confirmPassword = document.getElementById("cnfPsId").value;
    
    if(curPassword=="")
	{
    	flag='false';
		$("#curPasswordError").show();
        $("#curPasswordError").html("Please enter your current password.");	
        return false; 
	}
    
	if(newPassword=="")
	{
    	flag='false';
		$("#nwPasswordError").show();
        $("#nwPasswordError").html("Please enter a new password.");	
        return false; 
	}
    
    if(newPassword.length<8)
	{
    	flag='false';
		$("#nwPasswordError").show();
        $("#nwPasswordError").html("Your password atleast 6 character.");	
        return false; 
	}
    
    if(newPassword.length>=8)
	{
    	if(!password.match(alfanumeric))  
        {  			
    		flag='false';
    		$("#nwPasswordError").show();
            $("#nwPasswordError").html("Your password should be alphanumeric.");	
            return false;       
        }     	
	}
    
    if(confirmPassword=="")
	{
    	flag='false';
		$("#cnfPasswordError").show();
        $("#cnfPasswordError").html("Please confirm your entered password.");	
        return false; 
	}
    
    if(newPassword!=confirmPassword)
	{
    	flag='false';
		$("#cnfPasswordError").show();
        $("#cnfPasswordError").html("Password and confirm password not matched.");	
        document.getElementById("cnfPsId").value = "";
        return false; 
	}
    
    
    if(flag=="true")
    {
     	
    	$("#progressBar").show();
    	
    	$.ajax({  
			
		     type : "post",   
		     url : "checkCurrentPassword.html", 
		     data : "curPassword="+curPassword,	     	     	     
		     success : function(response) 
		     {  		
		    	 
		    	 if(response=="Matched")
		    		 {						   	 
			    		 $.ajax({  
			    	 			
			    		     type : "post",   
			    		     url : "updatepassword.html", 
			    		     data :$('#userPasswordForm').serialize(),	     	     	     
			    		     success : function(response) 
			    		     {  		
			    		    	 $("#progressBar").hide();
			    		    	 
			    				 if(response=="success")
			    					 {			
			    					     $("#popUpheading").html("Success Message");
			    					     $("#popUpMessage").html("Your password have been updated successfully.");	
			    					     $("#popUpDiv").show();
			    					 }
			    				 else
			    					 {
			    					     alert("Please Try again later!");
			    					 }
			    		     },  
			    	     });  
						    	 
		    		 }
		    	 else
		    		 {
		    		     $("#progressBar").hide(); 		    		 
		    		 	 $("#curPasswordError").show();
		    		 	 $("#curPasswordError").html("Given current pasword is not matched !");	
		 	             return false;       
		    		 }				    	 
		     },  
	     });  
    	
    }
    
    
}



// Show products

function displayProducts(type)
{	
	var currentValue = "";
	
	if(type=="Agriculture")
		{
			document.getElementById("hmCarId").checked = false;
			document.getElementById("herId").checked = false;
			
			currentValue = document.getElementById("agriProdId").value;
			
			document.getElementById("agriClass").className = "btn-box big span4 activeProductCategory";
			document.getElementById("herbalClass").className = "btn-box big span4";
			document.getElementById("hmCareClass").className = "btn-box big span4";
		}
	else if(type=="Herbal")
		{
		   document.getElementById("agriId").checked = false;
		   document.getElementById("hmCarId").checked = false;
		    
		    currentValue = document.getElementById("herbalProdId").value;
		    
		    document.getElementById("agriClass").className = "btn-box big span4";
			document.getElementById("herbalClass").className = "btn-box big span4 activeProductCategory";
			document.getElementById("hmCareClass").className = "btn-box big span4";
		}
	else
		{
			document.getElementById("agriId").checked = false;
			document.getElementById("herId").checked = false;
			
			currentValue = document.getElementById("hmCareProdId").value;
			
			document.getElementById("agriClass").className = "btn-box big span4";
			document.getElementById("herbalClass").className = "btn-box big span4";
			document.getElementById("hmCareClass").className = "btn-box big span4 activeProductCategory";
		}
	
	$("#progressBar").show();
	
	$.ajax({  
			
	     type : "post",   
	     url : "getProductAsPerType.html", 
	     data : "type="+type+"&existValue="+currentValue,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 $("#featurezone").html(response);			
	     },  
    });  
	
	
}



function captureSelectedProduct(selectId)
{	
	var currentSelectValue = document.getElementById(selectId).value;
	
	if(document.getElementById("agriId").checked == true)
	{   						
		if(document.getElementById(selectId).checked == true)
		{			
			document.getElementById("agriProdId").value += currentSelectValue + ",";			
		}
		else
		{
			var agriExtvalue = document.getElementById("agriProdId").value;	
			agriExtvalue = agriExtvalue.substring(0,agriExtvalue.length-1);
			
			var arrayValue = agriExtvalue.split(",");
			var temp = "";
			
			for(var i=0;i<arrayValue.length;i++)
				{
					if(currentSelectValue!=arrayValue[i])
						{
						   temp += arrayValue[i] + ",";
						}					
				}
			document.getElementById("agriProdId").value = temp;
		}
	}
	else if(document.getElementById("herId").checked == true)
	{		
		if(document.getElementById(selectId).checked == true)
		{
			document.getElementById("herbalProdId").value += currentSelectValue + ",";	
		}
		else
		{
			var herbalExtvalue = document.getElementById("herbalProdId").value;	
			herbalExtvalue = herbalExtvalue.substring(0,herbalExtvalue.length-1);
			
			var arrayValue = herbalExtvalue.split(",");
			var temp = "";
			
			for(var i=0;i<arrayValue.length;i++)
				{
					if(currentSelectValue!=arrayValue[i])
						{
						   temp += arrayValue[i] + ",";
						}					
				}

			document.getElementById("herbalProdId").value = temp;
		}
	}
	else
	{				
		if(document.getElementById(selectId).checked == true)
		{
			document.getElementById("hmCareProdId").value += currentSelectValue + ",";	
		}
		else
		{
			var hmCareExtvalue = document.getElementById("hmCareProdId").value;
			hmCareExtvalue = hmCareExtvalue.substring(0,hmCareExtvalue.length-1);
			
			var arrayValue = hmCareExtvalue.split(",");
			var temp = "";
			
			for(var i=0;i<arrayValue.length;i++)
				{
					if(currentSelectValue!=arrayValue[i])
						{
						   temp += arrayValue[i] + ",";
						}					
				}
			
			document.getElementById("hmCareProdId").value = temp;
		}
	}	
	
	
	var hmCare = document.getElementById("hmCareProdId").value;
	var herBal = document.getElementById("herbalProdId").value;
	var agriPro = document.getElementById("agriProdId").value;
		
	var noOfProducts =  (hmCare.split(",").length-1) + (herBal.split(",").length-1) + (agriPro.split(",").length-1);
	
	$("#noOfProId").html(noOfProducts);
	
}


function showCartProducts()
{
	var hmCare = document.getElementById("hmCareProdId").value;
	var herBal = document.getElementById("herbalProdId").value;
	var agriPro = document.getElementById("agriProdId").value;
	
	$("#progressBar").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "getInnerProductDetailsById.html", 
	     data :"agriProId="+agriPro+"&herBalId="+herBal+"&hmCareId="+hmCare,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 
	    	 $("#cartDetailsPopUp").slideDown();
			 $("#cartDetailsPopUp").html(response);				 	
			 
	     },  
    });  
	
}





//Show more about your product in a pop up

function viewMore(productId,productName)
{
    $("#progressBar").show();
	
	$.ajax({  
			
	     type : "post",   
	     url : "showMoreAboutProduct.html", 
	     data : "productId="+productId,	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 $("#popUPHeading").html("About "+productName);
	    	 $("#prductDetails").html(response);
	    	 $("#productDetailsPopUp").slideDown();
	    	 
	     },  
    });  
}

// End of show more about the product in the pop up


function saveSeletedProduct()
{
	var agri = document.getElementById("agriProdId").value;
	var herbal = document.getElementById("herbalProdId").value;
	var homecare = document.getElementById("hmCareProdId").value;
	
	var totalLength = 0;
	var agriLength = agri.split(",");
	var herbalLength = herbal.split(",");
	var homecareLength = homecare.split(",");
	
	if(agriLength.length>0)
	{
		totalLength  += (agriLength.length-1)
	}
	
	if(herbalLength.length>0)
	{
		totalLength  += (herbalLength.length-1)
	}
	
	if(homecareLength.length>0)
	{
		totalLength  += (homecareLength.length-1)
	}
	
	var flag = 'true';
	
	if(totalLength==0)
	{
		flag = 'false';
		$("#notifypopUpheading").html("Notification");
		$("#notifypopUpMessage").html("Please select some products to know your interest.");
		$("#NotifypopUpDiv").show();
		
		return false;
	}
	
	if(totalLength>10)
	{
		flag = 'false';
		$("#notifypopUpheading").html("Notification");
		$("#notifypopUpMessage").html("Please select minimum 10 products to know your interest.");
		$("#NotifypopUpDiv").show();
		
		return false;
	}
	
	
	if(flag=='true')
		{
		    $("#progressBar").show();
		
			$.ajax({  
				
			     type : "post",   
			     url : "captureProductSelection.html", 
			     data :$('#productSelectionForm').serialize(),	     	     	     
			     success : function(response) 
			     {  		
			    	 $("#progressBar").hide();
			    	 
					 if(response=="success")
						 {	
						 	$("#paymentMode").show();					 							 	
						 }
					 else
						 {
						     alert("Please Try again later!");
						 }
			     },  
		    });  
		}
}

function capturePaymentMode(checkId)
{
	if(checkId=="roundedTwo")
	{
		document.getElementById("roundedThree").checked = false;
	}
	else
	{
		document.getElementById("roundedTwo").checked = false;
	}
}


function gotoCheckOut()
{
	$("#paymentMode").hide();	
	
	if(document.getElementById("roundedTwo").checked==true)
	{	
		$("#progressBar").show();
		
		var mode = "Online";
		
		$.ajax({  
			
		     type : "post",   
		     url : "managePaymentMode.html", 
		     data :"mode="+mode,	     	     	     
		     success : function(response) 
		     {  		
		    	 $("#progressBar").hide();
		    	 
				 if(response=="success")
					 {	
					 	window.location = "gotoCheckOutPage.html";
					 }
				 else
					 {
					     alert("Please Try again later!");
					 }
		     },  
	    });  
	}
	else if(document.getElementById("roundedThree").checked==true)
	{		
		var mode = "Offline";
		$("#progressBar").show();
		
		$.ajax({  
			
		     type : "post",   
		     url : "managePaymentMode.html", 
		     data :"mode="+mode,	      	     	     
		     success : function(response) 
		     {  		
		    	 $("#progressBar").hide();
		    	 
				 if(response=="success")
					 {	
					 	 window.location = "gotoCheckOutPage.html";
					 }
				 else
					 {
					     alert("Please Try again later!");
					 }
		     },  
	    });  
	}
	else
	{
		alert("Please select a Payment mode before you proceed!")
	}
}

function proceedForOfflinePayment()
{
    $("#progressBar").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "saveUserPurchageDetails.html", 
	     data :$('#orderDetailForm').serialize(),	     	     	     
	     success : function(response) 
	     {  		
	    	 $("#progressBar").hide();
	    	 
			 if(response=="success")
				 {	
				 	$("#popUpheading").html("Success Message");
				 	$("#popUpMessage").html("Thank you for ordering ! further notification will notify to your contact details.");	
				 	$("#popUpDiv").show();
				 }
			 else
				 {
				    alert("Please Try again later!");
				 }
	     },  
    });  
}


//  End of capture product Id when user click on the select checkbox



// Show Order Status during Track Status

function showOrderStatus(orderId)
{
	if(document.getElementById("statusId"+orderId).style.display=="none")
		{
		   $("#"+"statusId"+orderId).slideDown("slow");
		}
	else
		{
		   $("#"+"statusId"+orderId).slideUp("slow");
		}
}



//  show Reference Status


function viewStatus(referenceId)
{
	if(document.getElementById("statusOf"+referenceId).style.display == "none")
		{
			 $("#progressBar").show();
				
				$.ajax({  
					
				     type : "post",   
				     url : "showReferenceStatus.html", 
				     data :"referenceId="+referenceId,	     	     	     
				     success : function(response) 
				     {  	
				    	 
				    	 $("#progressBar").hide();
				    	 
						 $("#statusOf"+referenceId).html(response);
						 $("#statusOf"+referenceId).slideDown("slow");
				     },  
			    });  
		}
	else
		{
		        $("#statusOf"+referenceId).slideUp("slow");
		}
   
}


//  Reference Policy Bonus Status

function refPolicyStatus()
{
	$("#progressBar").show();
	
	$.ajax({  
		
	     type : "post",   
	     url : "refBonusPolicyStatus.html",    	     	     
	     success : function(response) 
	     {  		    	 
	    	 $("#progressBar").hide();
	    	 
			 $("#refPolicyStatusDiv").html(response);			 
	     },  
    });  
}



function contactUsBiosys()
{
	var flag = 'true';
	
	var nameWithSpace = /^[a-zA-Z\s]*$/;	
	var phNo = /^[0-9]{10}$/;	
	var email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	
	var fullName = document.getElementById("fullNameId").value;
	var emailId = document.getElementById("emailId").value;
	var mobileNo = document.getElementById("mobileId").value;
	var contactMode = document.getElementById("categoryId").value;
	var desc = document.getElementById("descriptionId").value;
	
	if(fullName=="")
	{
		flag = 'false';
		$("#errorName").show();
        $("#errorName").html("Please enter full name.");	
        return false;
	}
	
	 if(fullName!="")
		{
			if(!fullName.match(nameWithSpace))  
	        {  		
				flag = 'false';
				$("#errorName").show();
		        $("#errorName").html("Only alphabets are allowed");	
		        return false;       
	        }
			else
			{
				$("#errorName").hide();
			}
		}
	 
	    if(emailId=="")
		{
		    flag = 'false';
			$("#errorEmail").show();
	        $("#errorEmail").html("Please enter a email id.");	
	        return false;    
		}
	 
	    if(emailId!="")
		{
			if(!emailId.match(email))  
	        {  			
				flag = 'false';
				$("#errorEmail").show();
		        $("#errorEmail").html("Please enter a valid email id.");	
		        return false;       
	        }
			else
			{
				$("#errorEmail").hide();
			}
		}
	     	   
	    if(mobileNo!="")
		{
			if(!mobileNo.match(phNo))  
	        {  			
				flag='false';
				$("#errorMobile").show();
		        $("#errorMobile").html("Please enter a valid mobile no.");	
		        return false;       
	        }
			else
			{
				$("#errorMobile").hide();
			}
		}
	    
	    if(mobileNo=="")
		{
	    	$("#errorMobile").hide();
		}
	    
	    if(contactMode=="Contact Mode")
		{
	    	flag='false';
			$("#errorCategory").show();
	        $("#errorCategory").html("Please select reason fro contact.");	
	        return false;      
		}
	    
	    if(contactMode!="Contact Mode")
		{
			$("#errorCategory").hide();    
		}
	    
	    if(desc=="")
		{
	    	flag='false';
			$("#errorDescription").show();
	        $("#errorDescription").html("Please describe something to know more about contact reason.");	
	        return false;      
		}
	    
	    if(desc!="")
		{
			$("#errorDescription").hide();    
		}
	
	    
	if(flag=='true')
	{
		$("#progressBar").show();
		
		var formData = new FormData($("#contactForm")[0]);
        
		$.ajax({
			type : "POST",
			url : "contactUsbiosys.html",
			data : formData,					
			success : function(response) {
                
				$("#progressBar").hide();
				
				if(response=="success")
				 {			
				     $("#popUpheading").html("Success Message");
				     $("#popUpMessage").html("Thank you ! Our team will respond you as soon as possible.");	
				     $("#popUpDiv").show();
				 }
			 else
				 {
				     alert("Please Try again later!");
				 }
			},
			cache: false,
 	        contentType: false,
 	        processData: false,
			
		});
	}
	
}

