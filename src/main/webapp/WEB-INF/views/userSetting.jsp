<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Setting</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
		
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
   
  
	<link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>
	
	<style type="text/css">
	
	.custom-date-style {
		background-color: red !important;
	}
	
	</style>
		  
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html" ><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="buyMorePackets.html"><i class="icon-gift"></i>  Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html"><i class=" icon-random"></i>      Track Order  </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="sideMenuActive" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="unstyled in collapse">
	                                        <li><a href="profileSetting.html" class="sideSubMenuActive"><i class="icon-edit"></i></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3 id="headingId">
                                        Profile Setting</h3>
                                </div>
                                
								<div class="module-body table">
                                   
									<div id="modularBodyId" class="module-body">






									<div class="headDiv"><h4 class="textStyle">Basic Information :</h4> </div>
                                    <br>
									
									<form id="basicProfileForm" name="basicProfileForm" class="form-horizontal row-fluid">
										
										<c:if test="${!empty userBasicInfo}">	
                                            <c:forEach items="${userBasicInfo}" var="det">
												
										
												<div class="control-group">
													<label for="basicinput" class="control-label">Email id</label>
													<div class="controls">
														<input type="text" class="span8"  placeholder="Enter email id" disabled="disabled" name="basicEmail" id="basicEmailId" value="${det.user_emailid}" onkeyup="validateOnEachUpdateBasicInfo()">	
														<div id="basicemailError" style="color:red;display:none;"></div>												
													</div>
												</div>
												
												<div class="control-group">
													<label for="basicinput" class="control-label">Mobile No</label>
													<div class="controls">
														<input type="text" class="span8" maxlength="10"  disabled="disabled" placeholder="Enter Proof of identity id." name="mobileNo" id="mobileNoId" value="${det.user_mobile_no}" onkeyup="validateOnEachUpdateBasicInfo()">
														<div id="basicMobileError"  style="color:red;display:none;"></div>				
													</div>
												</div>
		
		                                        <div class="control-group">
													<label for="basicinput" class="control-label">Shipping Address</label>
													<div class="controls">
														<textarea rows="5" class="span8" placeholder="Permanent Address" disabled="disabled" name="shippingAddress" id="shipAddress" onkeyup="validateOnEachUpdateBasicInfo()">${det.user_shipping_address}</textarea>
														<div id="shippAddressError" style="color:red;display:none;"></div>	
													</div>
												</div>
												
												<div class="control-group">
													<div class="controls">
														<input type="button" class="btn" id="basicButtonId" value="Edit" onclick="updateBasicInfo()">
													</div>
												</div>
										
										
										</c:forEach>
										
										</c:if>
										
										
										
									</form>


                                    <br>
									<div class="headDiv"><h4 class="textStyle">Other Profile Information :</h4> </div>
                                    <br>


									<form id="otherProfileForm" name="otherProfileForm" class="form-horizontal row-fluid" enctype="multipart/form-data">
										
										
										<c:if test="${!empty userOthDetails}">	
                                           
                                            <c:forEach items="${userOthDetails}" var="det">
												
														<div class="control-group">
															<label for="basicinput" class="control-label">Profile Picture</label>
															
															<div class="controls">
																<input type="file" class="span8" disabled="disabled" placeholder="Browse a file for profile picture" name="profilePhoto" id="fileId">	
																<div id="fileError" style="color:red;display:none;"></div>											
															</div>
															
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Date of Birth</label>
															<div class="controls">
															    
															    <input type="text" readonly placeholder="Date of Birth" name="dateOfBirth" id="datetimepicker" disabled="disabled" value="${det.user_dob}"/>
															    
															    								
															</div>
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Alternate Email id</label>
															<div class="controls">
																<input type="text" class="span8" disabled="disabled" placeholder="Enter alternate email id" name="alterEmailid" id="altEmailId" value="${det.user_alternate_emailid}" onkeyup="validateOnEachProfileOtherDetails()">	
																<div id="emailError" style="color:red;display:none;"></div>												
															</div>
														</div>
				
				                                        <div class="control-group">
															<label for="basicinput" class="control-label">Permanent Address</label>
															<div class="controls">
																<textarea rows="5" class="span8" disabled="disabled" placeholder="Permanent Address" name="permanentAddress" id="perAddress" > ${det.user_permanent_address} </textarea>
																<div id="addressError" style="color:red;display:none;"></div>	
															</div>
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Proof of Id Type</label>
															<div class="controls">
															
															<c:set var="idType"  value="${det.user_id_type}"/>
						                                    <%String idType = (String)pageContext.getAttribute("idType");%>
				                                            <%String idTypeString = "Pan Card,Aadhar Card,Voter Id,Driving Licence"; %>
				                                            <%String idTypeArray[] = idTypeString.split(","); %>
															
															
																<select class="span8" disabled="disabled" data-placeholder="Select a POI Type" id="poiTypeId" name="poiTypeName" tabindex="1" >
																	<option value="Select a POI Type">Select a POI Type</option>
																	
																	<%for(int i=0;i<idTypeArray.length;i++) {%>
																	    
																	    <%if(idTypeArray[i].equals(idType)) {%>
																	       <option value="<%=idTypeArray[i]%>" selected><%=idTypeArray[i]%></option>
																	    <%}else{ %>
																	       <option value="<%=idTypeArray[i]%>"><%=idTypeArray[i]%></option>
																	    <%} %>
																	    
																	<%} %>
																	
																</select>
																<div id="poiTypeError" style="color:red;display:none;"></div>	
															</div>
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Proof of Id</label>
															<div class="controls">
																<input type="text" disabled="disabled" class="span8" maxlength="20" placeholder="Enter Proof of identity id." name="poiIdName" id="poiId" value="${det.user_proof_of_id}" onkeyup="validateOnEachProfileOtherDetails()">
																<div id="poiIdError"  style="color:red;display:none;"></div>				
															</div>
														</div>
																						
				
														<div class="control-group">
															<div class="controls">
																<input type="button" class="btn"  id="othButtonId" value="Edit" onclick="saveProfileOtherDetails()" />
															</div>
														</div>
														
										      </c:forEach>
										
										</c:if>
										
										
										<c:if test="${empty userOthDetails}">	
                                           
														<div class="control-group">
															<label for="basicinput" class="control-label">Profile Picture</label>
															
															<div class="controls">
																<input type="file" class="span8" placeholder="Browse a file for profile picture" name="profilePhoto" id="fileId">	
																<div id="fileError" style="color:red;display:none;"></div>											
															</div>
															
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Date of Birth</label>
															<div class="controls">
															    
															    <input type="text" readonly placeholder="Date of Birth" name="dateOfBirth" id="datetimepicker"/>
															    								
															</div>
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Alternate Email id</label>
															<div class="controls">
																<input type="text" class="span8"  placeholder="Enter alternate email id" name="alterEmailid" id="altEmailId" onkeyup="validateOnEachProfileOtherDetails()">	
																<div id="emailError" style="color:red;display:none;"></div>												
															</div>
														</div>
				
				                                        <div class="control-group">
															<label for="basicinput" class="control-label">Permanent Address</label>
															<div class="controls">
																<textarea rows="5" class="span8" placeholder="Permanent Address" name="permanentAddress" id="perAddress" ></textarea>
																<div id="addressError" style="color:red;display:none;"></div>	
															</div>
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Proof of Id Type</label>
															<div class="controls">
																<select class="span8" data-placeholder="Select a POI Type" id="poiTypeId" name="poiTypeName" tabindex="1">
																	<option value="Select a POI Type">Select a POI Type</option>
																	<option value="PanCard">Pan Card</option>
																	<option value="Aadhar Card">Aadhar Card</option>
																	<option value="Voter Id">Voter Id</option>
																	<option value="Driving Licence">Driving Licence</option>
																</select>
																<div id="poiTypeError" style="color:red;display:none;"></div>	
															</div>
														</div>
														
														<div class="control-group">
															<label for="basicinput" class="control-label">Proof of Id</label>
															<div class="controls">
																<input type="text" class="span8" maxlength="20" placeholder="Enter Proof of identity id." name="poiIdName" id="poiId" onkeyup="validateOnEachProfileOtherDetails()">
																<div id="poiIdError"  style="color:red;display:none;"></div>				
															</div>
														</div>
				
													
				
														
				
														<div class="control-group">
															<div class="controls">
																<input type="button" class="btn" id="othButtonId" value="Save" onclick="saveProfileOtherDetails()" />
															</div>
														</div>
											
										</c:if>
										
										
									</form>
									
									
									
									
									
									
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
        
         <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="profileSetting.html";
		}
		
		</script>
         
         
         
        <script src="resources/datePicker/jquery.js"></script>
		<script src="resources/datePicker/jquery.datetimepicker.js"></script>
		<script>
		
		$('#datetimepicker').datetimepicker({
		dayOfWeekStart : 1,
		lang:'en',
		disabledDates:['08/01/1986','09/01/1986','10/01/1989'],
		startDate:	'01/01/2015'
		});
		$('#datetimepicker').datetimepicker({value:'',step:10});
		
		$('.some_class').datetimepicker();
		
		$('#default_datetimepicker').datetimepicker({
			formatDate:'d/m/Y',
			defaultDate:'+03/01/1970', 
			timepickerScrollbar:false
		});
		
		</script>
         
         
         
         
         
         
    </body>

</html>