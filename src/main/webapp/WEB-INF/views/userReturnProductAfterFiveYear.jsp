<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product Return</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">

<link href="resources/indexResources/css/demo.css" rel="stylesheet">
<link href="resources/indexResources/css/stepsForm.css" rel="stylesheet">
<script src="resources/indexResources/scripts/jquery-2.1.1.min.js"></script>

<script>

</script>

</head>
<body  onload="choosePaymentOption()">

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
       <a href="index.html" class="over"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
      <div class="topmenu"><a href="userLogin.html">Sign In</a> <!-- | <a href="userRegister.html"> Become a member</a> --></div>
            <ul class="topmenu">
              <li><a href="index.html"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
    
    <div class="container" style="padding: 50px !important;">
    	
        
        <!--STEPS FORM START ------------ -->
        <div class="stepsForm">
            
                                 
                <div class="sf-steps-form sf-radius"> 
                   
                   
                  <div class="checkOutBill" > 
                   
                    
                    <p style="color:yellowgreen;margin-left:203px;font-style:italic;font-size: 20px;">Thank you for to become  a valuable part  of the business.</p>
                    
                    
                    
		                    <div class="pricelist">
		                    
		                    
		                    <h2 style="color:#00b6f5;border-bottom:1px solid #ccc;padding-bottom: 5px;font-style:italic;">Return Agreement to business after 5 year </h2>
		                   
		                   <form id="returnDetailForm" name="returnDetailForm" method="post">
		                   
		                      						
		                        <table width="800" align="center" style="padding-bottom: 15px;">
		                        
		                        
				                        <tr class="rowStyle">   
				                         
				                         
				                         <td>
				                             <input type="hidden" name="userId" value="<%=session.getAttribute("regUserId")%>" />
				                             <select id="productSelectId" name="returnProduct" >
				                             
				                                      <option value="Default">Select the a product from the list</option>
				                                      <option value="Product 1">Product 1</option>
				                                      <option value="Product 1">Product 1</option>
				                                      <option value="Product 1">Product 1</option>
				                                      
				                             </select>				                             
				                         <td> 
				                         
				                         <td>:</td>
				                         
				                         <td class="rightHeading" style="width:300px;"><b>1 KG of selected product</b><td>     
				                        
				                         <td>:</td>  
				                             
				                         <td>
				                         <input type="hidden" name="amtReturnProduct" value="1 Kg" />
				                         <button id="sf-next" type="button" class="sf-button" onclick="saveReturnBackProduct()">Agree to give</button>
				                         </td>
				                         
				                        </tr>
		                         
		                        </table>
		                        
		                        
		                      </form>
		                      
		                         
		                   
		                    
		                    
		                    </div>
               
					
							
								<div class="paybutton" style="margin: 37px 403px 0 auto !important;">	
				                    <button id="sf-next" type="button" class="sf-button" onclick="ProceedToLogin()">Skip & Proceed</button>
				                </div>
	                            
                            
	                </div>
	                
	               
	               
                </div>
                
               
                
           
        </div>
        <!--STEPS FORM END -------------- -->
       
    </div>
    
    

<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 



<script src="resources/indexResources/scripts/userRegistrationScript.js"></script>

<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<div id="infoPopUpDiv" style="display:none;"> <%@include file="userPaymentModePopUp.jsp" %> </div>


<script>

function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location = "index.html";
}


</script>
</body>

</html>