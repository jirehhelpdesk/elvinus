<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bonus Amount</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
     
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html"><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="buyMorePackets.html"><i class="icon-gift"></i>  Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li ><a href="trackOrder.html"><i class=" icon-random"></i>      Track Order  </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a  data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="sideMenuActive" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="unstyled in collapse">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html" class="sideSubMenuActive"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Policy Amount Details as per <%=request.getAttribute("userPlan")%></h3>
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">
                                    
                                    
                                     <%                                       
                                       float tdsVal = 0;
                                       String tdsString = ""+request.getAttribute("tds");
                                       tdsVal= Float.parseFloat(tdsString);
                                     %>
                                     
        
                                      <c:forEach items="${policyDetails}" var="det">	
									         
                                               
                                                
                                                <div class="headDiv" style="margin-top:10px;"><h4 class="textStyle">Policy Amount for Order Id : ${det.user_order_id}</h4> </div>
                                                
                                                  <div class="checkOutBill" style="min-height:auto ! important;">                   
                   								                          
									               <div class="pricelist" style="min-height: 295px ! important;" >
									                    
									                   <form method="post" name="orderDetailForm" id="orderDetailForm">
									                   				
											                        <table width="800" align="center" style="padding-bottom: 15px;">
											                        <tbody>
											                          
											                         <tr class="rowStyle">
											                          <td class="rightHeading"><b>Order Id</b></td><td>     
											                         </td><td>:</td><td>       
											                         </td><td><i>${det.user_order_id}</i></td><td>
											                         
											                        </td></tr>
											                         <tr class="rowStyle">
											                         <td class="rightHeading"><b>Policy Status</b></td><td>     
											                         </td><td>:</td><td>       
											                         </td><td><i>${det.policy_status}</i></td><td>	
											                         			                         
											                        </td></tr>
											                        
											                        <tr class="rowStyle">
											                         <td class="rightHeading"><b>Policy Start Date</b></td><td>     
											                         </td><td>:</td><td>       
											                         </td><td><i>${det.policy_start_date}</i></td><td>											                            
											                        </td></tr>
											                        
											                        <tr class="rowStyle">
											                         <td class="rightHeading"><b>Policy End Date</b></td><td>     
											                         </td><td>:</td><td>       
											                         </td><td><i>${det.policy_end_date}</i></td><td>												                         			                         
											                        </td></tr>
											                        
											                        <c:set var="policy_status"  value="${det.policy_status}"/>
				 												    <%String policy_status = (String)pageContext.getAttribute("policy_status");%>
				                                                 
				                                                 
											                        <tr class="rowStyle">
											                         <td class="rightHeading" style="width: 161px !important;"><b>Check Delivery Status</b></td><td>     
											                         </td><td>:</td><td>       
											                         </td>
											                         <%if(!policy_status.equals("Success")){ %>     
											                         <td><i style="color:red;"><%=policy_status%></i></td>
											                         <%}else{ %>
											                         <td><i style="color:yellowgreen;"><%=policy_status%></i></td>
											                         <%} %>
											                         
											                         <td>												                         			                         
											                        </td></tr>
											                        
											                        
											                        </tbody></table>
									                        	
									                      </form>
									                      
									                         
									                     <div id="pricelist">
		                                                         
		                                                         <c:set var="policy_amount"  value="${det.policy_amount}"/>
				 												 <%int policy_amount = (Integer)pageContext.getAttribute("policy_amount");%>
				                                                 
				                                                 
		                                                         <% float tds = tdsVal; 
		                                                            float tdsAmount = (tds/100) * policy_amount;
		                                                            float total = policy_amount - tdsAmount;
		                                                         %>
		                                                             
		                                                         
										                        <table width="300" align="right">
										                        <tbody><tr>   
										                         <td style="width:155px;color: #00b6f5;font-style: italic;">Policy Amount</td><td>   
										                         </td><td>:</td>        
										                         <td>${det.policy_amount}.00</td><td>
										                        </td></tr>
										                          <tr>
										                           <td style="width:155px;color: #00b6f5;font-style: italic;">TDS (- <%=request.getAttribute("tds")%>)</td><td>
										                           </td><td>:</td>       
										                          <td><%=tdsAmount%></td><td>
										                        </td></tr>
										                        
										                         <tr>
										                          <td class="border"><strong>Total </strong></td><td></td><td>:</td>       
										                          <td class="border"><strong><%=total%></strong></td><td>
										                        </td></tr>
										                        </tbody></table>
										                    </div>
									                    
									                    
									                    </div>
							                         
								                </div>
                                        
		                                        <%if(!policy_status.equals("Success")){ %> 
		                                        
		                                                <div class="docs-example">
					                                        <ul>
					                                            <li><p class="text-info">${det.policy_status_details}</p></li>                                          
					                                            <li><p class="text-info">Before 3 weeks we will contact to your respective contact details.</p></li>
					                                            <li><p class="text-info">We will send a check with your name with your respective address.</p></li>		
					                                            <li><p class="text-info">For any query please drop a mail to <a href="#" style="color:red;">support@biosys.com.</a></p></li>	                                                                                   
					                                        </ul>                                       
					                                    </div>
					                              <%} %>      
                                        
                                        </c:forEach>
                                        
                                        
                                        
                                        
                                        
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
      
       <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="profileSetting.html";
		}
		
		</script>
         
         
    </body>

</html>