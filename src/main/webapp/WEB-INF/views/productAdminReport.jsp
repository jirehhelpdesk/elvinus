<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

<meta charset="utf-8">

<title>Reports</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">

<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">

<script type="text/javascript" src="resources/Admin_Resources/js/jquery-1.11.1.js"></script>

<link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>

<script>

$(document).keyup(function(e){
	  if(e.keyCode == 44) return false;
	});
	
</script>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="actimeMenu('menu2')">

<div> <%@include file="productAdminHeader.jsp" %> </div>
<!-- /subnavbar -->




<div class="main">
  <div class="main-inner">
    <div class="container">
                  
  				    <div class="widget-header">
	      				<i class="icon-shopping-cart"></i>
	      				<h3>Product Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						<div class="tabbable">
						
						<ul class="nav nav-tabs">
						 
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">Order Reports</a>
						  </li>
						  
						</ul>
						
						<br>
						
						<div class="tab-content">
						
						  <div id="formcontrols" class="tab-pane active">
								
							    <fieldset>
									
									<form name="orderReportForm" id="orderReportForm" class="form-horizontal">
								
									
										<div class="control-group">											
											<label class="control-label" for="username"><b>Report Type ::</b></label>
											<div class="controls">
													
													<select class="span4" id="reportTypeId" name="reportType">
													          
													          <option value="Select a Category">Select a Category</option>
													          <option value="All">All Order Reports</option>
													          <option value="Pending">Pending Order Reports</option>
													          <option value="Released">Released Order Reports</option>													          
													          <option value="Success">Success Order Reports</option>
													          
													</select>	
													
												<div class="errorStyle" id="errReportType"></div>											
											</div> <!-- /controls -->
											
																
																					
										</div> <!-- /control-group -->				
										
										
										<br>
										
											<div class="control-group">											
												<label class="control-label" for="username"><b>Select Duration ::</b></label>
													<div class="controls">
																				
														From : <input type="text" class="some_class" name="fromDate" value="" id="some_class_1">
														&nbsp;&nbsp;&nbsp; To : <input type="text" class="some_class" name="toDate" value="" id="some_class_2">
		
														<div class="errorStyle" id="errDuration"></div>
														
													</div>
															
											</div> <!-- /control-group -->		
											
										
										
										</form>
										
										
											
										<div class="form-actions">
										    
										    <input type="button" onclick="createOrderReport()" value="Generate Report" class="btn btn-primary"> 
											
										</div> <!-- /form-actions -->
										
										
									</fieldset>
									
									
									
									
									<fieldset>
										
										<div class="widget widget-table action-table" id="todaysCheckDiv">
								            
								            <div class="widget-header"> <i class="icon-th-list"></i>
								              <h3>This Month Generated Order Reports</h3>
								            </div>
								            <!-- /widget-header -->
								            <div class="widget-content">
								              
								              <div id="todaysCheckDiv">
								              					  
													              <table class="table table-striped table-bordered">
													               
													                <thead>
													                  
													                  <tr>
													                    <th> SL No </th>
													                    <th> Report Name </th>
													                    <th> Report Category </th>
													                    <th> Generated Date </th>
													                    
													                    <th style="width:100px;" class="td-actions"> Manage </th>
													                  </tr>
													                  
													                </thead>
													                
													                
													                <tbody>
													                
														                         
														                        <c:if test="${!empty existReportDetails}">	
																                
																	                <%int i = 1; %>
																	                 <c:forEach items="${existReportDetails}" var="det">
															
																                          <tr id="rowId">
																		                  
																		                    <td> <%=i++%> </td>
																		                    <td> ${det.report_name} </td>
																		                    <td> ${det.report_category}</td>
																		                    <td> ${det.report_generate_date}</td>
																		                    
																		                    <td class="td-actions">
																		                    
																		                        <a  href="downloadOrderReport.html?reportName=${det.report_name}&referCategory=${det.report_category}"  title="Download the Order Report" class="btn btn-small btn-success"><i class="btn-icon-only icon-download-alt" id="iconId38"> </i></a>
																		                         
																		                          <a onclick="deleteOrderReport('${det.report_name}','${det.report_category}','${det.report_id}')" href="javascript:;" title="Delete the Order Report" class="btn btn-danger btn-small">
																					         
																					                <i class="btn-icon-only icon-trash"> </i>
																					         
																						         </a>
																		                    </td>
																		                  
																		                  </tr>
																		                  
																					</c:forEach>		
																				
																				                      
																		    </c:if>  
																		    
																		    
																		    <c:if test="${empty existReportDetails}">	
																		    
																		    		<tr><td><p Style="color:red;">This month order reports are not generated.</p></td></tr>
																		    
																		    </c:if>
																                  
																                  
																	                 
													                 </tbody>
													                
													              </table>
								              				
								              	      	
								              
								              </div>
								              
								            </div>
								            <!-- /widget-content --> 
								          </div>
          
										
									</fieldset>
									
										
						</div>
								
								<!-- Remove Admin Division  -->
								 								
								<div id="jscontrols" class="tab-pane">
									
									<fieldset>
				
				                              <div id="updateorderDetailsDiv"></div>
		
									</fieldset>
									
									
								</div>
													
								<!-- End of Remove Admin Division  -->
							
							</div>
						  
						</div>
						
					</div>
					
					
					
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<div> <%@include file="supAdminFooter.jsp"%> </div>



<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/subAdminValidateForm.js"></script>


<script src="resources/datePicker/jquery.js"></script>
<script src="resources/datePicker/jquery.datetimepicker.js"></script>
<script>

$('.some_class').datetimepicker();


$('#datetimepicker_dark').datetimepicker({theme:'dark'})


</script>


</body>
</html>
