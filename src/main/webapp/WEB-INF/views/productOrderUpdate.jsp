<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Order</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>



          <div class="widget-header">
                          
				<i class="icon-th-list"></i>
				<h3 id="resultHeading">Update the order details of </h3>
				
		   </div>
			
			<%String ordercategory = "Pending,Approval,Released,Completed";
  
             String orderCatArray[] = ordercategory.split(",");%>  
  				
							      <c:forEach items="${orderDetails}" var="det">
			
			
				                               <div id="formcontrols" class="tab-pane active" style="margin-top:20px;">
												
												<form class="form-horizontal" id="updateOrderForm" name="updateOrderForm" >
													
													<fieldset>
																								
														<div class="control-group">											
															<label for="username" class="control-label">Order Status</label>
															<div class="controls">
														    
														    	 <c:set var="order_status"  value="${det.order_status}"/>
	 															 <%String order_status = (String)pageContext.getAttribute("order_status");%>
	 
																	<select style="width:370px;" id="updproductTypeId" name="ord_status">
																	        
																	        <option value="Select Order Status">Select Order Status</option>
																	        
																	        <%for(int i=0;i<orderCatArray.length;i++){%>
													        				
																		          <%if(order_status.equals(orderCatArray[i])){%>
																		                <option value="<%=orderCatArray[i]%>" selected="selected"><%=orderCatArray[i]%></option>
																		          <%}else{%>
																		                <option value="<%=orderCatArray[i]%>"><%=orderCatArray[i]%></option>
																		          <%}%>
																	          
																	        <%}%>
																	        
																	</select>
																
																<div id="errproductType" class="errorStyle"></div>
																											
															</div> <!-- /controls -->	
																		
														</div>
													
													    
													     <div class="control-group">											
															<label for="email" class="control-label">Name</label>
															<div class="controls">
																<input type="text"  id="updproductPriceId" disabled="true" value="${det.order_for_name}" name="ord_name" class="span4">
																<div id="errproductPrice" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
																		
														 <div class="control-group">											
															<label for="email" class="control-label">Email Id</label>
															<div class="controls">
																<input type="text"  id="updproductPriceId" disabled="true" value="${det.order_for_email_id}" name="ord_email_id" class="span4">
																<div id="errproductPrice" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
														<div class="control-group">											
															<label for="email" class="control-label">Contact No</label>
															<div class="controls">
																<input type="text"  id="updproductPriceId" disabled="true" value="${det.order_for_mobile_no}" name="ord_contact_no" class="span4">
																<div id="errproductPrice" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
													    <div class="control-group">											
															<label for="email" class="control-label">Order Amount</label>
															<div class="controls">
																<input type="text"  id="updproductPriceId" disabled="true" value="${det.order_amount}" name="ord_amount" class="span4">
																<div id="errproductPrice" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
														<div class="control-group">											
															<label for="password1" class="control-label">Order Shipping Address</label>
															<div class="controls">
																<c:out value="${det.order_shipping_address}"></c:out>
																<div id="errproductDesc" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->	
														
														<div class="control-group">											
															<label for="email" class="control-label">Order Date</label>
															<div class="controls">
																<input type="text" id="updproductNameId" disabled="true" value="${det.order_date}" name="ord_date" class="span4">
																<div id="errproductName" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
														<div class="control-group">											
															<label for="email" class="control-label">Expected Date</label>
															<div class="controls">
																<input type="text" id="updproductNameId" value="${det.order_expected_date}" name="exp_date" class="span4">
																<div id="errproductName" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->									
														
														<div class="control-group">											
															<label for="password1" class="control-label">Order Status Description</label>
															<div class="controls">
																<textarea class="span4" id="updproductDescId" name="order_description" style="height:200px;">${det.order_status_details} </textarea>
																<div id="errproductDesc" class="errorStyle"></div>
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->	
														
														
														<div class="control-group">											
															<label for="password1" class="control-label" style="width: 234px !important;"><b>${det.order_for_name}</b> interested product</label>
															<br>
															<div class="controls" style="margin-left: 251px ! important;">
																		
																		<ul>
																			<c:forEach items="${productDetails}" var="proDetail">
																																						
																				<li><c:out value="${proDetail.product_category} - ${proDetail.product_name}"></c:out></li>
																			
																			</c:forEach>
																		</ul>
																		
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->	
														
																	       	
														<div class="form-actions">
														    <input type="hidden" value="${det.order_id}" name="updorder_id" >
															<input class="btn btn-primary"  type="button" value="Update"  onclick="saveUpdatedOrderValues()"/>
															<input class="btn btn-primary"  type="button" value="Back"  onclick="bactoOrderDetails()"/>															
														</div> <!-- /form-actions -->
														
													</fieldset>
													
												</form>
												
												</div>
								
								</c:forEach>
						
								
</body>
</html>