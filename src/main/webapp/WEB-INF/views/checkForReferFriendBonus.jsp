<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Bonus Cheques</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">



<link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>


</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body onload="actimeMenu('menu3')">


<div> <%@include file="checkAdminHeader.jsp" %> </div>





<div class="main">
  <div class="main-inner">
    <div class="container">
                  
  				     <div class="widget-header">
	      				<i class="icon-credit-card "></i>
	      				<h3>Users Bonus Cheque Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols" onclick="reloadPage('referFriendBonusCheck.html')">Pending Bonus Cheque List</a>
						  </li>
						  <li class="" onclick="showAdminDetails()"><a data-toggle="tab" href="#jscontrols" >Search Bonus Cheque Details</a></li>
						</ul>
						
						
						<br>
						
							<div class="tab-content">
						
						    <div id="formcontrols" class="tab-pane active">
								
								
								<form class="form-horizontal" id="profileForm" name="profileForm">
								
									<fieldset>
										
										<div class="widget widget-table action-table" id="todaysCheckDiv">
								            
								            <div class="widget-header"> <i class="icon-th-list"></i>
								              <h3>Pending Cheques Records</h3>
								            </div>
								            <!-- /widget-header -->
								            <div class="widget-content">
								              
								              <div id="todaysCheckDiv">
								              
											            <c:if test="${!empty bonusDetail}">	
																						  
													              <table class="table table-striped table-bordered">
													               
													                <thead>
													                  
													                  <tr>
													                    <th> SL No </th>
													                    <th> Full Name </th>
													                    <th> Shipping Address </th>
													                    <th> Email Id </th>
													                    <th> Mobile No </th>
													                    <th> Generated Date </th>
													                    <th> Cheque Amount <a>(WithOut TDS)</a> </th>
													                    <th> Cheque Status </th>
													                    <th class="td-actions"> Update </th>
													                  </tr>
													                  
													                </thead>
													                
													                <tbody>
													                
													                <%int i=1; %>
														                
														                <c:forEach items="${bonusDetail}" var="tran">	
													
													                     <!-- style="background-color:#e8f6e4;" -->
															                     
															                     <c:set var="givenId"  value="${tran.user_id}"/>
				 																 <%int givenId = (Integer)pageContext.getAttribute("givenId");%>
				 
				 
															                     <c:forEach items="${userBasicDetails}" var="basic">
															                     
															                      		<c:set var="userId"  value="${basic.user_id}"/>
				 																 		<%int userId = (Integer)pageContext.getAttribute("userId");%>
				 
						                                                                         <%if(userId==givenId) {%>
																					                
																					                <c:set var="checkStatus"  value="${tran.bonus_check_status}"/>
						 																 		    <%String checkStatus = (String)pageContext.getAttribute("checkStatus");%>
						 																				
						 																			 
																					                            <tr id="rowId">
																							                  
																							                    <td> <%=i++%> </td>
																							                    <td> ${basic.user_name} </td>
																							                    <td> ${basic.user_shipping_address} </td>
																							                    <td> ${basic.user_emailid} </td>
																							                    <td> ${basic.user_mobile_no} </td>
																							                    <td> ${tran.update_date} </td>
																							                    <td> Rs.${tran.bonus_amount}.00 </td>
																							                    
																							                    <td> 
																							                        
																							                        <select style="width:80px;" id="checkStatus${tran.plan_bonus_id}" disabled="disabled">
								 																					     
								 																				       <%if(checkStatus.equals("Success")) {%>  
								 																				       	      
								 																					      <option value="Pending">Pending</option>
								 																					      <option selected value="Success">Success</option>
								 																					   <%}else{ %>
								 																					   
								 																					      <option selected value="Pending">Pending</option>
								 																					      <option value="Success">Success</option>
								 																					      
								 																					   <%} %>   
								 																					   
								 																					</select>
								 																					
								 																				</td>
																							                    
																							                    <td class="td-actions" ><a class="btn btn-small btn-success" title="Edit the Cheque status" href="javascript:;" onclick="editRefBonusCheckStatus('${tran.plan_bonus_id}')"><i id="iconId${tran.plan_bonus_id}" class="btn-icon-only icon-edit"> </i></a></td>
																							                  
																							                  </tr>
																			                      
																                          <%} %>
																                          
																                 </c:forEach>
																                 
														                </c:forEach>
													                
													                </tbody>
													                
													              </table>
								              				
								              	      </c:if>	
								              
								              
								              </div>
								              
								            </div>
								            <!-- /widget-content --> 
								          </div>
          
										
									</fieldset>
									
									
								</form>
								
								
								
								</div>
								
								
								
								
								
								
								
								<!-- Remove Admin Division  -->
								
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="edit-profile2">
										<fieldset>
				                                
								                       <div class="control-group">
															<label class="control-label" for="username">Search Cheques Via</label>
															<div class="controls">
																<select onchange="showRespectedDiv(this.value)">
																        <option value="Select Search Criteria">Select Search Criteria</option>
																		<option value="By Name">By Name</option>
																		<option value="By Duration">By Duration</option>														
																</select>
																<div class="errorStyle" id="errfullName"></div>
															</div>
															<!-- /controls -->
														</div>
														
														<div id="NameDiv" style="display:none;">
														
																<div class="control-group">
																	<label class="control-label" for="username">Enter Name</label>
																	<div class="controls">
																		<input type="text"  class="span4" id="fullName" name="fullName" placeholder="Enter full name for better search"/>
																		<div class="errorStyle" id="errfullName"></div>
																	</div>
																	<!-- /controls -->
																</div>
																
																<div class="form-actions">
																    <input type="button" onclick="searchCheckForBonusPayment('Name')" value="Search" class="btn btn-primary"> 										
																</div>
																
														</div>
														
														
														<div id="DateDiv" style="display:none;">
														
																<div class="control-group">
																	<label class="control-label" for="username">Select a duration</label>
																	<div class="controls">
																		
																		From : <input type="text" class="some_class" value="" id="some_class_1"/>
																		&nbsp;&nbsp;&nbsp; To : <input type="text" class="some_class" value="" id="some_class_2"/>
					
					
																		<div class="errorStyle" id="errfullName"></div>
																	</div>
																	<!-- /controls -->
																</div>
																
																<div class="form-actions">
																    <input type="button"  onclick="searchCheckForBonusPayment('Duration')"  value="Search" class="btn btn-primary"> 										
																</div>
																
														</div>
														
				                                    <div id="searchCheckStatusDiv"></div>
		
										</fieldset>
									</form>
								</div>
								
								
								
								<!-- End of Remove Admin Division  -->
								
								
								
								
								
								
								
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div>
					
					
					
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div class="extra">
  
</div>
<!-- /extra -->


<!-- Footer -->

  <%@include file="adminFooter.jsp" %>
  
<!-- /footer --> 



<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/subAdminValidateForm.js"></script>



<script src="resources/datePicker/jquery.js"></script>
<script src="resources/datePicker/jquery.datetimepicker.js"></script>
<script>

$('.some_class').datetimepicker();


$('#datetimepicker_dark').datetimepicker({theme:'dark'})

function reloadPage(url)
{
	window.location = url;	
}
</script>

</body>
</html>
