<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <title>Cost Management</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
    
    <link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">
      
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body onload="actimeMenu('menu4')">

    <div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
             <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
    <!-- /navbar -->
    
   <div> <%@include file="supAdminHeader.jsp" %> </div>
   
   
    <!-- /subnavbar -->
    <div class="main">
        <div class="main-inner">
            <div class="container">
                
               	
	     
		       <div class="widget-header">
	      				<i class="icon-reorder"></i>
	      				<h3>Cost Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">Manage Cost Values</a>
						  </li>
						  
						</ul>
						
						<br>
						
						<%
						  String cost_name = "";
						 %>
						
							<div class="tab-content">
								<div id="formcontrols" class="tab-pane active">
								<form class="form-horizontal" id="edit-profile">
									
									
									<c:if test="${!empty costDetails}">	
																	
										
												<fieldset>
														
														   <div class="control-group">											
																<label for="username" class="control-label">Per Kit Cost</label>
																<div class="controls">
																   
																           <%String kitFlag = "true"; %>
																		   <c:forEach items="${costDetails}" var="det">	
																	
																				<c:set var="cost_name"  value="${det.cost_name}"/>
						                                                        <%cost_name = (String)pageContext.getAttribute("cost_name");%>
				 
																			    <%if(cost_name.equals("Per Kit Cost")){%>
																				     
																				    <%kitFlag = "false";%> 
																					<input type="text"  id="KitCharge" value="${det.cost_value}" disabled="true" class="span4">	
								                                                	<input style="margin-left:15px;" id="butId0" class="btn btn-primary" type="button" value="Edit" onclick="saveCostDetails('Per Kit Cost','KitCharge','butId0')"/>												
																			   
																			    <%}%>
																			
																		   </c:forEach> 
																		   
																		   <%if(kitFlag.equals("true")){%>
																			      	
																			      	<input type="text"  id="KitCharge" class="span4">	
								                                                  	<input style="margin-left:15px;" id="butId0" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('Per Kit Cost','KitCharge','butId0')"/>												
																			    
																			 <%} %>
																		    
																		   
																</div> <!-- /controls -->			
					                                            										
															</div> <!-- /control-group -->	
															
															<div class="control-group">											
																<label for="username" class="control-label">Registration Charge</label>
																<div class="controls">
																   
																           <%String rcFlag = "true"; %>
																		   <c:forEach items="${costDetails}" var="det">	
																	
																				<c:set var="cost_name"  value="${det.cost_name}"/>
						                                                        <%cost_name = (String)pageContext.getAttribute("cost_name");%>
				 
																			    <%if(cost_name.equals("Registration Charge")){%>
																				     
																				    <%rcFlag = "false";%> 
																					<input type="text"  id="regCharge" value="${det.cost_value}" disabled="true" class="span4">	
								                                                	<input style="margin-left:15px;" id="butId1" class="btn btn-primary" type="button" value="Edit" onclick="saveCostDetails('Registration Charge','regCharge','butId1')"/>												
																			   
																			    <%}%>
																			
																		   </c:forEach> 
																		   
																		   <%if(rcFlag.equals("true")){%>
																			      	
																			      	<input type="text"  id="regCharge" class="span4">	
								                                                  	<input style="margin-left:15px;" id="butId1" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('Registration Charge','regCharge','butId1')"/>												
																			    
																			 <%} %>
																		    
																		   
																</div> <!-- /controls -->			
					                                            										
															</div> <!-- /control-group -->																				
																									
															
															<div class="control-group">											
																<label for="username" class="control-label">Service Tax</label>
																<div class="controls">
																	
																	<%String srFlag = "true"; %>
																	<c:forEach items="${costDetails}" var="det">	
																	
																			<c:set var="cost_name"  value="${det.cost_name}"/>
					                                                        <%cost_name = (String)pageContext.getAttribute("cost_name");%>
			 
				 
																			<%if(cost_name.equals("Service Tax")){%>
																			
																			    <%srFlag = "false"; %>
																				<input type="text"  id="serTax" value="${det.cost_value}" disabled="true" class="span4">	
								                                                <input style="margin-left:15px;" id="butId2" class="btn btn-primary" type="button" value="Edit" onclick="saveCostDetails('Service Tax','serTax','butId2')"/>																													
																			
																			<%}%>
																			
																	 </c:forEach>
																	 	
																	 	
																	 <%if(srFlag.equals("true")){%>
																	 		
																				<input type="text"  id="serTax" class="span4">	
								                                                <input style="margin-left:15px;" id="butId2" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('Service Tax','serTax','butId2')"/>																													
																			
																	  <%} %>		
																	
																  
																
																</div> <!-- /controls -->			
					                                            										
															</div> 																			
															
																									
															<div class="control-group">											
																<label for="username" class="control-label">VAT</label>
																<div class="controls">
																
																<%String vatFlag = "true"; %>
																<c:forEach items="${costDetails}" var="det">	
																	
																			<c:set var="cost_name"  value="${det.cost_name}"/>
					                                                        <%cost_name = (String)pageContext.getAttribute("cost_name");%>
					                                                        
					                                                        <%if(cost_name.equals("VAT")){%>
					                                                        
					                                                                <%vatFlag = "false"; %>
							                                                        <input type="text"  id="vat" value="${det.cost_value}" disabled="true" class="span4">	
							                                                		<input style="margin-left:15px;" id="butId3" class="btn btn-primary" type="button" value="Edit" onclick="saveCostDetails('VAT','vat','butId3')"/>													
							                                                        
					                                                        <%}%>
					                                                        
																</c:forEach>
																
																 <%if(vatFlag.equals("true")){%>
																                    
																          <input type="text"  id="vat" class="span4">	
							                                              <input style="margin-left:15px;" id="butId3" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('VAT','vat','butId3')"/>													
							                                                        
					                                              <%} %>
					                                                       
																
																</div> <!-- /controls -->			
					                                            										
															</div> 
																									
															
															<div class="control-group">											
																<label for="username" class="control-label">TDS</label>
																<div class="controls">
																
																    <%String tdsFlag = "true"; %>
																	<c:forEach items="${costDetails}" var="det">	
																	
																			<c:set var="cost_name"  value="${det.cost_name}"/>
					                                                        <%cost_name = (String)pageContext.getAttribute("cost_name");%>
					                                                        
					                                                        <%if(cost_name.equals("TDS")){%>
					                                                             
					                                                            <%tdsFlag="false"; %>
																				<input type="text"  id="tds" value="${det.cost_value}" disabled="true" class="span4">	
								                                                <input style="margin-left:15px;" id="butId4" class="btn btn-primary" type="button" value="Edit" onclick="saveCostDetails('TDS','tds','butId4')"/>												
																			
																			<%}%>
																			
																	</c:forEach>
																	
																	<%if(tdsFlag.equals("true")){ %>
																	
																				<input type="text"  id="tds" class="span4">	
								                                                <input style="margin-left:15px;" id="butId4" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('TDS','tds','butId4')"/>												
																			
																	 <%}%>		
																			
																</div> <!-- /controls -->			
					                                            										
															</div> 
															
												</fieldset>
												
										  
										
									</c:if>
									
									
									<c:if test="${empty costDetails}">	
									
											   <fieldset>
															
															<div class="control-group">											
																<label for="username" class="control-label">Registration Charge</label>
																<div class="controls">
																	<input type="text"  id="regCharge" class="span4">	
					                                                <input style="margin-left:15px;" id="butId1" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('Registration Charge','regCharge','butId1')"/>												
																</div> <!-- /controls -->			
					                                            										
															</div> <!-- /control-group -->																				
																									
															
															<div class="control-group">											
																<label for="username" class="control-label">Service Tax</label>
																<div class="controls">
																	<input type="text"  id="serTax" class="span4">	
					                                                <input style="margin-left:15px;" id="butId2" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('Service Tax','serTax','butId2')"/>												
																</div> <!-- /controls -->			
					                                            										
															</div> 																			
															
																									
															<div class="control-group">											
																<label for="username" class="control-label">VAT</label>
																<div class="controls">
																	<input type="text"  id="vat" class="span4">	
					                                                <input style="margin-left:15px;" id="butId3" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('VAT','vat','butId3')"/>													
																</div> <!-- /controls -->			
					                                            										
															</div> 
																									
															
															<div class="control-group">											
																<label for="username" class="control-label">TDS</label>
																<div class="controls">
																	<input type="text"  id="tds" class="span4">	
					                                                <input style="margin-left:15px;" id="butId4" class="btn btn-primary" type="button" value="Save" onclick="saveCostDetails('TDS','tds','butId4')"/>												
																</div> <!-- /controls -->			
					                                            										
															</div> 
															
												</fieldset>
												
									</c:if>
									
								</form>
								
								</div>
								
								
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="edit-profile2">
										<fieldset>
											
										</fieldset>
									</form>
								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div>
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
 
 
   

<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>
    
	
	
    <!-- /extra -->
    <div> <%@include file="supAdminFooter.jsp"%> </div>
   
 
   
    <!-- /footer -->
    
    
    
    
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
    <script src="resources/Admin_Resources/js/excanvas.min.js"></script>
    <script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script>
    <script src="resources/Admin_Resources/js/bootstrap.js"></script>
    <script src="resources/Admin_Resources/js/base.js"></script>
    
    <script src="resources/Admin_Resources/js/validateForm.js"></script>
    
</body>
</html>

