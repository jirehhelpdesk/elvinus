<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Header</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
</head>
<body>
<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        
                        
                        <ul class="nav pull-right">
                            <li class="dropdown hoverStyle"><a  class="dropdown-toggle" data-toggle="dropdown"><%=session.getAttribute("fullName")%>
                                <b class="caret1"></b></a>
                                
                            </li>
                            <li class="dropdown hoverStyle"><a>Vendor Id : <%=session.getAttribute("vendorId")%> </a></li>
                            
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                
                                <%String profilePhoto = (String)session.getAttribute("profilePicture"); %>
                                <%if(!profilePhoto.equals("No Photo")){ %>
                                	 	
                                     <img src="${pageContext.request.contextPath}<%="/previewProfilePicture.html?fileName="+profilePhoto+"&productType=dssdfd"%>" class="nav-avatar" />
                                	  	
                                <%}else{%>
                                
                                     <img src="resources/User_Resources/images/passimg.png" class="nav-avatar" />
                                
                                <%}%>
                                
                                
                                <b class="caret"></b> </a>
                                
                                <ul class="dropdown-menu">
                                    <li><a href="myProfile.html">Your Profile</a></li>                                    
                                    <li><a href="userLogOut.html">Logout</a></li>
                                </ul>
                                
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
</body>
</html>