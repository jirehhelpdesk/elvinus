<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Product</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>
<%String productType = ""; 
String productName = "";
String productDescription = "";
String productFile = "";
String productPrice = "";

String product_uni_id = "";
int product_id = 0;

%>


<c:if test="${!empty productDetails}">	
<c:forEach items="${productDetails}" var="det">


	 <c:set var="product_category"  value="${det.product_category}"/>
	 <%productType += (String)pageContext.getAttribute("product_category");%>
	 
	 <c:set var="product_name"  value="${det.product_name}"/>
	 <%productName += (String)pageContext.getAttribute("product_name");%>
	 
	  <c:set var="product_price"  value="${det.product_price}"/>
	 <%productPrice += (String)pageContext.getAttribute("product_price");%>
	 
	 <c:set var="product_description"  value="${det.product_description}"/>
	 <%productDescription += (String)pageContext.getAttribute("product_description");%>
	 
	 <c:set var="product_image_file_name"  value="${det.product_image_file_name}"/>
	 <%productFile += (String)pageContext.getAttribute("product_image_file_name");%>
	
	<c:set var="product_unique_id"  value="${det.product_unique_id}"/>
	 <%product_uni_id += (String)pageContext.getAttribute("product_unique_id");%>
	
	<c:set var="product_id"  value="${det.product_id}"/>
	 <%product_id += (Integer)pageContext.getAttribute("product_id");%>
	
	 																											
</c:forEach>
</c:if>
         
  <%String productcategory = "Agriculture,Herbal,HomeCare";
  
  String productCatArray[] = productcategory.split(",");%>       
                               
                               
                               <div class="widget-header">
                               
									<i class="icon-th-list"></i>
									<h3 id="resultHeading">Update the product details of <%=productName%></h3>
									
							   </div>
												
												
                               <div id="formcontrols" class="tab-pane active" style="margin-top:20px;">
								
								<form class="form-horizontal" id="updateproductdtailsForm" name="updateproductdtailsForm" enctype="multipart/form-data">
									
									<fieldset>
																				
										<div class="control-group">											
											<label for="username" class="control-label">Product Category</label>
											<div class="controls">
												
												<select id="updproductTypeId" name="upd_product_category">
												        <option value="Select Admin Type">Select Product Packet</option>
												       
												        <%for(int i=0;i<productCatArray.length;i++){%>
												        
														          <%if(productType.equals(productCatArray[i])){%>
														                <option value="<%=productCatArray[i]%>" selected="selected"><%=productCatArray[i]%></option>
														          <%}else{%>
														                <option value="<%=productCatArray[i]%>"><%=productCatArray[i]%></option>
														          <%}%>
												          
												        <%}%>
												     
												</select>
												
												<div id="errproductType" class="errorStyle"></div>
																							
											</div> <!-- /controls -->				
										</div>
									
										
										
										<div class="control-group">											
											<label for="email" class="control-label">Product Name</label>
											<div class="controls">
												<input type="text" id="updproductNameId" value="<%=productName%>" name="upd_product_name" class="span4">
												<div id="errproductName" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label for="email" class="control-label">Product Price</label>
											<div class="controls">
												<input type="text"  id="updproductPriceId" value="<%=productPrice%>" name="upd_product_price" class="span4">
												<div id="errproductPrice" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label for="password1" class="control-label">Product Description</label>
											<div class="controls">
												<textarea class="span4" id="updproductDescId" name="upd_product_description" style="height:200px;"><%=productDescription%> </textarea>
												<div id="errproductDesc" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
										
										<div class="control-group">											
											<label for="password2" class="control-label">Product Image</label>
											<div class="controls">
												<input type="file"  id="updproductImg" name="upd_product_image_file_name" class="span4">                     
												<div><b>Note:</b>  If want to change the picture then only browse by default exist one will update</div>
											    <div id="errproductImg" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
											
										 <br>
										
										
												<div style="float:right;margin-right:61px;margin-top:-386px;width:400px;height:300px;">
												
												      <img style="border-radius:10px;width:400px;height:300px;" src="${pageContext.request.contextPath}<%="/previewProducts.html?fileName="+productFile+"&productType="+productType+""%>"  /></img>
												
												</div>
												
										
										        	
										<div class="form-actions">
										
										    <input type="hidden"  name="product_uni_id" value="<%=product_uni_id%>" />
											<input type="hidden"  name="product_id" value="<%=product_id%>" />
											<input type="hidden"  name="existfileName" value="<%=productFile%>" />
											
											<input class="btn btn-primary"  type="button" value="Update"  onclick="updateProduct()"/>
											<input class="btn btn-primary"  type="button" value="Back"  onclick="getProductdetails()"/>
											
										</div> <!-- /form-actions -->
										
									</fieldset>
									
								</form>
								
								</div>
								
	
								
</body>
</html>