<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Index Fotter</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

<%
ResourceBundle resource = ResourceBundle.getBundle("resources/fotterSocialLinks");
String facebookLink=resource.getString("facebookLink"); 
String twitterLink=resource.getString("twitterLink"); 
String googlePlus=resource.getString("googlePlus"); 
String linkedIn=resource.getString("linkedIn"); 
%>
		 
		 
<div id="footercontainer">
  
   <div id="footer">
	   
	    <div id="footerleft">
	      <ul class="footerlink">
	        <li><a href="index.html"><span>Home</span></a></li>
	        <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
	        <li><a href="services.html"><span>Services</span></a></li>
	        <li><a href="productDetails.html"><span>Products</span></a></li>
	        <li style="border:none;"><a href="contact.html"><span>Contact&nbsp;Us</span></a></li>
	      </ul>
	    </div>
    
    <div class="centercontent" >  � Reserved By <a href="http://www.jirehsol.co.in/index.php" target="_blank">Biosys Private Limited</a> </div>
    
    <div id="footerright"> <!-- �  --> Powered By <a href="http://www.jirehsol.co.in/index.php" target="_blank">Jireh</a>
    		
    		 <ul class="social_media">
                       
                       <li>
                        <a target="_blank" href="<%=facebookLink%>" class="google_plus"></a>
                       </li>
                       <li>
                       <a target="_blank" href="<%=twitterLink%>" class="twitter"></a>
                       </li>
                       <li>
                       <a target="_blank" href="<%=googlePlus%>" class="fb"></a>
                       </li>
                       <li>
                       <a target="_blank" href="<%=linkedIn%>" class="ln"></a>
                       </li>
                       <!-- <li>
                       <a class="rss" href="http://feeder.co/" target="_blank"></a>
                       </li> -->
                       
                  </ul>
                                 
     </div>
     
     
    <div class="clear"></div>
  </div>
</div>
</body>
</html>