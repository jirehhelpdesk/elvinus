<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Settings</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="actimeMenu('menu6')">

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>

    
<div> <%@include file="supAdminHeader.jsp" %> </div>
    
    
<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="widget-header">
	      				<i class="icon-cogs"></i>
	      				<h3>Application Back Up Settings</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">Email Sent Status</a>
						  </li>
						  
						  <li onclick="getSMSDetails()">
						    <a data-toggle="tab" href="#jscontrols">SMS Sent Status</a>
						  </li>
						  
						</ul>
						
						<br>
						
							<div class="tab-content">
							
								<div id="formcontrols" class="tab-pane active">
								<form class="form-horizontal" id="edit-profile">
									<fieldset>
										
										
										
										<!-- Email Sent Report  -->
										
													<div class="widget widget-table action-table">
														
															<div class="widget-header">
																<i class="icon-th-list"></i>
																<h3 id="resultHeading">All Pending Email Sent Details</h3>
															</div>
															<!-- /widget-header -->
															<div class="widget-content">
																
																<table class="table table-striped table-bordered">
																	
																	<thead>
																		<tr>
																		    <th>Serial no</th>
																			<th>Email Id</th>
																			<th>Email Subject</th>
																			<th>Sent date</th>
																			
																			<th class="td-actions"></th>
																		</tr>
																	</thead>
																	
																	
																	<tbody>
																		
																	<c:if test="${!empty emailDetails}">	
																		
																		<%int i=1; %>
																		
																		
																		<c:forEach items="${emailDetails}" var="det">	
																		
																		
																		<!-- Restrict for 10 datas -->
																		
																				<tr>
																				        <td><%=i++%></td>
																						<td>${det.sent_email_id}</td>
																						<td>${det.email_subject}</td>																						
																						<td>${det.email_sent_date}</td>
																						
																						<td class="td-actions">
																						
																								<a class="btn btn-small btn-success" href="javascript:;" title="Send Agian" onclick="sentMailAgain('${det.email_id}')">
																								
																								     <i class="btn-icon-only icon-repeat"> </i>
																										
																								</a> 
																								
																						</td>																																				
																				</tr>
																		
																		</c:forEach>
																		
																	</c:if>	
																	
																	<c:if test="${empty emailDetails}">	
																	
																	            <tr>
																				        <td style="color:red;">As per the search details No data found !</td>
																				
																				</tr>
																	
																	</c:if>
																																	
																	</tbody>
																</table>
															</div>
															<!-- /widget-content -->
														</div>
										
										
										
										
										
										
										
									</fieldset>
									
									
								</form>
								
								</div>
								
								
								
								<div id="jscontrols" class="tab-pane">
									
									   
									    <fieldset>
											
											<div id="smsDetailsDiv"></div>
											
										</fieldset>
										
										
									
								</div>
								
							</div>
						  
						</div>
						
					</div>
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
<div> <%@include file="supAdminFooter.jsp"%> </div>
    


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
	
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script src="resources/Admin_Resources/js/base.js"></script>

<script src="resources/Admin_Resources/js/validateForm.js"></script>

  </body>

</html>
