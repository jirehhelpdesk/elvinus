<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Register</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">

<link href="resources/indexResources/css/demo.css" rel="stylesheet">
<link href="resources/indexResources/css/stepsForm.css" rel="stylesheet">
<script src="resources/indexResources/scripts/jquery-2.1.1.min.js"></script>

<script>


</script>


</head>
<body>

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
        <a href="index.html" class="over"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
     <div class="topmenu"><a href="userLogin.html">Sign In</a>  <!-- <a href="userRegister.html"> Become a member</a> --></div>
            <ul class="topmenu">
              <li><a href="index.html"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
 
	
    <div class="container">
    	
        
        <!--STEPS FORM START ------------ -->
        <div class="stepsForm">
           
           
            
               
                <div class="sf-steps">
                    <div class="sf-steps-content">
                    	<div class="sf-active">
                        	<span>1</span> Basic Information
                        </div>
                        <div>
                        	<span>2</span> Product Selection
                        </div>
                        <div>
                        	<span>3</span> Add Reference
                        </div>
						 <div >
                        	<span>4</span> Plan Selection
                        </div>
						 <div >
                        	<span>5</span> Payment
                        </div>
						
                    </div>
                </div>                
               
                    
                <!-- <div class="sf-steps-navigation sf-align-right">
                    <button id="sf-next" type="button" class="sf-button" onclick="saveBasicUserDetails()">Proceed >></button>
                </div> -->
                   
               <div class="sf-steps-navigation sf-align-right" style="height: 25px;"> </div>
               
               <form id="basicProfileForm" name="basicProfileForm" method="post">
                                           
                  <div class="sf-steps-form sf-radius" style="width:600px; margin:-34px auto;"> 
                    
                    <ul class="sf-content" > <!-- form step one --> 
                         
                         <li>
                            <div class="sf_columns" style="width: 40% !important;">
                            <p>Enter Full Name</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">
                               <input  type="text" name="fullName" id="fullNameId" maxlength="50" placeholder="Enter your full name" data-required="true"  onkeyup="validateEachBasic()"/>
                               <div id="nameError" class="basicError"></div>
                            </div>
                         </li>
                         <li>
                            <div class="sf_columns" style="width: 40% !important;">
                            <p>Email Id</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">
                               <input  type="text" name="emailId" id="EmailsId" maxlength="50"  placeholder="Enter your email id" data-required="true" onchange="validateEachBasic()"/>
                               <div id="emailError" class="basicError"></div>
                            </div>
							
                         </li>
                          <li>
                            <div class="sf_columns" style="width: 40% !important;">
                            <p>Mobile No</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">
                            
 		                     <input  type="text" name="mobileNo" id="mobilNoId" maxlength="10" placeholder="Enter your mobile no" data-required="true" onchange="validateEachBasic()" />
 		                     <div id="mobileError" class="basicError"></div>
 		                     
                            </div>
							
                         </li>
                         <li>
                          <div class="sf_columns" style="width: 40% !important;">
                           <p>Shipping Address</p>
                              
                            </div>
                           <div class="sf_columns " style="width:60% !important;">
                                <textarea placeholder="Enter the shipping address with Pincode" id="addressId" data-required="true" name="address" onkeyup="validateEachBasic()" ></textarea>
                                <div id="addressError" class="basicError"></div>
                            </div>
                         </li>
						 <li>
                          <div class="sf_columns" style="width: 40% !important;">
                            <p>Password</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">                                    
 		  						<input  type="password" name="pw" maxlength="15" id="pwId" placeholder="Password" data-required="true" onkeyup="validateEachBasic()" />
 		  						<div id="pwError" class="basicError"></div>
                            </div>
                         </li>
                         <li>
                          <div class="sf_columns" style="width: 40% !important;">
                            <p>Confirm Password</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">                          
 		 						 <input  type="password" name="cnpw" maxlength="15" id="cnPwId"  placeholder="Confirm Password" data-required="true" onkeyup="validateEachBasic()" />
 		 						 <div id="cnpwError" class="basicError"></div>
                            </div>
                         </li>
						 
                         <li>
                            <div class="sf_columns column" style=" margin-left: -17px;">
                           
                           
                                <div class="sf-check">
                                   
                                    <label>
                                    
                                    <input type="checkbox" value="test" id="agreeId" name="test" data-required="true" onclick="validateEachBasic()" ><span></span> 
                                       
                                       I agree with all <a href="termConditions.html" style="font-size: 13px;text-decoration: unset;" target="_blank" >terms & conditions.</a>
                                    
                                    </label>
                                    
                                    <div id="checkError" class="basicError"></div>
                                    
                                </div>
                            </div>
                         </li>
                         
                         
                    </ul>
                       
                    
					<button id="sf-next" type="button" class="sf-button" onclick="saveBasicUserDetails()" style="float: right;
    margin-right: 25px;">Proceed >></button>
                </div>
                
                
            </form>
        </div>
        <!--STEPS FORM END -------------- -->
       
    </div>
    
    
<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 



<script src="resources/indexResources/scripts/userRegistrationScript.js"></script>


<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<script>

function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location="productSelection.html";
}

</script>

</body>
</html>