<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Manage Business Admin</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

                       <div class="widget widget-table action-table">
											
												<div class="widget-header">
													<i class="icon-th-list"></i>
													<h3>List of Business Admin</h3>
												</div>
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
																<th>Admin Type</th>
																<th>Admin Name</th>
																<th>Admin Email id</th>
																<th>Admin Mobile no</th>
																<th>Admin Created Date</th>																
																<th class="td-actions"></th>
															</tr>
														</thead>
														
														
														<tbody>
															
														<c:if test="${!empty adminDetails}">	
															
															<c:forEach items="${adminDetails}" var="det">	
															
																	<tr>
																			<td>${det.admin_type}</td>
																			<td>${det.admin_full_name}</td>
																			<td>${det.admin_email_id}</td>
																			<td>${det.admin_mobile_no}</td>
																			<td>${det.admin_cr_date}</td>
																			
																			<td class="td-actions"><!-- <a
																				class="btn btn-small btn-success" href="javascript:;"><i
																					class="btn-icon-only icon-pencil"> </i></a> -->
																					
																					<a class="btn btn-danger btn-small" href="javascript:;" title="Remove Sub Admin" onclick="removeAdminDetails('${det.admin_id}')">
																				         
																				         <i class="btn-icon-only icon-trash"> </i>
																				         
																					</a>
																					
																			</td>																																				
																	</tr>
															
															</c:forEach>
															
														</c:if>	
																														
														</tbody>
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>