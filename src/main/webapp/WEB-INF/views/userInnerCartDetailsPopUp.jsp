<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link href="resources/User_Resources/popUpDesign/popUp.css" rel="stylesheet">
    
    
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,800' rel='stylesheet' type='text/css'>
    <link href='css/styles.css' rel='stylesheet' type='text/css'>
    
    <script>
    
    function hidePlanPopUp()
    {
    	$("#cartDetailsPopUp").slideUp();
    }
    
    </script>
    
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>


    <div id="about-modal" class="rv-vanilla-modal" style="left: 82% !important;min-height: auto !important;top: 19% !important;width: 30% !important;position:absolute !important;">
   
	    <div class="rv-vanilla-modal-header group">
	        <button class="rv-vanilla-modal-close" onclick="hidePlanPopUp()"><span class="text" style=" left: 0px ! important;">�</span></button>
	        <h2 class="rv-vanilla-modal-title" id="popUPHeading" style="line-height: 16px !important;"><img src="resources/User_Resources/images/Shopping-Cart.png" alt="" width="24" height="24" class="popUpShoping" />Selected Product Details</h2>
	    </div>
    
    
	    <div id="prductDetails" class="rv-vanilla-modal-body"  style="min-height: 450px !important;padding: 19px 19px 19px !important;">
	    
	    		<div>
		    		     <ul>
		    		     
		    		     		<li>
		    		     		      <h4 class="headingDesign">Agriculture Products</h4>
		    		     		      <br><br>
		    		     		      <div class="productListDesign">
			    		     		      <ul>
			    		     		          
			    		     		          <c:if test="${!empty agriProductDetails}">	
		                                            <%int i=1; %>
													<c:forEach items="${agriProductDetails}" var="det">							    		     		        
						    		     		          <li><%=i++%>.${det.product_name}</li>						    		     		          
			    		     		                </c:forEach>
			    		     		          
			    		     		          </c:if>   
			    		     		           
			    		     		           <c:if test="${empty agriProductDetails}">
			    		     		                  
			    		     		                  <li style="color:red">Nothing has selected from this category.</li>
			    		     		                  
			    		     		           </c:if>	
			    		     		           	     		      
			    		     		      </ul>
		    		     		      </div>  		    		     			    		     		
		    		     		</li>
		    		     	
		    		     	
		    		     		<li>
		    		     		      <h4 class="headingDesign">Herbal Products</h4>
		    		     			  <br><br>
		    		     		      <div class="productListDesign">
			    		     		      <ul >
			    		     		           <c:if test="${!empty herbalProductDetails}">	
		                                            <%int i=1; %>
													<c:forEach items="${herbalProductDetails}" var="det">							    		     		        
						    		     		          <li><%=i++%>.${det.product_name}</li>						    		     		          
			    		     		                </c:forEach>
			    		     		          
			    		     		          </c:if>   
			    		     		           
			    		     		           <c:if test="${empty herbalProductDetails}">
			    		     		                  
			    		     		                  <li style="color:red">Nothing has selected from this category.</li>
			    		     		                  
			    		     		           </c:if>	    		      
			    		     		      </ul>
		    		     		      </div>  
		    		     		</li>
		    		     		
		    		     		
		    		     		<li>
		    		     		      <h4 class="headingDesign">HomeCare Products</h4>
		    		     		      <br><br>
		    		     		      <div class="productListDesign">
			    		     		      <ul>
			    		     		          <c:if test="${!empty hmCareProductDetails}">	
		                                            <%int i=1; %>
													<c:forEach items="${hmCareProductDetails}" var="det">							    		     		        
						    		     		          <li><%=i++%>.${det.product_name}</li>						    		     		          
			    		     		                </c:forEach>
			    		     		          
			    		     		          </c:if>   
			    		     		           
			    		     		          <c:if test="${empty hmCareProductDetails}">
			    		     		                  
			    		     		                <li style="color:red">Nothing has selected from this category.</li>
			    		     		                  
			    		     		          </c:if>		    		     		      
			    		     		      </ul>
		    		     		      </div>  		    		     		    
		    		     	    </li>	
		    		     	   
		    		     	       		     	    		     
		    		     </ul>	    		
	    		</div>
	        
	    </div>
    
   </div>
    
    <!-- <div class="backGroundDiv"></div> -->
     
</body>


</html>