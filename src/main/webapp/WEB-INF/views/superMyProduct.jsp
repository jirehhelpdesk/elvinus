<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  
 <head>
    <meta charset="utf-8">
    <title>My Products</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
    
    <link href="resources/Admin_Resources/css/pages/reports.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">

  </head>

<body onload="actimeMenu('menu2')">

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>

    
<div> <%@include file="supAdminHeader.jsp" %> </div>
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	    	
	     
		 <div class="widget-header">
	      				<i class="icon-briefcase"></i>
	      				<h3>Product Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">New Product</a>
						  </li>
						  <li class="" onclick="getProductdetails()"><a data-toggle="tab" href="#jscontrols">Manage Product</a></li>
						</ul>
						
						<br>
						
							<div class="tab-content">
								
								
								<div id="formcontrols" class="tab-pane active">
								
								<form class="form-horizontal" id="productdtailsForm" name="productdtailsForm" enctype="multipart/form-data">
									
									<fieldset>
																				
										<div class="control-group">											
											<label for="username" class="control-label">Product Category</label>
											<div class="controls">
												<select id="productTypeId" name="product_category">
												        <option value="Select Admin Type">Select Product Packet</option>
														<option value="Agriculture">Agriculture</option>
														<option value="Herbal">Herbal</option>
														<option value="HomeCare">HomeCare</option>
												</select>
												
												<div id="errproductType" class="errorStyle"></div>
																							
											</div> <!-- /controls -->				
										</div>
										
										
										<div class="control-group">											
											<label for="email" class="control-label">Product Name</label>
											<div class="controls">
												<input type="text" id="productNameId" name="product_name" class="span4">
												<div id="errproductName" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label for="email" class="control-label">Product Price</label>
											<div class="controls">
												<input type="text"  id="productPriceId" name="product_price" class="span4">
												<div id="errproductPrice" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label for="password1" class="control-label">Product Description</label>
											<div class="controls">
												<textarea class="span4" id="productDescId" name="product_description" style="height:200px;"> </textarea>
												<div id="errproductDesc" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
										
										<div class="control-group">											
											<label for="password2" class="control-label">Product Image</label>
											<div class="controls">
												<input type="file"  id="productImg" name="product_image_file_name" class="span4">
												<div id="errproductImg" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
											
										 <br>
										
											
										<div class="form-actions">
											<input class="btn btn-primary"  type="button" value="Save"  onclick="newProductEntry()"/>
											<button class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								
								</div>
								
								
								
								<div id="jscontrols" class="tab-pane">
									
									    <fieldset>
											
											<div class="widget widget-nopad">
									            
									            <div class="widget-header"> <i class="icon-search"></i>
									              <h3> Search Products via</h3>
									            </div>
									            
									            <!-- /widget-header -->
									           
									            <div class="widget-content">
									              
									                  <div class="control-group">											
													
													     <ul>
															     <li class="manageLi">
							                                            <div class="controls" style="margin-left:10px;">
							                                            
								                                            <label class="radio inline">
								                                              <input type="radio" name="radiobtns" onclick="showsearchDiv('Product Id')"> Product Id
								                                            </label>
								                                            
								                                            <label class="radio inline">
								                                              <input type="radio" name="radiobtns" onclick="showsearchDiv('Product Name')"> Product Name
								                                            </label>
								                                            
								                                            <label class="radio inline">
								                                              <input type="radio" name="radiobtns" onclick="showsearchDiv('Product Category')"> Product Category
								                                            </label>
							                                            
							                                           </div>	<!-- /controls -->	
					                                          		   
				                                          		   </li>
				                                          		   
					                                          		<li class="manageLi">   
					                                          		    
					                                          		 
																	   
																	   <div class="control-group" >											
																			
																			
																			<div class="controls" id="categoryDiv" style="display:none;margin-top:20px;">
																				
																				<select id="searchproductTypeId" name="product_category" onchange="searchProducts('Product Category')">
																				        <option value="Select Admin Type">Select Product Packet</option>
																						<option value="Agriculture">Agriculture</option>
																						<option value="Herbal">Herbal</option>
																						<option value="HomeCare">HomeCare</option>
																				</select>
																				
																				<div id="errproductType" class="errorStyle"></div>
																															
																			</div> <!-- /controls -->	
																			
																			
																			
																			<div class="control-group" id="idDiv" style="display:none;margin-top:20px;">											
																				
																				<div class="controls">
																					<input type="text"  id="searchproductId" name="product_price" placeholder="Enter the product id to search" class="span4" style="margin-bottom: 2px ! important;">
																					<input type="button"  class="btn btn-primary" value="Search"  onclick="searchProducts('Product Id')">
																					<div id="errproductPrice" class="errorStyle"></div>
																				</div> 		
																					
																			</div> 
																			
																			
																			<div class="control-group" id="nameDiv" style="display:none;margin-top:20px;">											
																				
																				<div class="controls">
																					<input type="text"  id="searchproductNameId" name="product_price"  placeholder="Enter the product name to search" class="span4" style="margin-bottom: 2px ! important;">
																					<input type="button"  class="btn btn-primary" value="Search" onclick="searchProducts('Product Name')">
																					<div id="errproductPrice" class="errorStyle"></div>
																				</div> 		
																					
																			</div> 
																			
																			
													
																	   </div>
													   
													   
													   
													   
													               </li>
										                   </ul>
										                   
										              </div>
										   
									            </div>
									            
									            <!-- /widget-content --> 
									          </div>
									          
          
										</fieldset>
										
										
										
									   <!-- In this field set all the editable product details will display -->
										
										<fieldset>
											
											<div id="editproductDetailsDiv"></div>
											
										</fieldset>
										
									
									
										<!-- In this field set all the product details will display -->
										
										<fieldset>
											
											<div id="productDetailsDiv"></div>
											
										</fieldset>
										
									
								</div>
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div>
	
	     
	      
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    

 


<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

      
<div> <%@include file="supAdminFooter.jsp"%> </div>
    

<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
<script src="resources/Admin_Resources/js/excanvas.min.js"></script>
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script>
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script src="resources/Admin_Resources/js/base.js"></script>

<script src="resources/Admin_Resources/js/validateForm.js"></script>


  </body>

</html>
