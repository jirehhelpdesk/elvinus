<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Refer Friend</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
<link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
<link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    
            
</head>
<body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Company Logo </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        
                        
                        <ul class="nav pull-right">
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Abinash Raula
                                <b class="caret1"></b></a>
                                
                            </li>
                            <li><a href="#">Vendor Id : VEN123455 </a></li>
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>                                    
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                          
							<ul class="widget widget-menu unstyled">
                                <li class="active"><a href="userHomePage.html"><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="userReferFriends.html" class="sideMenuActive"><i class="icon-group"></i>   Refer Friend </a>                              
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="message.html"><i class="menu-icon icon-inbox"></i>  Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="task.html"><i class=" icon-random"></i>      Track Order  </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
                                    <ul id="togglePages" class="collapse unstyled">
                                        <li><a href="other-login.html"><i class="icon-inbox"></i>Profile Settings </a></li>
                                        <li><a href="other-user-profile.html"><i class="icon-inbox"></i>Password Settings </a></li>                                        
                                    </ul>
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                            
							     <li><a href="task.html"><i class="icon-money"></i>Bonus Amount  </a></li>
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Bonus Amount </a>
                                    <ul id="togglePages" class="collapse unstyled">
                                        <li><a href="other-login.html"><i class="icon-inbox"></i>Earned Reference Amount</a></li>
                                        <li><a href="other-user-profile.html"><i class="icon-inbox"></i>Policy Amount</a></li>                                        
                                    </ul>
                                </li>						
                            </ul>
							
                        </div>
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    <div class="span9">
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        My Profile</h3>
                                </div>
                                
								<div class="module-body table">
                                    
									<div style="float:right;">
										<img src="D:\UI TEMPLATES\UI Files\Super Admin Templates - 1\Edited Code\user_photoupload.png"></img>
											<p class="text-muted">
												New Users</p>
												   
										
									</div>
									
									<div class="module-body">

									<form class="form-horizontal row-fluid">
									
										<div class="control-group">
											<label for="basicinput" class="control-label">Name:</label>
											<div class="controls">												
												<span class="help-inline">Abinash Raula</span>
											</div>
										</div>

										<div class="control-group">
											<label for="basicinput" class="control-label">Email Id:</label>
											<div class="controls">												
												<span class="help-inline">abinash.raula@jirehsol.com</span>
											</div>
										</div>

										<div class="control-group">
											<label for="basicinput" class="control-label">Mobile no:</label>
											<div class="controls">												
												<span class="help-inline">8095035140</span>
											</div>
										</div>
										
										<div class="control-group">
											<label for="basicinput" class="control-label">Shipping Address:</label>
											<div class="controls">												
												<span class="help-inline">Jireh Software Solutions ,
																			#956,3rd Floor,
																			N.S.Plaza,16th Main,
																			BTM 2nd Stage
																			Bangalore - 560076.</span>
											</div>
										</div>
										
										<div class="control-group">
											<label for="basicinput" class="control-label">Email Id:</label>
											<div class="controls">												
												<span class="help-inline">abinash.raula@jirehsol.com</span>
											</div>
										</div>
										
										
									</form>
									
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2015 Jireh Software Solutions Pvt Ltd</b>  All rights reserved.
            </div>
        </div>
        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
    </body>
    
</html>