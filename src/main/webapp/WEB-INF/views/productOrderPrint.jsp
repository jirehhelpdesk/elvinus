<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Order Ticket</title>
   <!--  <link rel="stylesheet" href="resources/Admin_Resources/printResources/style.css" media="all" /> -->
  </head>
  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

  <body style="color: #555555 ! important;font-family: SourceSansPro ! important;font-size: 14px ! important; background: #ffffff none repeat scroll 0 0 ! important;
    height: auto ! important;
    margin: 0 auto ! important;
    position: relative ! important;
    width: 21cm ! important;">
   
    <header class="clearfix" style="clear: both;content:'';display: table; border-bottom: 1px solid #aaaaaa;margin-bottom: 20px;padding: 10px 0;width:100%;" >  <!--  -->
       
       <div>
       
		      <div  style="float: left;margin-top:8px;">
		        <img style="height: 70px;" src="resources/Admin_Resources/printResources/logo.png">
		      </div>
		      
		      <div style="float:right;text-align: right;">
		        <h2  style="font-size: 1.4em;font-weight: normal;margin: 0;">Company Name</h2>
		        <div>455 Foggy Heights, AZ 85004, US</div>
		        <div>(602) 519-0450</div>
		        <div><a href="mailto:company@example.com">company@example.com</a></div>
		      </div>
      
      </div>
      
    </header>
    
    <c:forEach items="${orderDetails}" var="det">	
    
    <main>
    
      <div id="details" class="clearfix" style="clear: both;content:'';display: table;margin-bottom: 50px;width: 100%;"> 
       
        <div id="client" style=" border-left: 6px solid #0087c3;float: left;padding-left: 6px;">
          <div class="to" style="color: #777777;">ORDER TO: ${det.order_unique_id}</div>
          <h2 class="name" style="">${det.order_for_name}</h2>
          
          <h4 class="name" style=""><u>Shipping Address</u></h4>
          <div style="">${det.order_shipping_address}</div>
          <div style=""><a href="${det.order_for_email_id}">${det.order_for_email_id}</a></div>
          <div style="">+91 &nbsp; ${det.order_for_mobile_no}</div>
        </div>
        
        <div style="float: right;text-align: right;">
          <h3>&nbsp;</h3>
          <div style="color: #777777;font-size: 1.1em;">Date of Order: ${det.order_date}</div>
          <!-- <div style="color: #777777;font-size: 1.1em;">Due Date: 30/06/2014</div> -->
        </div>
        
      </div>
     
     
     
      <table border="1" cellspacing="0" cellpadding="0" style="margin-bottom: 20px;border-collapse: collapse;border-spacing: 0;width: 100%;">
      
        <thead>
          <tr style="display: table-row;vertical-align: inherit;">
            <th style="background: #ccc none repeat scroll 0 0;color: #ffffff;font-size: 1.6em;font-weight: normal;white-space: nowrap;border-bottom: 1px solid #333;padding: 20px;">#</th>
            <th style="text-align: left;font-weight: normal;white-space: nowrap; background: #eeeeee none repeat scroll 0 0;border-bottom: 1px solid #333;padding: 20px;">Description</th>
            <th style="background:#dddddd none repeat scroll 0 0;font-weight: normal;white-space: nowrap;border-bottom: 1px solid #333;padding: 20px;text-align: center;">Kit Charge</th>
            <th style="text-align: left;font-weight: normal;white-space: nowrap; background: #eeeeee none repeat scroll 0 0;border-bottom: 1px solid #333;padding: 20px;">VAT(<%=request.getAttribute("vat")%>%)</th>
            <th style="background: #ccc none repeat scroll 0 0;color: #ffffff;font-weight: normal;white-space: nowrap; border-bottom: 1px solid #333;padding: 20px;text-align: center;">Sub Total(INR)</th>
          </tr>
        </thead>
       
       
        <tbody>
        
          <tr style="display: table-row;vertical-align: inherit;">
            <td style="border: medium none;background: #ccc none repeat scroll 0 0;color: #ffffff;font-size: 1.6em;text-align: right;padding: 20px;">01</td>
            <td style="text-align: left;background: #eeeeee none repeat scroll 0 0;padding: 20px;">As per your selected products the kit was prepared for you.</td>
            <td style="font-size: 1.2em;background: #dddddd none repeat scroll 0 0;text-align: right;padding: 20px;"><%=request.getAttribute("kitCharge")%></td>
            <td style="font-size: 1.2em;text-align: right;background: #eeeeee none repeat scroll 0 0;padding: 20px;"><%=request.getAttribute("vatPersentageVal")%></td>
            <td style="font-size: 1.2em;background: #ccc none repeat scroll 0 0;color: #ffffff;text-align: right;padding: 20px;"><%=request.getAttribute("subtotal")%></td>
          </tr>
                  
        </tbody>
        
        
        <tfoot>
        
          <tr>
            <td colspan="2" ></td>
            <td colspan="2" style="background: #ffffff none repeat scroll 0 0;border-bottom: medium none;font-size: 1.2em;padding: 10px 20px;white-space: nowrap;text-align: right;border-top: 1px solid #aaaaaa;">Registration Charge</td>
            <td style=" background: #ffffff none repeat scroll 0 0;border-bottom: medium none;font-size: 1.2em;padding: 10px 20px;white-space: nowrap;text-align: right;border-top: 1px solid #aaaaaa;"><%=request.getAttribute("regCharge")%></td>
          </tr>
          
        <!-- 
        
        <tr>
            <td colspan="2"></td>
            <td colspan="2" style=" background: #ffffff none repeat scroll 0 0;border-bottom: medium none;font-size: 1.2em;padding: 10px 20px;white-space: nowrap;text-align: right;border-top: 1px solid #aaaaaa;">TAX 25%</td>
            <td style=" background: #ffffff none repeat scroll 0 0;border-bottom: medium none;font-size: 1.2em;padding: 10px 20px;white-space: nowrap;text-align: right;border-top: 1px solid #aaaaaa;">1,300.00</td>
        </tr> 
        
        -->
          
          <tr>
            <td colspan="2"></td>
            <td colspan="2" style=" background: #ffffff none repeat scroll 0 0;border-bottom: medium none;font-size: 1.2em;padding: 10px 20px;white-space: nowrap;text-align: right;border-top: 1px solid #aaaaaa;">GRAND TOTAL</td>
            <td style=" background: #ffffff none repeat scroll 0 0;border-bottom: medium none;font-size: 1.2em;padding: 10px 20px;white-space: nowrap;text-align: right;border-top: 1px solid #aaaaaa;"><%=request.getAttribute("total")%></td>
          </tr>
          
        </tfoot>
        
        
      </table>
      
      
     </c:forEach>
      
      <div id="thanks" style="">Thank you!</div>
      <div id="notices" style="">
        <!-- <div>NOTICE:</div> -->
        <div class="notice" style="">For choosing and shopping products from us.Visit Again !</div>
      </div>
            
      
    </main>
    
    <footer>
      This order bill was created on a computer and is valid without the signature and seal.
    </footer>
    
  </body>
</html>