<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product List</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>

<%String productType = ""; 
String productName = "";
String productDescription = "";
String productFile = "";
String productPrice = "";

String product_uni_id = "";
int product_id = 0;

String existValue = (String)request.getAttribute("existValue");
String existValueArray[] = existValue.split(",");

String proComId = "";
%>


	<c:if test="${!empty productDetails}">	
		
		<c:forEach items="${productDetails}" var="det">	
		
				 <c:set var="product_category"  value="${det.product_category}"/>
				 <%productType = (String)pageContext.getAttribute("product_category");%>
				 
				 <c:set var="product_name"  value="${det.product_name}"/>
				 <%productName = (String)pageContext.getAttribute("product_name");%>
				 
				  <c:set var="product_price"  value="${det.product_price}"/>
				 <%productPrice = (String)pageContext.getAttribute("product_price");%>
				 
				 <c:set var="product_description"  value="${det.product_description}"/>
				 <%productDescription = (String)pageContext.getAttribute("product_description");%>
				 
				 <c:set var="product_image_file_name"  value="${det.product_image_file_name}"/>
				 <%productFile = (String)pageContext.getAttribute("product_image_file_name");%>
				
				<c:set var="product_unique_id"  value="${det.product_unique_id}"/>
				 <%product_uni_id = (String)pageContext.getAttribute("product_unique_id");%>
				
				<c:set var="product_id"  value="${det.product_id}"/>
				 <%product_id = (Integer)pageContext.getAttribute("product_id");%>
				 
				 <%proComId = product_id+""; %>
	                
	             <%String productFlag = "true"; %>
		
				<div class="featurecol">
					
					<div class="featurecoltop">
					 
					  <div class="featurecolbottom">
			          <h1></h1>
							 
							<img style="border-radius:5px;width:160px;height:120px;cursor:pointer;" title="View more about this product" src="${pageContext.request.contextPath}<%="/previewProductsForuser.html?fileName="+productFile+"&productType="+productType+""%>"  onclick="viewMore('${det.product_id}','${det.product_name}')"></img>
												
						    <span class="subheading" style="cursor:pointer;" id="prodoctName${det.product_id}" title="View more about this product" onclick="viewMore('${det.product_id}','${det.product_name}')">${det.product_name}</span>
						    
							<p><%=productDescription.substring(0, 50)%> ....</p>
							
							<%for(int i=0;i<existValueArray.length;i++){ %>
							      
							      <%if(proComId.equals(existValueArray[i])) {%>
							       
							        <%productFlag="false"; %>
							        <div class="readmoreimg"><label style="color:#00b6f5;"><input type="checkbox" checked value="${det.product_id}" id="productId${det.product_id}" name="accept" onclick="captureSelectedProduct('productId${det.product_id}')">Select Product</label></div>
								  
								  <%}%>								  
							
							<%} %>
							
							<%if(productFlag.equals("true")){ %>
							      
							      <div class="readmoreimg"><label style="color:#00b6f5;"><input type="checkbox" value="${det.product_id}" id="productId${det.product_id}" name="accept" onclick="captureSelectedProduct('productId${det.product_id}')">Select Product</label></div>
								  
						    <%}%>
						    	
					  </div>
					</div>
				</div>
		
		</c:forEach>
	
	 </c:if>
     
     
     <c:if test="${empty productDetails}">
     	       
         <%System.out.println("zvxcv"); %>
         
     </c:if>
     
     
</body>
</html>