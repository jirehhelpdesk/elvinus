<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Refer Bonus Policy</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

 <c:if test="${!empty referalBonusDetails}">	
		
		<c:forEach items="${referalBonusDetails}" var="det">	
		
		
		
			 <c:set var="bonus_check_status"  value="${det.bonus_check_status}"/>
			 <%String bonus_check_status = (String)pageContext.getAttribute("bonus_check_status");%>
 
 
			<div style="margin-top:10px;" class="headDiv"><h4 class="textStyle">Refer Bonus of initial ${det.user_plan_no_of_days} days </h4> </div>
			
			<div style="min-height:auto ! important;" class="referBonusBill">                   
                   								                          
	               <div style="min-height: 278px ! important;" class="pricelist">
	                    
	                   <form id="orderDetailForm" name="orderDetailForm" method="post">
	                   				
			                        <table width="800" align="center" style="padding-bottom: 15px;">
			                        <tbody>
			                          
			                         <tr class="rowStyle">
			                          <td class="bonusRightHeading"><b>No of days </b></td>     
			                         <td>:</td>       
			                         <td><i>${det.user_plan_no_of_days}</i></td><td>
			                         
			                        </td></tr>
			                         <tr class="rowStyle">
			                         <td class="bonusRightHeading"><b>No of friends </b></td>     
			                         <td>:</td>       
			                         <td><i>${det.no_of_refers_user}</i></td><td>	
			                         			                         
			                        </td></tr>
			                        
			                        <tr class="rowStyle">
			                         <td class="bonusRightHeading"><b>Bonus date</b></td>    
			                         <td>:</td>       
			                         <td><i>${det.update_date}</i></td><td>											                            
			                        </td></tr>
			                        
			                        <tr class="rowStyle">
			                         <td class="bonusRightHeading"><b>Complete the target</b></td>     
			                         <td>:</td>     
			                         <td><i>${det.bonus_type}</i></td><td>												                         			                         
			                        </td></tr>
			                        
			                        <tr class="rowStyle">
			                         <td class="bonusRightHeading"><b>Check delivery status</b></td>     
			                         <td>:</td>     
			                         <%if(!bonus_check_status.equals("Success")){ %>
			                         <td><i style="color:red;">${det.bonus_check_status}</i></td>
			                         <%}else{ %>
			                         <td><i style="color:yellowgreen;">${det.bonus_check_status}</i></td>
			                         <%} %>
			                         <td>												                         			                         
			                        </td></tr>
			                        
			                        </tbody></table>
	                        	
	                      </form>
	                      
	                         
	                     <div id="pricelist">	                                                        
                                 
                                 
                                 <c:set var="bonus_amount"  value="${det.bonus_amount}"/>
			                     
				                   <%
				                       int bonus_amount = (Integer)pageContext.getAttribute("bonus_amount");
				                   %>
  								   
  								   <%      
                                       float tdsVal = 0;
                                       String tdsString = ""+request.getAttribute("tds");
                                       tdsVal= Float.parseFloat(tdsString);                                       
                                       float subTotal = (tdsVal/100) * bonus_amount;                                       
                                       float total = bonus_amount - subTotal;
                                   %>
                   
			                        <table width="300" align="right">
				                        <tbody>
				                        
					                        <tr>   
					                          <td style="width:155px;color: #00b6f5;font-style: italic;">Bonus Amount</td><td>   
					                          </td><td>:</td>        
					                          <td><%=bonus_amount%>.00</td><td></td>
					                        </tr>
					                        
					                        <tr>
					                           <td style="width:155px;color: #00b6f5;font-style: italic;">TDS (- <%=tdsVal%>%)</td><td>
					                           </td><td>:</td>       
					                           <td><%=subTotal%></td><td></td>
					                        </tr>
				                        
					                        <tr>
					                          <td class="border"><strong>Total</strong></td><td></td><td>:</td>       
					                          <td class="border"><strong><%=total%></strong></td><td></td>
					                        </tr>
					                        
				                        </tbody>
			                        </table>
		                    </div>
	                    
	                    
	                    </div>
                        
                </div>
			
	
				<%if(!bonus_check_status.equals("Success")){ %>
								                								                
		            <div class="docs-example">
		                <ul>	                                                      
		                    <li><p class="text-info">With in 2 weeks we will contact to your respective contact details.</p></li>
		                    <li><p class="text-info">We will send a check with your name with your respective address.</p></li>		
		                    <li><p class="text-info">For any query please drop a mail to <a style="color:red;" href="#">support@biosys.com.</a></p></li>	                                                                                   
		                </ul>                                       
		            </div>
	
	            <%} %>
	
			</c:forEach>
			
	</c:if>		 
	
	
	<c:if test="${empty referalBonusDetails}">
	
			<div class="docs-example">
                <ul>
                    <li><p class="text-info" style="color:red;">As of now you didn't reached the bonus line as per your plan.</p></li>                                          
                    <li><p class="text-info" style="color:red;">Try to refer as much as friends to join the venture and get the benefits of it.</p></li>
                </ul>                                       
            </div>
	
	</c:if>
</body>
</html>