<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment Mode</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
<link href="resources/User_Resources/popUpDesign/popUp.css" rel="stylesheet">
    
    
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,800' rel='stylesheet' type='text/css'>
    <link href='css/styles.css' rel='stylesheet' type='text/css'>
    
    <link href='resources/indexResources/css/checkBox.css' rel='stylesheet' type='text/css'>

   
</head>


<body>


    <div id="about-modal" class="rv-vanilla-modal">
		   
		    <div class="rv-vanilla-modal-header group">
		        
		        <h2 class="rv-vanilla-modal-title">Choose Payment Mode</h2>
		    </div>
		    
		    <div class="rv-vanilla-modal-body" style="padding: 13px 20px 20px !important;">
		    	       
		    	       <!-- Rounded TWO -->
						

	    	       <ul>
							<li>
							    <p style="margin: 56px 215px;font-size: 16px;font-style: italic;">Payment Online</p>
								<div class="roundedTwo">							   
									<input type="checkbox" value="Online" id="roundedTwo"  onclick="capturePaymentMode('roundedTwo')"/>
									<label for="roundedTwo"></label>
								</div>
							</li>

							<li>
							    <p style="margin: 120px 217px;width: 250px;font-size: 16px;font-style: italic;">Payment Offline / Cash On delivery</p>
								<div class="roundedThree">
									<input type="checkbox" value="Offline" id="roundedThree"  onclick="capturePaymentMode('roundedThree')"/>
									<label for="roundedThree"></label>
								</div>
							</li>
			</ul>
		       
		       <input type="hidden" id="userRegId" name="userId" value="<%=session.getAttribute("regUserId")%>" />
		       <button class="buttonSelect" onclick="proceedToCheckOut()">Proceed</button>
		       
		    </div>
		    
		         
		     
    
   </div>
    
    <div class="backGroundDiv"></div>
     
</body>


</html>