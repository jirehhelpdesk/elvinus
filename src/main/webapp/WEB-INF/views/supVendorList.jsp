<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Vendor</title>
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

                       <div class="widget widget-table action-table">
											
												<div class="widget-header">
													<i class="icon-th-list"></i>
													<h3 id="resultHeading">Search result of Vendor</h3>
												</div>
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
															    <th>Serial no</th>
																<th>Vendor Id</th>
																<th>Full Name</th>
																<th>Email id</th>
																<th>Mobile no</th>
																<th>Registered Date</th>
																
																<th class="td-actions">See more</th>
															</tr>
														</thead>
														
														
														<tbody>
															
														<c:if test="${!empty vendorDetails}">	
															
															<%int i=1; %>
															
															
															<c:forEach items="${vendorDetails}" var="det">	
															
															
															<!-- Restrict for 10 datas -->
															
																	<tr>
																	        <td><%=i++%></td>
																			<td>${det.vendor_id}</td>
																			<td>${det.user_name}</td>
																			<td>${det.user_emailid}</td>
																			<td>${det.user_mobile_no}</td>
																			<td>${det.user_cr_date}</td>
																			
																			<td class="td-actions">
																			
																					<a class="btn btn-small btn-success" href="javascript:;" title="View" onclick="viewVendorDetails('${det.user_id}','${det.user_name}')">
																					
																					     <i class="btn-icon-only icon-eye-open"> </i>
																							
																					</a> 
																					
																					<a class="btn btn-small btn-success" href="javascript:;" title="print" onClick="window.print();">
																					
																					     <i class="btn-icon-only icon-print"> </i>
																							
																					</a>  
																					
																			</td>																																				
																	</tr>
															
															</c:forEach>
															
														</c:if>	
														
														<c:if test="${empty vendorDetails}">	
														
														            <tr>
														            <td></td>
																	        <td style="color:red;">As per the search details No data found !</td>
																	
																	</tr>
														
														</c:if>
																														
														</tbody>
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>