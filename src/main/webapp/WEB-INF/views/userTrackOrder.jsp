<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Track Order</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
     
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html" ><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="buyMorePackets.html"><i class="icon-gift"></i> Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html" class="sideMenuActive"><i class=" icon-random"></i> Track Order </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        Track Order
                                    </h3>
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">

										<form class="form-horizontal row-fluid">

													
											  <c:if test="${!empty orderDetails}">	
                                           
	                                               <c:forEach items="${orderDetails}" var="det">
	                                               		 		                                                     
			                                          
			                                          <div class="headDiv"><h4 class="textStyle">Order Id : ${det.order_unique_id}</h4> </div>
                                    				  <br>
                                          
                                          					<c:set var="order_status"  value="${det.order_status}"/>
						                                    <%String orderStatus = (String)pageContext.getAttribute("order_status");%>
						                                    
				                                               
				                                                
				                                                   <%if(orderStatus.equals("Success")) {%>
				                                                   
				                                                          <div class="trackOrderDiv">
				                                                         
							                                                   <div class="trackOrderleftDiv">
							                                                      
								                                                   <ul>
								                                                   	   	 
									                                                     <li>${det.order_for_name}</li>
									                                                     <li>${det.order_for_email_id}</li>
									                                                     <li>${det.order_for_mobile_no}</li>		                                                    
									                                                     <li style="color:#33CC33">Order Date : ${det.order_date}</li>
									                                                     <li style="color:#33CC33">Delivery Date : ${det.order_expected_date}</li>
									                                                                     
										                                           </ul>
										                                           
								                                               </div>
								                                                
								                                                 
								                                             <div class="trackOrderSuccessrightDiv"><p class="SuccesstextAlign">Successfully Delivered</p></div>
								                                           
								                                           </div>
								                                           
								                                   <%}else{ %>
								                                            
								                                             <div class="trackOrderDiv">
								                                              
										                                             <div class="trackOrderPendingleftDiv">
						 
										                                                   <ul>
										                                                    
										                                                     <li>${det.order_for_name}</li>
										                                                     <li>${det.order_for_email_id}</li>
										                                                     <li>${det.order_for_mobile_no}</li>		                                                    
										                                                     <li style="color:#FF3333">Order Date : ${det.order_date}</li>
										                                                    
												                                           </ul>
											                                           
										                                             </div>
										                                                
										                                             <div class="trackOrderPendingright1Div" onclick="showOrderStatus('${det.order_id}')"><p class="PendingtextAlign">Check Status</p></div>    
										                                             <div class="trackOrderPendingright2Div"><p class="PendingtextAlign">Pending</p></div>
										                                             
								                                             </div>
								                                             
								                                             <div class="trackOrderStatusDiv" id="statusId${det.order_id}" style="display:none;">
								                                                     
								                                                     <div class="trackStatusleftDiv">
										                                                     <ul>
										                                                     	   <li style="color:#FF5050;font-style: italic;">Expected Date : <p style="color:#0066FF;font-style: italic;">${det.order_expected_date}</p></li>
										                                                     	   <li style="color:#FF5050;font-style: italic;">Status Details : <p style="color:#0066FF;font-style: italic;">${det.order_status_details}</p></li>
									                                                         </ul>									                                                         
							                                                         </div>
							                                                         
							                                                         <div class="trackStatusrightDiv" onclick="showOrderStatus('${det.order_id}')"><p class="PendingtextAlign">Close</p></div>
							                                                        
								                                             </div>
								                                             								                                             
								                                    <%} %>       
								                                  
	                                               </c:forEach>
                                               
                                              </c:if>
												
                                        
												
									   </form>
									
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
         <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="profileSetting.html";
		}
		
		</script>

    </body>

</html>