<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Vendor</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

<%

String productType = ""; 
String userName = "";
String productDescription = "";
String productFile = "";
String productPrice = "";

String product_uni_id = "";
int product_id = 0;

%>


<div class="widget-header">
                               
									<i class="icon-list-alt"></i>
									<h3 id="resultHeading"></h3>
									
							   </div>
												
												
                               <div id="formcontrols" class="tab-pane active" style="margin-top:20px;">
								
								<form class="form-horizontal" id="updateproductdtailsForm" name="updateproductdtailsForm" enctype="multipart/form-data">
									
											
												
								<fieldset>
								
												
									<c:forEach items="${vendorBasicDetails}" var="det">	
											
											<div class="control-group">		
											    <div style="height: 307px; width: 343px;">  <!-- float: right; margin-top: -331px; margin-right: -50px;  -->
															
															<c:set var="profilePic"  value="${det.user_profile_image_name}"/>
				 											
				 											<%String profilePic = (String)pageContext.getAttribute("profilePic");%>
				 											
												            <%if(profilePic!=null){ %>
                                							
							                                     <img style="border-radius: 10px; height: 300px; width: 250px;" src="${pageContext.request.contextPath}<%="/previewProfilePictureForAdmin.html?fileName="+profilePic+"&userId="+request.getAttribute("userId")%>" >
							                                
							                                <%}else{ %>
							                                
							                                     <img style="border-radius: 10px; height: 300px; width: 250px;" src="resources/User_Resources/images/passimg.png">
							                                
							                                <%} %>
							                             
												</div>
											</div>	
										
											   <div style="margin-left: 245px; margin-top: -300px; margin-bottom: 150px;">	
											   	
														<div class="control-group">											
															<label for="username" class="control-label">Vendor Id : </label>
															<div class="controls">
																<c:out value="${det.vendor_id}"></c:out>											
															</div> <!-- /controls -->				
														</div>
													
																							
														<div class="control-group">											
															<label for="username" class="control-label">Email Id : </label>
															<div class="controls">
																<c:out value="${det.user_emailid}"></c:out>											
															</div> <!-- /controls -->				
														</div>
													
																
														<div class="control-group">											
															<label for="email" class="control-label">Mobile no : </label>
															<div class="controls">
																<c:out value="${det.user_mobile_no}"></c:out>		
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
														
														<div class="control-group">											
															<label for="email" class="control-label">Joined on : </label>
															<div class="controls">
																<c:out value="${det.user_cr_date}"></c:out>	
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
														
												</div>
										
										
										
										
										
										<div class="control-group">											
											<label for="email" class="control-label">Shipping Address : </label>
											<div class="controls" style="width: 650px;">
												<c:out value="${det.user_shipping_address}"></c:out>	
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
								</c:forEach>	
									
								<c:forEach items="${vendorOtherDetails}" var="det">	
										
										<div class="control-group">											
											<label for="password1" class="control-label">Permanent Address : </label>
											<div class="controls" style="width: 650px;">
												<c:out value="${det.user_permanent_address}"></c:out>	
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
										
										
										<div class="control-group">											
											<label for="username" class="control-label">Alternate emailid : </label>
											<div class="controls">
												<c:out value="${det.user_alternate_emailid}"></c:out>											
											</div> <!-- /controls -->				
										</div>
									
									    <div class="control-group">											
											<label for="password2" class="control-label">Date of Birth: </label>
											<div class="controls">
												<c:out value="${det.user_dob}"></c:out>	
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
									     <div class="control-group">											
											<label for="password2" class="control-label">Proof of Identity Type: </label>
											<div class="controls">
												<c:out value="${det.user_id_type}"></c:out>	
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										
										
										<div class="control-group">											
											<label for="password2" class="control-label">Proof of Identity : </label>
											<div class="controls">
												<c:out value="${det.user_proof_of_id}"></c:out>	
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
									</c:forEach>		
										 <br>
										
										
												
									</fieldset>
									
								</form>
								
								</div>
								
								
								
</body>
</html>