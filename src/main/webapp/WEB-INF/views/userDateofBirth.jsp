<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User DOB</title>

<link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>

<style type="text/css">

.custom-date-style {
	background-color: red !important;
}

</style>

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
	                              
	       <input type="text" readonly placeholder="Date of Birth" name="dateOfBirth" id="datetimepicker"/><br><br>
    
</body>


<script src="resources/datePicker/jquery.js"></script>
<script src="resources/datePicker/jquery.datetimepicker.js"></script>
<script>

$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['08/01/1986','09/01/1986','10/01/1989'],
startDate:	'01/01/2015'
});
$('#datetimepicker').datetimepicker({value:'',step:10});

$('.some_class').datetimepicker();

$('#default_datetimepicker').datetimepicker({
	formatDate:'d/m/Y',
	defaultDate:'+03/01/1970', 
	timepickerScrollbar:false
});

</script>


</html>
