<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Account Admin Header</title>
</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="accountAdmin.html">Account Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Account Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutActAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    
    <div class="container">
     
           <ul class="mainnav">
			
				<li id="menu1"><a href="accountAdmin.html"><i class="icon-money"></i><span>Transaction</span> </a> </li>
				<li id="menu2"><a href="amountReceived.html"><i class="icon-caret-down"></i><i class="icon-money"></i><span>Amount Received</span> </a> </li>
				<li id="menu3"><a href="taxAmountToGovt.html"><i class="icon-caret-up"></i><i class="icon-briefcase"></i><span>VAT Amount to Govt</span> </a> </li>
				<li id="menu4"><a href="payAmtToCustomer.html"><i class="icon-caret-up"></i><i class="icon-group"></i><span>Pay Amount to Customer</span> </a> </li>
				<li id="menu5"><a href="TDSamountReceived.html"><i class="icon-caret-down"></i><i class="icon-money"></i><span>TDS Amount to Business</span> </a> </li>
				<li id="menu6"><a href="accountReports.html"><i class="icon-list-alt "></i><span>Reports</span> </a> </li>
				
		   </ul>
		   
    </div>
    
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->


</body>
</html>