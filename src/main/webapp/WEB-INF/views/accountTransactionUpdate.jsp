<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Transaction</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>



          <div class="widget-header">
                          
				<i class="icon-th-list"></i>
				<h3 id="resultHeading1"></h3>
				
		   </div>
			
			<%String ordercategory = "Pending,Success";
  
             String orderCatArray[] = ordercategory.split(",");%>  
  				
							      <c:forEach items="${tranDetails}" var="det">
			         			
				                               <div id="formcontrols" class="tab-pane active" style="margin-top:20px;">
												
												<form class="form-horizontal" id="updateTranForm" name="updateTranForm" >
													
													<fieldset>
																								
														<div class="control-group">											
															<label for="username" class="control-label">Transaction Status</label>
															<div class="controls">
														    
														    	 <c:set var="order_status"  value="${det.transaction_status}"/>
	 															 <%String order_status = (String)pageContext.getAttribute("order_status");%>
	 
																<select style="width:370px;" id="updproductTypeId" name="ord_status">
																        <option value="Select Order Status">Select Order Status</option>
																        
																        <%for(int i=0;i<orderCatArray.length;i++){%>
												        				
																	          <%if(order_status.equals(orderCatArray[i])){%>
																	                <option value="<%=orderCatArray[i]%>" selected="selected"><%=orderCatArray[i]%></option>
																	          <%}else{%>
																	                <option value="<%=orderCatArray[i]%>"><%=orderCatArray[i]%></option>
																	          <%}%>
																          
																        <%}%>
																        
																</select>
																
																<div id="errproductType" class="errorStyle"></div>
																											
															</div> <!-- /controls -->	
																		
														</div>
																										    
													     <div class="control-group">											
															<label for="email" class="control-label">Amount</label>
															<div class="controls">
																${det.transaction_amount}
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
																		
														 <div class="control-group">											
															<label for="email" class="control-label">Transaction Mode</label>
															<div class="controls">
																${det.transaction_mode}
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
														
														<div class="control-group">											
															<label for="email" class="control-label">Transaction Date</label>
															<div class="controls">
																${det.transaction_date}
															</div> <!-- /controls -->				
														</div> <!-- /control-group -->
																											   								
														<br>
																	       	
														<div class="form-actions">
														    <input type="hidden" value="${det.transaction_unique_id}" name="updorder_id" >
															<input class="btn btn-primary"  type="button" value="Update"  onclick="saveUpdatedTransaction()"/>
															<input class="btn btn-primary"  type="button" value="Back"  onclick="bactoTranDetails()" />															
														</div> <!-- /form-actions -->
														
														
													</fieldset>
													
												</form>
												
												</div>
								
								</c:forEach>
						
								
</body>
</html>