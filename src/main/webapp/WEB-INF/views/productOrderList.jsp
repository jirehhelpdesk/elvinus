<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Orders</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

                       <div class="widget widget-table action-table">
											
												<div class="widget-header">
													<i class="icon-th-list"></i>
													<h3 id="resultHeading">Search result of Order details</h3>
												</div>
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
															    <th>Serial no</th>
																<th>Order Id</th>
																<th>Customer Name</th>
																<th>Email id</th>
																<th>Mobile no</th>
																<th>Order Status</th>
																<th>Order Date</th>
																
																<th class="td-actions" style="width:100px;">Update</th>
															</tr>
														</thead>
														
														
														<tbody>
															
														<c:if test="${!empty orderDetails}">	
															
															<%int i=1; %>
															
															
															<c:forEach items="${orderDetails}" var="det">	
															
															<c:set var="orderStatus"  value="${det.order_status}"/>
						 									<%String orderStatus = (String)pageContext.getAttribute("orderStatus");%>
						 																			
						 																			
															<!-- Restrict for 10 datas -->
															
																	<tr>
																	    
																	        <td><%=i++%></td>
																			<td>${det.order_unique_id}</td>
																			<td>${det.order_for_name}</td>
																			<td>${det.order_for_email_id}</td>
																			<td>${det.order_for_mobile_no}</td>
																			
																			
																			<%if(orderStatus.equals("Completed")) {%>
																						
																						
																								<td style="color:yellowgreen;">${det.order_status}</td>
																								<td style="color:yellowgreen;">${det.order_date}</td>
																								
																								<td class="td-actions" style="color:yellowgreen;">
																								
																										<a class="btn btn-small btn-success" href="javascript:;" title="Print Order" onClick="printOrderDetails('${det.order_unique_id}')">
																										
																										     <i class="btn-icon-only icon-print"> </i>
																												
																										</a> 
																										
																								</td>
																						
																						<%}else if(orderStatus.equals("Released")){ %>
																						
																									<td style="color:#FF4719;">${det.order_status}</td>
																									<td style="color:#FF4719;">${det.order_date}</td>
																									
																									<td class="td-actions">
																									
																											<a class="btn btn-small btn-success" href="javascript:;" title="Update Order" onclick="updateOrderDetails('${det.order_unique_id}')">
																											
																											     <i class="btn-icon-only icon-edit"> </i>
																													
																											</a> 
																											
																											<a class="btn btn-small btn-success" href="javascript:;" title="Print Order" onClick="printOrderDetails('${det.order_unique_id}')">
																											
																											     <i class="btn-icon-only icon-print"> </i>
																													
																											</a> 
																											
																									</td>
																									
																						
																						<%}else{ %>
																						
																									<td>${det.order_status}</td>
																									<td>${det.order_date}</td>
																									
																									<td class="td-actions">
																									
																											<a class="btn btn-small btn-success" href="javascript:;" title="Update Order" onclick="updateOrderDetails('${det.order_unique_id}')">
																											
																											     <i class="btn-icon-only icon-edit"> </i>
																													
																											</a> 
																											
																											<a class="btn btn-small btn-success" href="javascript:;" title="Print Order" onClick="printOrderDetails('${det.order_unique_id}')">
																											
																											     <i class="btn-icon-only icon-print"> </i>
																													
																											</a> 
																											
																									</td>
																									
																						<%} %>
																								
																			
																			
																																																						
																	</tr>
															
															</c:forEach>
															
														</c:if>	
														
														<c:if test="${empty orderDetails}">	
														
													            <tr>
													                    <td></td>
																        <td style="color:red;">As per the search details No data found !</td>
																</tr>
														
														</c:if>
																														
														</tbody>
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>