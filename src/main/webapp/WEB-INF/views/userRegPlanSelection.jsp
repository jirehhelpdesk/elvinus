<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Plan Selection</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">

<link href="resources/indexResources/css/demo.css" rel="stylesheet">
<link href="resources/indexResources/css/stepsForm.css" rel="stylesheet">
<script src="resources/indexResources/scripts/jquery-2.1.1.min.js"></script>

<link href="resources/indexResources/css/Planstyle.css" rel="stylesheet">

<link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">

<%String completeStatus = (String)session.getAttribute("completeStatus");%>

<%if(completeStatus!=null){ %>

<script>

function showCompleteStatus()
{
	$("#notifypopUpheading").html("Notification");
	$("#notifypopUpMessage").html("You didn't complete your registration. Please complete !");
	$("#NotifypopUpDiv").show();
}

</script>

<%session.removeAttribute("completeStatus");%>

<%}else{ %>

<script>

function showCompleteStatus()
{
	
}
	
</script>

<%} %>


</head>

<body onload="showCompleteStatus()">

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
        <a href="index.html" class="over"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
     
      <div class="topmenu"> | <a href="userLogin.html">Sign In</a>  
            
	     <%--  <a href="#" onclick="showCartProducts()"> 
	                  <div class="topblock2" >
							<img src="resources/User_Resources/images/shopping.gif" alt="" width="24" height="24" class="shopping" />
						 	<p>Shopping cart</p> <p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong> <span>items</span></p>
					  </div>					  	
		  </a>  --%>
		  
		  <a href="#" onclick="showCartProducts()"> 
                 <div class="topblock2" >
					<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
				 	<p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong></p>
			  </div>					  	
	      </a>
        
        
     </div>         
            
            
            <ul class="topmenu">
              <li><a href="index.html"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
 
	
    <div class="container">
    	
        
        <!--STEPS FORM START ------------ -->
        <div class="stepsForm">
           
                <div class="sf-steps">
                    <div class="sf-steps-content">
                    	<div>
                        	<span>1</span> Basic Information
                        </div>
                        <div>
                        	<span>2</span> Product Selection
                        </div>
                        <div>
                        	<span>3</span> Add Reference
                        </div>
						 <div class="sf-active" >
                        	<span>4</span> Plan Selection
                        </div>
						 <div>
                        	<span>5</span> Payment
                        </div>
						
                    </div>
                </div>
                
                
                 <div class="sf-steps-navigation sf-align-right" style="height:25px;">
                    
                </div>
                
                
                <div id="radiobutton"> 
               
		        </div>     
                               
                
      <form id="planForm" name="planForm" method="post">
        
	   <div class="wrapper">
        <!-- PRICING-TABLE CONTAINER -->
        <div class="pricing-table group">
           
         

		 <!-- PERSONAL -->
            <div class="block personal fl" id="personal">
                <h2 class="title">personal</h2>
                <!-- CONTENT -->
                <div class="content">
                    <div class="price">
                        <sup>PLAN A</sup>
                        <span></span>
                        <sub></sub>
                    </div>
                    
                    <a style="color: #ffffff !important;" href="#" onclick="showPlanDetails('Plan A')"><div class="hint">Know More</div></a>
                    
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>Double at 5 yrs scheme</li>
                    <li><span class="fontawesome-star"></span>Earn Rs.1667 per each refer</li>
                    <li><span class="fontawesome-dashboard"></span>No bonus amount</li>
                    <li><span class="fontawesome-cloud"></span>No Incentive</li>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer" onclick="capturePlanSelect('Plan A','personal')">
                    <p>Select Plan</p>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /PERSONAL -->
			
			
			
			
            <!-- PROFESSIONAL -->
            <div class="block professional fl" id="professional">
                <h2 class="title">Professional</h2>
                <!-- CONTENT -->
                <div class="content">
                    <div class="price">
                        <sup>Plan B</sup>
                        <span></span>
                        <sub></sub>
                    </div>
                    
                    <a style="color: #ffffff !important;" href="#" onclick="showPlanDetails('Plan B')"><div class="hint">Know More</div></a>
                    
                </div>
                <!-- /CONTENT -->
                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>NO double- only incentive</li>
                    <li><span class="fontawesome-star"></span>Earn Rs.3333 per each refer</li>
                    <li><span class="fontawesome-dashboard"></span>Bonus amount as per refer</li>
                    <li><span class="fontawesome-cloud"></span>Incentive per year</li>
                </ul>
                <!-- /FEATURES -->
                <!-- PT-FOOTER -->
                <div class="pt-footer" onclick="capturePlanSelect('Plan B','professional')">
                    <p>Select Plan</p>
                </div>
                <!-- /PT-FOOTER -->
            </div>
            <!-- /PROFESSIONAL -->
			
			
			
			
            <!-- BUSINESS -->
            <div class="block business fl" id="business">
                <h2 class="title">Business</h2>
                <!-- CONTENT -->
                <div class="content">
                    <div class="price">
                        <sup>Plan C</sup>
                        <span></span>
                        <sub></sub>
                    </div>
                    
                    <a style="color: #ffffff !important;" href="#" onclick="showPlanDetails('Plan C')"><div class="hint">Know More</div></a>
                    
                </div>
                <!-- /CONTENT -->

                <!-- FEATURES -->
                <ul class="features">
                    <li><span class="fontawesome-cog"></span>Double at 5 yrs scheme</li>
                    <li><span class="fontawesome-star"></span>Earn Rs.2222 per each refer</li>
                    <li><span class="fontawesome-dashboard"></span>Bonus amount as per refer</li>
                    <li><span class="fontawesome-cloud"></span>Incentive per year</li>
                </ul>
                <!-- /FEATURES -->

                <!-- PT-FOOTER -->
                <div class="pt-footer" onclick="capturePlanSelect('Plan C','business')">
                    <p>Select Plan</p>
                </div>
                <!-- /PT-FOOTER -->
                
            </div>
            <!-- /BUSINESS -->
            
            <div style="clear:both; margin:15px auto;">
				<button id="sf-next" type="button" class="sf-button" onclick="savePlanDetails()" style="padding: 15px 40px;
    margin: 35px 0 0 0;">Proceed >></button>
			</div>				
        </div>
        <!-- /PRICING-TABLE -->
    </div>
    
                  <input type="hidden" name="userId" value="<%=session.getAttribute("regUserId")%>" />
                  <input type="hidden" name="userRegId" value="" />
                  <input type="hidden" name="selectPlanName" id="planValueId" />
                  
                  
                  <input  type="hidden" id="agriProdId" name="agricultureProduct" value="<%=request.getAttribute("agriId")%>"  />
			      <input  type="hidden" id="herbalProdId" name="herbalProduct" value="<%=request.getAttribute("herbalId")%>" />
			      <input  type="hidden" id="hmCareProdId" name="homecareProduct" value="<%=request.getAttribute("hmCareId")%>" />
			       
						       
 </form>
             
        </div>
        <!--STEPS FORM END -------------- -->
       
    </div>


<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 



<script src="resources/indexResources/scripts/userRegistrationScript.js"></script>

<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<div id="infoPopUpDiv" style="display:none;"> <%@include file="userPlanDetailsPopUp.jsp" %> </div>


<div id="cartDetailsPopUp" style="display:none;"></div>

<div id="NotifypopUpDiv" style="display:none;"> <%@include file="aaaaNotifyPopUp.jsp" %> </div>


<script>

function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location="paymentMode.html";
}

function hideNotification()
{
	$("#NotifypopUpDiv").hide();
}
</script>

</body>

</html>