<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Accounts</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="actimeMenu('menu3')">


<div> <%@include file="accountAdminHeader.jsp" %> </div>



<div class="main">
  <div class="main-inner">
    <div class="container">
                  
                  
  				    <div class="widget-header">
	      				<i class="icon-credit-card"></i>
	      				<h3>Account Management</h3>
	  				</div>
					
					<div class="widget-content">
					
						<div class="tabbable">
							
							<ul class="nav nav-tabs">
							  <li class="active">
							    <a data-toggle="tab" href="#formcontrols">Tax Amount to Government(Monthly Record)</a>
							  </li>
							  
							 <!--  <li>
							    <a data-toggle="tab" href="#jscontrols">Tax Amount to Government(Weekly Record)</a>
							  </li>
							  
							  <li>
							    <a data-toggle="tab" href="#jscontrols2">Tax Amount to Government(Monthly Record)</a>
							  </li>
							   -->
							  
							  <!-- -->
							</ul>
						
						<br>
						
							<div class="tab-content">
						
						   <div class="tab-pane active" id="formcontrols">
								
								
								<form name="profileForm" id="profileForm" class="form-horizontal">
								
									<fieldset>
										   
											   <div class="widget widget-nopad">
										          
										            <div class="widget-header"> <i class="icon-list-alt"></i>
										              <h3> Total TAX(VAT) Amount </h3>
										            </div>
										            <!-- /widget-header -->
										            <div class="widget-content">
										              <div class="widget big-stats-container">
										                <div class="widget-content">
										                 
										                  <h6 class="bigstats">As of now this month total tax amount,</h6>
										                  
										                  <div id="big_stats" class="cf">
										                    <div class="stat">  </div>
										                    <!-- .stat -->
										                    
										                    <div class="stat">  </div>
										                    <!-- .stat -->
										                    
										                    
										                    <div class="stat">  Total <i></i> <!-- <i class="icon-bullhorn"></i> <img src="resources/Admin_Resources/img/rupeesicon.png" width="15" height="18" />  <span class="value">Rs</span> --> <span class="value"> <b style="color:red;"> <%=request.getAttribute("vatPersentageVal")%> </b> INR</span> </div>
										                    <!-- .stat --> 
										                  </div>
										                  
										                </div>
										                <!-- /widget-content --> 
										                
										              </div>
										            </div>
										            
										        </div>
									
									
										
										<div class="widget widget-table action-table">
											
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
															    <th>Serial no</th>
																<th>Tax Duration</th>																
																<th>Tax Date</th>
																<th>Tax Amount</th>
																<th>Report Status</th>
																<th>Download</th>												
															</tr>
														</thead>
														
														
												       <tbody>
													
															<!-- Restrict for 10 datas -->
															
															<%int i=1; %>
															<c:forEach items="${taxDetails}" var="taxDetails">	
																	  
																	<tr>																	   																	      
																       
																        <td><%=i++%></td>
																		<td>${taxDetails.tax_report_name}</td>
																		<td>${taxDetails.tax_report_date}</td>																			
																		<td style="color:red;">${taxDetails.tax_amount}</td>
																		<td style="color:green;">${taxDetails.tax_status}</td>																	  																																																			
																	    
																	    <c:set var="status"  value="${taxDetails.tax_status}"/>
				 														<%String status = (String)pageContext.getAttribute("status");%>
				 
																	    
																	    <td class="td-actions">
																	                 
																	           <a  href="downloadVatReport.html?reportName=${taxDetails.tax_report_name}"  title="Download the Report" class="btn btn-small btn-success"><i class="btn-icon-only icon-download-alt" id="iconId38"> </i></a>
																	    
																	    </td>
																	    
																	    
																	</tr>
																	
															</c:forEach>
															
																						
														</tbody>
														
													</table>
												</div>
												<!-- /widget-content -->
												
											</div>
											
									</fieldset>
									
								</form>
								
									
							</div>
							
						
					<!-- Remove Admin Division  -->
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="edit-profile2">
										<fieldset>
										
				                              <div id="adminDetailsDiv"></div>
											
										</fieldset>
									</form>
								</div>
								
					<!-- End of Remove Admin Division  -->
				
				
				
								
							</div>			  
						  
						</div>
						
					</div>
					
					
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<div> <%@include file="supAdminFooter.jsp"%> </div>



<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/subAdminValidateForm.js"></script>


</body>
</html>
