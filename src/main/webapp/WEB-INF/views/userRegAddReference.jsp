<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Reference</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	

<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">

<link href="resources/indexResources/css/demo.css" rel="stylesheet">
<link href="resources/indexResources/css/stepsForm.css" rel="stylesheet">
<script src="resources/indexResources/scripts/jquery-2.1.1.min.js"></script>

<link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">

<%String completeStatus = (String)session.getAttribute("completeStatus");%>

<%if(completeStatus!=null){ %>

<script>

function showCompleteStatus()
{
	$("#notifypopUpheading").html("Notification");
	$("#notifypopUpMessage").html("You didn't complete your registration. Please complete !");
	$("#NotifypopUpDiv").show();
}

</script>

<%session.removeAttribute("completeStatus");%>

<%}else{ %>

<script>

function showCompleteStatus()
{
	
}
	
</script>

<%} %>


</head>
<body onload="showCompleteStatus()">

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
        <a href="index.html" class="over"><img src="resources/indexResources/images/logo.png" width="287" height="55"></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
      
      <div class="topmenu"> | <a href="userLogin.html">Sign In</a>  
            
	      
		  
		  <a href="#" onclick="showCartProducts()"> 
                 <div class="topblock2" >
					<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
				 	<p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong></p>
			  </div>					  	
	     </a>
	   
        
     </div>     
            
            
            <ul class="topmenu">
              <li><a href="index.html"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
 
	
    <div class="container">
    	
        
        <!--STEPS FORM START ------------ -->
        <div class="stepsForm">
            
                <div class="sf-steps">
                    <div class="sf-steps-content">
                    	<div>
                        	<span>1</span> Basic Information
                        </div>
                        <div>
                        	<span>2</span> Product Selection
                        </div>
                        <div class="sf-active">
                        	<span>3</span> Add Reference
                        </div>
						<div>
                        	<span>4</span> Plan Selection
                        </div>
						<div>
                        	<span>5</span> Payment
                        </div>
						
                    </div>
                </div>     
                
                  
                <div class="sf-steps-navigation sf-align-right">
                    <button id="sf-next" type="button" class="sf-button" onclick="hidePopUp()">Skip & Proceed >></button>
                </div>
               
              <form id="referenceForm" name="referenceForm" method="post">
                         
                <div class="sf-steps-form sf-radius" style="width:600px; margin:0 auto;"> 
                    
                    <ul class="sf-content"> <!-- form step one --> 
                         <li style="padding: 3px 90px !important;">
                            <div class="sf_columns" style="width: 19% !important;">
                            <p>Full Name</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">
                               <input  type="text" id="referNameId" name="referName" placeholder="Enter full name" data-required="true" onkeyup="checkReferenceDetails()">
                               <div id="nameError" class="basicError">sdfsdf</div>
                            </div>
                         </li>
                         <li style="padding: 3px 90px !important;">
                              <div class="sf_columns" style="width: 19% !important;">
                            <p>Email Id</p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">
                               <input  type="text"id="referEmailId" name="referEmail"  placeholder="Enter email id" data-required="true" onchange="checkReferenceDetails()">
                               <div id="emailIdError" class="basicError"></div>
                            </div>
                         </li>
                         <li style="padding: 3px 90px !important;">
                           <div class="sf_columns" style="width: 19% !important;">
                            <p>Mobile No </p>
                              
                            </div>
                            <div class="sf_columns " style="width:60% !important;">
                               <input  type="text" id="referMobileNoId" name="referMobileNo" maxlength="10"  placeholder="Enter Mobile No" data-required="true" onchange="checkReferenceDetails()">
                               <div id="mobileNoError" class="basicError"></div>
                            </div>
                            </li>
                            
                          <li>  
                            <div class="sf_columns" style="width: 40% !important;">
                            
                            </div>
                            <div class="sf_columns " style="width:54% !important;text-align: right;">	
			                    <button id="sf-next" type="button" class="sf-button" style="background-color:#00b6f5;" onclick="saveReferenceDetails()">Refer</button>
			                </div>
			              </li>
			                
                        </ul>
					
                </div>
                
                
                <br>
               
            </form>
            
            
                              <input  type="hidden" id="agriProdId" name="agricultureProduct" value="<%=request.getAttribute("agriId")%>"  />
						      <input  type="hidden" id="herbalProdId" name="herbalProduct" value="<%=request.getAttribute("herbalId")%>" />
						      <input  type="hidden" id="hmCareProdId" name="homecareProduct" value="<%=request.getAttribute("hmCareId")%>" />
						       
						       
        </div>
        <!--STEPS FORM END -------------- -->
       
    </div>
    

<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 




 <script src="resources/indexResources/scripts/userRegistrationScript.js"></script>

<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<div id="cartDetailsPopUp" style="display:none;"></div>

<div id="NotifypopUpDiv" style="display:none;"> <%@include file="aaaaNotifyPopUp.jsp" %> </div>


<script>

function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location="planSelection.html";
}

function hideNotification()
{
	$("#NotifypopUpDiv").hide();
}

</script>


</body>
</html>