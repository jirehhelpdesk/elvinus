<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product view</title>

<link href="resources/User_Resources/popUpDesign/popUp.css" rel="stylesheet">
    
    
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,800' rel='stylesheet' type='text/css'>
    <link href='css/styles.css' rel='stylesheet' type='text/css'>
</head>

<body>


    <div id="about-modal" class="rv-vanilla-modal">
    <div class="rv-vanilla-modal-header group">
        <button class="rv-vanilla-modal-close"><span class="text">�</span></button>
        <h2 class="rv-vanilla-modal-title">About</h2>
    </div>
    <div class="rv-vanilla-modal-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum sed, eveniet, odit recusandae facilis voluptate 
		   iste commodi eligendi amet molestiae corporis magnam facere et temporibus cumque molestias aperiam laborum tenetur.</p>
    </div>
   </div>
    
     
</body>




<script src="resources/User_Resources/popUpDesign/popUp.js"></script>

<script>


document.addEventListener('DOMContentLoaded', function() {
    /* global RvVanillaModal */
    'use strict';
    var modal = new RvVanillaModal({
        showOverlay: true
    });

    // each method
    modal.each(function(element) {
       var target = element.getAttribute('data-rv-vanilla-modal');
       var targetElement = document.querySelector(target);
       var closeBtn = targetElement.querySelector(modal.settings.closeSelector);

       // close click listerner
       closeBtn.addEventListener('click', function(event) {
        event.preventDefault();
        modal.close(targetElement);
       });

       // open click listerner
       element.addEventListener('click', function(event) {
        event.preventDefault();
        modal.open(targetElement);
       });
    });
    
    
}, false);

</script>
</html>