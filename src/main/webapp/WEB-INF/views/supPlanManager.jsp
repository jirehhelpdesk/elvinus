<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Plan Management</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
    
    
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>



<body onload="actimeMenu('menu5')">

    <div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
    <!-- /navbar -->
    
    
    <div> <%@include file="supAdminHeader.jsp" %> </div>
    
    
    <!-- /subnavbar -->
    <div class="main">
        <div class="main-inner">
            <div class="container">
                
               	
	     
		 <div class="widget-header">
	      				<i class="icon-book"></i>
	      				<h3>Plan Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">A- PLAN(Double at 5 years scheme)</a>
						  </li>
						  <li class="" onclick="showPlanB()"><a data-toggle="tab" href="#jscontrols">B- PLAN (NO double- only insentive)</a></li>
						  <li class="" onclick="showPlanC()"><a data-toggle="tab" href="#jscontrols1">C-PLAN(HYBRID)</a></li>
						</ul>
						
						<br>
						
							<div class="tab-content">
								<div id="formcontrols" class="tab-pane active">
								
								<form class="form-horizontal" id="planAForm">
									<fieldset>
									
									<%String plan_name = "";
									   String planAFlag = "true";%>
									
									
										
										<c:forEach items="${planDetails}" var="det">	
																	
													<c:set var="plan_name"  value="${det.plan_name}"/>
                                                    <%plan_name = (String)pageContext.getAttribute("plan_name");%>

                                               
                                               <%if(plan_name.equals("Plan A")){%>
                                               
				                                       <%planAFlag = "false"; %>        
				                                       
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 5 years</label>
															<div class="controls">
																<input type="text" value="${det.bonus_amt_after_five_year}" disabled="true" id="planValId1A" name="planVal1" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount per friend   </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_per_friend}"  disabled="true" id="planValId2A"  name="planVal2" class="span4">														
															</div> <!-- /controls -->			
				                                            										
														</div> <!-- /control-group -->																				
														
														
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 25 days  </label>
															<div class="controls">
																<input type="text" value="${det.bonus_amt_within_25_days}"  disabled="true"  id="planValId3A" name="planVal3" class="span4">														
															</div> <!-- /controls -->			
				                                            				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 25 days  </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_after_25_days}"  disabled="true"  id="planValId4A" name="planVal4" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 																			
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 50 days  </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_within_50_days}"  disabled="true"  id="planValId5A" name="planVal5" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 50 days  </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_after_50_days}"  disabled="true"  id="planValId6A" name="planVal6" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 75 days  </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_within_75_days}"  disabled="true"  id="planValId7A" name="planVal7" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 75 days   </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_after_75_days}"  disabled="true"  id="planValId8A" name="planVal8" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 100 days   </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_within_100_days}"  disabled="true"  id="planValId9A" name="planVal9" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 100 days    </label>
															<div class="controls">
																<input type="text"  value="${det.bonus_amt_after_100_days}"  disabled="true"  id="planValId10A" name="planVal10" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
													<div class="control-group">											
														
														<label for="username" class="control-label" style="width: 230px ! important;">Plus Bonus After 100 days   </label>
														<div class="controls">
															<input type="text"  id="planValId11" value="${det.additional_bonus}" disabled="true"  name="planVal11A" class="span4">														
														</div> <!-- /controls -->			
			                                         									
													</div> 	
													
													 <br>
													 
													 <div class="form-actions">
													    <input type="hidden"  name="planName" value="Plan A" />			
													    <input type="button" class="btn btn-primary" id="AButtonId" value="Edit" onclick="savePlanDetails('planAForm','Plan A','AButtonId','A')"/> 
														<button class="btn">Cancel</button>
													</div> <!-- /form-actions -->
												   
                                               <%}%>
                                               
                                               
				                        </c:forEach>	
				                    
				                        
				                        <%if(planAFlag.equals("true")){%>
                                               
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 5 years</label>
															<div class="controls">
																<input type="text"  id="afterBonus" name="planVal1" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount per friend   </label>
															<div class="controls">
																<input type="text"  id="bnamt"  name="planVal2" class="span4">														
															</div> <!-- /controls -->			
				                                            										
														</div> <!-- /control-group -->																				
														
														
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 25 days  </label>
															<div class="controls">
																<input type="text"  id="beforBonus" name="planVal3" class="span4">														
															</div> <!-- /controls -->			
				                                            				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 25 days  </label>
															<div class="controls">
																<input type="text"  id="afterBonus" name="planVal4" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 																			
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 50 days  </label>
															<div class="controls">
																<input type="text"  id="beforBonus" name="planVal5" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 50 days  </label>
															<div class="controls">
																<input type="text"  id="afterBonus" name="planVal6" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 75 days  </label>
															<div class="controls">
																<input type="text"  id="beforBonus" name="planVal7" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 75 days   </label>
															<div class="controls">
																<input type="text"  id="afterBonus" name="planVal8" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Bonus amount on or before 100 days   </label>
															<div class="controls">
																<input type="text"  id="beforBonus" name="planVal9" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
														
														<div class="control-group">											
															
				                                            <label for="username" class="control-label"  style="width: 230px ! important;">Bonus amount after 100 days    </label>
															<div class="controls">
																<input type="text"  id="afterBonus" name="planVal10" class="span4">														
															</div> <!-- /controls -->		
																									
														</div> 
														
														
														<div class="control-group">											
															<label for="username" class="control-label" style="width: 230px ! important;">Plus Bonus After 100 days   </label>
															<div class="controls">
																<input type="text"  id="beforBonus" name="planVal11" class="span4">														
															</div> <!-- /controls -->			
				                                            
				                                           									
														</div> 	
										
										
														
														 <br>
														 
														 
														 <div class="form-actions">
														    <input type="hidden"  name="planName" value="Plan A" />			
														    <input type="button" class="btn btn-primary" id="AButtonId" value="Save" onclick="savePlanDetails('planAForm','Plan A','AButtonId','A')"/> 
															<button class="btn">Cancel</button>
														</div> <!-- /form-actions -->
														
                                               
                                               <%}%>
										
										
										

									</fieldset>
								</form>
								
								</div>
								
								
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="planBForm">
										<fieldset>
										
										    <div id="planBDiv"></div>
										
										</fieldset>
									</form>
								</div>
								
								
								
								<div id="jscontrols1" class="tab-pane">
									<form class="form-vertical" id="planCForm">
										<fieldset>
											
											 <div id="planCDiv"></div>
					
										</fieldset>
									</form>
								</div>
								
								
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div>
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    
	
	
    <!-- /extra -->
    
    
    <div> <%@include file="supAdminFooter.jsp"%> </div>
    
    
    <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
    <script src="resources/Admin_Resources/js/excanvas.min.js"></script>
    <script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script>
    <script src="resources/Admin_Resources/js/bootstrap.js"></script>
    <script src="resources/Admin_Resources/js/base.js"></script>
    
    <script src="resources/Admin_Resources/js/validateForm.js"></script>
    
</body>
</html>
