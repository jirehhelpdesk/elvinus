<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of SMS Status</title>
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

                       <div class="widget widget-table action-table">
											
												<div class="widget-header">
													<i class="icon-th-list"></i>
													<h3 id="resultHeading">All Pending SMS Sent to Vendor</h3>
												</div>
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
															    <th>Serial no</th>
																<th>Mobile no</th>
																<th>SMS Message</th>
																<th>SMS Date</th>
																
																<th class="td-actions"></th>
															</tr>
														</thead>
														
														
														<tbody>
															
														<c:if test="${!empty smsDetails}">	
															
															<%int i=1; %>
															
															
															<c:forEach items="${smsDetails}" var="det">	
															
															
															<!-- Restrict for 10 datas -->
															
																	<tr>
																	        <td><%=i++%></td>
																			<td>${det.sent_mobile_no}</td>
																			<td>${det.sms_message}</td>
																			<td>${det.sms_sent_date}</td>
																			
																			<td class="td-actions">
																			
																					<a class="btn btn-small btn-success" href="javascript:;" title="Send Agian" onclick="sentSmsAgain'${det.sms_id}')">
																					
																					     <i class="btn-icon-only icon-repeat"> </i>
																							
																					</a> 
																					
																			</td>																																				
																	</tr>
															
															</c:forEach>
															
														</c:if>	
														
														<c:if test="${empty smsDetails}">	
														
														            <tr>
																	        <td style="color:red;">As per the search details No data found !</td>
																	
																	</tr>
														
														</c:if>
																														
														</tbody>
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>