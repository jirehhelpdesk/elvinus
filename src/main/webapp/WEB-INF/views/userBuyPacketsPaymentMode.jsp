<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buy Packet</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


	   
  <link href="resources/User_Resources/popUpDesign/popUp.css" rel="stylesheet">
	   
  <link href='resources/indexResources/css/innerCheckBox.css' rel='stylesheet' type='text/css'>

</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
           
                                       <div id="about-modal" class="rv-vanilla-modal">
		   
											    <div class="rv-vanilla-modal-header group">
											        
											        <h2 class="rv-vanilla-modal-title">Choose Payment Mode</h2>
											    </div>
											    
											    <div class="rv-vanilla-modal-body" style="padding: 1px 1px 20px !important;height:200px;">
											    	       
											    	       <!-- Rounded TWO -->
															
															
																    <p style="margin: 56px 215px;font-size: 16px;font-style: italic;">Payment Online</p>
																	<div class="roundedTwo">							   
																		<input type="checkbox" value="Online" id="roundedTwo" style="visibility: hidden !important;"  onclick="capturePaymentMode('roundedTwo')"/>
																		<label for="roundedTwo"></label>
																	</div>
																
																
																    <p style="margin: 120px 217px;width: 250px;font-size: 16px;font-style: italic;">Payment Offline / Cash On delivery</p>
																	<div class="roundedThree">
																		<input type="checkbox" value="Offline" id="roundedThree" style="visibility: hidden !important;"  onclick="capturePaymentMode('roundedThree')"/>
																		<label for="roundedThree"></label>
																	</div>
														
																
													
											       <div class="popButtonDiv">
															<input type="button" onclick="gotoCheckOut()" value="Proceed" id="basicButtonId" class="btn">
												   </div>
													
											    </div>
											    
									   </div>
									   
									   
									   <div class="backGroundDiv"></div>
</body>

</html>