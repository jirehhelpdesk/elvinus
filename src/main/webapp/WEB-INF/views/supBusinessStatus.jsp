<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Business Status</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
   
     <link href="resources/Admin_Resources/css/pages/reports.css"  rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="actimeMenu('menu7')">

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>


    
<div> <%@include file="supAdminHeader.jsp" %> </div>
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	    	
	     <div class="row">
	      	
	      	<div class="span12">
	      
	      	<div class="info-box">
               <div class="row-fluid stats-box">
                  
                  <div class="span4" style="width: 46.624% !important;">
                  	<div class="stats-box-title">Total No.Of User</div>
                    <div class="stats-box-all-info"><i style="color:#3366cc;" class="icon-user"></i><%=request.getAttribute("noOfUsers")%></div>
                  
                  </div>
                  
                  <%-- 
                  <div class="span4">
                    <div class="stats-box-title">Likes</div>
                    <div class="stats-box-all-info"><i style="color:#F30" class="icon-thumbs-up"></i> 66.66</div>
                    <div class="wrap-chart"><div style="padding: 0px; position: relative;" class="chart" id="order-stat"><canvas width="325" height="150" class="chart-holder" id="bar-chart2" style="width: 325px; height: 150px;"></canvas></div></div> 
                  
                  </div> --%>
                  
                  
                  <div class="span4" style="width: 49.624% !important;border-left: 1px solid #ccc;">
                    <div class="stats-box-title">Total No.Of Orders</div>
                    <div class="stats-box-all-info"><i style="color:#3C3" class="icon-shopping-cart"></i><%=request.getAttribute("noOfOrders")%></div>
                    
                  </div>
               </div>
               
               
             </div>
               
               
         </div>
         </div>      
	      	
	  	  <!-- /row -->
	
	      <div class="row">
	      	
	      	<div class="span6">
	      		
	      		<div class="widget">
						
					<div class="widget-header">
						<i class="icon-star"></i>
						<h3>Some Stats</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<canvas width="538" height="250" class="chart-holder" id="pie-chart" style="width: 538px; height: 250px;"></canvas>
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
				
	      		
	      		
	      		
		    </div> <!-- /span6 -->
	      	
	      	
	      	<div class="span6">
	      		
	      		<div class="widget">
							
					<div class="widget-header">
						<i class="icon-list-alt"></i>
						<h3>Another Chart</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<canvas width="538" height="250" class="chart-holder" id="bar-chart" style="width: 538px; height: 250px;"></canvas>
					</div> <!-- /widget-content -->
				
				</div> <!-- /widget -->
									
		      </div> <!-- /span6 -->
	      	
	      </div> <!-- /row -->
	      
	      
	      
	      
			
	      
	      
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div>
    
 
    
 <div> <%@include file="supAdminFooter.jsp"%> </div>


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
	
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script src="resources/Admin_Resources/js/base.js"></script>

<script src="resources/Admin_Resources/js/validateForm.js"></script>

  </body>

</html>
