<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">


<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<script src="resources/indexResources/Slider/src/jquery.js"></script>
<script src="resources/indexResources/Slider/src/skdslider.min.js"></script>
<link href="resources/indexResources/Slider/src/skdslider.css" rel="stylesheet" />


<script type="text/javascript">

		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({delay:5000, animationSpeed: 2000,showNextPrev:true,showPlayButton:true,autoSlide:true,animationType:'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			  $(window).trigger('resize');
			});
			
		});
		
</script>

</head>
<body>

<!-- Header Div -->  

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
        <a href="index.html" class="over"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
      <div class="topmenu"><a href="userLogin.html">Sign In</a> | <a href="userRegister.html"> Become a member</a></div>
            <ul class="topmenu">
              <li><a href="index.html" class="over"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
  
  <!-- End Header Div --> 
  
  
  <!-- Slider Header Div --> 
  
  <div id="header">
   
 
			   <div class="skdslider" >
				<ul id="demo1" class="slides">
			
			<li> <img src="resources/indexResources/Slider/slides/slide1.jpg" />
			<!--Slider Description example
			 <div class="slide-desc">
					<h2>Slider Title 1</h2>
					<p>Demo description here. Demo description here. Demo description here. Demo description here. Demo description here. <a class="more" href="#">more</a></p>
			  </div>-->
			</li>
			
			<li> <img src="resources/indexResources/Slider/slides/slide2.jpg" />
			<!--Slider Description example
			 <div class="slide-desc">
					<h2>Slider Title 1</h2>
					<p>Demo description here. Demo description here. Demo description here. Demo description here. Demo description here. <a class="more" href="#">more</a></p>
			  </div>-->
			</li>
			
			
			
			</ul>
			</div>


  </div>
  
  <!-- End of Slider Header Div --> 
  
  
  
  <!-- Middle  Page Content Div --> 
  
  
  <div id="featurezone">
  	<div class="featurecol">
		<div class="featurecoltop">
		  <div class="featurecolbottom">
				<img src="resources/indexResources/images/quickregister.png" alt="">
				<h1>Quick Register</h1>
			    <span class="subheading">Nemo enim ipsam voluptatem</span>
				<p>Register,Shop, save, and get Products...</p>
				<div class="readmoreimg"></div>
		  </div>
		</div>
	</div>
	
	<div class="featurecol">
		<div class="featurecoltop">
			<div class="featurecolbottom">
				<img src="resources/indexResources/images/beapartner.png" alt="">
				<h1>Be A partner</h1>
				<span class="subheading">Nemo enim ipsam voluptatem</span>
				<p>Join an affiliate network that pays out more than what you expect...</p>
				<div class="readmoreimg"></div>
			</div>
		</div>
	</div>
	
	<div class="featurecol">
		<div class="featurecoltop">
			<div class="featurecolbottom">
				<img src="resources/indexResources/images/safepayment.png" alt="">
				<h1>Safe Payment</h1>
				<span class="subheading">Nemo enim ipsam voluptatem</span>
				<p>Pay with the world�s most popular and secure payment methods...</p>
				<div class="readmoreimg"></div>
			</div>
		</div>
	</div>
   
	<div class="featurecol_last">
		<div class="featurecoltop">
			<div class="featurecolbottom">
				<img src="resources/indexResources/images/greatvalue.png" alt="">
				<h1>Great Value</h1>
				<span class="subheading">Nemo enim ipsam voluptatem</span>
				<p>We offer competitive prices on our 100 million plus product range...</p>
				<div class="readmoreimg"></div>
			</div>
		</div>
    </div>
    
  </div>
  
  
  <!-- End of Middle Page Content Div --> 
  
  
  
  <!-- Middle extra Page Content Div --> 
  
  
  <div id="welcomezone">
    <h1>Welcome to our planed finance site!</h1>
   <p class="subheading"  style="text-align:center;width:900px; margin:0 auto;">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
   
   <p style="text-align:justify;width:900px;margin:0 auto;"> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
    It is a long established fact that a reader will be distracted by 
    the readable content of a page when looking at its layout.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. 
    to repeat predefined chunks as necessary, making this the first true generator.
    </p>
  </div>
	
 
  <p class="clear"></p>
  
</div>

<!-- end of Middle extra Page Content Div --> 



<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 


<script>document.write=null;window.open=null;document.open=null;</script>
</body>
</html>