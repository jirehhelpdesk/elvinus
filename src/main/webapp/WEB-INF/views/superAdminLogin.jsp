<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Super Admin Login</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet" type="text/css">
<link href="resources/Admin_Resources/css/pages/signin.css" rel="stylesheet" type="text/css">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet" type="text/css">


</head>

<%@page import="java.io.File" %><%   
%><%@page import="java.io.*" %><%
%><%@page import="java.util.*" %><% 
%><%@page language="java" trimDirectiveWhitespaces="true"%><%
%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="adminLogin.html">
				Business Restriction			
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
			                
			               <li>
				                   <a class="brand" href="adminLogin.html" style="font-size: 12px ! important;">
				                   <i class="icon-group"></i>
									    Admin Access			
								   </a>
						   </li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->


              				<% String actionValue=(String)request.getAttribute("actionMessage");%>		
							
							<%if(actionValue!=null) {%>
							
									<%if(actionValue.equals("failed")) {%>
										
												  <div class="alert alert-error" id="alertDiv" style="margin-top: 20px;">
		                                             <button type="button" class="close" data-dismiss="alert">&times;</button>
		                                             <strong>Warning !</strong> Please enter the proper credential.
		                                          </div>
		                             <%} %>
                             
							<%} %>
							
							   
                                  
<div class="account-container">
	
	
	<div class="content clearfix">
		
		<form action="authSupadmin.html" method="post">
		
			<h1>Super Admin Login</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>
				
				<div class="field">
					<label for="username">Username</label>
					<input type="text" id="username" name="adminid"  placeholder="Username" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="adminPassword"  placeholder="Password" class="login password-field"/>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					
				</span>
									
				<button class="button btn btn-success btn-large" onclick="validateForm()">Sign In</button>
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
<script src="resources/Admin_Resources/js/bootstrap.js"></script>

<script src="resources/Admin_Resources/js/signin.js"></script>


<script src="resources/Admin_Resources/js/validateForm.js"></script>

</body>

</html>
