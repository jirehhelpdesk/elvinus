<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buy Packet</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
   
    <link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">
   
   
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body onload="displayProducts('Agriculture')">
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html" ><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="buyMorePackets.html" class="sideMenuActive"><i class="icon-gift"></i> Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html"><i class=" icon-random"></i> Track Order </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                       Buy More Packets
                                       
                                       <a href="#" onclick="showCartProducts()"> 
								                  <div class="innertopblock2" >
														<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
													 	<p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong></p>
												  </div>					  	
										   </a>
                                       
                                    </h3>
                                    
                                    
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">

										<div class="headDiv"><h4 class="textStyle">Check Out</h4> </div>
									                                                     
                                 			<div class="checkOutBill">                   
                   								                          
									               <div class="pricelist">
									                    
									                   
									                     <%String paymentMode = (String)session.getAttribute("paymentMode"); %>
									                   
									                   <form id="orderDetailForm" name="orderDetailForm" method="post">
									                   
									                       				
															 <c:forEach items="${userBasicInformation}" var="det">	 
																						
											                        <table width="800" align="center" style="padding-bottom: 15px;">
											                        <tr class="rowStyle">   
											                         <td class="rightHeading"><b>Name</b><td>     
											                         <td>:<td>       
											                         <td><i>${det.user_name}</i><td>
											                         <input type="hidden" name="fullname" value="${det.user_name}" />
											                        </tr>
											                          <tr class="rowStyle">
											                          <td class="rightHeading"><b>Email Id</b><td>     
											                         <td>:<td>       
											                         <td><i>${det.user_emailid}</i><td>
											                         <input type="hidden" name="emailId" value="${det.user_emailid}" />
											                        </tr>
											                         <tr class="rowStyle">
											                          <td class="rightHeading"><b>Mobile No</b><td>     
											                         <td>:<td>       
											                         <td>+91 &nbsp; <i>${det.user_mobile_no}</i><td>
											                         <input type="hidden" name="mobileNo" value="${det.user_mobile_no}" />
											                        </tr>
											                         <tr class="rowStyle">
											                          <td class="rightHeading"><b>Shipping Address</b><td>     
											                         <td>:<td>       
											                         <td><i>${det.user_shipping_address}</i><td>	
											                         <input type="hidden" name="shAddress" value="${det.user_shipping_address}" />			                         
											                        </tr>
											                        <tr class="rowStyle">
											                          <td class="rightHeading"><b>Description</b><td>     
											                         <td>:<td>       
											                         <td style="text-align:justify;"><i> As per the interested product you have selected in product selection section business will allocate 
											                         								  a <b>KIT</b> to you where it will be a combination of your selected product.</i>
																	 <td>
											                        </tr>
											                        <tr class="rowStyle">
											                         <td class="rightHeading"><b>Delivery Report</b><td>     
											                         <td>:<td>       
											                         <td style="text-align:justify;"><i> As soon as possible with in two or less then a week. This will notify to your email id and contact no.</i>
											                         
																	 <td>
											                        </tr>
											                        </table>
									                        
									                        
									                        </c:forEach> 
									                        
									                        
									                             <input type="hidden" name="userId" value="<%=session.getAttribute("regUserId")%>" />
									                             <input type="hidden" name="paymentMode" value="<%=paymentMode%>" />
									                             
									                             <input type="hidden" name="kitcharge" value="<%=request.getAttribute("kitCharge")%>" />
									                             <input type="hidden" name="vat" value="<%=request.getAttribute("vat")%>" />
									                             <input type="hidden" name="vatPersentageVal" value="<%=request.getAttribute("vatPersentageVal")%>" />
									                             <input type="hidden" name="subtotal" value="<%=request.getAttribute("subtotal")%>" />
									                             <input type="hidden" name="regCharge" value="<%=request.getAttribute("regCharge")%>" />
									                             <input type="hidden" name="total" value="<%=request.getAttribute("total")%>" />
									                             
									                        
									                      </form>
									                      
									                         
									                     <div id="pricelist">
		                        
										                        <table width="300" align="right">
										                        <tr>   
										                         <td style="width:155px;color: #00b6f5;font-style: italic;">KIT Amount<td>   
										                         <td>:</td>        
										                         <td><%=request.getAttribute("kitCharge")%><td>
										                        </tr>
										                          <tr>
										                           <td style="width:155px;color: #00b6f5;font-style: italic;">VAT (<%=request.getAttribute("vat")%>%)<td>
										                           <td>:</td>       
										                          <td><%=request.getAttribute("vatPersentageVal")%><td>
										                        </tr>
										                         <tr>
										                           <td style="width:155px;color: #00b6f5;font-style: italic;">Sub Total<td>
										                           <td>:</td>       
										                          <td><%=request.getAttribute("subtotal")%><td>
										                        </tr>
										                        
										                        <tr>
										                          <td style="width:155px;color: #00b6f5;font-style: italic;">Registration Charge<td>
										                          <td>:</td>       
										                          <td><%=request.getAttribute("regCharge")%><td>
										                        </tr>	                        
										                        
										                         <tr >
										                          <td class="border"><strong>Total </strong><td><td>:</td>       
										                          <td class="border"><strong><%=request.getAttribute("total")%></strong><td>
										                        </tr>
										                        </table>
										                    </div>
									                    
									                    
									                    </div>
							               
												
														
														
														<%if(paymentMode.equals("Online")) {%>
														
														    <div class="controls payButton" >
																<input type="button" onclick="proceedForOnlinePayment()" value="Pay Now" id="basicButtonId" class="btn">
															</div>
															
							                            <%}else{ %>
							                           
							                               <div class="controls payButton">
																<input type="button" onclick="proceedForOfflinePayment()" value="Finish" id="basicButtonId" class="btn">
															</div>
															
							                            <%} %>
								                </div>
							              
							              
							        </div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
												    <input  type="hidden" id="agriProdId" name="agricultureProduct" value="<%=request.getAttribute("agriId")%>"  />
						      						<input  type="hidden" id="herbalProdId" name="herbalProduct" value="<%=request.getAttribute("herbalId")%>" />
						      						<input  type="hidden" id="hmCareProdId" name="homecareProduct" value="<%=request.getAttribute("hmCareId")%>" />
						       

       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
      
       <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<div id="cartDetailsPopUp" style="display:none;"></div>
		
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="buyMorePackets.html";
		}
		
		</script>
         
         
    </body>

</html>