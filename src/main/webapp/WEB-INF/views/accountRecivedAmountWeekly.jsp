<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Amount Received in week</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>


                                         <div class="widget widget-nopad">
										          
										            <div class="widget-header"> <i class="icon-list-alt"></i>
										              <h3> This week Total Amount Received from <%=request.getAttribute("today")%> to <%=request.getAttribute("sevenDayBefore")%></h3>
										            </div>
										            <!-- /widget-header -->
										            <div class="widget-content">
										              <div class="widget big-stats-container">
										                <div class="widget-content">
										                 
										                  <h6 class="bigstats">As of this week transaction Business got the total amount,</h6>
										                  
										                  <div class="cf" id="big_stats">
										                    <div class="stat">  </div>
										                    <!-- .stat -->
										                    
										                    
										                    <div class="stat">
										                    Generate Record
										                    <i class="icon-list-alt" title="Excel Record" style="cursor:pointer;" onclick="genWeeklyRecRecord()"></i>
										                     </div>
										                    <!-- .stat -->
										                    
										                     <div class="stat">  Total Amount with Out VAT(<%=request.getAttribute("vat")%>%)<i></i> <!-- <i class="icon-bullhorn"></i> <img src="resources/Admin_Resources/img/rupeesicon.png" width="15" height="18" />  <span class="value">Rs</span> --> <span class="value"> <b style="color:red;"> <%=request.getAttribute("totalAmountWithOutVat")%> </b> INR</span> </div>
										                    <!-- .stat -->
										                    
										                    <div class="stat"> Total <i></i><!-- <i class="icon-bullhorn"></i> <img src="resources/Admin_Resources/img/rupeesicon.png" width="15" height="18" />  <span class="value">Rs</span> --> <span class="value"> <b style="color:red;"> <%=request.getAttribute("totalTurnOver")%> </b> INR</span> </div>
										                    <!-- .stat --> 
										                  </div>
										                  
										                </div>
										                <!-- /widget-content --> 
										                
										              </div>
										            </div>
										            
										        </div>
										        
                       <div class="widget widget-table action-table">
											
												
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															
															<tr>
															
															    <th>Serial no</th>
																<th>Transaction Id</th>																
																<th>Transaction Mode</th>
																<th>Transaction Status</th>
																<th>Transaction Date</th>
																<th>Transaction Amount</th>
																
															</tr>
															
														</thead>
														
														
												       <tbody>
													
													<c:if test="${!empty weeklyTranDetails}">	
															
														<%int i=1; %>
															
															
														<c:forEach items="${weeklyTranDetails}" var="det">	
															
															<!-- Restrict for 10 datas -->
															
																	<tr>
																	   
																	        <td><%=i++%></td>
																			<td>${det.transaction_unique_id}</td>
																			<td>${det.transaction_mode}</td>
																			<td>${det.transaction_status}</td>
																			<td>${det.transaction_date}</td>
																			<td style="color:green;">${det.transaction_amount}</td>
																																																			
																	</tr>
														
														</c:forEach>
														
														</c:if>
														
														<c:if test="${empty weeklyTranDetails}">	
															   
															   <tr>     
															         <td style="color:red;"> As of now no transaction has done in the business ! </td>
															   </tr>   
															    
														</c:if>
																									
														</tbody>
														
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>