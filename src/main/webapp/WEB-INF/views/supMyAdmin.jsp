<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>My Admin</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">


</head>

<body onload="actimeMenu('menu1')">


<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->


<div> <%@include file="supAdminHeader.jsp" %> </div>


<!-- /subnavbar -->

<div class="main">
  
  <div class="main-inner">
    
    <div class="container">
                  
  				    <div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Adminstration Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
					  <div class="tabbable">
						
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">Create Admin</a>
						  </li>
						  <li class="" onclick="showAdminDetails()"><a data-toggle="tab" href="#jscontrols" >Delete Admin</a></li>
						</ul>
						
						<br>
						
							<div class="tab-content">
						
						    <div id="formcontrols" class="tab-pane active">
								
								
								<form class="form-horizontal" id="profileForm" name="profileForm">
								
									<fieldset>
										
										<div class="control-group">											
											<label for="username" class="control-label">Admin Type</label>
											<div class="controls">
												<select id="adminType" name="admincategory" onchange="createAdminProfileOnChange()">
												        <option value="Select Admin Type">Select Admin Type</option>
														<option value="Account">Account</option>
														<option value="Product">Product</option>
														<option value="Check">Check</option>
												</select>			
												
												<div id="errAdminType" class="errorStyle"></div>
																				
											</div> <!-- /controls -->				
										</div>
										
										
										<div class="control-group">											
											<label for="username" class="control-label">Full Name</label>
											<div class="controls">
												<input type="text" name="fullName" id="fullName" class="span4" onchange="createAdminProfileOnKeyUp()" />	
												<div id="errfullName" class="errorStyle"></div>											
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																				
										
										
										<div class="control-group">											
											<label for="email" class="control-label">Email Address</label>
											<div class="controls">
												<input type="text" name="emailId" id="email" class="span4" onchange="createAdminProfileOnKeyUp()" />
												<div id="erremail" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label for="email" class="control-label">Mobile no</label>
											<div class="controls">
												<input type="text" name="contactNo" maxlength="10" id="mobileno" class="span4" onchange="createAdminProfileOnKeyUp()" />
												<div id="errmobileno" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label for="password1" class="control-label">Password</label>
											<div class="controls">
												<input type="password" name="password" id="password1" class="span4"  onchange="createAdminProfileOnKeyUp()" />
												<div id="errpassword1" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->										
										
										<div class="control-group">											
											<label for="password2" class="control-label">Confirm Password</label>
											<div class="controls">
												<input type="password"  id="password2" class="span4" onchange="createAdminProfileOnKeyUp()" />
												<div id="errpassword2" class="errorStyle"></div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
											
										 <br>
										
											
										<div class="form-actions">
										    <input type="button" class="btn btn-primary" value="Save" onclick="createAdminProfile()"/> 
											<button class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
									
								</form>
								
								</div>
								
								
								
								<!-- Remove Admin Division  -->
								
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="edit-profile2">
										<fieldset>
				
				                              <div id="adminDetailsDiv"></div>
		
										</fieldset>
									</form>
								</div>
								
								
								
								<!-- End of Remove Admin Division  -->
								
								
							</div>
						  
						</div>
						
					</div>
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<!-- /extra -->

<div> <%@include file="supAdminFooter.jsp"%> </div>

<!-- /footer --> 



<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/validateForm.js"></script>

</body>
</html>
