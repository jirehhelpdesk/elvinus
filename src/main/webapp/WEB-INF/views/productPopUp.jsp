<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
 .productDetailsDiv{
	Width:900px;
	height:auto;
	margin:0px auto 0;
	/* padding:25px 20px; */
	/* box-shadow:0 3px 8px #ABABAB; */
 }
 
 .imageWrapper{
	text-align:center;
	margin:0 auto 30px; 
 }
 
 .prodcontent{
	text-align:left;
	margin:0 auto;
 }
 
  .prodcontent p{
	margin:0 auto;
 }
 
</style>

</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
    
    <%String productFilePopUP = ""; %>
    <%String productTypePopUp = ""; %>
    
    
    <div class="productDetailsDiv">
         
        <c:forEach items="${productDetailsUseInPopUp}" var="det">	
		
			 <c:set var="productFilePopUP"  value="${det.product_image_file_name}"/>
			 <% productFilePopUP = (String)pageContext.getAttribute("productFilePopUP");%>
			
			 <c:set var="productTypePopUp"  value="${det.product_category}"/>
			 <% productTypePopUp = (String)pageContext.getAttribute("productTypePopUp");%>
					 
				 
		         <div class="imageWrapper">
		          
		          <img style="border-radius:6px;width:470px;height:300px;"  src="${pageContext.request.contextPath}<%="/previewProductsForuser.html?fileName="+productFilePopUP+"&productType="+productTypePopUp+""%>"  ></img>
							
		         </div>
		         		          
		         
		         <h4>Description for ${det.product_name} :</h4>
		         <div class="prodcontent">
		         
		         	<p>${det.product_description}</p>
		         	
		         </div>  
		         
		         
		</c:forEach>
		            
    </div>
    
    
</body>
</html>
