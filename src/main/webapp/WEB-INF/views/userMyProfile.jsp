<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>My Profile</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
     
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li><a href="myProfile.html" class="sideMenuActive"><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="userReferFriends.html"><i class="icon-group"></i> Refer Friend </a>                              
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="buyMorePackets.html"><i class="icon-gift"></i> Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html"><i class=" icon-random"></i> Track Order </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                        My Profile :</h3>
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">

										<form class="form-horizontal row-fluid">


                                        <div class="headDiv"><h4 class="textStyle">Basic Profile Info</h4></div>
                                        <br>
                                            <c:forEach items="${userBasicInfo}" var="det">
													
													<div class="profile-head media" style="padding:30px;">
														
														<a class="media-avatar pull-left" href="#"> 
														
														    <%String profilePic = (String)session.getAttribute("profilePicture"); %>
                               								<%if(!profilePic.equals("No Photo")){ %>
                                
                                
							                                     <img src="${pageContext.request.contextPath}<%="/previewProfilePicture.html?fileName="+profilePic+"&productType=dssdfd"%>" >
							                                
							                                <%}else{ %>
							                                
							                                     <img src="resources/User_Resources/images/passimg.png">
							                                
							                                <%} %>
                                
															
														</a>
														
														<div class="media-body">
															
															<h4>${det.user_name}</h4>
															
															<p style="color:#00b6f5;">Email Id:</p>	 <p class="profile-brief">${det.user_emailid} </p>
															<p style="color:#00b6f5;">Mobile No:</p> <p class="profile-brief">${det.user_mobile_no} </p>																													
															<p style="color:#00b6f5;">Shipping Address:</p>	 <p class="profile-brief">${det.user_shipping_address} </p>
																
															
														</div>
														
													</div>
													
												</c:forEach>
												
											
											<div class="headDiv"><h4 class="textStyle">Other Profile Info</h4></div>
                                            <br>
                                            
                                            <c:if test="${!empty userOtherInfo}">	
                                            <c:forEach items="${userOtherInfo}" var="det">
													
													<div class="profile-head media" style="padding:30px;">
														
														<div class="media-body">
															
															<p style="color:#00b6f5;">Alternative Email Id:</p>	 <p class="profile-brief">${det.user_alternate_emailid} </p>
															<p style="color:#00b6f5;">Permanent Address:</p> <p class="profile-brief">${det.user_permanent_address} </p>																													
															<p style="color:#00b6f5;">Date of birth:</p>	 <p class="profile-brief">${det.user_dob}</p>
															<p style="color:#00b6f5;">Proof of Identity</p>	 <p class="profile-brief">${det.user_id_type} - ${det.user_proof_of_id} </p>
															
														</div>
														
													</div>
													
											</c:forEach>
											</c:if>	
											
											
											<c:if test="${empty userOtherInfo}">	
                                            	
													<div class="profile-head media" style="padding:30px;">
														
														<div class="media-body">
															
															<p style="color:#00b6f5;">Alternative Email Id:</p>	 <p class="profile-brief">Not Updated</p>
															<p style="color:#00b6f5;">Permanent Address:</p> <p class="profile-brief">Not Updated </p>																													
															<p style="color:#00b6f5;">Date of birth:</p>	 <p class="profile-brief">Not Updated</p>
															<p style="color:#00b6f5;">Proof of Identity</p>	 <p class="profile-brief">Not Updated</p>
															
														</div>
														
													</div>
											
											</c:if>	
												
												
										   <div class="headDiv"><h4 class="textStyle">Last Order Details</h4></div>
                                            
                                            <br>
                                               <c:forEach items="${userLastOrder}" var="det">
													
													<div class="profile-head media" style="padding:30px;">
														
														<div class="media-body">
															
															<p style="color:#00b6f5;">Order Id:</p>	 <p class="profile-brief">${det.order_unique_id}</p>
															<p style="color:#00b6f5;">Order Date:</p> <p class="profile-brief">${det.order_date} </p>
															<p style="color:#00b6f5;">Order Amount:</p> <p class="profile-brief">${det.order_amount} </p>																													
															<p style="color:#00b6f5;">Order Status:</p>	 <p class="profile-brief">${det.order_status}</p>
															<p style="color:#00b6f5;">Order Status Details:</p>	 <p class="profile-brief">${det.order_status_details}</p>
															
														</div>
														
													</div>
													
												</c:forEach>
												
												
											</form>
									
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
    </body>

</html>