<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bonus Amount</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    
  <link type="text/css" href="resources/User_Resources/css/referDesign.css" rel="stylesheet">
 
 
 <link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">

   
        
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html"><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="buyMorePackets.html"><i class="icon-gift"></i>  Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li ><a href="trackOrder.html"><i class=" icon-random"></i>      Track Order  </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a  data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="sideMenuActive" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="unstyled in collapse">
	                                        <li><a href="earcnedReferenceAmt.html" class="sideSubMenuActive"><i class="icon-money"></i> Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i> Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    
                                    <h3>
                                          Reference Amount Details as per <%=request.getAttribute("userPlan")%>
                                        
						                  <div class="innertopblock2" style="margin: -12px -42px 3px !important; width: 157px;">
												<img src="resources/User_Resources/images/wallet1.png" alt="" class="innershopping" />
											 	<p>Credit Amount</p> <p>Rs.<b  style="color:#f00;"><%=request.getAttribute("totalCreditAmount")%></b> <span></span></p>
										  </div>					  											  
                                    </h3>
                                    
                                    
                                    
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">

										<ul class="profile-tab nav nav-tabs" style="margin-top:-30px;">
		                                    <li class="active"><a data-toggle="tab" href="#activity" onclick="refresh('earcnedReferenceAmt.html')">Reference Amount Status</a></li>
		                                    <li class=""><a data-toggle="tab" href="#friends" onclick="refPolicyStatus()">Reference Plan Policy</a></li>
		                                </ul>
									
									   
									   
										   <div class="profile-tab-content tab-content">
			                                    
			                                    
						                                    <div id="activity" class="tab-pane fade active in">
						                                       
										                                     
																		<c:if test="${!empty referDetails}">			
																				
																				<div id="table">
																					
																					<div class="header-row1 row1">
																				    <span class="cell primary" style="width:6% ! important;">Sl no.</span>
																				    <span class="cell" style="width:25% ! important;">Refereed Name</span>
																				     <span class="cell" style="width:29% ! important;">Refereed Email id</span>
																				    <span class="cell" style="width:20% ! important;">Refereed Date</span>
																				    <span class="cell" style="width:10% ! important;">Status</span>
																				    <span class="cell" style="width:10% ! important;">View Status</span>
																				  </div>
																				
																			    </div>	
																		</c:if>		  
																			 <%int i=1; %>        
																				            
															                   <c:if test="${!empty referDetails}">	
								
																					<c:forEach items="${referDetails}" var="det">	
																					
																							 <c:set var="reference_status"  value="${det.reference_status}"/>
																							 <%String reference_status = (String)pageContext.getAttribute("reference_status");%>
										                                                         
										                                                      
										                                                         
								                                                       <%if(reference_status.equals("Success")) {%>
								                                                         
								                                                              <div id="table">   
								                                                              
													                                              <div style="background-color:#e8f6e4;" class="row1">
													                                                  
													                                                    <span class="cell primary slNo" data-label="Vehicle"><%=i++%></span>
																								         <span class="cell referName" data-label="Exterior" >${det.referee_name}</span>
																								         <span class="cell referEmailId" data-label="Interior" > ${det.referee_emailid}</span>
																								         <span class="cell referDate" data-label="Engine" >${det.reference_given_date}</span>
																								         <span class="cell status" data-label="Trans" >${det.reference_status}</span>
																								         <span class="cell viewStatus" data-label="Trans"><a href="#" id="button${det.reference_id}" onclick="viewStatus('${det.reference_id}')">View Status</a></span>																				    
																						    
																								  </div>
																							      
																			                  </div>
																			                  
																			                  <div id="statusOf${det.reference_id}" class="StatusDiv" style="display:none;"></div>
																			                   	
																			                  
													                                     <%}else{ %>
													                                     
													                                         <div id="table">   
													                                         
													                                             <div class="row1">
													                                             
													                                                     <span class="cell primary slNo"  data-label="Vehicle" style="width:6% ! important;"><%=i++%></span>
																								         <span class="cell referName" data-label="Exterior" style="width:25% ! important;">${det.referee_name}</span>
																								         <span class="cell referEmailId" data-label="Interior" style="width:29% ! important;	text-overflow: ellipsis;overflow: hidden;"> ${det.referee_emailid}</span>
																								         <span class="cell referDate" data-label="Engine" style="width:20% ! important;">${det.reference_given_date}</span>
																								         <span class="cell status" data-label="Trans" style="width:10% ! important;">${det.reference_status}</span>
																								         <span class="cell viewStatus" data-label="Trans" style="width:10% ! important;"><a href="#"></a></span>																				    
																						    
																								  </div>
																							      
																			                  </div>
																			                  
																			                  
													                                     <%} %>   
													                                     
																				  
																								         
																			    </c:forEach>
																			
																			
																			</c:if>	  
																				 
																		
																		<c:if test="${empty referDetails}">	
																		
																		      <div class="docs-example">
																	                <ul>
																	                    <li><p class="text-info" style="color:red;">As of now you didn't refered any one.</p></li>                                          
																	                    <li><p class="text-info" style="color:red;">Try to refer as much as friends to join the venture and get the benefits of it.</p></li>
																	                </ul>                                       
																	          </div>
																		
																		</c:if>
																		
						                                       
						                            </div>
						                                          
						                                    
						                                    <div id="friends" class="tab-pane fade">				                                        
						                                        <div class="module-body">
						                                            
						                                                  <div id="refPolicyStatusDiv" class="bonusDivStyle"></div>
						                                            
						                                        </div>
						                                    </div>
				                                  
			                                </div>
                                
                                
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

		
        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
        <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="profileSetting.html";
		}
		
		function refresh(url)
		{
			window.location = url;
		}
		
		</script>
         
         
    </body>

</html>