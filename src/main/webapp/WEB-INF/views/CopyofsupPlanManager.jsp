<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Plan Management</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
    
    
    <style>
    .errorStyle
    {
    color:red;
    display:none;
    }
    
    </style>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>



<body>
    <div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
          </li>
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
    <!-- /navbar -->
    <div class="subnavbar">
        <div class="subnavbar-inner">
            <div class="container">
                
                <ul class="mainnav">
			
					<li ><a href="myAdmin.html"><i class="icon-user-md"></i><span>My Admin</span> </a> </li>
					<li ><a href="myProduct.html"><i class="icon-gift"></i><span>My Product</span> </a> </li>
					<li><a href="myvendor.html"><i class="icon-group"></i><span>My Vendor</span> </a></li>
					<li><a href="costmanage.html"><i class="icon-bar-chart"></i><span>Cost Management</span> </a> </li>
					<li class="active"><a href="planmanage.html"><i class="icon-th-list"></i><span>Plan Management</span> </a> </li>
					<li><a href="supadminsettings.html"><i class="icon-cog"></i><span>Settings</span> </a> </li>
					
					<li><a href="businessStatus.html"><i class="icon-time"></i><span>Business Status</span> </a> </li>
					
			   </ul>
			   
            </div>
            <!-- /container -->
        </div>
        <!-- /subnavbar-inner -->
    </div>
    <!-- /subnavbar -->
    <div class="main">
        <div class="main-inner">
            <div class="container">
                
               	
	     
		 <div class="widget-header">
	      				<i class="icon-book"></i>
	      				<h3>Plan Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active" onclick="gotoHome()">
						    <a data-toggle="tab" href="#" >Plan A(Double at 5 years scheme)</a>
						  </li>
						  <li class="" onclick="showPlanB()"><a data-toggle="tab" href="#jscontrols">Plan B(NO double- only insentive)</a></li>
						  <li class="" onclick="showPlanC()"><a data-toggle="tab" href="#jscontrols1">Plan C(HYBRID)</a></li>
						</ul>
						
						<br>
						
							<div class="tab-content">
								
								<div id="formcontrols" class="tab-pane active">
								
										<form class="form-horizontal" name="planAForm" id="planAForm">
											
												<fieldset>
												
												<%String plan_name = "";
												  int plan_id = 0;
												%>
												
												
												<c:if test="${!empty businessPlan}">	
												
													<c:forEach items="${businessPlan}" var="det">	
																				
																<c:set var="plan_name"  value="${det.plan_name}"/>
			                                                    <%plan_name = (String)pageContext.getAttribute("plan_name");%>
			
			                                                    
			                                                
			                                                     <%if(plan_name.equals("Plan A")){%>
			                                               
				                                                    <c:set var="plan_id"  value="${det.plan_id}"/>
				                                                    <%plan_id = (Integer)pageContext.getAttribute("plan_id");%>
				
			
																	<div class="control-group">																										
							                                            <label for="username" class="control-label"  style="width: 230px ! important;">Policy Amount after 5 year</label>
																		<div class="controls">
																			<input type="text" value="${det.plan_policy_amount}" disabled="true" id="planValId1A" name="planVal1" class="span4">	
																			<div id="error1A" style="color:red;display:none;"></div>													
																		</div> <!-- /controls -->		
																												
																	</div> 
																	
																	<div class="control-group">											
																		<label for="username" class="control-label" style="width: 230px ! important;">Amount per each refer friend</label>
																		<div class="controls">
																			<input type="text"  value="${det.plan_amount_refer_per_friend}"  disabled="true" id="planValId2A"  name="planVal2" class="span4">
																			<div id="error2A" style="color:red;display:none;"></div>															
																		</div> <!-- /controls -->			
							                                            										
																	</div> <!-- /control-group -->																				
																																													
																 <br>
																 
																 <div class="form-actions">
																 
																    <input type="hidden"  name="plan_id" value="${det.plan_id}" />
																    <input type="hidden" id="planNameId" name="planName" value="Plan A" />			
																    <input type="button" class="btn btn-primary" id="AButtonId" value="Edit" onclick="saveBusinessPlan('planAForm','Plan A','A','AButtonId')"/> 
																    
																    <input type="button" class="btn btn-primary" id="addFeatureId" value="Add Feature" onclick="showFeature()"/> 
																								
																 </div> <!-- /form-actions -->
															   
			                                               <%}%>
			                                               			                                               
							                        </c:forEach>	
							                    
							                    </c:if>
							                    
												</fieldset>
												
											</form>
											
											
											
											<!-- Add Features Form -->
											
															<fieldset  >
															
															<div id="featureFormDiv" style="display:none;" class="widget widget-nopad">
													            
													            <div class="widget-header"> <i class="icon-search"></i>
													              <h3> Add Features in Plan A</h3>
													            </div>
													            
													            <!-- /widget-header -->
													           
													            <div class="widget-content">
													              
													                  <div class="control-group">											
																	
																	     <ul>
																			    
									                                          		<li class="manageLi">   
									                                          		  
																					   <div class="control-group">											
																						
																						<form id="featureForm" name="featureForm">
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">											
																								
																								<div class="controls">
																									Restrict Days <input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter the restricted days" name="noofDays" id="dayId">
																									
																									<div class="errorStyle" id="errday"></div>
																								</div> 
																							 
																							 </div>
																							 
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">	
																								
																								<div class="controls">
																									Restrict Friends <input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter the restricted Friends" name="noofFriend" id="friendId">
																									<div class="errorStyle" id="errfriend"></div>
																								</div> 
																							
																							</div>
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">	
																								
																								<div class="controls">
																									complete with in duration Bonus Amount <input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter the bonus Amount" name="bonusAmount" id="bonusAmountId">
																									<div class="errorStyle" id="errBnAmt"></div>
																								</div> 	
																							
																							</div>
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">		
																								<div class="controls">
																									complete after duration Compliment Amount<input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter Compliment Amount" name="compAmount" id="compAmountId">
																									<div class="errorStyle" id="errCmpAmt"></div>
																								</div> 	
																							
																							</div>
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">	
																								
																								<div class="controls">
																									<input type="hidden"  name="plan_id" value="<%=plan_id%>" />
																									<input type="button" onclick="addFeatures('Plan A')" value="Add" class="btn btn-primary">
																									<div class="errorStyle" id="errproductPrice"></div>
																								</div> 		
																										
																									
																							</div> 																			
																	
																	                     </form>
																	                     
																					   </div>
																	   
																	               </li>
														                   </ul>
														                   
														              </div>
														   
													            </div>
													            
													            <!-- /widget-content --> 
													          </div>
													          
				          
														</fieldset>
											
											<!-- End of Add Fatures Form -->
											
											
											
											
											
											
											
											<!-- Feature Details -->
											
											<c:if test="${!empty planFeatures}">	
												
													
											<div class="widget widget-table action-table">
										           
										            <div class="widget-header"> <i class="icon-th-list"></i>
										              
										              <h3>List of All benifits for Plan A</h3>
										            </div>
										            <!-- /widget-header -->
										            <div class="widget-content">
										              <table class="table table-striped table-bordered">
										                <thead>
										                  <tr>
										                    <th> With this days </th>
										                    <th> Add total refer friend</th>
										                    <th class="td-actions"> Complete task with in period bonus amount </th>
										                    <th class="td-actions"> Complete task after period complement amount </th>
										                    <th class="td-actions"> Delete Feature</th>
										                  </tr>
										                </thead>
										                <tbody>
										                
										                <%int condtition = 0; %>  
										               <c:forEach items="${planFeatures}" var="det">
											             
											             <c:set var="plan_id"  value="${det.plan_id}"/>
				                                          <%condtition = (Integer)pageContext.getAttribute("plan_id");%>
				
				                                          <%if(condtition==1) {%>
													  
												                  <tr>
												                    <td> ${det.feature_restrict_day}</td>
												                    <td> ${det.feature_restrict_friend} </td>
												                    <td> ${det.feature_restrict_bonus_amount} </td>
												                    <td> ${det.feature_restrict_compliment_amount} </td>
												                    <td class="td-actions"> <a class="btn btn-danger btn-small" href="javascript:;" onclick="deleteFeature('${det.feature_id}','Plan A')"><i class="btn-icon-only icon-remove"> </i></a></td>
												                  </tr>
										                  
										                  <%} %>
										                </c:forEach>
										                
										                </tbody>
										              </table>
										            </div>
										            <!-- /widget-content --> 
										          </div>
											
											
											
											
										</c:if>	
											
								</div>
								
								
								
								<!-- Div for PLAN B -->
								
								<div id="jscontrols" class="tab-pane">
										
										 <div id="planBDiv"></div>
									
								</div>
								
								
								<!-- Div Fro PLAN C -->
								
								
								<div id="jscontrols1" class="tab-pane">
									
											 <div id="planCDiv"></div>
					
								</div>
								
								
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div>
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    
	
	
    <!-- /extra -->
    <div class="footer">
        <div class="footer-inner">
            <div class="container">
                <div class="row">
                    <div class="span12"> &copy; 2015 Jireh Software Solutions Pvt Ltd</b>    All rights reserved. </div>
                    <!-- /span12 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /footer-inner -->
    </div>
    <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
    <script src="resources/Admin_Resources/js/excanvas.min.js"></script>
    <script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script>
    <script src="resources/Admin_Resources/js/bootstrap.js"></script>
    <script src="resources/Admin_Resources/js/base.js"></script>
    
    <script src="resources/Admin_Resources/js/validateForm.js"></script>
    
</body>
</html>
