<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>About Products</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
	<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
	
	
	<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">
	
	<style>
	.heading
	{
		color: #00b6f5 !important;
	    font-size: 18px;
	    font-style: italic !important;
	}
	
	</style>
	</head>
<body>

	<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
        <a href="index.html" class="over"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
      <div class="topmenu"><a href="userLogin.html">Sign In</a> | <a href="userRegister.html"> Become a member</a></div>
            <ul class="topmenu">
              <li><a href="index.html" ><span>Home</span></a></li>
              <li><a href="aboutUs.html" ><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html" class="over"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
   

	<div class="wrapper">
		<div class="container">
			
			<div class="module">
							<div class="module-head">
								<h3>About Our Products ::</h3>
							</div>
							
							
							<div class="module-body">
									
								<section class="docs">
                                    
                                    Basically we have three categories of products such are,
                                    
                                    <h3 class="heading">
                                        Agriculture Products</h3>
                                    <p>
                                        A list of terms with their associated descriptions.</p>
                                    <div class="docs-example">
                                        <dl>
                                            <dt>Description lists</dt>
                                            <dd>
                                                A description list is perfect for defining terms.</dd>
                                            <dt>Euismod</dt>
                                            <dd>
                                                Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                                            <dd>
                                                Donec id elit non mi porta gravida at eget metus.</dd>
                                            <dt>Malesuada porta</dt>
                                            <dd>
                                                Etiam porta sem malesuada magna mollis euismod.</dd>
                                        </dl>
                                       <!--  <pre class="pre"><span class="tag">&lt;dl&gt;</span>
										  </pre> -->
                                    </div>
                                    
                                    
                                    <br>
                                    
                                    
                                    <h3 class="heading">
                                        Hebal Products</h3>
                                    <p>
                                        A list of terms with their associated descriptions.</p>
                                    <div class="docs-example">
                                        <dl>
                                            <dt>Description lists</dt>
                                            <dd>
                                                A description list is perfect for defining terms.</dd>
                                            <dt>Euismod</dt>
                                            <dd>
                                                Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                                            <dd>
                                                Donec id elit non mi porta gravida at eget metus.</dd>
                                            <dt>Malesuada porta</dt>
                                            <dd>
                                                Etiam porta sem malesuada magna mollis euismod.</dd>
                                        </dl>
                                         
                                    </div>                            
									
										
									<br>
                                    
                                    
                                    <h3 class="heading" >
                                    	
                                        HomeCare Products
                                    	    
                                    </h3>
                                    
                                    
                                    <p>A list of terms with their associated descriptions.</p>
                                    
                                    <div class="docs-example">
                                        
                                        <dl>
                                            <dt>Description lists</dt>
                                            <dd>A description list is perfect for defining terms.</dd>
                                            <dt>Euismod</dt>
                                            <dd>Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
                                            <dd>Donec id elit non mi porta gravida at eget metus.</dd>
                                            <dt>Malesuada porta</dt>
                                            <dd>Etiam porta sem malesuada magna mollis euismod.</dd>
                                        </dl>
                                                                              
                                    </div>
											 	                                    
                                </section>
                                
							</div>
						</div>
			  
			  
		</div>
		
  
  <!-- End of Middle Page Content Div --> 
		
		
	</div><!--/.wrapper-->



    

<!-- footer Content Div --> 

  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 


<script src="resources/User_Resources/scripts/userOperationScript.js"></script>


<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<!-- end of footer Content Div --> 

<script>
function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location="contact.html";
}

</script>


	<script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>

