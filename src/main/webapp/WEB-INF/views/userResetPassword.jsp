<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>User Reset Password</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
	<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
		
	<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">
	
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				 
				 <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i>
                 </a>
                        
                 <a class="brand" href="index.html"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a>

				<div class="nav-collapse collapse navbar-inverse-collapse">
				
					<ul class="nav pull-right">

                        <li><a href="userLogin.html">
							Login
						</a></li>
						
						<li><a href="userRegister.html">
							Become a member
						</a></li>

						<li><a href="forgetPassword.html">
							Forgot your password?
						</a></li>
					</ul>
				</div><!-- /.nav-collapse -->
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->



	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="module module-login span4 offset4">
					
					
					<form id="resetForm" name="resetForm" class="form-vertical">
					
						<div class="module-head">
							<h3>Reset Password</h3>
						</div>
						<div class="module-body">
							
							
							<input type="hidden" id="emailId" name="regEmailId" value="<%=request.getAttribute("requestedEmailID")%>">
							<input type="hidden" id="uniqueId" name="reqUniqueId" value="<%=request.getAttribute("uniqueId")%>">
							
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="password" name="changePassword" id="password" placeholder="Enter Password">
									<div id="pwError" style="color:red;display:none;"></div>
								</div>
							</div>
							
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="password" id="confirmPassword" placeholder="Confirm Password">
									<div id="cnpwError" style="color:red;display:none;"></div>
								</div>
							</div>
							
						</div>
						<div class="module-foot">
							<div class="control-group">
								<div class="controls clearfix">
									<button type="button" class="btn btn-primary pull-right" onclick="resetUserPassword()">Submit</button>
									
								</div>
							</div>
						</div>
					</form>
					
					
				</div>
			</div>
		</div>
		
		
		<div style="border-bottom:1px solid #bbb;margin-top:47px;"></div>
		
		<!-- Middle  Page Content Div --> 
  
  
	  <div id="featurezone" style="margin-top:40px !important;">
	  
	  	<div class="featurecol">
			<div class="featurecoltop">
			  <div class="featurecolbottom">
					<img src="resources/indexResources/images/quickregister.png" alt="">
					<h1>Quick Register</h1>
				    <span class="subheading">Nemo enim ipsam voluptatem</span>
					<p>Register,Shop, save, and get Products...</p>
					<div class="readmoreimg"></div>
			  </div>
			</div>
		</div>
		
		<div class="featurecol">
			<div class="featurecoltop">
				<div class="featurecolbottom">
					<img src="resources/indexResources/images/beapartner.png" alt="">
					<h1>Be A partner</h1>
					<span class="subheading">Nemo enim ipsam voluptatem</span>
					<p>Join an affiliate network that pays out more than what you expect...</p>
					<div class="readmoreimg"></div>
				</div>
			</div>
		</div>
		
		<div class="featurecol">
			<div class="featurecoltop">
				<div class="featurecolbottom">
					<img src="resources/indexResources/images/safepayment.png" alt="">
					<h1>Safe Payment</h1>
					<span class="subheading">Nemo enim ipsam voluptatem</span>
					<p>Pay with the world�s most popular and secure payment methods...</p>
					<div class="readmoreimg"></div>
				</div>
			</div>
		</div>
	   
		<div class="featurecol_last">
			<div class="featurecoltop">
				<div class="featurecolbottom">
					<img src="resources/indexResources/images/greatvalue.png" alt="">
					<h1>Great Value</h1>
					<span class="subheading">Nemo enim ipsam voluptatem</span>
					<p>We offer competitive prices on our 100 million plus product range...</p>
					<div class="readmoreimg"></div>
				</div>
			</div>
	    </div>
	    
	  </div>
	  
  
  <!-- End of Middle Page Content Div --> 
		
		
	</div><!--/.wrapper-->



    

<!-- footer Content Div --> 


<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 




<!-- end of footer Content Div --> 

<script src="resources/indexResources/scripts/userRegistrationScript.js"></script>


<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<!-- end of footer Content Div --> 

<script>
function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location="userLogin.html";
}

</script>



	<script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>