<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buy Packet</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
   
   <link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">
   
   
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body onload="displayProducts('Agriculture')">
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html" ><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="buyMorePackets.html" class="sideMenuActive"><i class="icon-gift"></i> Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html"><i class=" icon-random"></i> Track Order </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                    <h3>
                                       Order CheckOut
                                       
                                       
                                        <a href="#" onclick="showCartProducts()"> 
								                  <div class="innertopblock2" >
														<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
													 	<p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong></p>
												  </div>					  	
										   </a>
                                       
                                    </h3>
                                    
                                    
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">


									<form action="buypayOnline.html" method="post" name="frm" id="theForm" autocomplete="off" >
					                   
									                    
										<div class="headDiv"><h4 class="textStyle">Check Out</h4> </div>
									                                                     
                                 			<div class="checkOutBill">                   
                   								                          
									               <div class="pricelist">
									                    
									                   
									                     <%String paymentMode = (String)session.getAttribute("paymentMode"); %>
									                     
										                   <%String fullName = ""; %>
										                   <%String emailId = ""; %>
										                   <%String mobileNo = ""; %>
										                   <%String shAddress = ""; %>
		                   
									                      				
															 <c:forEach items="${userBasicInformation}" var="det">	 
																			
																	<c:set var="user_name"  value="${det.user_name}"/>
											 						<%fullName = (String)pageContext.getAttribute("user_name");%>
											 
											 						<c:set var="user_emailid"  value="${det.user_emailid}"/>
											 						<%emailId = (String)pageContext.getAttribute("user_emailid");%>
											                        
											                        <c:set var="user_mobile_no"  value="${det.user_mobile_no}"/>
											 						<%mobileNo = (String)pageContext.getAttribute("user_mobile_no");%>
											                        
											                        <c:set var="user_shipping_address"  value="${det.user_shipping_address}"/>
											 						<%shAddress = (String)pageContext.getAttribute("user_shipping_address");%>
											                        											 						
											                        <table width="800" align="center" style="padding-bottom: 15px;">
											                        <tr class="rowStyle">   
											                         <td class="rightHeading"><b>Name</b><td>     
											                         <td>:<td>       
											                         <td><i>${det.user_name}</i><td>
											                         <input type="hidden" name="fullname" value="${det.user_name}" />
											                        </tr>
											                          <tr class="rowStyle">
											                          <td class="rightHeading"><b>Email Id</b><td>     
											                         <td>:<td>       
											                         <td><i>${det.user_emailid}</i><td>
											                         <input type="hidden" name="emailId" value="${det.user_emailid}" />
											                        </tr>
											                         <tr class="rowStyle">
											                          <td class="rightHeading"><b>Mobile No</b><td>     
											                         <td>:<td>       
											                         <td>+91 &nbsp; <i>${det.user_mobile_no}</i><td>
											                         <input type="hidden" name="mobileNo" value="${det.user_mobile_no}" />
											                        </tr>
											                         <tr class="rowStyle">
											                          <td class="rightHeading"><b>Shipping Address</b><td>     
											                         <td>:<td>       
											                         <td><i>${det.user_shipping_address}</i><td>	
											                         <input type="hidden" name="shAddress" value="${det.user_shipping_address}" />			                         
											                        </tr>
											                        <tr class="rowStyle">
											                          <td class="rightHeading"><b>Description</b><td>     
											                         <td>:<td>       
											                         <td style="text-align:justify;"><i> As per the interested product you have selected in product selection section business will allocate 
											                         								  a <b>KIT</b> to you where it will be a combination of your selected product.</i>
																	 <td>
											                        </tr>
											                        <tr class="rowStyle">
											                         <td class="rightHeading"><b>Delivery Report</b><td>     
											                         <td>:<td>       
											                         <td style="text-align:justify;"><i> As soon as possible with in two or less then a week. This will notify to your email id and contact no.</i>
											                         
																	 <td>
											                        </tr>
											                        </table>
									                        
									                        
									                        </c:forEach> 
									                        
									                             <input type="hidden" name="userId" value="<%=session.getAttribute("regUserId")%>" />
									                             <input type="hidden" name="paymentMode" value="<%=paymentMode%>" />
									                             
									                             <input type="hidden" name="kitcharge" value="<%=request.getAttribute("kitCharge")%>" />
									                             <input type="hidden" name="vat" value="<%=request.getAttribute("vat")%>" />
									                             <input type="hidden" name="vatPersentageVal" value="<%=request.getAttribute("vatPersentageVal")%>" />
									                             <input type="hidden" name="subtotal" value="<%=request.getAttribute("subtotal")%>" />
									                             <input type="hidden" name="regCharge" value="<%=request.getAttribute("regCharge")%>" />
									                             <input type="hidden" name="total" value="<%=request.getAttribute("total")%>" />
									                             
									                      
									                         
									                     <div id="pricelist">
		                        
										                        <table width="300" align="right">
										                        <tr>   
										                         <td style="width:155px;color: #00b6f5;font-style: italic;">KIT Amount<td>   
										                         <td>:</td>        
										                         <td><%=request.getAttribute("kitCharge")%><td>
										                        </tr>
										                          <tr>
										                           <td style="width:155px;color: #00b6f5;font-style: italic;">VAT (<%=request.getAttribute("vat")%>%)<td>
										                           <td>:</td>       
										                          <td><%=request.getAttribute("vatPersentageVal")%><td>
										                        </tr>
										                         <tr>
										                           <td style="width:155px;color: #00b6f5;font-style: italic;">Sub Total<td>
										                           <td>:</td>       
										                          <td><%=request.getAttribute("subtotal")%><td>
										                        </tr>
										                        
										                        <tr>
										                          <td style="width:155px;color: #00b6f5;font-style: italic;">Registration Charge<td>
										                          <td>:</td>       
										                          <td><%=request.getAttribute("regCharge")%><td>
										                        </tr>	                        
										                        
										                         <tr >
										                          <td class="border"><strong>Total </strong><td><td>:</td>       
										                          <td class="border"><strong><%=request.getAttribute("total")%></strong><td>
										                        </tr>
										                        </table>
										                    </div>
									                    
									                    
									                    </div>
							               
												               
												               
												            <div class="controls payButton">	
						
															    <input type="hidden" name="V3URL" value="https://secure.ebs.in/pg/ma/payment/request" />							 
																<input type="hidden" id="account_id" name="account_id" value="17663"/> 							
																<input type="hidden" id="channel" name="channel" value="0"/>  <!-- <option value="0">Standard</option><option value="2">Direct</option>-->							
															    <input type="hidden" id="currency" name="currency" value="INR" /> 						    
															    <input type="hidden" id="return_url" value="http://182.18.162.221:8080/Project788/userBuyPacketOnlineRedirect.html" name="return_url" /> 
														        <input id="mode" name="mode" type="hidden" value="TEST" />
																<input id="algo" name="algo" type="hidden" value="MD5" />							 
															    <input type="hidden" id="reference_no" name="reference_no" value="<%=(String)session.getAttribute("orderId")%>" />						
															    <input type="hidden" id="amount" name="amount" value="<%=request.getAttribute("total")%>" />						   
															    <input id="description" name="description" type="hidden" value="Test Transaction" /> 						   
															    <input id="name" name="name" type="hidden" value="<%=fullName%>" />						   
															    <input id="address" name="address" type="hidden" value="a123456" />						   
															    <input id="city" name="city" type="hidden" value="a123456" />						    
															    <input id="state" name="state" type="hidden" value="a123456" /> 						    
															    <input id="postal_code" name="postal_code" type="hidden" value="123456" />
															    <input id="postal_code" name="country" type="hidden" value="IND" />							
															    <input id="email" name="email" type="hidden" value="<%=emailId%>" /> 						   
															    <input id="phone" name="phone" type="hidden" value="<%=mobileNo%>" />						   
															    <input id="ship_name" name="ship_name" type="hidden" value="a123456" />						   
															    <input id="ship_address" name="ship_address" type="hidden" value="a123456" />						   
															    <input id="ship_city" name="ship_city" type="hidden" value="a123456" />						   
															    <input id="ship_state" name="ship_state" type="hidden" value="a123456" />
															    <input id="ship_country" name="ship_country" type="hidden" value="IND" />						    
															    <input id="ship_phone" name="ship_phone" type="hidden" value="<%=mobileNo%>" />						
															    <input id="ship_postal_code" name="ship_postal_code" type="hidden" value="123456" /> 
															 
															    <input id="sf-next"  class="btn" name="submit" value="Pay Now" type="submit" />&nbsp;
															 		
													       </div>							
							              
							              </div>
							
							         </form>
							 
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                    </div>
                    
                    
                    
                   								    <input  type="hidden" id="agriProdId" name="agricultureProduct" value="<%=request.getAttribute("agriId")%>"  />
						      						<input  type="hidden" id="herbalProdId" name="herbalProduct" value="<%=request.getAttribute("herbalId")%>" />
						      						<input  type="hidden" id="hmCareProdId" name="homecareProduct" value="<%=request.getAttribute("hmCareId")%>" />
						       
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
      
       <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<div id="cartDetailsPopUp" style="display:none;"></div>
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="buyMorePackets.html";
		}
		
		</script>
         
         
    </body>

</html>