<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Accounts</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">

</head>

<body onload="actimeMenu('menu1')">


<div> <%@include file="accountAdminHeader.jsp" %> </div>


<div class="main">
  <div class="main-inner">
    <div class="container">
                  
                  
  				    <div class="widget-header">
	      				<i class="icon-credit-card"></i>
	      				<h3>Account Management</h3>
	  				</div>
					
					<div class="widget-content">
					
						<div class="tabbable">
							
							<ul class="nav nav-tabs">
							  <li class="active">
							    <a data-toggle="tab" href="#formcontrols">Manage Transaction</a>
							  </li>
							  <!-- <li class="" onclick="showAdminDetails()"><a data-toggle="tab" href="#jscontrols" >Manage Transaction</a></li> -->
							</ul>
						
						<br>
						
							<div class="tab-content">
						
						    <div id="formcontrols" class="tab-pane active">
								
								
								<form class="form-horizontal" id="profileForm" name="profileForm">
								
									<fieldset>
										
										<div class="control-group">											
											<label for="username" class="control-label">Transaction Id</label>
											<div class="controls">
												<input type="text" name="transactionInfo" id="tranId" placeholder="Please enter transaction id" class="span4" />	
												<div id="errfullName" class="errorStyle"></div>											
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																				
										
										 <br>
										
											
										<div class="form-actions">
										    <input type="button" class="btn btn-primary" value="Search" onclick="searchTransactionDetails()"/> 
											
										</div> <!-- /form-actions -->
										
									</fieldset>
									
								</form>
								
								
								<fieldset>
				
				                         <div id="updateTransactionDiv"></div>
		
								</fieldset>
									
									
								<fieldset>
							
				                         <div id="transactionListDiv"></div>
						
								</fieldset>
									
									
									
							</div>
							
						
					<!-- Remove Admin Division  -->
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="edit-profile2">
										<fieldset>
										
				                              <div id="adminDetailsDiv"></div>
											
										</fieldset>
									</form>
								</div>
								
					<!-- End of Remove Admin Division  -->
				
								
							</div>			  
						  
						</div>
						
					</div>
					
					
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div> <%@include file="supAdminFooter.jsp"%> </div>


<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/subAdminValidateForm.js"></script>


</body>
</html>
