<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search User Cheque</title>
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>



<body>


<fieldset>
										
										<div class="widget widget-table action-table">
								            
								            <div class="widget-header"> <i class="icon-th-list"></i>
								              <h3>Records from your Search criteria</h3>
								            </div>
								            <!-- /widget-header -->
								            <div class="widget-content">
								              
								              
											            <c:if test="${!empty bonusDetail}">	
																						  
													              <table class="table table-striped table-bordered">
													               
													                <thead>
													                  
													                  <tr>
													                    <th> SL No </th>
													                    <th> Full Name </th>
													                    <th> Shipping Address </th>
													                    <th> Email Id </th>
													                    <th> Mobile No </th>
													                    <th> Generated Date </th>
													                    <th> Cheque Amount <a>(WithOut TDS)</a> </th>
													                    <th> Cheque Status </th>
													                    <th class="td-actions"> Update </th>
													                  </tr>
													                  
													                </thead>
													                
													                <tbody>
													                
													                <%int i=1; %>
														                
														                <c:forEach items="${bonusDetail}" var="tran">	
													
													                     <!-- style="background-color:#e8f6e4;" -->
															                     
															                    <c:set var="givenId"  value="${tran.user_id}"/>
				 																 <%int givenId = (Integer)pageContext.getAttribute("givenId");%>
				 
				 
															                     <c:forEach items="${userBasicDetails}" var="basic">
															                     
															                      		<c:set var="userId"  value="${basic.user_id}"/>
				 																 		<%int userId = (Integer)pageContext.getAttribute("userId");%>
				 
						                                                                         <%if(userId==givenId) {%>
																					                
																					                <c:set var="checkStatus"  value="${tran.policy_status}"/>
						 																 		    <%String checkStatus = (String)pageContext.getAttribute("checkStatus");%>
						 																				
						 																			 
						 																			
																					                            <tr id="rowId">
																							                  
																							                    <td> <%=i++%> </td>
																							                    <td> ${basic.user_name} </td>
																							                    <td> ${basic.user_shipping_address} </td>
																							                    <td> ${basic.user_emailid} </td>
																							                    <td> ${basic.user_mobile_no} </td>
																							                    <td> ${tran.policy_end_date} </td>
																							                    <td> Rs.${tran.policy_amount}.00 </td>
																							                    
																							                    <td> 
																							                         
								 																				       <%if(checkStatus.equals("Success")) {%>  
								 																				       	   
								 																				       	   <p style="color:yellowgreen;"><%=checkStatus%></p>

								 																					   <%}else{ %>
								 																					     
								 																					     <select style="width:80px;" id="checkStatus${tran.policy_id}" disabled="disabled">
								 																					   
								 																					      	<option selected value="Pending">Pending</option>
								 																					      	<option value="Success">Success</option>
								 																					     
								 																					     </select>
								 																					      
								 																					   <%} %>   
								 																					  
								 																				</td>
																							                    
																							                    <td class="td-actions" >
																							                          
																							                          <%if(!checkStatus.equals("Success")) {%>
																							                          
																							                           	<a class="btn btn-small btn-success" title="Edit the Cheque status" href="javascript:;" onclick="editPolicyBonusCheckStatus('${tran.policy_id}')"><i id="iconId${tran.policy_id}" class="btn-icon-only icon-edit"> </i></a>
																							                          
																							                          <%}else{%>
																							                          
																							                            <p style="color:yellowgreen;">Done</p>
																							                            
																							                          <%} %>
																							                         
																							                         </td>
																							                  
																							                  </tr>
																			                         
																			                        
																                          <%} %>
																                          
																                 </c:forEach>
																                 
														                </c:forEach>
													                
													                </tbody>
													                
													              </table>
								              				
								              	      </c:if>	
								              
								                      <c:if test="${empty bonusDetail}">	
								              
								                      		<p style="color:red;"> As per the search criteria no data found.</p>
								                      
								                      </c:if>
								                      
								                      
								            </div>
								            <!-- /widget-content --> 
								          </div>
          
										
									</fieldset>

</body>
</html>