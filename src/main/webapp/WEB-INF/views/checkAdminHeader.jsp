<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cheque Admin Header</title>
</head>

<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Cheque Admin Console </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i 
                            class="icon-user"></i> Cheque Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutCheAdmin()">Logout</a></li>
            </ul>
            
          </li>
          
        </ul>
        
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
     
           <ul class="mainnav">
			
				<li id="menu1"><a href="checkAdmin.html"><i class="icon-shopping-cart"></i><span>Offline Transaction Cheques</span> </a> </li>
				<li id="menu2"><a href="referFriendCheck.html"><i class="icon-user"></i><span>Refer Friend Cheques</span> </a> </li>
				<li id="menu3"><a href="referFriendBonusCheck.html"><i class="icon-group"></i><span>Refer Friend Bonus Cheques</span> </a> </li>
				<li id="menu4"><a href="userPlanPolicyCheck.html"><i class="icon-list-alt "></i><span>User Plan Policy Cheques</span> </a> </li>
				<li id="menu5"><a href="checkReports.html"><i class="icon-table"></i><span>Reports</span> </a> </li>
				
			</ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->

</body>
</html>