<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CheckOut</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">

<link href="resources/indexResources/css/demo.css" rel="stylesheet">
<link href="resources/indexResources/css/stepsForm.css" rel="stylesheet">
<script src="resources/indexResources/scripts/jquery-2.1.1.min.js"></script>


<link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body  onload="choosePaymentOption()">

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
       <a href="index.html" class="over"><img src="resources/indexResources/images/logo.png" width="287" height="55"></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
      
      <div class="topmenu"> | <a href="userLogin.html">Sign In</a>  
            
	      <%-- <a href="#" onclick="showCartProducts()"> 
	                  <div class="topblock2" >
							<img src="resources/User_Resources/images/shopping.gif" alt="" width="24" height="24" class="shopping" />
						 	<p>Shopping cart</p> <p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong> <span>items</span></p>
					  </div>					  	
		  </a>  --%>
		  
		  <a href="#" onclick="showCartProducts()"> 
                 <div class="topblock2" >
					<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
				 	<p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong></p>
			  </div>					  	
	     </a>
        
      </div>          
            
            <ul class="topmenu">
              <li><a href="index.html"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
 
	
	
    <div class="container">
    	
        
        <!--STEPS FORM START ------------ -->
        <div class="stepsForm">
            
            
          
            
            
                <div class="sf-steps">
                    <div class="sf-steps-content">
                    	<div>
                        	<span>1</span> Basic Information
                        </div>
                        <div>
                        	<span>2</span> Product Selection
                        </div>
                        <div>
                        	<span>3</span> Add Reference
                        </div>
						 <div>
                        	<span>4</span> Plan Selection
                        </div>
						 <div class="sf-active" >
                        	<span>5</span> Payment
                        </div>
						
                    </div>
                </div>                
                <div class="sf-steps-form sf-radius"> 
                   
                   
                  <div class="checkOutBill"> 
                   
                    
		                    <div class="pricelist">
		                    <h2 style="border-bottom:1px solid #ccc;padding-bottom: 5px;">Check Out</h2>
		                       
		                   <%String paymentMode = (String)session.getAttribute("paymentMode"); %>
		                   
		                   <form id="orderDetailForm" name="orderDetailForm" method="post">
		                   
		                       				
								<c:forEach items="${userBasicInformation}" var="det">	
															
				                        <table width="800" align="center" style="padding-bottom: 15px;">
				                        <tr class="rowStyle">   
				                         <td class="rightHeading"><b>Name</b><td>     
				                         <td>:<td>       
				                         <td><i>${det.user_name}</i><td>
				                         <input type="hidden" name="fullname" value="${det.user_name}" />
				                        </tr>
				                          <tr class="rowStyle">
				                          <td class="rightHeading"><b>Email Id</b><td>     
				                         <td>:<td>       
				                         <td><i>${det.user_emailid}</i><td>
				                         <input type="hidden" name="emailId" value="${det.user_emailid}" />
				                        </tr>
				                         <tr class="rowStyle">
				                          <td class="rightHeading"><b>Mobile No</b><td>     
				                         <td>:<td>       
				                         <td>+91 &nbsp; <i>${det.user_mobile_no}</i><td>
				                         <input type="hidden" name="mobileNo" value="${det.user_mobile_no}" />
				                        </tr>
				                         <tr class="rowStyle">
				                          <td class="rightHeading"><b>Shipping Address</b><td>     
				                         <td>:<td>       
				                         <td><i>${det.user_shipping_address}</i><td>	
				                         <input type="hidden" name="shAddress" value="${det.user_shipping_address}" />			                         
				                        </tr>
				                        <tr class="rowStyle">
				                          <td class="rightHeading"><b>Description</b><td>     
				                         <td>:<td>       
				                         <td style="text-align:justify;"><i> As per the interested product you have selected in product selection section business will allocate 
				                         								  a <b>KIT</b> to you where it will be a combination of your selected product.</i>
										 <td>
				                        </tr>
				                        <tr class="rowStyle">
				                         <td class="rightHeading"><b>Delivery Report</b><td>     
				                         <td>:<td>       
				                         <td style="text-align:justify;"><i> As soon as possible with in two or less then a week. This will notify to your email id and contact no.</i>
				                         
										 <td>
				                        </tr>
				                        </table>
		                        
		                        
		                          </c:forEach>
		                        
		                        
		                             <input type="hidden" name="userId" value="<%=session.getAttribute("regUserId")%>" />
		                             <input type="hidden" name="paymentMode" value="<%=paymentMode%>" />
		                             
		                             <input type="hidden" name="kitcharge" value="<%=request.getAttribute("kitCharge")%>" />
		                             <input type="hidden" name="vat" value="<%=request.getAttribute("vat")%>" />
		                             <input type="hidden" name="vatPersentageVal" value="<%=request.getAttribute("vatPersentageVal")%>" />
		                             <input type="hidden" name="subtotal" value="<%=request.getAttribute("subtotal")%>" />
		                             <input type="hidden" name="regCharge" value="<%=request.getAttribute("regCharge")%>" />
		                             <input type="hidden" name="total" value="<%=request.getAttribute("total")%>" />
		                             
		                        
		                      </form>
		                      
		                         
		                     <div id="pricelist">
		                        
		                        <table width="300" align="right">
		                        <tr>   
		                         <td style="width:155px;color: #00b6f5;font-style: italic;">KIT Amount<td>   
		                         <td>:</td>        
		                         <td><%=request.getAttribute("kitCharge")%><td>
		                        </tr>
		                          <tr>
		                           <td style="width:155px;color: #00b6f5;font-style: italic;">VAT (<%=request.getAttribute("vat")%>%)<td>
		                           <td>:</td>       
		                          <td><%=request.getAttribute("vatPersentageVal")%><td>
		                        </tr>
		                         <tr>
		                           <td style="width:155px;color: #00b6f5;font-style: italic;">Sub Total<td>
		                           <td>:</td>       
		                          <td><%=request.getAttribute("subtotal")%><td>
		                        </tr>
		                        
		                        <tr>
		                          <td style="width:155px;color: #00b6f5;font-style: italic;">Registration Charge<td>
		                          <td>:</td>       
		                          <td><%=request.getAttribute("regCharge")%><td>
		                        </tr>	                        
		                        
		                         <tr >
		                          <td class="border"><strong>Total </strong><td><td>:</td>       
		                          <td class="border"><strong><%=request.getAttribute("total")%></strong><td>
		                        </tr>
		                        </table>
		                    </div>
		                    
		                    
		                    </div>
               
					
                           
	                            <div class="paybutton">	
				                    <button id="sf-next" type="button" class="sf-button" onclick="proceedForOfflinePayment()">Finish</button>
				                </div>
	                          
	                </div>
	                
	               
	               
                </div>
                
               
                
           
        </div>
        <!--STEPS FORM END -------------- -->
                
                
    </div>
       
       
      						  <input  type="hidden" id="agriProdId" name="agricultureProduct" value="<%=request.getAttribute("agriId")%>"  />
						      <input  type="hidden" id="herbalProdId" name="herbalProduct" value="<%=request.getAttribute("herbalId")%>" />
						      <input  type="hidden" id="hmCareProdId" name="homecareProduct" value="<%=request.getAttribute("hmCareId")%>" />
						       
    
    
  
<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 




<script src="resources/indexResources/scripts/userRegistrationScript.js"></script>

<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<div id="cartDetailsPopUp" style="display:none;"></div>

<script>

function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location = "userReturnProduct.html";
}

</script>

</body>

</html>