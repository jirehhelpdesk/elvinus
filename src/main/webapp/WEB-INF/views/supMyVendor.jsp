<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>My Vendors</title>
    <link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
    
    <link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
    
    
    <link href="js/guidely/guidely.css" rel="stylesheet"> 

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
  </head>

<body onload="actimeMenu('menu3')">

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="#">Super Admin Console </a>
      
      <div class="nav-collapse">
      
        <ul class="nav pull-right">
          
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> Super Admin <b class="caret"></b></a>
            <ul class="dropdown-menu">            
              <li><a href="#" onclick="logoutAdmin()">Logout</a></li>
            </ul>
            
          </li>
          
        </ul>
        
      </div>
      
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
 
<div> <%@include file="supAdminHeader.jsp" %> </div> 
    
<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	          
			  
			  <div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Vendor Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols">Search Vendor</a>
						  </li>
						  
						</ul>
						
						<br>
						
							<div class="tab-content">
								<div id="formcontrols" class="tab-pane active">
								<form class="form-horizontal" id="edit-profile">
									<fieldset>
										
										
										<div class="control-group">											
											<label for="username" class="control-label">Search Vendor Via</label>
											<div class="controls">
											    
											    <select id="searchTypeId" name="search_category" onchange="searchVendor(this.value)">
												        <option value="Select Search Type">Select Search Via</option>
														<option value="Id">Vendor Id</option>
														<option value="Name">Vendor Name</option>
														<option value="emailid">Vendor Email id</option>
												</select>
																				
												<input type="text" id="vendorId" class="span4">												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																				
										
										<div class="form-actions">
											<input type="button" value="Search" class="btn btn-primary" onclick="getVendorSearchResult()"/>									
										</div> <!-- /form-actions -->
										
									</fieldset>
									
									<fieldset>
											
											<div id="vendorViewDetailsDiv"></div>
											
									</fieldset>
										
										
									
									<fieldset>
											
											<div id="vendorDetailsDiv"></div>
											
									</fieldset>
										
										
								</form>
								
								</div>
															
							</div>
						  
						  
						</div>
						
					</div>
					
	    </div> <!-- /container -->
    
	</div> <!-- /main-inner -->
	    
</div> <!-- /main -->
    
 
<div> <%@include file="supAdminFooter.jsp"%> </div>
    

<!-- javascript
==================================================
-->
 
<!-- Placed at the end of the document so the pages load faster -->

<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script>
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script src="resources/Admin_Resources/js/base.js"></script>
<script src="resources/Admin_Resources/js/guidely/guidely.min.js"></script>
<script src="resources/Admin_Resources/js/validateForm.js"></script>


</body>


</html>
