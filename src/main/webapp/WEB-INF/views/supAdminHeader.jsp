<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Super Admin Header</title>
</head>
<body>

<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
     
           <ul class="mainnav">
				
				<li id="menu1"><a href="myAdmin.html"><i class="icon-user-md"></i><span>My Admin</span> </a> </li>
				<li id="menu2"><a href="myProduct.html"><i class="icon-gift"></i><span>My Product</span> </a> </li>
				<li id="menu3"><a href="myvendor.html"><i class="icon-group"></i><span>My Vendor</span> </a></li>
				<li id="menu4"><a href="costmanage.html"><i class="icon-bar-chart"></i><span>Cost Management</span> </a> </li>
				<li id="menu5"><a href="planmanage.html"><i class="icon-th-list"></i><span>Plan Management</span> </a> </li>				
				<li id="menu6"><a href="supadminsettings.html"><i class="icon-cog"></i><span>Settings</span> </a> </li>
				
				<li id="menu7"><a href="businessStatus.html"><i class="icon-time"></i><span>Business Status</span> </a> </li>
				
			</ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>


</body>
</html>