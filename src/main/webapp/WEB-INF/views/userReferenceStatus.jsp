<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>user Reference Status</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>



<body>

<%String checkStatus = ""; %>

                      <div style="min-height:auto ! important;" class="refercheckOutBill">                   
                   								                          
								<div style="min-height: 260px !important;" class="referpriceList">
									                  
									         <form id="orderDetailForm" name="orderDetailForm" method="post">
									                   				
											                        <table width="704" align="center" style="padding-bottom: 15px;">
											                        <tbody>
											                 
											                         
													                          <c:forEach items="${eachreferDetails}" var="refer">	
													                          
																                         <tr class="rowStyle">
																                          <td class="rightHeading"><b>Refereed Name</b></td><td>     
																                         </td><td>:</td><td>       
																                         </td><td><i>${refer.referee_name}</i></td><td>
																                         
																                        </td></tr>
																                         <tr class="rowStyle">
																                         <td class="rightHeading"><b>Refereed Date</b></td><td>     
																                         </td><td>:</td><td>       
																                         </td><td><i>${refer.reference_given_date}</i></td><td>	
																                         			                         
																                        </td></tr>
																                        
																                        <tr class="rowStyle">
																                         <td class="rightHeading"><b>Join Date</b></td><td>     
																                         </td><td>:</td><td>       
																                         </td><td><i>${refer.reference_accept_date}</i></td><td>											                            
																                        </td></tr>
																                        
																                        <c:set var="checkStatus"  value="${refer.reference_check_status}"/>
				 																		<%checkStatus = (String)pageContext.getAttribute("checkStatus");%>
				 
																                        <tr class="rowStyle">
																                         <td class="rightHeading"><b>Delivery Check Status</b></td><td>     
																                         </td><td>:</td><td>       
																                         </td>
																                         <%if(!checkStatus.equals("Success")){ %>
																                         
																                         <td><i style="color:red;"><%=checkStatus%></i></td>
																                         
																                         <%}else{ %>
																                         
																                          <td><i style="color:yellowgreen;"><%=checkStatus%></i></td>
																                          
																                         <%} %>
																                         <td>											                            
																                        </td>
																                        </tr>
																                        
													                        </c:forEach>
											                        
											                        </tbody>
											                        
											                    </table>
									                        	
									                      </form>
									                      
									                         
									                     <div id="pricelist">
		                                                         
										                        <table width="300" align="right">
										                        <tbody><tr>   
										                         <td style="width:155px;color: #00b6f5;font-style: italic;">Refer Amount</td><td>   
										                         </td><td>:</td>        
										                         <td><%=request.getAttribute("referAmount")%>.00</td><td>
										                        </td></tr>
										                          <tr>
										                           <td style="width:155px;color: #00b6f5;font-style: italic;">TDS (- <%=request.getAttribute("tds")%>)</td><td>
										                           </td><td>:</td>       
										                          <td><%=request.getAttribute("tdsAmount")%></td><td>
										                        </td></tr>
										                        
										                         <tr>
										                          <td class="border"><strong>Total </strong></td><td></td><td>:</td>       
										                          <td class="border"><strong><%=request.getAttribute("total")%></strong></td><td>
										                        </td></tr>
										                        </tbody></table>
										                    </div>
									                    
									                    
									                    </div>
							                         
								                </div>
        
                                                <%if(!checkStatus.equals("Success")){ %>
                                                <div class="docs-example">
			                                        <ul>
			                                            <li><p class="text-info">Check will be deliver to you with in 15 working days.</p></li> 
			                                            <li><p class="text-info">Refer amount is calculated as per your selected plan.</p></li>                                          	
			                                            <li><p class="text-info">For any query please drop a mail to <a style="color:red;" href="#">support@biosys.com.</a></p></li>	                                                                                   
			                                        </ul>                                       
			                                    </div>
                                                <%} %>
        
        
</body>
</html>