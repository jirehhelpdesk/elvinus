<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Plan C</title>
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

        <form class="form-horizontal" name="planCForm" id="planCForm">
											
												<fieldset>
												
												<%String plan_name = "";
												  int plan_id = 0;
												%>
												
												
												<c:if test="${!empty businessPlan}">	
												
													<c:forEach items="${businessPlan}" var="det">	
																				
																<c:set var="plan_name"  value="${det.plan_name}"/>
			                                                    <%plan_name = (String)pageContext.getAttribute("plan_name");%>
			
			                                                    
			                                                
			                                                     <%if(plan_name.equals("Plan C")){%>
			                                               
				                                                    <c:set var="plan_id"  value="${det.plan_id}"/>
				                                                    <%plan_id = (Integer)pageContext.getAttribute("plan_id");%>
				
			
																	<div class="control-group">																										
							                                            <label for="username" class="control-label"  style="width: 230px ! important;">Policy Amount after 5 year</label>
																		<div class="controls">
																			<input type="text" value="${det.plan_policy_amount}" disabled="true" id="planValId1C" name="planVal1" class="span4">	
																			<div id="error1C" style="color:red;display:none;"></div>													
																		</div> <!-- /controls -->		
																												
																	</div> 
																	
																	<div class="control-group">											
																		<label for="username" class="control-label" style="width: 230px ! important;">Amount per each refer friend</label>
																		<div class="controls">
																			<input type="text"  value="${det.plan_amount_refer_per_friend}"  disabled="true" id="planValId2C"  name="planVal2" class="span4">
																			<div id="error2C" style="color:red;display:none;"></div>															
																		</div> <!-- /controls -->			
							                                            										
																	</div> <!-- /control-group -->																				
																																													
																 <br>
																 
																 <div class="form-actions">
																 
																    <input type="hidden"  name="plan_id" value="${det.plan_id}" />
																    <input type="hidden" id="planNameId" name="planName" value="Plan C" />			
																    <input type="button" class="btn btn-primary" id="CButtonId" value="Edit" onclick="saveBusinessPlan('planCForm','Plan C','C','CButtonId')"/> 
																    
																    <input type="button" class="btn btn-primary" id="addFeatureId" value="Add Feature" onclick="showFeature()"/> 
																								
																 </div> <!-- /form-actions -->
															   
			                                               <%}%>
			                                               			                                               
							                        </c:forEach>	
							                    
							                    </c:if>
							                    
												</fieldset>
												
											</form>
											
											
											
											<!-- Add Features Form -->
											
															<fieldset  >
															
															<div id="featureFormDiv" style="display:none;" class="widget widget-nopad">
													            
													            <div class="widget-header"> <i class="icon-search"></i>
													              <h3> Add Features in Plan C</h3>
													            </div>
													            
													            <!-- /widget-header -->
													           
													            <div class="widget-content">
													              
													                  <div class="control-group">											
																	
																	     <ul>
																			    
									                                          		<li class="manageLi">   
									                                          		  
																					   <div class="control-group">											
																						
																						<form id="featureForm" name="featureForm">
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">											
																								
																								<div class="controls">
																									Restrict Days <input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter the restricted days" name="noofDays" id="dayId">
																									
																									<div class="errorStyle" id="errday"></div>
																								</div> 
																							 
																							 </div>
																							 
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">	
																								
																								<div class="controls">
																									Restrict Friends <input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter the restricted Friends" name="noofFriend" id="friendId">
																									<div class="errorStyle" id="errfriend"></div>
																								</div> 
																							
																							</div>
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">	
																								
																								<div class="controls">
																									complete with in duration Bonus Amount <input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter the bonus Amount" name="bonusAmount" id="bonusAmountId">
																									<div class="errorStyle" id="errBnAmt"></div>
																								</div> 	
																							
																							</div>
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">		
																								<div class="controls">
																									complete after duration Complement Amount<input type="text" style="margin-bottom: 2px ! important;" class="span4" placeholder="Enter Compliment Amount" name="compAmount" id="compAmountId">
																									<div class="errorStyle" id="errCmpAmt"></div>
																								</div> 	
																							
																							</div>
																							
																							<div style="margin-top: 20px;" id="idDiv" class="control-group">	
																								
																								<div class="controls">
																									<input type="hidden"  name="plan_id" value="<%=plan_id%>" />
																									<input type="button" onclick="addFeatures('Plan C')" value="Add" class="btn btn-primary">
																									<div class="errorStyle" id="errproductPrice"></div>
																								</div> 		
																										
																									
																							</div> 																			
																	
																	                     </form>
																	                     
																					   </div>
																	   
																	               </li>
														                   </ul>
														                   
														              </div>
														   
													            </div>
													            
													            <!-- /widget-content --> 
													          </div>
													          
				          
														</fieldset>
											
											<!-- End of Add Fatures Form -->
											
											
											
											
											
											
											
											<!-- Feature Details -->
											
											<c:if test="${!empty planFeatures}">	
												
													
											<div class="widget widget-table action-table">
										           
										            <div class="widget-header"> <i class="icon-th-list"></i>
										              
										              <h3>List of All benefits for Plan C</h3>
										            </div>
										            <!-- /widget-header -->
										            <div class="widget-content">
										              <table class="table table-striped table-bordered">
										                <thead>
										                  <tr>
										                    <th> With this days </th>
										                    <th> Add total refer friend</th>
										                    <th class="td-actions"> Complete task with in period bonus amount </th>
										                    <th class="td-actions"> Complete task after period complement amount </th>
										                    <th class="td-actions"> Delete Feature</th>
										                  </tr>
										                </thead>
										                <tbody>
										                  
										              <%int condtition = 0; %>  
										               <c:forEach items="${planFeatures}" var="det">
											             
											             <c:set var="plan_id"  value="${det.plan_id}"/>
				                                          <%condtition = (Integer)pageContext.getAttribute("plan_id");%>
				
				                                          <%if(condtition==3) {%>
													  
												                  <tr>
												                    <td> ${det.feature_restrict_day}</td>
												                    <td> ${det.feature_restrict_friend} </td>
												                    <td> ${det.feature_restrict_bonus_amount} </td>
												                    <td> ${det.feature_restrict_compliment_amount} </td>
												                    <td class="td-actions"> <a class="btn btn-danger btn-small" href="javascript:;" onclick="deleteFeature('${det.feature_id}','Plan C')"><i class="btn-icon-only icon-trash"> </i></a></td>
												                  </tr>
										                  
										                  <%} %>
										                  
										                </c:forEach>
										                
										                </tbody>
										              </table>
										            </div>
										            <!-- /widget-content --> 
										          </div>
											
											
											
											
										</c:if>	
																	
				
		
</body>
</html>