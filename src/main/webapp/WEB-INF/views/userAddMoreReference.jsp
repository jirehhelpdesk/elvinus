<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Reference</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
     
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html" ><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="userReferFriends.html" class="sideMenuActive"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="buyMorePackets.html"><i class="icon-gift"></i> Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html"><i class=" icon-random"></i> Track Order </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">



					<div class="content">

						<!--/#btn-controls-->


						<!--/.module-->

						<div class="module">
						
							<div class="module-head">
								<h3>Add Reference :</h3>
							</div>

							<div class="module-body table">

								<div class="module-body">
                                     
                                    
                                   
                                   
                                   
                                   
                                    <div class="headDiv"><h4 class="textStyle">How you will get benefits if you refer someone ::</h4> </div>
									
									<div class="docs-example">
                                        <ul>
                                            <li><p class="text-info">Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis.</p></li>                                          
                                            <li><p class="text-info">Facilisis in pretium nisl aliquet</p></li>
                                            <li><p class="text-info">Nulla volutpat aliquam velit</p></li>
                                            <li><p class="text-info">Faucibus porta lacus fringilla vel</p></li>                                            
                                        </ul>                                       
                                    </div>
                                    
                                    <br>
                                     
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="headDiv"><h4 class="textStyle">Add Reference</h4> </div>
                                    <br>
									<form id="referForm" name="referForm" class="form-horizontal row-fluid">
										
										<div class="control-group">
											<label for="basicinput" class="control-label">Reference Name</label>
											<div class="controls">
												<input type="text" class="span8" placeholder="Enter Full Name" name="referName" id="fullNameId">	
												<div id="nameError" style="color:red;display:none;"></div>											
											</div>
										</div>

										<div class="control-group">
											<label for="basicinput" class="control-label">Reference Email id</label>
											<div class="controls">
												<input type="text"  class="span8" placeholder="Enter Email id" name="emailId" id="EmailsId">
												<div id="emailError" style="color:red;display:none;"></div>	
											</div>
										</div>

										<div class="control-group">
											<label for="basicinput" class="control-label">Reference Contact</label>
											<div class="controls">
												<input type="text" class="span8 tip" maxlength="10" id="mobilNoId" name="referMobileNo" placeholder="Enter mobile no" >
												<div id="mobileError" style="color:red;display:none;"></div>	
											</div>
										</div>


										<div class="control-group">
											<div class="controls">
												<button class="btn" type="button" onclick="saveReferenceDetails()">Refer</button>
											</div>
										</div>
										
									</form>

									
									<br>
									
									<c:if test="${!empty referenceDetails}">	
                                    
                                    <div class="headDiv"><h4 class="textStyle">Last Reference Status</h4> </div>
                                    <br>
                                           
                                            <c:forEach items="${referenceDetails}" var="det">
                                            
		                                            <div class="referMailDiv">
		                                                
		                                                 <div class="referMailleftDiv">
		  
		                                                   <ul>
		                                                     <li>${det.referee_name}</li>
		                                                     <li>${det.referee_emailid}</li>
		                                                     <li>${det.referee_mobile_no}</li>		                                                    
		                                                     <li>Reference Given Date : ${det.reference_given_date}</li>
		                                                     
		                                                     
		                                                   <c:set var="status"  value="${det.reference_status}"/>
						                                   <%String status = (String)pageContext.getAttribute("status");%>
				                                            
				                                           <%if(status.equals("Success")) {%>
				                                                
				                                                <li>Reference Accepted Date : ${det.reference_accept_date}</li>
				                                                
				                                               </ul>
			                                                  </div>
			                                                
			                                                 
			                                                 <div class="referMailSuccessrightDiv"><p class="referSuccesstextAlign"><%=status%></p></div>
			                                                 
				                                           
				                                           <%}else{ %>
				                                           
					                                           </ul>
					                                           
			                                                  </div>
			                                                
			                                                 
			                                                 <div class="referMailPendingrightDiv"><p class="referPendingtextAlign"><%=status%></p></div>
			                                                 
				                                           <%} %> 
		                                                   
		                                            </div>
                                            
                                            </c:forEach>
                                            
                                    </c:if>
                                    
                                                 
								</div>

							</div>
							
							 
							 
							
						</div>

						<!--/.module-->

					</div>
					<!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
         
         
        <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();
			window.location="userReferFriends.html";
		}
		
		</script>
         
         
    </body>

</html>