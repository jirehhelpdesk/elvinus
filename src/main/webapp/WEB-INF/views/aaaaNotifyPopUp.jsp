<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Notify Pop Up</title>


<link href='resources/User_Resources/popUpDesign/success-error-popup.css' rel='stylesheet' type='text/css'>
<script>

</script>
</head>

<body>

<div class="confirm">
 
  <h1 id="notifypopUpheading"></h1>
  <p id="notifypopUpMessage"></p>
  
  <button autofocus onclick="hideNotification()">Ok</button>
  
</div>

<div class="backGroundDiv"></div>

</body>
</html>