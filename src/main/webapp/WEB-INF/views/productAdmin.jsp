<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>

<meta charset="utf-8">

<title>Product</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">




<link rel="stylesheet" type="text/css" href="resources/datePicker/jquery.datetimepicker.css"/>



<script type="text/javascript" src="resources/Admin_Resources/js/jquery-1.11.1.js"></script>

<script>

$(document).keyup(function(e){
	  if(e.keyCode == 44) return false;
	});
	
</script>
</head>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>



<body onload="actimeMenu('menu1')">


<div> <%@include file="productAdminHeader.jsp" %> </div>


<div class="main">
  <div class="main-inner">
    <div class="container">
                  
  				    <div class="widget-header">
	      				<i class="icon-shopping-cart"></i>
	      				<h3>Product Management</h3>
	  				</div>
					
					<div class="widget-content">
						
						<div class="tabbable">
						
						<ul class="nav nav-tabs">
						 
						  <li class="active">
						    <a data-toggle="tab" href="#formcontrols" onclick="OrderPage('productAdmin.html')">This week Order Details</a>
						  </li>
						  
						 <li class="" ><a data-toggle="tab" href="#jscontrols" onclick="showSearchOrderPanel()">Search Order Details</a></li>
						  
						</ul>
						
						<br>
						
						<div class="tab-content">
						
						<div id="formcontrols" class="tab-pane active">
								
									<fieldset>
									
								     	<form class="form-horizontal" id="profileForm" name="profileForm">
										
											<div class="control-group">											
												<label for="username" class="control-label">Order Id</label>
												<div class="controls">
													<input type="text" name="fullName" id="searchValue" placeholder="Please enter order id" class="span4" onchange="createAdminProfileOnKeyUp()" />	
													<div id="errfullName" class="errorStyle"></div>											
												</div> <!-- /controls -->				
											</div> <!-- /control-group -->																				
											
											<br>
												
											<div class="form-actions">
											    <input type="button" class="btn btn-primary" value="Search" onclick="searchOrder('OrderId')"/> 
												<button class="btn">Cancel</button>
											</div> <!-- /form-actions -->
										    
										</form>
									
									</fieldset>
									
									
									<fieldset>
				
				                              <div id="updateorderDetailsDiv"> </div>
		
									</fieldset>
									
									
									
											<fieldset>
		
											<div id="ThisweekorderDetailsDiv">
		
												<div class="widget widget-table action-table">
		
													<div class="widget-header">
														<i class="icon-th-list"></i>
														<h3 id="resultHeading">This week Order details</h3>
													</div>
													<!-- /widget-header -->
													<div class="widget-content">
		
														<table class="table table-striped table-bordered">
		
															<thead>
																<tr>
																	<th>Serial no</th>
																	<th>Order Id</th>
																	<th>Customer Name</th>
																	<th>Email id</th>
																	<th>Mobile no</th>
																	<th>Order Status</th>
																	<th>Order Date</th>
		
																	<th class="td-actions">Update</th>
																</tr>
															</thead>
		
															<tbody>
					
																			<%int i=1; %>
																		
																		
																	<c:if test="${!empty orderDetails}">	
														
																		
																		<c:forEach items="${orderDetails}" var="det">	
																		
																		<c:set var="orderStatus"  value="${det.order_status}"/>
						 												<%String orderStatus = (String)pageContext.getAttribute("orderStatus");%>
						 										
																		<!-- Restrict for 10 datas -->
																		
																				<tr>
																				    
																				        <td><%=i++%></td>
																						<td>${det.order_unique_id}</td>
																						<td>${det.order_for_name}</td>
																						<td>${det.order_for_email_id}</td>
																						<td>${det.order_for_mobile_no}</td>
																						
																						<%if(orderStatus.equals("Completed")) {%>
																						
																								<td style="color:yellowgreen;">${det.order_status}</td>
																								<td style="color:yellowgreen;">${det.order_date}</td>
																								
																								<td class="td-actions" style="color:yellowgreen;">
																								
																										<a class="btn btn-small btn-success" href="javascript:;" title="Print Order" onClick="printOrderDetails('${det.order_unique_id}')">
																										
																										     <i class="btn-icon-only icon-print"> </i>
																												
																										</a> 
																										
																								</td>
																						
																						<%}else if(orderStatus.equals("Released")){ %>
																						
																								<td style="color:#FF4719;">${det.order_status}</td>
																								<td style="color:#FF4719;">${det.order_date}</td>
																								
																								<td class="td-actions">
																								
																										<a class="btn btn-small btn-success" href="javascript:;" title="Update Order" onclick="updateOrderDetails('${det.order_unique_id}')">
																										
																										     <i class="btn-icon-only icon-edit"> </i>
																												
																										</a> 
																										
																										<a class="btn btn-small btn-success" href="javascript:;" title="Print Order" onClick="printOrderDetails('${det.order_unique_id}')">
																										
																										     <i class="btn-icon-only icon-print"> </i>
																												
																										</a> 
																										
																								</td>
																						
																						<%}else{ %>
																						
																								<td>${det.order_status}</td>
																								<td>${det.order_date}</td>
																								
																								<td class="td-actions">
																								
																										<a class="btn btn-small btn-success" href="javascript:;" title="Update Order" onclick="updateOrderDetails('${det.order_unique_id}')">
																										
																										     <i class="btn-icon-only icon-edit"> </i>
																												
																										</a> 
																										
																										<a class="btn btn-small btn-success" href="javascript:;" title="Print Order" onClick="printOrderDetails('${det.order_unique_id}')">
																										
																										     <i class="btn-icon-only icon-print"> </i>
																												
																										</a> 
																										
																								</td>
																									
																						<%}%>
																																																										
																				</tr>
																		
																		</c:forEach>
																		
																	</c:if>
																	
																	
																	<c:if test="${empty orderDetails}">	
														
														                       <tr><td> <p Style="color:red;">This week nothing has ordered.</p></td></tr>
														            </c:if>	
																		
															</tbody>
														</table>
													</div>
													<!-- /widget-content -->
												</div>
		
											</div>
		
										</fieldset>
									
									
									
									
									<fieldset>
				
				                              <div id="searchorderDetailsDiv"></div>
		
									</fieldset>
										
										
										
										
									<fieldset>
				
				                              <div id="printOrderDetails" style="display:none;"></div>
		
									</fieldset>
											
							
						</div>
								
								
								
								<!-- Remove Admin Division  -->
								
								
								
								<div id="jscontrols" class="tab-pane">
									
										<fieldset>
				
				                              <div id="">
				                              
				                                  <form class="form-horizontal" id="profileForm" name="profileForm">


														<div class="control-group">
															
															<label class="control-label">Search Order Records</label>
			
															<div class="controls">
																
																<label class="radio inline"> <input type="radio" id="durationId" checked name="radiobtns" onclick="showOrderSearchDiv('Duration')" /> By Duration </label>
																
																<label class="radio inline"> <input type="radio" id="orderId" name="radiobtns" onclick="showOrderSearchDiv('Order')" /> By Order Id </label>
																
															</div>
															<!-- /controls -->
														</div>

														
														    <div id="orderIdDiv" style="display:none;">
														   
																	<div class="control-group">
																		<label for="username" class="control-label">Order Id</label>
																		<div class="controls">
																			<input type="text" name="fullName" id="searchValue"
																				class="span4" onchange="createAdminProfileOnKeyUp()" />
																			<div id="errfullName" class="errorStyle"></div>
																		</div>
																		<!-- /controls -->
																	</div>
																	<!-- /control-group -->
																	
																	<br>
																		
																	<div class="form-actions">
																	    <input type="button" class="btn btn-primary" value="Search" onclick="searchOrderDetails('OrderId')"/> 
																		<button class="btn">Cancel</button>
																	</div> <!-- /form-actions -->

															</div>
															
															
                                                            <div id="durationDiv" style="display:block;">
                                                            
                                                            
													            <div class="control-group">											
																	<label for="username" class="control-label">From</label>
																	<div class="controls">
																		<input type="text" class="some_class" value="" id="some_class_1"/>
																		<div id="errfullName" class="errorStyle"></div>											
																	</div> <!-- /controls -->				
																</div> <!-- /control-group -->		
																
																<div class="control-group">											
																	<label for="username" class="control-label">To</label>
																	<div class="controls">
																		<input type="text" class="some_class" value="" id="some_class_2"/>
																		<div id="errfullName" class="errorStyle"></div>											
																	</div> <!-- /controls -->				
																</div> <!-- /control-group -->		
																
																																		
																
																
																<br>
																	
																<div class="form-actions">
																    <input type="button" class="btn btn-primary" value="Search" onclick="searchOrderDetails('Duration')"/> 
																	<button class="btn">Cancel</button>
																</div> <!-- /form-actions -->
													
															</div>
													
													
													
													</form>
				                              
				                              </div>
		
										</fieldset>
									
									   
									   <fieldset>
				
				                              <div id="updateorderDetailsDiv">   </div>
		
									  </fieldset>
									
									 <fieldset>
				
				                              <div id="orderDetailsDiv"></div>
		
									 </fieldset>
									
									<fieldset>
				
				                              <div id="printOrderDetails" style="display:none;"></div>
		
									</fieldset>
											
											
								</div>
								
								
								
								<!-- End of Remove Admin Division  -->
							
							</div>
						  
						</div>
						
						
						
						
						
					</div>
					
					
					
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<div> <%@include file="supAdminFooter.jsp"%> </div>


<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/subAdminValidateForm.js"></script>



<script src="resources/datePicker/jquery.js"></script>
<script src="resources/datePicker/jquery.datetimepicker.js"></script>
<script>

$('.some_class').datetimepicker();


$('#datetimepicker_dark').datetimepicker({theme:'dark'})

function reloadPage(url)
{
	window.location = url;	
}
</script>


</body>
</html>
