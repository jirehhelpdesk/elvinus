<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Contact Us</title>
	<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
	<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
	<link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
	
	
	<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">
	
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				 
				 <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i>
                 </a>
                        
                 <a class="brand" href="index.html"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a>

				<div class="nav-collapse collapse navbar-inverse-collapse">
				
					<ul class="nav pull-right">


						<li><a href="userLogin.html">
							Sign In
						</a></li>


						<li><a href="userRegister.html">
							Become a member
						</a></li>

						

					</ul>
				</div><!-- /.nav-collapse -->
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->
   

	<div class="wrapper">
		<div class="container">
			
			<div class="module">
							<div class="module-head">
								<h3>Feel free to contact us .</h3>
							</div>
							<div class="module-body">
									
									<div class="alert">
										<button data-dismiss="alert" class="close" type="button">�</button>
										<strong>Warning ! &nbsp;</strong> * notify that those components are compulsory.
									</div>
									
									<br>

									<form class="form-horizontal row-fluid" id="contactForm" name="contactForm" >
									
									
										<div class="control-group">
											<label for="basicinput" class="control-label">Full Name *</label>
											<div class="controls">
												<input type="text" class="span8" placeholder="Enter your full name" name="fullName" id="fullNameId">
												<div id="errorName" class="help-inline" style="color:red;display:none;"></div>
											</div>
										</div>
										
										<div class="control-group">
											<label for="basicinput" class="control-label">Email Id *</label>
											<div class="controls">
												<input type="text" class="span8 tip" placeholder="Enter your contact email id." name="email" id="emailId">
												<div id="errorEmail" class="help-inline" style="color:red;display:none;"></div>
											</div>
										</div>
										
										<div class="control-group">
											<label for="basicinput" class="control-label">Contact No</label>
											<div class="controls">
												<input type="text" class="span8 tip" maxlength="10" placeholder="Enter your mobile no." name="mobileNo" id="mobileId" >
												<div id="errorMobile" class="help-inline" style="color:red;display:none;"></div>
											</div>
										</div>

										<div class="control-group">
											<label for="basicinput" class="control-label"  >Reason for contact *</label>
											<div class="controls">
												<select class="span8" data-placeholder="Select here" tabindex="1" id="categoryId" name="category">
													<option value="Contact Mode">Select here..</option>
													<option value="Feedback">Feedback</option>
													<option value="Business">Business</option>
													<option value="Complain">Complain</option>
													<option value="Products">Products</option>
												</select>
												<div id="errorCategory" class="help-inline" style="color:red;display:none;"></div>
											</div>
										</div>

										<div class="control-group">
											<label for="basicinput" class="control-label">Description </label>
											<div class="controls">
												<textarea rows="5" class="span8" id="descriptionId" name="description" ></textarea>
												<div id="errorDescription" class="help-inline" style="color:red;display:none;"></div>
											</div>
										</div>

										<div class="control-group">
											<div class="controls">
												<button class="btn" type="button" onclick="contactUsBiosys()">Send</button>
											</div>
										</div>
										
									</form>
							</div>
						</div>
			
			
		</div>
		
  
  <!-- End of Middle Page Content Div --> 
		
		
	</div><!--/.wrapper-->



    

<!-- footer Content Div --> 

  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 


<script src="resources/User_Resources/scripts/userOperationScript.js"></script>


<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<!-- end of footer Content Div --> 

<script>
function hidePopUp()
{
	$("#popUpDiv").hide();
	window.location="contact.html";
}

</script>


	<script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</body>

</html>

