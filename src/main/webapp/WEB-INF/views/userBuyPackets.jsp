<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Buy Packet</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">

	
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/css/theme.css" rel="stylesheet">
  <link type="text/css" href="resources/User_Resources/images/icons/css/font-awesome.css" rel="stylesheet">
  <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
   
   
   <link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">

   
   
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<%String fromEBS = (String)session.getAttribute("fromEBS"); %>

<%if(fromEBS!=null){ %>

<script>

function showSuccessPopUp()
{
	$("#popUpheading").html("Success Message");
	$("#popUpMessage").html("Thank you for ordering ! further notification will notify to your contact details.");	
	$("#popUpDiv").show();	
	
	<%session.removeAttribute("fromEBS");%>
}

</script>

<%}else{ %>

<script>

function showSuccessPopUp()
{
	
}

</script>

<%} %>


<body onload="showSuccessPopUp();displayProducts('Agriculture')">
        
        <div> <%@include file="userHeader.jsp" %> </div>
               
        <!-- /navbar -->
        
		
		<div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        
                        <div class="sidebar">
                            														
							<ul class="widget widget-menu unstyled">
                                <li ><a href="myProfile.html" ><i class="icon-user"></i>Profile Details
                                </a></li>                               
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li ><a href="userReferFriends.html"><i class="icon-group"></i>   Refer Friend </a>    </li>                          
                            </ul>
							
							<ul class="widget widget-menu unstyled">                               
                                <li><a href="buyMorePackets.html" class="sideMenuActive"><i class="icon-gift"></i> Buy Packets </a></li>                               
                            </ul>
                            
							<ul class="widget widget-menu unstyled">                              
                                <li><a href="trackOrder.html"><i class=" icon-random"></i> Track Order </a></li>									
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#togglePages" ><i class="menu-icon icon-cog">
                                </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
                                </i>Settings </a>
	                                 
	                                  <div style="display:block;">  
	                                    <ul id="togglePages" class="collapse unstyled">
	                                        <li><a href="profileSetting.html" ><i class="icon-edit"></i>Profile Settings </a></li>
	                                        <li><a href="showPasswordSetting.html"><i class="icon-key"></i>Password Settings </a></li>                                        
	                                    </ul>
	                                  </div> 
	                                              
                                </li>						
                            </ul>
							
							<ul class="widget widget-menu unstyled">                             
								<li><a class="collapsed" data-toggle="collapse" href="#toggleamount"><i class="icon-briefcase"></i>
                                <i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>
                                Bonus Amount </a>
                                   <div id="amtToggle" style="display:block;">
	                                    <ul id="toggleamount" class="collapse unstyled">
	                                        <li><a href="earcnedReferenceAmt.html"><i class="icon-money"></i>Earned Reference Amount</a></li>
	                                        <li><a href="policyAmount.html"><i class="icon-money"></i>Policy Amount</a></li>                                        
	                                    </ul>
                                    </div>
                                </li>						
                            </ul>
							
                        </div>
                        
                        
                        
						
                        <!--/.sidebar-->
						
                    </div>
                    <!--/.span3-->
					
                    
                    
                    
                    <div class="span9">
                        
                        
                        
                        <div class="content">
                            
                            <!--/#btn-controls-->
                           
						   
                            <!--/.module-->
                            
                            <div class="module">
                                <div class="module-head">
                                   
                                    <h3>
                                       Buy More Packets
                                       
	                                       <a href="#" onclick="showCartProducts()"> 
								                  <div class="innertopblock2" >
														<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
													 	<p><strong id="noOfProId">0</strong></p>
												  </div>					  	
										   </a>
									  
                                     </h3>
                                     
                                     
                                     
                                </div>
                                
								<div class="module-body table">
                                   
									<div class="module-body">
                                        
                               
                                         <div class="btn-box-row row-fluid">
			                                    <a class="btn-box big span4 activeProductCategory" id="agriClass" href="#" ><label><b><input  type="checkbox" id="agriId" checked onclick="displayProducts('Agriculture')">Agriculture Products</b></label></a>			                                       
			                                    <a class="btn-box big span4" id="herbalClass" href="#" ><label><b><input   type="checkbox" id="herId" onclick="displayProducts('Herbal')">Herbal Products</b></label></a>
			                                    <a class="btn-box big span4" id="hmCareClass" href="#" ><label><b><input  type="checkbox" id="hmCarId" onclick="displayProducts('HomeCare')">HomeCare Products</b></label></a>
			                             </div>
                                
                                         
                                        <div class="controls buyButtonDiv">
												<input type="button" onclick="saveSeletedProduct()" value="Proceed" id="basicButtonId" class="btn">
										</div>       
                                    
                                    
                                         <form class="form-horizontal row-fluid" id="productSelectionForm" name="productSelectionForm" method="post">    
			    			     			                                       
											       <input  type="hidden" id="agriProdId" name="agricultureProduct" value=""  />
											       <input  type="hidden" id="herbalProdId" name="herbalProduct" value="" />
											       <input  type="hidden" id="hmCareProdId" name="homecareProduct" value="" />
											       
											      <div id="featurezone" style="width: 747px !important;">
												   
											    
											      </div>											       
											       											       
						                 </form>
							              
							              
							</div>
							
							
							
                                </div>
                            </div>
							
                            <!--/.module-->
                        
                        </div>
                        <!--/.content-->
                        
                        
                        
                        
                    </div>
                    
                    
                    
                    
                    
                    
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->


       <div> <%@include file="userFotter.jsp" %> </div>

        <script src="resources/User_Resources/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="resources/User_Resources/scripts/common.js" type="text/javascript"></script>
      
      
       <script src="resources/User_Resources/scripts/userOperationScript.js"></script>

		<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>
		
		<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>
		
		<div id="paymentMode" style="display:none;"> <%@include file="userBuyPacketsPaymentMode.jsp" %> </div>
		
		<div id="productDetailsPopUp" style="display:none;"> <%@include file="userProductDetailsPopUp.jsp" %> </div>
		
		<div id="cartDetailsPopUp" style="display:none;"></div>
		
		<div id="NotifypopUpDiv" style="display:none;"> <%@include file="aaaaNotifyPopUp.jsp" %> </div>
		
		
		<script>
		
		function hidePopUp()
		{
			$("#popUpDiv").hide();			
		}
		function hideNotification()
		{
			$("#NotifypopUpDiv").hide();
		}
		</script>
         
         
    </body>

</html>