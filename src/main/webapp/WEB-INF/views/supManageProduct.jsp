<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Products</title>
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

                       <div class="widget widget-table action-table">
											
												<div class="widget-header">
													<i class="icon-th-list"></i>
													<h3 id="resultHeading">Latest List of Products for business</h3>
												</div>
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
															    <th>Serial no</th>
																<th>Product Id</th>
																<th>Product Category</th>
																<th>Product Name</th>
																<th>Product Price    (INR)</th>
																
																<th class="td-actions"></th>
															</tr>
														</thead>
														
														
														<tbody>
															
														<c:if test="${!empty productDetails}">	
															
															<%int i=1; %>
															
															
															<c:forEach items="${productDetails}" var="det">	
															
															
															<!-- Restrict for 10 datas -->
															
																	<tr>
																	        <td><%=i++%></td>
																			<td>${det.product_unique_id}</td>
																			<td>${det.product_category}</td>
																			<td>${det.product_name}</td>
																			<td>${det.product_price}</td>
																			
																			<td class="td-actions">
																			
																					<a class="btn btn-small btn-success" href="javascript:;" title="Edit" onclick="editProductDetails('${det.product_id}')">
																					
																					     <i class="btn-icon-only icon-edit"> </i>
																							
																					</a> 
																					
																					<a class="btn btn-danger btn-small" href="javascript:;" title="Delete" onclick="removeProductDetails('${det.product_id}')">
																				         
																				         <i class="btn-icon-only icon-trash"> </i>
																				         
																					</a>
																					
																			</td>																																				
																	</tr>
															
															</c:forEach>
															
														</c:if>	
														
														<c:if test="${empty productDetails}">	
														
														            <tr>
																	        <td style="color:red;">As per the search details No data found !</td>
																	
																	</tr>
														
														</c:if>
																														
														</tbody>
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>