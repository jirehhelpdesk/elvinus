<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List of Transaction</title>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body>

                       <div class="widget widget-table action-table">
											
												<div class="widget-header">
													<i class="icon-th-list"></i>
													<h3 id="resultHeading">Search result of transaction details</h3>
												</div>
												<!-- /widget-header -->
												<div class="widget-content">
													
													<table class="table table-striped table-bordered">
														
														<thead>
															<tr>
															    <th>Serial no</th>
																<th>Transaction Id</th>
																<th>Transaction Amount</th>
																<th>Transaction Mode</th>
																<th>Transaction Status</th>
																<th>Transaction Date</th>
																
																<th class="td-actions">Update</th>
															</tr>
														</thead>
														
														
												<tbody>
															
													<c:if test="${!empty tranDetails}">	
															
															<%int i=1; %>
															
															
														<c:forEach items="${tranDetails}" var="det">	
															
															
															<!-- Restrict for 10 datas -->
															
																	<tr>
																	    
																	        <td><%=i++%></td>
																			<td>${det.transaction_unique_id}</td>
																			<td>${det.transaction_amount}</td>
																			<td>${det.transaction_mode}</td>
																			<td>${det.transaction_status}</td>
																			<td>${det.transaction_date}</td>
																			
																			<td class="td-actions">
																			
																					<a class="btn btn-small btn-success" href="javascript:;" title="Update Transaction" onclick="showTransDetails('${det.transaction_unique_id}')">
																					
																					     <i class="btn-icon-only icon-edit"> </i>
																							
																					</a> 
																																								
																			</td>
																																																							
																	</tr>
															
															</c:forEach>
															
														</c:if>	
														
														<c:if test="${empty tranDetails}">	
														
												            <tr>
												                    <td></td>
															        <td style="color:red;">As per the search details No data found !</td>
															</tr>
														
														</c:if>
																														
														</tbody>
													</table>
												</div>
												<!-- /widget-content -->
											</div>
		
		
</body>
</html>