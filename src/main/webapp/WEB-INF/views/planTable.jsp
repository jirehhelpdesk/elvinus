<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
 .planTableBody{
	Width:600px;
	height:auto;
	margin:0px auto 0; 
	padding:25px 20px;
	box-shadow:0 3px 8px #ABABAB;
	text-align:left;
 }
 .planTableBody h4{
	margin:0 0 15px 0; 
 }
 .planTableBody table{
	
 }
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
table th {
    background:#CCC;
}
th {
    padding: 5px;
   text-align: center;
}

td {
    padding: 5px;
   text-align: center;
}
 
</style>

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>


<body>
<div class="planTableBody">
	
	
	<c:forEach items="${planDetails}" var="det">	
	           
	           <c:set var="policyAmount"  value="${det.plan_policy_amount}"/>
	           <%int policyAmount = (Integer)pageContext.getAttribute("policyAmount");
	 			
	           if(policyAmount!=0)
	           {  %>
	        	   <h4 style="font-style:italic;">1.Policy amount will double after five year with <span style="color:#00b6f5;">Rs.${det.plan_policy_amount}</span></h4>
	          <% }
	           else
	           { %>
	        	   <h4 style="font-style:italic;">1.No double of your amount only incentive will applicable.</h4>
	          <% } %> 
	           
	               <h4 style="font-style:italic;">2.You will get <span style="color:#00b6f5;">Rs.${det.plan_amount_refer_per_friend}</span> per each success refer friend.</h4>
	           
    </c:forEach>
    
    
    <table style="width:600px">
    
    <c:if test="${!empty planFeatures}">	
		   
		      <tr>
		        <th style="width:100px;">With in </th>
		        <th style="width:180px;">Success refer friend</th>
		        <th>Bonus amount complete with in duration </th>
		        <th>Bonus amount complete after duration</th>       
		      </tr>
		      
    </c:if>  
      
      <c:if test="${!empty planFeatures}">	
      
		      <c:forEach items="${planFeatures}" var="det">	
							
							
							<tr>
						        <td><span style="color:#00b6f5;">${det.feature_restrict_day}</span> days</td>
						        <td>Add <span style="color:#00b6f5;">${det.feature_restrict_friend}</span> friends in the venture.</td>
						        <td>Rs.${det.feature_restrict_bonus_amount}.00</td>
						        <td>Rs.${det.feature_restrict_compliment_amount}.00</td>						        
						   </tr>
      												
			  </c:forEach>
			  
      </c:if>
      
      
      
    </table>
   
    <br></br>
    <b>Note:</b><h4 style="color:#00b6f5;font-style:italic;">All the prices may differ after government taxes.</h4>
    		 
</div>
</body>
</html>
