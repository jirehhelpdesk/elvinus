<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Accounts</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="resources/Admin_Resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="resources/Admin_Resources/css/font-awesome.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/style.css" rel="stylesheet">
<link href="resources/Admin_Resources/css/pages/dashboard.css" rel="stylesheet">

<link href="resources/Admin_Resources/css/superAdminManagable.css" rel="stylesheet">

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>

<body onload="actimeMenu('menu4')">


<div> <%@include file="accountAdminHeader.jsp" %> </div>




<div class="main">
  <div class="main-inner">
    <div class="container">
                  
                  
  				    <div class="widget-header">
	      				<i class="icon-credit-card"></i>
	      				<h3>Pay to customer account management</h3>
	  				</div>
					
					<div class="widget-content">
					
						<div class="tabbable">
							
							<ul class="nav nav-tabs">
							  <li class="active">
							    <a data-toggle="tab" href="#formcontrols">Daily Basis</a>
							  </li>
							  <!-- <li class="" onclick="showAdminDetails()"><a data-toggle="tab" href="#jscontrols" >Monthly Basis Amount</a></li>  -->
							</ul>
						
						<br>
						
							<div class="tab-content">
						
						    <div id="formcontrols" class="tab-pane active">
								
								
								<form class="form-horizontal" id="profileForm" name="profileForm">
								
									<fieldset>
										
										            
											   <div class="widget widget-nopad">
										          	
										          	
										            <div class="widget-header"> <i class="icon-list-alt"></i>
										              <h3> Today's Total Amount need to Pay</h3>
										            </div>
										            
										            
										            <!-- /widget-header -->
										            <div class="widget-content">
										              <div class="widget big-stats-container">
										                <div class="widget-content">
										                 
										                  <h6 class="bigstats">As of now today's transaction Business got the total amount,</h6>
										                  
										                  <div id="big_stats" class="cf">
										                    <div class="stat">  </div>
										                    <!-- .stat -->
										                    
										                    <div class="stat">  </div>
										                    <!-- .stat -->
										                    
										                    <div class="stat">  </div>
										                     <!-- .stat -->
										                    
										                    <div class="stat">  Total(After deducting of TDS) <i></i> <span class="value"> <b style="color:red;"><%=request.getAttribute("totalAmountToPay")%>  </b> INR</span> </div>
										                    <!-- .stat --> 
										                  </div>
										                  
										                </div>
										                <!-- /widget-content --> 
										                
										              </div>
										              
										            </div>
										            
										        </div>
									
									
										
										<div class="widget widget-table action-table">
											
											<c:if test="${!empty refAmount}">	
												
													    <div class="widget-header"> <i class="icon-list-alt"></i>
												              <h3> Today's Total Referal amount need to Pay</h3>
												        </div>
												            
														<!-- /widget-header -->
														<div class="widget-content">
															
															<table class="table table-striped table-bordered">
																
																<thead>
																	<tr>
																	    
																	    <th>Serial no</th>
																		<th>Full Name</th>																
																		<th>Email Id</th>
																		<th>Mobile No</th>
																		<th>Reference Status</th>
																		<th>Check Status</th>
																		<th>Refer Amount</th>
																		
																	</tr>
																</thead>
																
														       <tbody>
															   
															   <%int i = 1;%>
																	  
																	  
																	  <c:forEach items="${refAmount}" var="referAmt">	
																	  
																		  <c:set var="givenId"  value="${referAmt.reference_given_user_id}"/>
							 											  <%int givenId = (Integer)pageContext.getAttribute("givenId");%>
							 
																			  <c:forEach items="${userBasicDetails}" var="basic">	
																			  
																			     <c:set var="userId"  value="${basic.user_id}"/>
						 														 <%int userId = (Integer)pageContext.getAttribute("userId");%>
						 
						 															 <%if(userId==givenId) { %>
						 															 
							 															     <tr>
																							        <td><%=i++%></td>
																									<td>${basic.user_name}</td>
																									<td>${basic.user_emailid}</td>
																									<td>${basic.user_mobile_no}</td>
																									<td>${referAmt.reference_status}</td>
																									<td>${referAmt.reference_check_status}</td>
																									<td style="color:green;">${referAmt.reference_amount}</td>																																																				
																							 </tr>
						 															 
						 															 <%  }  %>
																						
																			  </c:forEach>
																	
																	</c:forEach>
																									
																</tbody>
																
															</table>
															
														</div>
														
														<!-- /widget-content -->
													
												</c:if>
												
												
												<c:if test="${empty refAmount}">	
												
												         <div class="widget-header"> <i class="icon-list-alt"></i>
												              <h3 style="color:red;">Today there is no Referal amount need to Pay.</h3>
												         </div>
												
												</c:if>
												
												
												
											</div>
											
											
											
											
											<div class="widget widget-table action-table">
											
											     <c:if test="${!empty refBonusAmount}">	
												
														    <div class="widget-header"> <i class="icon-list-alt"></i>
													              <h3> Today's total referal bonus amount need to Pay</h3>
													        </div>
													            
															<!-- /widget-header -->
															<div class="widget-content">
																
																<table class="table table-striped table-bordered">
																	
																	<thead>
																	
																		<tr>
																		    
																		    <th>Serial no</th>
																			<th>Full Name</th>																
																			<th>Email Id</th>
																			<th>Mobile No</th>
																			<th>Reference Status</th>																			
																			<th>Refer Bonus Amount</th>
																			
																		</tr>
																		
																	</thead>
																	
															       <tbody>
																   
																   <%int j = 1;%>
																		  
																		  
																		  <c:forEach items="${refBonusAmount}" var="referAmt">	
																		  
																			  <c:set var="givenId"  value="${referAmt.user_id}"/>
								 											  <%int givenId = (Integer)pageContext.getAttribute("givenId");%>
								 
																				  <c:forEach items="${userBasicDetailForReferBonus}" var="basic">	
																				  
																				     <c:set var="userId"  value="${basic.user_id}"/>
							 														 <%int userId = (Integer)pageContext.getAttribute("userId");
							 														 
							 														 System.out.println("Comparision="+userId+"=="+givenId);
							 														 %>
							  
							 															 <%if(userId==givenId) { %>
							 															 
								 															     <tr>
																								        <td><%=j++%></td>
																										<td>${basic.user_name}</td>
																										<td>${basic.user_emailid}</td>
																										<td>${basic.user_mobile_no}</td>
																										<td>${referAmt.bonus_check_status}</td>																									
																										<td style="color:green;">${referAmt.bonus_amount}</td>																																																				
																								</tr>
							 															 
							 															 <%} %>
																							
																				  </c:forEach>
																		
																		</c:forEach>
																										
																	</tbody>
																	
																</table>
															</div>
													
													
													</c:if>		
													
													<c:if test="${empty refBonusAmount}">	
												
														<div class="widget-header"> <i class="icon-list-alt"></i>
												              <h3 style="color:red;"> Today there is no referal bonus amount need to Pay.</h3>
												        </div>
										        	    
													</c:if>	
														
												<!-- /widget-content -->
												
											</div>
											
											
											
											
											
											
											<div class="widget widget-table action-table">
											
										        <c:if test="${!empty policyAmount}">	
													
														<div class="widget-header"> <i class="icon-list-alt"></i>
												              <h3> Today's total policy bonus amount need to Pay</h3>
												        </div>
										        	    
												<!-- /widget-header -->
												
														<div class="widget-content">
															
															<table class="table table-striped table-bordered">
																
																<thead>
																	<tr>
																	    
																	    <th>Serial no</th>
																		<th>Full Name</th>																
																		<th>Email Id</th>
																		<th>Mobile No</th>
																		<th>Policy Status</th>
																	
																		<th>Policy Amount</th>
																		
																	</tr>
																</thead>
																
														       <tbody>
															   
															   <%int k = 1;%>
																	  
																	  
																	  <c:forEach items="${policyAmount}" var="policyAmt">	
																	  
																		  <c:set var="givenId"  value="${policyAmt.user_id}"/>
							 											  <%int givenId = (Integer)pageContext.getAttribute("givenId");%>
							 
																			  <c:forEach items="${userBasicDetailForPolicyBonus}" var="basic">	
																			  
																			     <c:set var="userId"  value="${basic.user_id}"/>
						 														 <%int userId = (Integer)pageContext.getAttribute("userId");%>
						 
						 															 <%if(userId==givenId) {%>
						 															 
							 															     <tr>
																							        <td><%=k++%></td>
																									<td>${basic.user_name}</td>
																									<td>${basic.user_emailid}</td>
																									<td>${basic.user_mobile_no}</td>
																									<td>${policyAmt.policy_status}</td>
																		
																									<td style="color:green;">${policyAmt.policy_amount}</td>																																																				
																							</tr>
						 															 
						 															 <%} %>
																						
																			  </c:forEach>
																	
																	</c:forEach>
																									
																</tbody>
																
															</table>
															
														</div>
														
												</c:if>
												
												<c:if test="${empty policyAmount}">	
														
														<div class="widget-header"> <i class="icon-list-alt"></i>
												              <h3 style="color:red;"> Today there is no policy bonus amount need to Pay.</h3>
												        </div>
										        	    
												</c:if>		
												<!-- /widget-content -->
												
											</div>
											
											
											
											
											
											
									</fieldset>
									
								</form>
								
								
								<fieldset>
				
				                         <div id="updatetransactionListDiv"></div>
		
								</fieldset>
									
									
								<fieldset>
				
				                         <div id="transactionListDiv"></div>
		
								</fieldset>
									
									
									
							</div>
							
						
					<!-- Remove Admin Division  -->
								
								<div id="jscontrols" class="tab-pane">
									<form class="form-vertical" id="edit-profile2">
										<fieldset>
										
				                              <div id="adminDetailsDiv"></div>
											
										</fieldset>
									</form>
								</div>
								
					<!-- End of Remove Admin Division  -->
				
								
							</div>			  
						  
						</div>
						
					</div>
					
					
					
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div> <%@include file="supAdminFooter.jsp"%> </div>



<div id="progressDark" style="display:none;"></div>

<div id="progressLight" style="display:none;">

<div class="progress progress-striped active" style="margin-top:10px;">
       <div style="width: 100%;" class="bar"></div>
</div>

</div>

<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 


<script src="resources/Admin_Resources/js/jquery-1.7.2.min.js"></script> 
<script src="resources/Admin_Resources/js/excanvas.min.js"></script> 
<script src="resources/Admin_Resources/js/chart.min.js" type="text/javascript"></script> 
<script src="resources/Admin_Resources/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="resources/Admin_Resources/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="resources/Admin_Resources/js/base.js"></script> 

<script src="resources/Admin_Resources/js/subAdminValidateForm.js"></script>


</body>
</html>
