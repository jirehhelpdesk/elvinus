<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment Mode</title>
<link rel="icon" href="resources/User_Resources/images/favicon-16x16.ico" type="images/x-icon">
	
	
<link type="text/css" rel="stylesheet" href="resources/indexResources/css/style.css">

<link href="resources/indexResources/css/demo.css" rel="stylesheet">
<link href="resources/indexResources/css/stepsForm.css" rel="stylesheet">
<script src="resources/indexResources/scripts/jquery-2.1.1.min.js"></script>


<link href="resources/User_Resources/css/extraFeatureStyle.css" rel="stylesheet">

<%String completeStatus = (String)session.getAttribute("completeStatus");%>

<%if(completeStatus!=null){ %>

<script>

function showCompleteStatus()
{
	$("#notifypopUpheading").html("Notification");
	$("#notifypopUpMessage").html("You didn't complete your registration. Please complete !");
	$("#NotifypopUpDiv").show();
}

<%session.removeAttribute("completeStatus");%>

</script>

<%}else{ %>

<script>

function showCompleteStatus()
{
	
}
	
</script>

<%} %>

</head>

<body  onload="choosePaymentOption();showCompleteStatus()">

<div id="layout">
  <div id="topzone">
  	<div id="topmenuleft">
      	<div class="logomain">
       <a href="index.html" class="over"><img src="resources/User_Resources/images/logo.png" style="margin:8px 0; "></a> </div>
      </div>
  	<div id="topmenuzone">

	  <div id="topmenuright">
      
      
      <div class="topmenu"> | <a href="userLogin.html">Sign In</a>  
            
	      <%-- <a href="#" onclick="showCartProducts()"> 
	                  <div class="topblock2" >
							<img src="resources/User_Resources/images/shopping.gif" alt="" width="24" height="24" class="shopping" />
						 	<p>Shopping cart</p> <p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong> <span>items</span></p>
					  </div>					  	
		  </a>  --%>
		  
		   <a href="#" onclick="showCartProducts()"> 
                 <div class="topblock2" >
					<img src="resources/User_Resources/images/cart.png" alt="" class="innershopping" />
				 	<p><strong id="noOfProId"><%=request.getAttribute("totalProduct")%></strong></p>
			  </div>	
			  				  	
	      </a>
        
     </div>          
            
            <ul class="topmenu">
              <li><a href="index.html"><span>Home</span></a></li>
              <li><a href="aboutUs.html"><span>About&nbsp;Us</span></a></li>
              <li style="border:0px;"><a href="productDetails.html"><span>Products</span></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
 
	
    <div class="container">
    	
        
        <!--STEPS FORM START ------------ -->
        <div class="stepsForm">
            <form method="post">
                <div class="sf-steps">
                    <div class="sf-steps-content">
                    	<div>
                        	<span>1</span> Basic Information
                        </div>
                        <div>
                        	<span>2</span> Product Selection
                        </div>
                        <div>
                        	<span>3</span> Add Reference
                        </div>
						 <div>
                        	<span>4</span> Plan Selection
                        </div>
						 <div class="sf-active" >
                        	<span>5</span> Payment
                        </div>
						
                    </div>
                </div>                
               
                <div class="sf-steps-form sf-radius"> 
                   
                   
                  
	                
	               
	               
                </div>
                
            </form>
        </div>
        <!--STEPS FORM END -------------- -->
       
    </div>

<!-- footer Content Div --> 


  <%@include file="indexFotter.jsp" %>


<!-- end of footer Content Div --> 


<script src="resources/indexResources/scripts/userRegistrationScript.js"></script>

<script>document.write=null;window.open=null;document.open=null;</script>

<div id="progressBar" style="display:none;"> <%@include file="aaaaProgressBar.jsp" %> </div>

<div id="popUpDiv" style="display:none;"> <%@include file="aaaaSuccessErrorNotiPopUp.jsp" %> </div>

<div id="infoPopUpDiv" style="display:block;"> <%@include file="userPaymentModePopUp.jsp" %> </div>

</body>

</html>