package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="monthly_tds_report")
public class TDSReportModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="tds_id")
	private int tds_id;
	
	@Column(name="tds_month")
	private String tds_month;
		
	@Column(name="tds_amount")
	private float tds_amount;
	
	@Column(name="tds_report_name")
	private String tds_report_name;
	
	@Column(name="tds_generated_date")
	private Date tds_generated_date;

	
	public int getTds_id() {
		return tds_id;
	}

	public void setTds_id(int tds_id) {
		this.tds_id = tds_id;
	}

	public String getTds_month() {
		return tds_month;
	}

	public void setTds_month(String tds_month) {
		this.tds_month = tds_month;
	}

	public float getTds_amount() {
		return tds_amount;
	}

	public void setTds_amount(float tds_amount) {
		this.tds_amount = tds_amount;
	}

	public String getTds_report_name() {
		return tds_report_name;
	}

	public void setTds_report_name(String tds_report_name) {
		this.tds_report_name = tds_report_name;
	}

	public Date getTds_generated_date() {
		return tds_generated_date;
	}

	public void setTds_generated_date(Date tds_generated_date) {
		this.tds_generated_date = tds_generated_date;
	}
	
}
