package com.biosys.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="business_plan_model")
public class BusinessPlanModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="plan_id")
	private int plan_id;
	
	@Column(name="plan_name")
	private String plan_name;
	
	@Column(name="plan_policy_amount")
	private int plan_policy_amount;
	
	@Column(name="plan_amount_refer_per_friend")
	private int plan_amount_refer_per_friend;

	
	public int getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}

	public String getPlan_name() {
		return plan_name;
	}

	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}

	public int getPlan_policy_amount() {
		return plan_policy_amount;
	}

	public void setPlan_policy_amount(int plan_policy_amount) {
		this.plan_policy_amount = plan_policy_amount;
	}

	public int getPlan_amount_refer_per_friend() {
		return plan_amount_refer_per_friend;
	}

	public void setPlan_amount_refer_per_friend(int plan_amount_refer_per_friend) {
		this.plan_amount_refer_per_friend = plan_amount_refer_per_friend;
	}

}
