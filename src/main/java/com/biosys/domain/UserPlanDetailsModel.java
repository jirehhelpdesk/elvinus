package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="user_plan_details")
public class UserPlanDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_plan_id")
	private int user_plan_id;
	
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="user_plan_name")
	private String user_plan_name;
		
	@Column(name="user_plan_select_date")
	private Date user_plan_select_date;

	
	public int getUser_plan_id() {
		return user_plan_id;
	}

	public void setUser_plan_id(int user_plan_id) {
		this.user_plan_id = user_plan_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_plan_name() {
		return user_plan_name;
	}

	public void setUser_plan_name(String user_plan_name) {
		this.user_plan_name = user_plan_name;
	}

	public Date getUser_plan_select_date() {
		return user_plan_select_date;
	}

	public void setUser_plan_select_date(Date user_plan_select_date) {
		this.user_plan_select_date = user_plan_select_date;
	}
	
	
}
