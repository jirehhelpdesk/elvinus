package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="policy_amount_details")
public class PolicyAmountModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="policy_id")
	private int policy_id;
	
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="user_order_id")
	private String user_order_id;
		
	@Column(name="paid_amount")
	private String paid_amount;
	
	@Column(name="policy_amount")
	private int policy_amount;
	
	@Column(name="policy_start_date")
	private Date policy_start_date;
	
	@Column(name="policy_end_date")
	private Date policy_end_date;
	
	@Column(name="policy_status")
	private String policy_status;
	
	@Column(name="policy_status_details")
	private String policy_status_details;

	@Column(name="policy_entry_date")
	private Date policy_entry_date;
	
	public int getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_order_id() {
		return user_order_id;
	}

	public void setUser_order_id(String user_order_id) {
		this.user_order_id = user_order_id;
	}

	public String getPaid_amount() {
		return paid_amount;
	}

	public void setPaid_amount(String paid_amount) {
		this.paid_amount = paid_amount;
	}

	public int getPolicy_amount() {
		return policy_amount;
	}

	public void setPolicy_amount(int policy_amount) {
		this.policy_amount = policy_amount;
	}

	public Date getPolicy_start_date() {
		return policy_start_date;
	}

	public void setPolicy_start_date(Date policy_start_date) {
		this.policy_start_date = policy_start_date;
	}

	public Date getPolicy_end_date() {
		return policy_end_date;
	}

	public void setPolicy_end_date(Date policy_end_date) {
		this.policy_end_date = policy_end_date;
	}

	public String getPolicy_status() {
		return policy_status;
	}

	public void setPolicy_status(String policy_status) {
		this.policy_status = policy_status;
	}

	public String getPolicy_status_details() {
		return policy_status_details;
	}

	public void setPolicy_status_details(String policy_status_details) {
		this.policy_status_details = policy_status_details;
	}

	public Date getPolicy_entry_date() {
		return policy_entry_date;
	}

	public void setPolicy_entry_date(Date policy_entry_date) {
		this.policy_entry_date = policy_entry_date;
	}
	
	
}
