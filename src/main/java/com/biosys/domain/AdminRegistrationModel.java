package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="admin_registration_details")
public class AdminRegistrationModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="admin_id")
	private int admin_id;
	
	@Column(name="admin_type")
	private String admin_type;
		
	@Column(name="admin_full_name")
	private String admin_full_name;
		
	@Column(name="admin_email_id")
	private String admin_email_id;
	
	@Column(name="admin_mobile_no")
	private String admin_mobile_no;
	
	@Column(name="admin_password")
	private String admin_password;
	
	@Column(name="admin_cr_date")
	private Date admin_cr_date;

	
	public int getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(int admin_id) {
		this.admin_id = admin_id;
	}

	public String getAdmin_type() {
		return admin_type;
	}

	public void setAdmin_type(String admin_type) {
		this.admin_type = admin_type;
	}

	public String getAdmin_full_name() {
		return admin_full_name;
	}

	public void setAdmin_full_name(String admin_full_name) {
		this.admin_full_name = admin_full_name;
	}

	public String getAdmin_email_id() {
		return admin_email_id;
	}

	public void setAdmin_email_id(String admin_email_id) {
		this.admin_email_id = admin_email_id;
	}

	public String getAdmin_mobile_no() {
		return admin_mobile_no;
	}

	public void setAdmin_mobile_no(String admin_mobile_no) {
		this.admin_mobile_no = admin_mobile_no;
	}

	public String getAdmin_password() {
		return admin_password;
	}

	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}

	public Date getAdmin_cr_date() {
		return admin_cr_date;
	}

	public void setAdmin_cr_date(Date admin_cr_date) {
		this.admin_cr_date = admin_cr_date;
	}
	
	
}
