package com.biosys.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="business_plan_feature")
public class BusinessPlanFeatureModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="feature_id")
	private int feature_id;
	
	@Column(name="plan_id")
	private int plan_id;
	
	@Column(name="feature_restrict_day")
	private int feature_restrict_day;
	
	@Column(name="feature_restrict_friend")
	private int feature_restrict_friend;
	
	@Column(name="feature_restrict_bonus_amount")
	private int feature_restrict_bonus_amount;
	
	@Column(name="feature_restrict_compliment_amount")
	private int feature_restrict_compliment_amount;

	
	public int getFeature_id() {
		return feature_id;
	}

	public void setFeature_id(int feature_id) {
		this.feature_id = feature_id;
	}

	public int getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}

	public int getFeature_restrict_day() {
		return feature_restrict_day;
	}

	public void setFeature_restrict_day(int feature_restrict_day) {
		this.feature_restrict_day = feature_restrict_day;
	}

	public int getFeature_restrict_friend() {
		return feature_restrict_friend;
	}

	public void setFeature_restrict_friend(int feature_restrict_friend) {
		this.feature_restrict_friend = feature_restrict_friend;
	}

	public int getFeature_restrict_bonus_amount() {
		return feature_restrict_bonus_amount;
	}

	public void setFeature_restrict_bonus_amount(int feature_restrict_bonus_amount) {
		this.feature_restrict_bonus_amount = feature_restrict_bonus_amount;
	}

	public int getFeature_restrict_compliment_amount() {
		return feature_restrict_compliment_amount;
	}

	public void setFeature_restrict_compliment_amount(
			int feature_restrict_compliment_amount) {
		this.feature_restrict_compliment_amount = feature_restrict_compliment_amount;
	}
	
}
