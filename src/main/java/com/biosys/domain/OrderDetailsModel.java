package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="order_details")
public class OrderDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="order_id")
	private int order_id;
	
	@Column(name="order_unique_id")
	private String order_unique_id;
	
	@Column(name="order_status")
	private String order_status;
	
	@Column(name="order_status_details")
	private String order_status_details;
	
	@Column(name="order_for_user_id")
	private String order_for_user_id;
	
	@Column(name="order_for_name")
	private String order_for_name;
	
	@Column(name="order_for_email_id")
	private String order_for_email_id;
	
	@Column(name="order_for_mobile_no")
	private String order_for_mobile_no;
	
	@Column(name="order_amount")
	private String order_amount;

	@Column(name="order_shipping_address")
	private String order_shipping_address;
	
	@Column(name="order_date")
	private Date order_date;
	
	@Column(name="order_expected_date")
	private String order_expected_date;

	
	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public String getOrder_unique_id() {
		return order_unique_id;
	}

	public void setOrder_unique_id(String order_unique_id) {
		this.order_unique_id = order_unique_id;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getOrder_status_details() {
		return order_status_details;
	}

	public void setOrder_status_details(String order_status_details) {
		this.order_status_details = order_status_details;
	}

	public String getOrder_for_user_id() {
		return order_for_user_id;
	}

	public void setOrder_for_user_id(String order_for_user_id) {
		this.order_for_user_id = order_for_user_id;
	}

	public String getOrder_for_name() {
		return order_for_name;
	}

	public void setOrder_for_name(String order_for_name) {
		this.order_for_name = order_for_name;
	}

	public String getOrder_for_email_id() {
		return order_for_email_id;
	}

	public void setOrder_for_email_id(String order_for_email_id) {
		this.order_for_email_id = order_for_email_id;
	}

	public String getOrder_for_mobile_no() {
		return order_for_mobile_no;
	}

	public void setOrder_for_mobile_no(String order_for_mobile_no) {
		this.order_for_mobile_no = order_for_mobile_no;
	}

	public String getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(String order_amount) {
		this.order_amount = order_amount;
	}
	
	public String getOrder_shipping_address() {
		return order_shipping_address;
	}

	public void setOrder_shipping_address(String order_shipping_address) {
		this.order_shipping_address = order_shipping_address;
	}

	public Date getOrder_date() {
		return order_date;
	}

	public void setOrder_date(Date order_date) {
		this.order_date = order_date;
	}

	public String getOrder_expected_date() {
		return order_expected_date;
	}

	public void setOrder_expected_date(String order_expected_date) {
		this.order_expected_date = order_expected_date;
	}
	
	
}
