package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="user_return_material_after_5_year")
public class UserReturnProductModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="return_id")
	private int return_id;
	
	@Column(name="user_id")
	private int user_id;
		
	@Column(name="user_return_product_name")
	private String user_return_product_name;
	
	@Column(name="return_product_quantity_kg")
	private String return_product_quantity_kg;
		
	@Column(name="agreed_date")
	private Date agreed_date;

	
	
	public int getReturn_id() {
		return return_id;
	}

	public void setReturn_id(int return_id) {
		this.return_id = return_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_return_product_name() {
		return user_return_product_name;
	}

	public void setUser_return_product_name(String user_return_product_name) {
		this.user_return_product_name = user_return_product_name;
	}

	public String getReturn_product_quantity_kg() {
		return return_product_quantity_kg;
	}

	public void setReturn_product_quantity_kg(String return_product_quantity_kg) {
		this.return_product_quantity_kg = return_product_quantity_kg;
	}

	public Date getAgreed_date() {
		return agreed_date;
	}

	public void setAgreed_date(Date agreed_date) {
		this.agreed_date = agreed_date;
	}

}
