package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="reset_password_authentication")
public class UserResetPasswordModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="reset_id")
	private int reset_id;
	
	@Column(name="reset_unique_id")
	private String reset_unique_id;
		
	@Column(name="reset_email_id")
	private String reset_email_id;
	
	@Column(name="reset_status")
	private String reset_status;
		
	@Column(name="reset_date")
	private Date reset_date;

	
	
	public int getReset_id() {
		return reset_id;
	}

	public void setReset_id(int reset_id) {
		this.reset_id = reset_id;
	}

	public String getReset_unique_id() {
		return reset_unique_id;
	}

	public void setReset_unique_id(String reset_unique_id) {
		this.reset_unique_id = reset_unique_id;
	}

	public String getReset_email_id() {
		return reset_email_id;
	}

	public void setReset_email_id(String reset_email_id) {
		this.reset_email_id = reset_email_id;
	}

	public String getReset_status() {
		return reset_status;
	}

	public void setReset_status(String reset_status) {
		this.reset_status = reset_status;
	}

	public Date getReset_date() {
		return reset_date;
	}

	public void setReset_date(Date reset_date) {
		this.reset_date = reset_date;
	}
	
}
