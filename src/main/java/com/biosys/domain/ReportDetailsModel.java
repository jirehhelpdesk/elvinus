package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="admin_report_generate_details")
public class ReportDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="report_id")
	private int report_id;
	
	@Column(name="report_name")
	private String report_name;
		
	@Column(name="generate_from")
	private String generate_from;
		
	@Column(name="report_category")
	private String report_category;
		
	@Column(name="report_generate_system_ip")
	private String report_generate_system_ip;
			
	@Column(name="report_exist_status")
	private String report_exist_status;
	
	@Column(name="report_generate_date")
	private Date report_generate_date;

	
	
	
	public int getReport_id() {
		return report_id;
	}

	public void setReport_id(int report_id) {
		this.report_id = report_id;
	}

	public String getReport_name() {
		return report_name;
	}

	public void setReport_name(String report_name) {
		this.report_name = report_name;
	}

	public String getGenerate_from() {
		return generate_from;
	}

	public void setGenerate_from(String generate_from) {
		this.generate_from = generate_from;
	}

	public String getReport_category() {
		return report_category;
	}

	public void setReport_category(String report_category) {
		this.report_category = report_category;
	}

	public String getReport_generate_system_ip() {
		return report_generate_system_ip;
	}

	public void setReport_generate_system_ip(String report_generate_system_ip) {
		this.report_generate_system_ip = report_generate_system_ip;
	}
	
	
	public String getReport_exist_status() {
		return report_exist_status;
	}

	public void setReport_exist_status(String report_exist_status) {
		this.report_exist_status = report_exist_status;
	}

	public Date getReport_generate_date() {
		return report_generate_date;
	}

	public void setReport_generate_date(Date report_generate_date) {
		this.report_generate_date = report_generate_date;
	}
	
	
}
