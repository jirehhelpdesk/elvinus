package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="transaction_details")
public class TransactionDetailModel {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="transaction_id")
	private int transaction_id;
	
	@Column(name="transaction_unique_id")
	private String transaction_unique_id;
		
	@Column(name="transaction_id_from_EBS_gateway")
	private String transaction_id_from_EBS_gateway;
	
	@Column(name="transaction_amount")
	private String transaction_amount;
	
	@Column(name="transaction_mode")
	private String transaction_mode;

	@Column(name="order_unique_id")
	private String order_unique_id;

	@Column(name="transaction_against_user_id")
	private int transaction_against_user_id;

	@Column(name="transaction_status")
	private String transaction_status;

	@Column(name="transaction_date")
	private Date transaction_date;

	@Column(name="transaction_TDS_amount")
	private String transaction_TDS_amount;

	@Column(name="transaction_VAT_amount")
	private String transaction_VAT_amount;

	@Column(name="transaction_ServiceTax_amount")
	private String transaction_ServiceTax_amount;

	
	public int getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getTransaction_unique_id() {
		return transaction_unique_id;
	}

	public void setTransaction_unique_id(String transaction_unique_id) {
		this.transaction_unique_id = transaction_unique_id;
	}

	
	public String getTransaction_id_from_EBS_gateway() {
		return transaction_id_from_EBS_gateway;
	}

	public void setTransaction_id_from_EBS_gateway(
			String transaction_id_from_EBS_gateway) {
		this.transaction_id_from_EBS_gateway = transaction_id_from_EBS_gateway;
	}

	public String getTransaction_amount() {
		return transaction_amount;
	}

	public void setTransaction_amount(String transaction_amount) {
		this.transaction_amount = transaction_amount;
	}

	public String getTransaction_mode() {
		return transaction_mode;
	}

	public void setTransaction_mode(String transaction_mode) {
		this.transaction_mode = transaction_mode;
	}

	
	public String getOrder_unique_id() {
		return order_unique_id;
	}

	public void setOrder_unique_id(String order_unique_id) {
		this.order_unique_id = order_unique_id;
	}

	public int getTransaction_against_user_id() {
		return transaction_against_user_id;
	}

	public void setTransaction_against_user_id(int transaction_against_user_id) {
		this.transaction_against_user_id = transaction_against_user_id;
	}

	public String getTransaction_status() {
		return transaction_status;
	}

	public void setTransaction_status(String transaction_status) {
		this.transaction_status = transaction_status;
	}

	public Date getTransaction_date() {
		return transaction_date;
	}

	public void setTransaction_date(Date transaction_date) {
		this.transaction_date = transaction_date;
	}

	public String getTransaction_TDS_amount() {
		return transaction_TDS_amount;
	}

	public void setTransaction_TDS_amount(String transaction_TDS_amount) {
		this.transaction_TDS_amount = transaction_TDS_amount;
	}

	public String getTransaction_VAT_amount() {
		return transaction_VAT_amount;
	}

	public void setTransaction_VAT_amount(String transaction_VAT_amount) {
		this.transaction_VAT_amount = transaction_VAT_amount;
	}

	public String getTransaction_ServiceTax_amount() {
		return transaction_ServiceTax_amount;
	}

	public void setTransaction_ServiceTax_amount(
			String transaction_ServiceTax_amount) {
		this.transaction_ServiceTax_amount = transaction_ServiceTax_amount;
	}

}
