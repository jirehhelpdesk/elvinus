package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="user_registration_stage")
public class UserRegistrationStageModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_reg")
	private int user_reg;
	
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="user_reg_steps")
	private String user_reg_steps;
		
	@Column(name="cost_reg_date")
	private Date cost_reg_date;

	
	public int getUser_reg() {
		return user_reg;
	}

	public void setUser_reg(int user_reg) {
		this.user_reg = user_reg;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_reg_steps() {
		return user_reg_steps;
	}

	public void setUser_reg_steps(String user_reg_steps) {
		this.user_reg_steps = user_reg_steps;
	}

	public Date getCost_reg_date() {
		return cost_reg_date;
	}

	public void setCost_reg_date(Date cost_reg_date) {
		this.cost_reg_date = cost_reg_date;
	}
	
}
