package com.biosys.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="super_admin_details")
public class SuperAdminModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="super_admin_id")
	private int super_admin_id;
	
	@Column(name="super_admin_unique_id")
	private String super_admin_unique_id;
		
	@Column(name="super_admin_name")
	private String super_admin_name;
		
	@Column(name="super_admin_emailid")
	private String super_admin_emailid;
		
	@Column(name="super_admin_password")
	private String super_admin_password;
	
	
}
