package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="tax_amount_record_for_govt")
public class TaxReportModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="tax_id")
	private int tax_id;
	
	@Column(name="tax_duration_details")
	private String tax_duration_details;
		
	@Column(name="tax_report_name")
	private String tax_report_name;
	
	@Column(name="tax_amount")
	private double tax_amount;
	
	@Column(name="tax_status")
	private String tax_status;
	
	@Column(name="tax_report_date")
	private Date tax_report_date;
	
	
	
	
	public int getTax_id() {
		return tax_id;
	}

	public void setTax_id(int tax_id) {
		this.tax_id = tax_id;
	}

	public String getTax_duration_details() {
		return tax_duration_details;
	}

	public void setTax_duration_details(String tax_duration_details) {
		this.tax_duration_details = tax_duration_details;
	}

	public String getTax_report_name() {
		return tax_report_name;
	}

	public void setTax_report_name(String tax_report_name) {
		this.tax_report_name = tax_report_name;
	}

	public double getTax_amount() {
		return tax_amount;
	}

	public void setTax_amount(double tax_amount) {
		this.tax_amount = tax_amount;
	}

	public String getTax_status() {
		return tax_status;
	}

	public void setTax_status(String tax_status) {
		this.tax_status = tax_status;
	}

	public Date getTax_report_date() {
		return tax_report_date;
	}

	public void setTax_report_date(Date tax_report_date) {
		this.tax_report_date = tax_report_date;
	}
	
}
