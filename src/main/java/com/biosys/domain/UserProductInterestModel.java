package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="user_product_interest")
public class UserProductInterestModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_product_int_id")
	private int user_product_int_id;
	
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="user_interest_agriculture")
	private String user_interest_agriculture;
		
	@Column(name="user_interest_herbal")
	private String user_interest_herbal;
	
	@Column(name="user_interest_homecare")
	private String user_interest_homecare;

	@Column(name="update_date")
	private Date update_date;
	
	@Column(name="order_unique_id")
	private String order_unique_id;

	
	public int getUser_product_int_id() {
		return user_product_int_id;
	}

	public void setUser_product_int_id(int user_product_int_id) {
		this.user_product_int_id = user_product_int_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_interest_agriculture() {
		return user_interest_agriculture;
	}

	public void setUser_interest_agriculture(String user_interest_agriculture) {
		this.user_interest_agriculture = user_interest_agriculture;
	}

	public String getUser_interest_herbal() {
		return user_interest_herbal;
	}

	public void setUser_interest_herbal(String user_interest_herbal) {
		this.user_interest_herbal = user_interest_herbal;
	}

	public String getUser_interest_homecare() {
		return user_interest_homecare;
	}

	public void setUser_interest_homecare(String user_interest_homecare) {
		this.user_interest_homecare = user_interest_homecare;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getOrder_unique_id() {
		return order_unique_id;
	}

	public void setOrder_unique_id(String order_unique_id) {
		this.order_unique_id = order_unique_id;
	}
	
}
