package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="user_refer_plan_bonus_amount")
public class ReferBonusAmountModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="plan_bonus_id")
	private int plan_bonus_id;
	
	@Column(name="user_id")
	private int user_id;
	
	@Column(name="user_plan_no_of_days")
	private int user_plan_no_of_days;
	
	@Column(name="no_of_refers_user")
	private int no_of_refers_user;
	
	@Column(name="bonus_type")
	private String bonus_type;
	
	@Column(name="bonus_amount")
	private int bonus_amount;
	
	@Column(name="update_date")
	private Date update_date;

	@Column(name="bonus_check_status")
	private String bonus_check_status;

	public int getPlan_bonus_id() {
		return plan_bonus_id;
	}

	public void setPlan_bonus_id(int plan_bonus_id) {
		this.plan_bonus_id = plan_bonus_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getUser_plan_no_of_days() {
		return user_plan_no_of_days;
	}

	public void setUser_plan_no_of_days(int user_plan_no_of_days) {
		this.user_plan_no_of_days = user_plan_no_of_days;
	}

	public int getNo_of_refers_user() {
		return no_of_refers_user;
	}

	public void setNo_of_refers_user(int no_of_refers_user) {
		this.no_of_refers_user = no_of_refers_user;
	}

	public String getBonus_type() {
		return bonus_type;
	}

	public void setBonus_type(String bonus_type) {
		this.bonus_type = bonus_type;
	}

	public int getBonus_amount() {
		return bonus_amount;
	}

	public void setBonus_amount(int bonus_amount) {
		this.bonus_amount = bonus_amount;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getBonus_check_status() {
		return bonus_check_status;
	}

	public void setBonus_check_status(String bonus_check_status) {
		this.bonus_check_status = bonus_check_status;
	}

}
