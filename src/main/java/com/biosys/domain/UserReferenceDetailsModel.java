package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="reference_details")
public class UserReferenceDetailsModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="reference_id")
	private int reference_id;
	
	@Column(name="reference_given_user_id")
	private int reference_given_user_id;
	
	@Column(name="referee_name")
	private String referee_name;
	
	@Column(name="referee_emailid")
	private String referee_emailid;
	
	@Column(name="referee_mobile_no")
	private String referee_mobile_no;
		
	@Column(name="reference_given_date")
	private Date reference_given_date;

	@Column(name="reference_status")
	private String reference_status;
	
	
	@Column(name="reference_amount")
	private int reference_amount;
	
	
	@Column(name="reference_accept_date")
	private Date reference_accept_date;

	@Column(name="reference_check_status")
	private String reference_check_status;
	
	
	/*private UserBasicInfoModel userBasicInfo;
	
	
	@ManyToOne
    @JoinColumn(name = "user_id")
    public UserBasicInfoModel getProduct() {
        return userBasicInfo;
    }*/
	
	public int getReference_id() {
		return reference_id;
	}

	public void setReference_id(int reference_id) {
		this.reference_id = reference_id;
	}

	public int getReference_given_user_id() {
		return reference_given_user_id;
	}

	public void setReference_given_user_id(int reference_given_user_id) {
		this.reference_given_user_id = reference_given_user_id;
	}

	public String getReferee_name() {
		return referee_name;
	}

	public void setReferee_name(String referee_name) {
		this.referee_name = referee_name;
	}

	public String getReferee_emailid() {
		return referee_emailid;
	}

	public void setReferee_emailid(String referee_emailid) {
		this.referee_emailid = referee_emailid;
	}

	public String getReferee_mobile_no() {
		return referee_mobile_no;
	}

	public void setReferee_mobile_no(String referee_mobile_no) {
		this.referee_mobile_no = referee_mobile_no;
	}

	public Date getReference_given_date() {
		return reference_given_date;
	}

	public void setReference_given_date(Date reference_given_date) {
		this.reference_given_date = reference_given_date;
	}

	public String getReference_status() {
		return reference_status;
	}

	public void setReference_status(String reference_status) {
		this.reference_status = reference_status;
	}

	public Date getReference_accept_date() {
		return reference_accept_date;
	}

	
	public int getReference_amount() {
		return reference_amount;
	}

	public void setReference_amount(int reference_amount) {
		this.reference_amount = reference_amount;
	}

	public void setReference_accept_date(Date reference_accept_date) {
		this.reference_accept_date = reference_accept_date;
	}

	public String getReference_check_status() {
		return reference_check_status;
	}

	public void setReference_check_status(String reference_check_status) {
		this.reference_check_status = reference_check_status;
	}
	
	
}
