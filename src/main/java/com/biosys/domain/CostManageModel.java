package com.biosys.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;

@Entity
@Table(name="cost_manage_details")
public class CostManageModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cost_id")
	private int cost_id;
	
	@Column(name="cost_name")
	private String cost_name;
		
	@Column(name="cost_value")
	private String cost_value;
		
	@Column(name="cost_cr_date")
	private Date cost_cr_date;
	
	
	public int getCost_id() {
		return cost_id;
	}

	public void setCost_id(int cost_id) {
		this.cost_id = cost_id;
	}

	public String getCost_name() {
		return cost_name;
	}

	public void setCost_name(String cost_name) {
		this.cost_name = cost_name;
	}

	public String getCost_value() {
		return cost_value;
	}

	public void setCost_value(String cost_value) {
		this.cost_value = cost_value;
	}

	public Date getCost_cr_date() {
		return cost_cr_date;
	}

	public void setCost_cr_date(Date cost_cr_date) {
		this.cost_cr_date = cost_cr_date;
	}
		
}
