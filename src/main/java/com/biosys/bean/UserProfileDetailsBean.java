package com.biosys.bean;

import java.util.Date;

public class UserProfileDetailsBean {

	private int user_profile_id;
	private String user_alternate_emailid;
	private String user_permanent_address;
	private String user_id_type;
	private String user_proof_of_id;
	private String user_dob;
	private String user_profile_image_name;
	private Date user_profile_cr_date;
	
		
	public int getUser_profile_id() {
		return user_profile_id;
	}
	public void setUser_profile_id(int user_profile_id) {
		this.user_profile_id = user_profile_id;
	}
	public String getUser_alternate_emailid() {
		return user_alternate_emailid;
	}
	public void setUser_alternate_emailid(String user_alternate_emailid) {
		this.user_alternate_emailid = user_alternate_emailid;
	}
	public String getUser_permanent_address() {
		return user_permanent_address;
	}
	public void setUser_permanent_address(String user_permanent_address) {
		this.user_permanent_address = user_permanent_address;
	}
	public String getUser_id_type() {
		return user_id_type;
	}
	public void setUser_id_type(String user_id_type) {
		this.user_id_type = user_id_type;
	}
	public String getUser_proof_of_id() {
		return user_proof_of_id;
	}
	public void setUser_proof_of_id(String user_proof_of_id) {
		this.user_proof_of_id = user_proof_of_id;
	}
	
	public String getUser_dob() {
		return user_dob;
	}
	public void setUser_dob(String user_dob) {
		this.user_dob = user_dob;
	}
	public String getUser_profile_image_name() {
		return user_profile_image_name;
	}
	public void setUser_profile_image_name(String user_profile_image_name) {
		this.user_profile_image_name = user_profile_image_name;
	}
	public Date getUser_profile_cr_date() {
		return user_profile_cr_date;
	}
	public void setUser_profile_cr_date(Date user_profile_cr_date) {
		this.user_profile_cr_date = user_profile_cr_date;
	}
	
}
