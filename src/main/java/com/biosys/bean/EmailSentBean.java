package com.biosys.bean;

import java.util.Date;

public class EmailSentBean {


	private int email_id;
	private String sent_email_id;
	private String email_subject;
	private String email_message_content;
	private String email_sent_status;
	private Date email_sent_date;

		
	public int getEmail_id() {
		return email_id;
	}

	public void setEmail_id(int email_id) {
		this.email_id = email_id;
	}

	public String getSent_email_id() {
		return sent_email_id;
	}

	public void setSent_email_id(String sent_email_id) {
		this.sent_email_id = sent_email_id;
	}

	public String getEmail_subject() {
		return email_subject;
	}

	public void setEmail_subject(String email_subject) {
		this.email_subject = email_subject;
	}

	public String getEmail_message_content() {
		return email_message_content;
	}

	public void setEmail_message_content(String email_message_content) {
		this.email_message_content = email_message_content;
	}

	public String getEmail_sent_status() {
		return email_sent_status;
	}

	public void setEmail_sent_status(String email_sent_status) {
		this.email_sent_status = email_sent_status;
	}

	public Date getEmail_sent_date() {
		return email_sent_date;
	}

	public void setEmail_sent_date(Date email_sent_date) {
		this.email_sent_date = email_sent_date;
	}
	
	
}
