package com.biosys.bean;

import java.util.Date;

public class UserBasicInfoBean {

	private int user_id;
	private String vendor_id;
	private String user_name;
	private String user_emailid;
	private String user_mobile_no;
	private Date user_cr_date;
	private String user_shipping_address;
	private String user_profile_image_name;

	
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getVendor_id() {
		return vendor_id;
	}

	public void setVendor_id(String vendor_id) {
		this.vendor_id = vendor_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_emailid() {
		return user_emailid;
	}

	public void setUser_emailid(String user_emailid) {
		this.user_emailid = user_emailid;
	}

	public String getUser_mobile_no() {
		return user_mobile_no;
	}

	public void setUser_mobile_no(String user_mobile_no) {
		this.user_mobile_no = user_mobile_no;
	}

	public Date getUser_cr_date() {
		return user_cr_date;
	}

	public void setUser_cr_date(Date user_cr_date) {
		this.user_cr_date = user_cr_date;
	}

	public String getUser_shipping_address() {
		return user_shipping_address;
	}

	public void setUser_shipping_address(String user_shipping_address) {
		this.user_shipping_address = user_shipping_address;
	}

	public String getUser_profile_image_name() {
		return user_profile_image_name;
	}

	public void setUser_profile_image_name(String user_profile_image_name) {
		this.user_profile_image_name = user_profile_image_name;
	}
	
}
