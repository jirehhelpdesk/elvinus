package com.biosys.bean;

import java.util.Date;


public class TransactionDetailBean {

	
	private int transaction_id;
	private String transaction_unique_id;
	private String transaction_id_from_EBS_gateway;
	private String transaction_amount;
	private String transaction_mode;
	private String transaction_type_for_organization;
	private int transaction_against_user_id;
	private String transaction_status;
	private Date transaction_date;
	private String transaction_TDS_amount;
	private String transaction_VAT_amount;
	private String transaction_ServiceTax_amount;

	
	public int getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(int transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getTransaction_unique_id() {
		return transaction_unique_id;
	}

	public void setTransaction_unique_id(String transaction_unique_id) {
		this.transaction_unique_id = transaction_unique_id;
	}

	
	
	public String getTransaction_id_from_EBS_gateway() {
		return transaction_id_from_EBS_gateway;
	}

	public void setTransaction_id_from_EBS_gateway(
			String transaction_id_from_EBS_gateway) {
		this.transaction_id_from_EBS_gateway = transaction_id_from_EBS_gateway;
	}

	public String getTransaction_amount() {
		return transaction_amount;
	}

	public void setTransaction_amount(String transaction_amount) {
		this.transaction_amount = transaction_amount;
	}

	public String getTransaction_mode() {
		return transaction_mode;
	}

	public void setTransaction_mode(String transaction_mode) {
		this.transaction_mode = transaction_mode;
	}

	public String getTransaction_type_for_organization() {
		return transaction_type_for_organization;
	}

	public void setTransaction_type_for_organization(
			String transaction_type_for_organization) {
		this.transaction_type_for_organization = transaction_type_for_organization;
	}

	public int getTransaction_against_user_id() {
		return transaction_against_user_id;
	}

	public void setTransaction_against_user_id(int transaction_against_user_id) {
		this.transaction_against_user_id = transaction_against_user_id;
	}

	public String getTransaction_status() {
		return transaction_status;
	}

	public void setTransaction_status(String transaction_status) {
		this.transaction_status = transaction_status;
	}

	public Date getTransaction_date() {
		return transaction_date;
	}

	public void setTransaction_date(Date transaction_date) {
		this.transaction_date = transaction_date;
	}

	public String getTransaction_TDS_amount() {
		return transaction_TDS_amount;
	}

	public void setTransaction_TDS_amount(String transaction_TDS_amount) {
		this.transaction_TDS_amount = transaction_TDS_amount;
	}

	public String getTransaction_VAT_amount() {
		return transaction_VAT_amount;
	}

	public void setTransaction_VAT_amount(String transaction_VAT_amount) {
		this.transaction_VAT_amount = transaction_VAT_amount;
	}

	public String getTransaction_ServiceTax_amount() {
		return transaction_ServiceTax_amount;
	}

	public void setTransaction_ServiceTax_amount(
			String transaction_ServiceTax_amount) {
		this.transaction_ServiceTax_amount = transaction_ServiceTax_amount;
	}

	
}
