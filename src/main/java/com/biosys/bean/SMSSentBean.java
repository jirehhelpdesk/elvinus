package com.biosys.bean;

import java.util.Date;


public class SMSSentBean {

	private int sms_id;
	private String sent_mobile_no;
	private String sms_message;
	private Date sms_sent_date;
	private String sms_sent_status;

	public int getSms_id() {
		return sms_id;
	}

	public void setSms_id(int sms_id) {
		this.sms_id = sms_id;
	}

	public String getSent_mobile_no() {
		return sent_mobile_no;
	}

	public void setSent_mobile_no(String sent_mobile_no) {
		this.sent_mobile_no = sent_mobile_no;
	}

	public String getSms_message() {
		return sms_message;
	}

	public void setSms_message(String sms_message) {
		this.sms_message = sms_message;
	}

	public Date getSms_sent_date() {
		return sms_sent_date;
	}

	public void setSms_sent_date(Date sms_sent_date) {
		this.sms_sent_date = sms_sent_date;
	}

	public String getSms_sent_status() {
		return sms_sent_status;
	}

	public void setSms_sent_status(String sms_sent_status) {
		this.sms_sent_status = sms_sent_status;
	}
	
}
