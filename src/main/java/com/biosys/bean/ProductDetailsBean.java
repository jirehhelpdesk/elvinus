package com.biosys.bean;

import java.util.Date;



public class ProductDetailsBean {

    private int product_id;
	private String product_unique_id;
	private String product_category;
	private String product_name;
	private String product_price;
	private String product_description;
	private String product_image_file_name;
	private Date product_cr_date;
	
	
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public String getProduct_unique_id() {
		return product_unique_id;
	}
	public void setProduct_unique_id(String product_unique_id) {
		this.product_unique_id = product_unique_id;
	}
	public String getProduct_category() {
		return product_category;
	}
	public void setProduct_category(String product_category) {
		this.product_category = product_category;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getProduct_price() {
		return product_price;
	}
	public void setProduct_price(String product_price) {
		this.product_price = product_price;
	}
	public String getProduct_description() {
		return product_description;
	}
	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}
	public String getProduct_image_file_name() {
		return product_image_file_name;
	}
	public void setProduct_image_file_name(String product_image_file_name) {
		this.product_image_file_name = product_image_file_name;
	}
	public Date getProduct_cr_date() {
		return product_cr_date;
	}
	public void setProduct_cr_date(Date product_cr_date) {
		this.product_cr_date = product_cr_date;
	}
	
	
	
}
