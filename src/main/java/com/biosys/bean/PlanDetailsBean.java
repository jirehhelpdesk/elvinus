package com.biosys.bean;


public class PlanDetailsBean {


	private int plan_id;
	
	private String plan_name;
	private int bonus_amt_per_friend;
	
	private int bonus_amt_after_five_year;
	private int bonus_amt_within_25_days;
	private int bonus_amt_within_50_days;
	private int bonus_amt_within_75_days;
	private int bonus_amt_within_100_days;
	private int bonus_amt_after_25_days;
	private int bonus_amt_after_50_days;
	private int bonus_amt_after_75_days;
	private int bonus_amt_after_100_days;
	
    private int additional_bonus;
	
	public int getPlan_id() {
		return plan_id;
	}

	public void setPlan_id(int plan_id) {
		this.plan_id = plan_id;
	}

	public String getPlan_name() {
		return plan_name;
	}

	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}

	public int getBonus_amt_per_friend() {
		return bonus_amt_per_friend;
	}

	public void setBonus_amt_per_friend(int bonus_amt_per_friend) {
		this.bonus_amt_per_friend = bonus_amt_per_friend;
	}

	public int getBonus_amt_after_five_year() {
		return bonus_amt_after_five_year;
	}

	public void setBonus_amt_after_five_year(int bonus_amt_after_five_year) {
		this.bonus_amt_after_five_year = bonus_amt_after_five_year;
	}

	public int getBonus_amt_within_25_days() {
		return bonus_amt_within_25_days;
	}

	public void setBonus_amt_within_25_days(int bonus_amt_within_25_days) {
		this.bonus_amt_within_25_days = bonus_amt_within_25_days;
	}

	public int getBonus_amt_within_50_days() {
		return bonus_amt_within_50_days;
	}

	public void setBonus_amt_within_50_days(int bonus_amt_within_50_days) {
		this.bonus_amt_within_50_days = bonus_amt_within_50_days;
	}

	public int getBonus_amt_within_75_days() {
		return bonus_amt_within_75_days;
	}

	public void setBonus_amt_within_75_days(int bonus_amt_within_75_days) {
		this.bonus_amt_within_75_days = bonus_amt_within_75_days;
	}

	public int getBonus_amt_within_100_days() {
		return bonus_amt_within_100_days;
	}

	public void setBonus_amt_within_100_days(int bonus_amt_within_100_days) {
		this.bonus_amt_within_100_days = bonus_amt_within_100_days;
	}

	public int getBonus_amt_after_25_days() {
		return bonus_amt_after_25_days;
	}

	public void setBonus_amt_after_25_days(int bonus_amt_after_25_days) {
		this.bonus_amt_after_25_days = bonus_amt_after_25_days;
	}

	public int getBonus_amt_after_50_days() {
		return bonus_amt_after_50_days;
	}

	public void setBonus_amt_after_50_days(int bonus_amt_after_50_days) {
		this.bonus_amt_after_50_days = bonus_amt_after_50_days;
	}

	public int getBonus_amt_after_75_days() {
		return bonus_amt_after_75_days;
	}

	public void setBonus_amt_after_75_days(int bonus_amt_after_75_days) {
		this.bonus_amt_after_75_days = bonus_amt_after_75_days;
	}

	public int getBonus_amt_after_100_days() {
		return bonus_amt_after_100_days;
	}

	public void setBonus_amt_after_100_days(int bonus_amt_after_100_days) {
		this.bonus_amt_after_100_days = bonus_amt_after_100_days;
	}

	public int getAdditional_bonus() {
		return additional_bonus;
	}

	public void setAdditional_bonus(int additional_bonus) {
		this.additional_bonus = additional_bonus;
	}
	
	
}
