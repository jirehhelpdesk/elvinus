package com.biosys.service;


import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.TransactionDetailBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.dao.SubAdminDao;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.ReportDetailsModel;
import com.biosys.domain.TDSReportModel;
import com.biosys.domain.TaxReportModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.util.CaptureIPandMACaddressUtil;

@Service("subAdminService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SubAdminServiceImpl implements SubAdminService {

	@Autowired
	private SubAdminDao subAdminDao;
	
	@Transactional(readOnly = false)
	public String getValidateSubAdmin(String admincategory,String uid,String pwd)
	{
		return subAdminDao.getValidateSubAdmin(admincategory,uid,pwd);
	}
	
	@Transactional(readOnly = false)
	public List<OrderDetailsBean> getOrderDetails(String OrderId,String searchType,String startDate,String endDate)
	{
		return subAdminDao.getOrderDetails(OrderId,searchType,startDate,endDate);
	}
	
	@Transactional(readOnly = false)
	public String getUpdateOrderDetails(OrderDetailsModel ordModel)
	{
		return subAdminDao.getUpdateOrderDetails(ordModel);		
	}
	
	@Transactional(readOnly = false)
	public List<TransactionDetailBean> getListOfTransaction(String tranId,String tranType)
	{
		return subAdminDao.getListOfTransaction(tranId,tranType);
	}
	
	@Transactional(readOnly = false)
	public List<TransactionDetailModel> getListOfTodaysTransaction()
	{
		return subAdminDao.getListOfTodaysTransaction();
	}
	
	@Transactional(readOnly = false)
	public List<TransactionDetailModel> getListOfWeeklyTransaction()
	{
		return subAdminDao.getListOfWeeklyTransaction();
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsModel> getReferenceCheckDetails()
	{
		return subAdminDao.getReferenceCheckDetails();
	}
	
	@Transactional(readOnly = false)
	public List<UserBasicInfoBean> getUserDetailsViaRefBean(List<UserReferenceDetailsModel> referenceDetails)
	{
		return subAdminDao.getUserDetailsViaRefBean(referenceDetails);
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsModel> getReferenceCheckDetailsByCriteria(String searchType,String name,String startDate,String endDate)
	{
		return subAdminDao.getReferenceCheckDetailsByCriteria(searchType,name,startDate,endDate);		
	}
	
	@Transactional(readOnly = false)
	public String updateReferCheckStatus(int referId,String checkStatus)
	{
		return subAdminDao.updateReferCheckStatus(referId,checkStatus);
	}
	
	@Transactional(readOnly = false)
	public List<TransactionDetailModel> getOffLineTransacitonCheckDetails()
	{
		return subAdminDao.getOffLineTransacitonCheckDetails();
	}
	
	@Transactional(readOnly = false)
	public List<UserBasicInfoBean> getUserDetailsByUserId(List<Integer> userIdList)
	{
		return subAdminDao.getUserDetailsByUserId(userIdList);
	}
	
	@Transactional(readOnly = false)
	public String updateCheckStatusByCriteria(int uniqueId,String checkStatus,String updateType)
	{
		return subAdminDao.updateCheckStatusByCriteria(uniqueId,checkStatus,updateType);
	}
	
	@Transactional(readOnly = false)
	public List<TransactionDetailModel> getOffLineTransacitonCheckDetails(String searchType,String name,String startDate,String endDate)
	{
		return subAdminDao.getOffLineTransacitonCheckDetails(searchType,name,startDate,endDate);
	}
	
	@Transactional(readOnly = false)
	public List<ReferBonusAmountModel> getReferenceBonusChecksWithCriteria(String searchType,String name,String startDate,String endDate)
	{
		return subAdminDao.getReferenceBonusChecksWithCriteria(searchType,name,startDate,endDate);
	}
	
	@Transactional(readOnly = false)
	public List<PolicyAmountModel> getPolicyBonusChecksWithCriteria(String searchType,String name,String startDate,String endDate)
	{
		return subAdminDao.getPolicyBonusChecksWithCriteria(searchType,name,startDate,endDate);
	}
	
	
	@Transactional(readOnly = false)
	public List<OrderDetailsModel> getOrderDetailsWithCriteria(String duration)
	{
		return subAdminDao.getOrderDetailsWithCriteria(duration);
	}
	
	@Transactional(readOnly = false)
	public List<TransactionDetailModel> getTaxTransaction()
	{
		return subAdminDao.getTaxTransaction();
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsModel> getReferAmountDetails(String criteria)
	{
		return subAdminDao.getReferAmountDetails(criteria);
	}
	
	@Transactional(readOnly = false)
    public List<ReferBonusAmountModel> getReferBonusDetails(String criteria)
    {
    	return subAdminDao.getReferBonusDetails(criteria);
    }
	
	@Transactional(readOnly = false)
    public List<PolicyAmountModel> getPolicyBonusDetails(String criteria)
    {
    	return subAdminDao.getPolicyBonusDetails(criteria);
    }
	
	@Transactional(readOnly = false)
    public List<TransactionDetailModel> getSuccessTransactionDetails()
    {
    	return subAdminDao.getSuccessTransactionDetails();
    }
    
	@Transactional(readOnly = false)
    public String saveTaxReportDetails(TaxReportModel taxModel)
    {
    	return subAdminDao.saveTaxReportDetails(taxModel);
    }
    
	@Transactional(readOnly = false)
    public int getTodaysTaxId(String reportName)
    {
    	return subAdminDao.getTodaysTaxId(reportName);
    }
    
	@Transactional(readOnly = false)
    public List<TaxReportModel> getAllTaxReport(String criteria)
    {
    	return subAdminDao.getAllTaxReport(criteria);
    }
    
	@Transactional(readOnly = false)
    public List<UserBasicInfoModel> getUserDetailModelByUserId(List<Integer> useIds)
    {
    	return subAdminDao.getUserDetailModelByUserId(useIds);
    }
    
	@Transactional(readOnly = false)
    public List<TDSReportModel> getAllTdsReport(String restriction)
    {
    	return subAdminDao.getAllTdsReport(restriction);
    }
    
	@Transactional(readOnly = false)
    public String saveTDSReportDetails(TDSReportModel tdsReport)
    {
    	return subAdminDao.saveTDSReportDetails(tdsReport);  
    }
    
	@Transactional(readOnly = false)
    public List<ReferBonusAmountModel> getAllReferenceBonusDetails()
    {
    	return subAdminDao.getAllReferenceBonusDetails();  
    }
	
	@Transactional(readOnly = false)
	public List<PolicyAmountModel> getAllPolicyBonusDetails()
	{
		return subAdminDao.getAllPolicyBonusDetails();  
	}

	@Transactional(readOnly = false)
	public int getTodaysTDSId(String reportName)
	{
		return subAdminDao.getTodaysTDSId(reportName);
	}
    
	@Transactional(readOnly = false)
	public List<ReportDetailsModel> getCheckReportDetails(String restriction)
	{
		return subAdminDao.getCheckReportDetails(restriction);
	}
	
	@Transactional(readOnly = false)
	public List<UserProductInterestModel> getInterestProductDetails(String OrderId)
	{
		return subAdminDao.getInterestProductDetails(OrderId);
	}
	
	@Transactional(readOnly = false)
	public List<ProductDetailsModel> getProductDetails(List<UserProductInterestModel> userInterestProduct)
	{
		return subAdminDao.getProductDetails(userInterestProduct);
	}
	
	
	
	
	// Check Report Generation
	
	@Transactional(readOnly = false)
	public String createCheckReport(String reportType,String reportFor,String fromDate,String toDate,HttpServletRequest request)
	{		
        List<Integer> userIdList = new ArrayList<Integer>();
        
		
		if(reportFor.equals("Offline transaction"))
		{
			List<TransactionDetailModel> tranDetails = subAdminDao.getOffLineTransacitonCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<tranDetails.size();i++)
	    	{
	    		userIdList.add(tranDetails.get(i).getTransaction_against_user_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String offLineTransationReportDirectory = fileResource.getString("offLineTransationReportDirectory");
	    	
	    	String fileDirectory = offLineTransationReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
	    	 try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address")); 
	             writableSheet.addCell(new Label(5, 0, "Payment Mode")); 
	             writableSheet.addCell(new Label(6, 0, "Payment Date")); 
	             writableSheet.addCell(new Label(7, 0, "Payment Amount")); 
	             writableSheet.addCell(new Label(8, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	                 {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));
	            	 writableSheet.addCell(new Label(5, j, tranDetails.get(i).getTransaction_mode()));
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(6, j, sdf.format(tranDetails.get(i).getTransaction_date())));	            	 
	            	 writableSheet.addCell(new Label(7, j, tranDetails.get(i).getTransaction_amount()));
	            	 writableSheet.addCell(new Label(8, j, tranDetails.get(i).getTransaction_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
    	         } catch (IOException e) {
    	             e.printStackTrace();
    	         } catch (RowsExceededException e) {
    	             e.printStackTrace();
    	         } catch (WriteException e) {
    	             e.printStackTrace();
    	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Check Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try {
						ipAddress = ip.getIPddress(request);
					} catch (SocketException | UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    }
	    	   
	           // End of Report Generator 	
	    	
		}
		else if(reportFor.equals("Refer Friend Check"))
		{
			List<UserReferenceDetailsModel> referDetails = subAdminDao.getReferenceCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<referDetails.size();i++)
	    	{
	    		userIdList.add(referDetails.get(i).getReference_given_user_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String referFriendReportDirectory = fileResource.getString("referFriendReportDirectory");
	    	
	    	String fileDirectory = referFriendReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
            try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address"));  
	             writableSheet.addCell(new Label(5, 0, "Reference Accept Date")); 
	             writableSheet.addCell(new Label(6, 0, "Reference Amount")); 
	             writableSheet.addCell(new Label(7, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));	            	 
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(5, j, sdf.format(referDetails.get(i).getReference_accept_date())));	            	 
	            	 writableSheet.addCell(new Number(6, j, referDetails.get(i).getReference_amount()));
	            	 writableSheet.addCell(new Label(7, j, referDetails.get(i).getReference_check_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
    	         } catch (IOException e) {
    	             e.printStackTrace();
    	         } catch (RowsExceededException e) {
    	             e.printStackTrace();
    	         } catch (WriteException e) {
    	             e.printStackTrace();
    	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Check Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try 
	    	    	{
						ipAddress = ip.getIPddress(request);
					} 
	    	    	catch (SocketException | UnknownHostException e) 
	    	    	{
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    }
		}
		else if(reportFor.equals("Refer Friend Bonus"))
		{
			List<ReferBonusAmountModel> referDetails = subAdminDao.getReferenceBonusCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<referDetails.size();i++)
	    	{
	    		userIdList.add(referDetails.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String referFriendBonusReportDirectory = fileResource.getString("referFriendBonusReportDirectory");
	    	
	    	String fileDirectory = referFriendBonusReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
	    	try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address"));  
	             writableSheet.addCell(new Label(5, 0, "Reference Bonus Date")); 
	             writableSheet.addCell(new Label(6, 0, "Bonus Amount")); 
	             writableSheet.addCell(new Label(7, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));	            	 
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(5, j, sdf.format(referDetails.get(i).getUpdate_date())));	            	 
	            	 writableSheet.addCell(new Number(6, j, referDetails.get(i).getBonus_amount()));
	            	 writableSheet.addCell(new Label(7, j, referDetails.get(i).getBonus_check_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
   	         } catch (IOException e) {
   	             e.printStackTrace();
   	         } catch (RowsExceededException e) {
   	             e.printStackTrace();
   	         } catch (WriteException e) {
   	             e.printStackTrace();
   	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Check Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try 
	    	    	{
						ipAddress = ip.getIPddress(request);
					} 
	    	    	catch (SocketException | UnknownHostException e) 
	    	    	{
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    }
		}
		else
		{
			List<PolicyAmountModel> policyDetails = subAdminDao.getPolicyBonusCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<policyDetails.size();i++)
	    	{
	    		userIdList.add(policyDetails.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String userPlanPolicyReportDirectory = fileResource.getString("userPlanPolicyReportDirectory");
	    	
	    	String fileDirectory = userPlanPolicyReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
	    	try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address"));  
	             writableSheet.addCell(new Label(5, 0, "Reference Bonus Date")); 
	             writableSheet.addCell(new Label(6, 0, "Bonus Amount")); 
	             writableSheet.addCell(new Label(7, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));	            	 
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(5, j, sdf.format(policyDetails.get(i).getPolicy_end_date())));	            	 
	            	 writableSheet.addCell(new Number(6, j, policyDetails.get(i).getPolicy_amount()));
	            	 writableSheet.addCell(new Label(7, j, policyDetails.get(i).getPolicy_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
   	         } catch (IOException e) {
   	             e.printStackTrace();
   	         } catch (RowsExceededException e) {
   	             e.printStackTrace();
   	         } catch (WriteException e) {
   	             e.printStackTrace();
   	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Check Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try 
	    	    	{
						ipAddress = ip.getIPddress(request);
					} 
	    	    	catch (SocketException | UnknownHostException e) 
	    	    	{
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    }
		}
		
		
		
		return "";
	}
	
	
	@Transactional(readOnly = false)
	public String deleteReports(String reportCategoryResource,String reportName,int reportId,HttpServletRequest request)
	{
		String checkReportStatus = "";
		try
		{    		
			File excelReport = new File(reportCategoryResource + "/" + reportName+".xls");       	
    		if(excelReport.delete())
    		{    			
    			checkReportStatus = "Done";    	
    			CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
    	    	
    			String ipAddress = "";
    	    	
    	    	try 
    	    	{
					ipAddress = ip.getIPddress(request);
				} 
    	    	catch (SocketException | UnknownHostException e) 
    	    	{
					e.printStackTrace();
				}
    	    	
    			subAdminDao.getUpdateReportStatus(reportId,ipAddress);
    		}
    		else
    		{
    			checkReportStatus = "Not Done";    			
    		}   	   
    	}
		catch(Exception e)
		{    		
    		e.printStackTrace();    		
    	}		
		return checkReportStatus;
	}
	
	
	// End of Check report Generation
	
	
	
	
	
	
	
	
	//  Starts ORDER REPORT ////////////////////////
	
	@Transactional(readOnly = false)
	public String createOrderReport(String reportType,String fromDate,String toDate,HttpServletRequest request)
	{
		String reportStatus = "";
		
		List<OrderDetailsModel> orderDetails = subAdminDao.getOrderDetailsForReport(reportType,fromDate,toDate);
		
		// Report Generator
    	
    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
    	String orderReportDirectory = fileResource.getString("orderReportDirectory");
    	
    	String fileDirectory = orderReportDirectory;
    	
    	String reportName = "Order_Report_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
    	
    	try {
    		 
             File exlDirectory = new File(fileDirectory);
             if(!exlDirectory.exists())
         	 {
            	 exlDirectory.mkdirs();
         	 }
             
             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
             
             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
             
             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
        	 
             //Add the created Cells to the heading of the sheet
             
             writableSheet.addCell(new Label(0, 0, "SERIAL NO"));
             writableSheet.addCell(new Label(1, 0, "ORDER ID"));
             writableSheet.addCell(new Label(2, 0, "FULL NAME"));
             writableSheet.addCell(new Label(3, 0, "EMAIL ID"));
             writableSheet.addCell(new Label(4, 0, "MOBILE NO"));
             writableSheet.addCell(new Label(5, 0, "SHIPPING ADDRESS"));  
             writableSheet.addCell(new Label(6, 0, "ORDER STATUS")); 
             writableSheet.addCell(new Label(7, 0, "ORDER DATE")); 
             writableSheet.addCell(new Label(8, 0, "EXPECTED DELIVIRY DATE"));
             
             int j = 1;
             
             for(int i=0;i<orderDetails.size();i++)
             {  	
            	 writableSheet.addCell(new Number(0, j, j));
            	 writableSheet.addCell(new Label(1, j, orderDetails.get(i).getOrder_unique_id()));
            	 writableSheet.addCell(new Label(2, j, orderDetails.get(i).getOrder_for_name()));
            	 writableSheet.addCell(new Label(3, j, orderDetails.get(i).getOrder_for_email_id()));
            	 writableSheet.addCell(new Label(4, j, orderDetails.get(i).getOrder_for_mobile_no()));
            	 writableSheet.addCell(new Label(5, j, orderDetails.get(i).getOrder_shipping_address()));	            	 
            	 writableSheet.addCell(new Label(6, j, orderDetails.get(i).getOrder_status()));
            	 
            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            	 
            	 writableSheet.addCell(new Label(7, j, sdf.format(orderDetails.get(i).getOrder_date())));	            	           	 
            	 writableSheet.addCell(new Label(8, j, orderDetails.get(i).getOrder_expected_date()));
            	 
            	 j = j + 1;
             }
                	             
             writableWorkbook.write();
             writableWorkbook.close();
  
	         } catch (IOException e) {
	             e.printStackTrace();
	         } catch (RowsExceededException e) {
	             e.printStackTrace();
	         } catch (WriteException e) {
	             e.printStackTrace();
	         }
    	   	    
    	 
    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
    	    {
    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
    	    	
    	    	reportModel.setReport_name(reportName);
    	    	reportModel.setGenerate_from("Product Admin");
    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
    	    	String ipAddress = "";
    	    	
    	    	try 
    	    	{
					ipAddress = ip.getIPddress(request);
				} 
    	    	catch (SocketException | UnknownHostException e) 
    	    	{
					e.printStackTrace();
				}
    	    	
    	    	reportModel.setReport_generate_system_ip(ipAddress);
    	    	reportModel.setReport_category("Order Report");
    	    	reportModel.setReport_exist_status("Exist");
    	    	reportModel.setReport_generate_date(new Date());
    	    	
    	    	subAdminDao.saveReportDetails(reportModel);
    	    	
    	    	reportStatus = "Success";
    	    }
    	    else
    	    {
    	    	reportStatus = "Failed";
    	    }
		
		return reportStatus;
	}
	
	
	@Transactional(readOnly = false)
	public List<ReportDetailsModel> getReportDetails(String restriction,String reportGenerateFrom)
	{
		return subAdminDao.getReportDetails(restriction,reportGenerateFrom);
	}
	
	
	
	/// Account Report  //////
	
	
	@Transactional(readOnly = false)
	public String createAccountReport(String reportType,String reportFor,String fromDate,String toDate,HttpServletRequest request)
	{
		String reportStatus = "";
		
		List<Integer> userIdList = new ArrayList<Integer>();
	        			
		if(reportFor.equals("Purchased Packet Transaction"))
		{
			List<TransactionDetailModel> tranDetails = subAdminDao.getPurchaseTransationForReport(reportType,fromDate,toDate);
			
			for(int i=0;i<tranDetails.size();i++)
	    	{
	    		userIdList.add(tranDetails.get(i).getTransaction_against_user_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String rootDirectory = fileResource.getString("purchaseTransationReportDirectory");
	    	
	    	String fileDirectory = rootDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
	    	 try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address")); 
	             writableSheet.addCell(new Label(5, 0, "Transaction Mode")); 
	             writableSheet.addCell(new Label(6, 0, "Transaction Date")); 
	             writableSheet.addCell(new Label(7, 0, "Transaction Amount")); 
	             writableSheet.addCell(new Label(8, 0, "VAT")); 
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));
	            	 writableSheet.addCell(new Label(5, j, tranDetails.get(i).getTransaction_mode()));
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(6, j, sdf.format(tranDetails.get(i).getTransaction_date())));	            	 
	            	 writableSheet.addCell(new Label(7, j, tranDetails.get(i).getTransaction_amount()));
	            	 writableSheet.addCell(new Label(8, j, tranDetails.get(i).getTransaction_VAT_amount()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
    	         } catch (IOException e) {
    	             e.printStackTrace();
    	         } catch (RowsExceededException e) {
    	             e.printStackTrace();
    	         } catch (WriteException e) {
    	             e.printStackTrace();
    	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Account Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try {
						ipAddress = ip.getIPddress(request);
					} catch (SocketException | UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    	reportStatus = "Success";
	    	    }
	    	    else
	    	    {
	    	    	reportStatus = "Failed";
	    	    }
	    	   
	           // End of Report Generator 	
		}
		else if(reportFor.equals("Refer Friend Transaction"))
		{
			
			List<UserReferenceDetailsModel> referDetails = subAdminDao.getReferenceCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<referDetails.size();i++)
	    	{
	    		userIdList.add(referDetails.get(i).getReference_given_user_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String referFriendReportDirectory = fileResource.getString("referFriendReportDirectory");
	    	
	    	String fileDirectory = referFriendReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
            try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address"));  
	             writableSheet.addCell(new Label(5, 0, "Reference Accept Date")); 
	             writableSheet.addCell(new Label(6, 0, "Reference Amount")); 
	             writableSheet.addCell(new Label(7, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));	            	 
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(5, j, sdf.format(referDetails.get(i).getReference_accept_date())));	            	 
	            	 writableSheet.addCell(new Number(6, j, referDetails.get(i).getReference_amount()));
	            	 writableSheet.addCell(new Label(7, j, referDetails.get(i).getReference_check_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
    	         } catch (IOException e) {
    	             e.printStackTrace();
    	         } catch (RowsExceededException e) {
    	             e.printStackTrace();
    	         } catch (WriteException e) {
    	             e.printStackTrace();
    	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {    	    	    	
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Account Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try 
	    	    	{
						ipAddress = ip.getIPddress(request);
					} 
	    	    	catch (SocketException | UnknownHostException e) 
	    	    	{
						e.printStackTrace();
					}
	    	    		    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist"); 
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    	reportStatus = "Success";
	    	    }
	    	    else
	    	    {
	    	    	reportStatus = "Failed";
	    	    }
		}
		else if(reportFor.equals("Refer Friend Bonus Transaction"))
		{
            List<ReferBonusAmountModel> referDetails = subAdminDao.getReferenceBonusCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<referDetails.size();i++)
	    	{
	    		userIdList.add(referDetails.get(i).getUser_id());
	    	}
	    				
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String referFriendBonusReportDirectory = fileResource.getString("referFriendBonusReportDirectory");
	    	
	    	String fileDirectory = referFriendBonusReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
	    	try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address"));  
	             writableSheet.addCell(new Label(5, 0, "Reference Bonus Date")); 
	             writableSheet.addCell(new Label(6, 0, "Bonus Amount")); 
	             writableSheet.addCell(new Label(7, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));	            	 
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(5, j, sdf.format(referDetails.get(i).getUpdate_date())));	            	 
	            	 writableSheet.addCell(new Number(6, j, referDetails.get(i).getBonus_amount()));
	            	 writableSheet.addCell(new Label(7, j, referDetails.get(i).getBonus_check_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
   	         } catch (IOException e) {
   	             e.printStackTrace();
   	         } catch (RowsExceededException e) {
   	             e.printStackTrace();
   	         } catch (WriteException e) {
   	             e.printStackTrace();
   	         }
	    	   	    
	    	
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Account Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try 
	    	    	{
						ipAddress = ip.getIPddress(request);
					} 
	    	    	catch (SocketException | UnknownHostException e) 
	    	    	{
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    	reportStatus = "Success";
	    	    }
	    	    else
	    	    {
	    	    	reportStatus = "Failed";
	    	    }
		}
		else
		{
            List<PolicyAmountModel> policyDetails = subAdminDao.getPolicyBonusCheckReports(reportType,reportFor,fromDate,toDate);
	    	
			for(int i=0;i<policyDetails.size();i++)
	    	{
	    		userIdList.add(policyDetails.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetails = subAdminDao.getUserDetailModelByUserId(userIdList);
	    	
	    	// Report Generator
	    	
	    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	    	String userPlanPolicyReportDirectory = fileResource.getString("userPlanPolicyReportDirectory");
	    	
	    	String fileDirectory = userPlanPolicyReportDirectory;
	    	
	    	String reportName = reportFor+"_"+fromDate.replaceAll("/", "-")+"_"+toDate.replaceAll("/", "-");
	    	
	    	try {
	    		 
	             File exlDirectory = new File(fileDirectory);
	             if(!exlDirectory.exists())
	         	 {
	            	 exlDirectory.mkdirs();
	         	 }
	             
	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
	             
	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
	             
	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
	        	 
	             //Add the created Cells to the heading of the sheet
	             
	             writableSheet.addCell(new Label(0, 0, "Serial No"));
	             writableSheet.addCell(new Label(1, 0, "Full Name"));
	             writableSheet.addCell(new Label(2, 0, "Email Id"));
	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
	             writableSheet.addCell(new Label(4, 0, "Shipping Address"));  
	             writableSheet.addCell(new Label(5, 0, "Reference Bonus Date")); 
	             writableSheet.addCell(new Label(6, 0, "Bonus Amount")); 
	             writableSheet.addCell(new Label(7, 0, "Check Status"));
	             
	             int j = 1;
	             
	             for(int i=0;i<userBasicDetails.size();i++)
	             {  	
	            	 writableSheet.addCell(new Number(0, j, j));
	            	 writableSheet.addCell(new Label(1, j, userBasicDetails.get(i).getUser_name()));
	            	 writableSheet.addCell(new Label(2, j, userBasicDetails.get(i).getUser_emailid()));
	            	 writableSheet.addCell(new Label(3, j, userBasicDetails.get(i).getUser_mobile_no()));
	            	 writableSheet.addCell(new Label(4, j, userBasicDetails.get(i).getUser_shipping_address()));	            	 
	            	 
	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	            	 
	            	 writableSheet.addCell(new Label(5, j, sdf.format(policyDetails.get(i).getPolicy_end_date())));	            	 
	            	 writableSheet.addCell(new Number(6, j, policyDetails.get(i).getPolicy_amount()));
	            	 writableSheet.addCell(new Label(7, j, policyDetails.get(i).getPolicy_status()));
	            	 
	            	 j = j + 1;
	             }
	                	             
	             writableWorkbook.write();
	             writableWorkbook.close();
	  
   	         } catch (IOException e) {
   	             e.printStackTrace();
   	         } catch (RowsExceededException e) {
   	             e.printStackTrace();
   	         } catch (WriteException e) {
   	             e.printStackTrace();
   	         }
	    	   	    
	    	 
	    	    if((new File(fileDirectory + "/" + reportName+".xls")).exists())
	    	    {
	    	    	ReportDetailsModel reportModel = new ReportDetailsModel();
	    	    	
	    	    	reportModel.setReport_name(reportName);
	    	    	reportModel.setGenerate_from("Account Admin");
	    	    	CaptureIPandMACaddressUtil ip = new CaptureIPandMACaddressUtil();
	    	    	String ipAddress = "";
	    	    	
	    	    	try 
	    	    	{
						ipAddress = ip.getIPddress(request);
					} 
	    	    	catch (SocketException | UnknownHostException e) 
	    	    	{
						e.printStackTrace();
					}
	    	    	
	    	    	reportModel.setReport_generate_system_ip(ipAddress);
	    	    	reportModel.setReport_category(reportFor);
	    	    	reportModel.setReport_exist_status("Exist");
	    	    	reportModel.setReport_generate_date(new Date());
	    	    	
	    	    	subAdminDao.saveReportDetails(reportModel);
	    	    	
	    	    	reportStatus = "Success";
	    	    }
	    	    else
	    	    {
	    	    	reportStatus = "Failed";
	    	    }
		}
		
		
		return reportStatus;
	}
	
	
	
	///  END OF ACCOUNT REPORT //////////
}
