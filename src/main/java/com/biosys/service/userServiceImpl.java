package com.biosys.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserProfileDetailsBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.dao.userDao;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.util.PasswordGenerator;

@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class userServiceImpl implements userService {

	@Autowired
	private userDao userDao;
	
	@Transactional(readOnly = false)
	public String validateUserEmailIdExistOrNot(int userId,String emailId)
	{
		return userDao.validateUserEmailIdExistOrNot(userId,emailId);
	}
	
	@Transactional(readOnly = false)
	public List<UserBasicInfoBean> getUserBasicDetailsViaVendorId(String vendorId)
	{
		return userDao.getUserBasicDetailsViaVendorId(vendorId);
	}
	
	@Transactional(readOnly = false)
	public String getFullNameViaVenId(String vendorId)
	{
		return userDao.getFullNameViaVenId(vendorId);
	}
	
	@Transactional(readOnly = false)
	public int getUserIdViaVendorId(String vendorId)
	{
		return userDao.getUserIdViaVendorId(vendorId);
	}
	
	@Transactional(readOnly = false)
	public List<UserProfileDetailsBean> getUserOtherProfileDetailsViaUserId(int userId)
	{
		return userDao.getUserOtherProfileDetailsViaUserId(userId);
	}
	
	@Transactional(readOnly = false)
	public List<OrderDetailsBean> getLastOrderDetailsViaUserId(int userId)
	{
		return userDao.getLastOrderDetailsViaUserId(userId);
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsBean> GetReferenceDetailsViaUserId(int userId)
	{
		return userDao.GetReferenceDetailsViaUserId(userId);
	}
	
	@Transactional(readOnly = false)
	public String saveUserOtherProfileDetails(UserProfileDetailsModel userOthModel)
	{
		return userDao.saveUserOtherProfileDetails(userOthModel);
	}
	
	@Transactional(readOnly = false)
	public int getUserProfileIdViaUserId(int userId)
	{
		return userDao.getUserProfileIdViaUserId(userId);
	}
	
	@Transactional(readOnly = false)
	public String updateProfileImageInBasicInfo(int userId,String imageName)
	{
		return userDao.updateProfileImageInBasicInfo(userId,imageName);
	}
	
	@Transactional(readOnly = false)
	public String getProfilePhoto(int userId)
	{
		return userDao.getProfilePhoto(userId);
	}
	
	@Transactional(readOnly = false)
	public String getUpdateUserBasicInfo(UserBasicInfoModel basicInfo)
	{
		return userDao.getUpdateUserBasicInfo(basicInfo);
	}
	
	@Transactional(readOnly = false)
	public String checkCurrentPassword(String password,String vendorId)
	{
		String checkStatus = "";
		
		String getCurrentPassword = userDao.getCurrentPassword(vendorId);
		PasswordGenerator pwUtil = new PasswordGenerator();
		String encpwd = "";
		
		try {
		     encpwd = pwUtil.encrypt(password);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(encpwd.equals(getCurrentPassword))
		{
			checkStatus = "Matched";
		}
		else
		{
			checkStatus = "Not Matched";
		}
		return checkStatus;
	}
	
	@Transactional(readOnly = false)
	public String updatePassword(String password,String vendorId)
	{
		PasswordGenerator pwUtil = new PasswordGenerator();
		String encpwd = "";
		
		try {
		     encpwd = pwUtil.encrypt(password);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userDao.updatePassword(encpwd,vendorId);
	}
	
	@Transactional(readOnly = false)
	public List<OrderDetailsBean> getAllOrderDetails(int userId)
	{
		return userDao.getAllOrderDetails(userId);
	}
	
	@Transactional(readOnly = false)
	public String getUserPlanViaUserId(int userId)
	{
		return userDao.getUserPlanViaUserId(userId);
	}
	
	@Transactional(readOnly = false)
	public int getPolicyAmountAsPerPlan(String userPlan)
	{
		return userDao.getPolicyAmountAsPerPlan(userPlan);
	}
	
	@Transactional(readOnly = false)
	public List<OrderDetailsModel> getUserOrderDetails(int userId)
	{
		return userDao.getUserOrderDetails(userId);
	}
	
	@Transactional(readOnly = false)
	public String saveUserPolicyAmountDetails(PolicyAmountModel policyAmount)
	{
		return userDao.saveUserPolicyAmountDetails(policyAmount);
	}

	@Transactional(readOnly = false)
	public List<PolicyAmountModel> getUserAllPolicyDetails(int userId)
	{
		return userDao.getUserAllPolicyDetails(userId);
	}
	
	@Transactional(readOnly = false)
	public String getUpdateUserRegDate(int regUserId)
	{
		return userDao.getUpdateUserRegDate(regUserId);	
	}
	
	@Transactional(readOnly = false)
	public String getUpdateReferDetailsIfExist(String emailId)
	{
		String updateStatus = "";
		String planBonusUpdate = "";
		
		List<UserReferenceDetailsModel> referDetails = userDao.getReferDetailsWhoRefer(emailId);
		
		if(referDetails.size()>0)
		{
			
			int userId = referDetails.get(0).getReference_given_user_id();
			String userPlan = userDao.getUserPlanViaUserId(userId);			
			int referAmount = userDao.getReferAmountAsPerPlan(userPlan);
			
			updateStatus = userDao.getUpdateUserReferStatus(referDetails.get(0).getReference_id(),referAmount);
			
			
			// Check User Update Refer Status and Update Bonus Amount
			   
		        Date registeredDate = userDao.getUserRegDate(userId);
		        Date today = new Date();
		        
		        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		        String startDate = format.format(registeredDate);
		        String endDate = format.format(today);
		        
		        Date d1 = null;
				Date d2 = null;
				try {
					
					d1 = format.parse(startDate);
					d2 = format.parse(endDate);
					long diff = d2.getTime() - d1.getTime();
					long diffDays = diff / (24 * 60 * 60 * 1000) + 2;
					
					int plan_Id = userDao.getPlanIdViaPlanName(userPlan);
					
				    if(plan_Id>0)
				    {				    	
				    	List<BusinessPlanFeatureModel> planFeature = userDao.getAllPlanFeature(plan_Id);
				    	if(planFeature.size()>0)
				    	{
				    		List<Integer> noOfPlanDays = new ArrayList<Integer>();
					    	List<Integer> noOfPlanFriends = new ArrayList<Integer>();
					    	
					    	for(int i=0;i<planFeature.size();i++)
					    	{
					    		noOfPlanDays.add(planFeature.get(i).getFeature_restrict_day());
					    		noOfPlanFriends.add(planFeature.get(i).getFeature_restrict_friend());
					    	}
					    	
					    	List<UserReferenceDetailsModel> noOfJoinedRefer = userDao.getNoOfUserReferJoined(userId);	
					    	
					    	for(int d=0;d<planFeature.size();d++)
					    	{
					    		System.out.println("Day Diff="+diffDays+"=="+noOfPlanDays.get(d));
					    		
					    		if(diffDays <= noOfPlanDays.get(d))
					    		{							    			
					    			int checkDataExistOrNot = userDao.checkPlanBonusExistOrNot(noOfPlanDays.get(d));
					    			
					    			if(checkDataExistOrNot==0)
					    			{					    				
					    				if(noOfJoinedRefer.size()>=noOfPlanFriends.get(d))
					    				{					    					
					    					ReferBonusAmountModel planBonus = new ReferBonusAmountModel();
					    					
					    					planBonus.setUser_id(userId);
					    					planBonus.setUser_plan_no_of_days(noOfPlanDays.get(d));
					    					planBonus.setNo_of_refers_user(noOfPlanFriends.get(d));
					    					planBonus.setBonus_type("With in duration");
					    					planBonus.setBonus_amount(planFeature.get(d).getFeature_restrict_bonus_amount());
					    					planBonus.setUpdate_date(new Date());
					    					planBonus.setBonus_check_status("Pending");
					    					
					    					planBonusUpdate = userDao.saveUserPlanBonus(planBonus);
					    					
					    					break;
					    				}
					    				else
					    				{
					    					planBonusUpdate = "Not Required";
					    				}
					    			}
					    			else
					    			{
					    				planBonusUpdate = "Not Required";
					    			}
					    		}
					    		else if(diffDays > noOfPlanDays.get(d))
					    		{
					    			int checkDataExistOrNot = userDao.checkPlanBonusExistOrNot(noOfPlanDays.get(d));
					    			if(checkDataExistOrNot==0)
					    			{
					    				if(noOfJoinedRefer.size()>=noOfPlanFriends.get(d))
					    				{
					    					ReferBonusAmountModel planBonus = new ReferBonusAmountModel();
					    					
					    					planBonus.setUser_id(userId);
					    					planBonus.setUser_plan_no_of_days(noOfPlanDays.get(d));
					    					planBonus.setNo_of_refers_user(noOfPlanFriends.get(d));
					    					planBonus.setBonus_type("After duration");
					    					planBonus.setBonus_amount(planFeature.get(d).getFeature_restrict_compliment_amount());
					    					planBonus.setUpdate_date(new Date());					    					
					    					planBonus.setBonus_check_status("Pending");
					    					
					    					planBonusUpdate = userDao.saveUserPlanBonus(planBonus);
					    					
					    					break;
					    				}
					    				else
					    				{
					    					planBonusUpdate = "Not Required";
					    				}
					    			}
					    			else
					    			{
					    				planBonusUpdate = "Not Required";
					    			}
					    		}
					    		else
					    		{
					    			planBonusUpdate = "Not Required";
					    		}
					    	}
					    					    	
				    	}				    	
				    }				    										
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
								
			// End Check User Update Refer Status and Update Bonus Amount
		        
		}
		else
		{
			updateStatus = "Not Required";
		}
		
		System.out.println("Plan Bonus update status="+planBonusUpdate);
		
		return updateStatus;
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsModel> getUserReferDetails(int userId)
	{
		return userDao.getUserReferDetails(userId);
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsModel> getReferDetailViaReferId(int referenceId)
	{
		return userDao.getReferDetailViaReferId(referenceId);
	}
	
	@Transactional(readOnly = false)
	public List<ReferBonusAmountModel> getReferalBonusDetails(int userId)
	{
		return userDao.getReferalBonusDetails(userId);
	}
	
	
}
