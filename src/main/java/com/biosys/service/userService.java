package com.biosys.service;

import java.util.List;

import com.biosys.bean.BusinessPlanBean;
import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserProfileDetailsBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;


public interface userService {

	public String validateUserEmailIdExistOrNot(int userId,String emailId);
		
	public List<UserBasicInfoBean> getUserBasicDetailsViaVendorId(String vendorId);
	
	public String getFullNameViaVenId(String vendorId);
	
	public int getUserIdViaVendorId(String vendorId);
	
	public List<UserProfileDetailsBean> getUserOtherProfileDetailsViaUserId(int userId);
	
	public List<OrderDetailsBean> getLastOrderDetailsViaUserId(int userId);
	
	public List<UserReferenceDetailsBean> GetReferenceDetailsViaUserId(int userId);
	
	public String saveUserOtherProfileDetails(UserProfileDetailsModel userOthModel);
	
	public int getUserProfileIdViaUserId(int userId);
	
	public String updateProfileImageInBasicInfo(int userId,String imageName);
	
	public String getProfilePhoto(int userId);
	
	public String getUpdateUserBasicInfo(UserBasicInfoModel basicInfo);
	
	public String checkCurrentPassword(String password,String vendorId);
	
	public String updatePassword(String password,String vendorId);
	
	public List<OrderDetailsBean> getAllOrderDetails(int userId);
	
	public String getUserPlanViaUserId(int userId);
	
	public int getPolicyAmountAsPerPlan(String userPlan);
	
	public List<OrderDetailsModel> getUserOrderDetails(int userId);
	
	public String saveUserPolicyAmountDetails(PolicyAmountModel policyAmount);
	
	public List<PolicyAmountModel> getUserAllPolicyDetails(int userId);
	
	public String getUpdateReferDetailsIfExist(String emailId);
	
	public List<UserReferenceDetailsModel> getUserReferDetails(int userId);
	
	public List<UserReferenceDetailsModel> getReferDetailViaReferId(int referenceId);
	
	public String getUpdateUserRegDate(int regUserId);	
	
	public List<ReferBonusAmountModel> getReferalBonusDetails(int userId);
	
 }
