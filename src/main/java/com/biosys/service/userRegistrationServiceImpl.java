package com.biosys.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biosys.bean.UserBasicInfoBean;
import com.biosys.dao.userRegistrationDao;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserPlanDetailsModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.domain.UserRegistrationStageModel;
import com.biosys.domain.UserResetPasswordModel;
import com.biosys.domain.UserReturnProductModel;
import com.biosys.util.EmailSentUtil;
import com.biosys.util.PasswordGenerator;
import com.biosys.util.SendSms;


@Service("userRegService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class userRegistrationServiceImpl implements userRegistrationService {

	@Autowired
	userRegistrationDao userRegDao;
	
	@Transactional(readOnly = false)
	public String saveUserBasicDetails(UserBasicInfoModel userModel)
	{
		return userRegDao.saveUserBasicDetails(userModel);
	}
	
	@Transactional(readOnly = false)
	public int getLastUserId()
	{
		return userRegDao.getLastUserId();
	}
	
	@Transactional(readOnly = false)
	public String checkEmailIdExistOrNot(String emailId)
	{
		return userRegDao.checkEmailIdExistOrNot(emailId);
	}
	
	@Transactional(readOnly = false)
	public int getUserIdViaEmailId(String emailId)
	{
		return userRegDao.getUserIdViaEmailId(emailId);
	}
	
	@Transactional(readOnly = false)
	public String saveUserStepDetails(UserRegistrationStageModel regStage)
	{
		return userRegDao.saveUserStepDetails(regStage);
	}
	
	@Transactional(readOnly = false)
	public String saveProductInterestOfUser(UserProductInterestModel intModel)
	{
		return userRegDao.saveProductInterestOfUser(intModel);
	}
	
	@Transactional(readOnly = false)
	public int getUserRegStageId(int regUserId)
	{
		return userRegDao.getUserRegStageId(regUserId);
	}
	
	@Transactional(readOnly = false)
	public String saveReferenceDetails(UserReferenceDetailsModel refModel)
	{
		return userRegDao.saveReferenceDetails(refModel);
	}
	
	@Transactional(readOnly = false)
	public String saveUserPlanDetails(UserPlanDetailsModel planModel)
	{
		return userRegDao.saveUserPlanDetails(planModel);
	}
	
	@Transactional(readOnly = false)
	public List<UserBasicInfoBean> getUserBasicInfoViaUserId(int regUserId)
	{
		return userRegDao.getUserBasicInfoViaUserId(regUserId);
	}
	
	@Transactional(readOnly = false)
	public String saveTransactionDetails(TransactionDetailModel tranModel)
	{
		return userRegDao.saveTransactionDetails(tranModel);
	}
	
	@Transactional(readOnly = false)
	public String saveOrderDetails(OrderDetailsModel ordModel)
	{
		return userRegDao.saveOrderDetails(ordModel);
	}
	
	@Transactional(readOnly = false)
    public int getLastTransactionId()
    {
    	return userRegDao.getLastTransactionId();
    }
	
	@Transactional(readOnly = false)
	public int getLastOrderId()
	{
		return userRegDao.getLastOrderId();
	}
	
	@Transactional(readOnly = false)
	public String saveReturnProducDetails(UserReturnProductModel returnModel)
	{
		return userRegDao.saveReturnProducDetails(returnModel);
	}
	
	@Transactional(readOnly = false)
	public List<BusinessPlanModel> getPlanDetailsViaPlanName(String planType)
	{
		return userRegDao.getPlanDetailsViaPlanName(planType);
	}
	
	@Transactional(readOnly = false)
	public String getRegistrationStage(int regUserId)
	{
		String regStage = userRegDao.getRegistrationStage(regUserId);
		
		if(!regStage.contains("Product"))
		{
			return "Product";
		}
		else if(!regStage.contains("Reference"))
		{
			if(!regStage.contains("Plan"))
			{
				return "Plan";
			}
			else if(!regStage.contains("Payment"))
			{
				return "Payment";
			}
			else
			{
				return "Not Required";
			}			
		}
		else if(!regStage.contains("Plan"))
		{
			return "Plan";
		}
		else if(!regStage.contains("Payment"))
		{
			return "Payment";
		}
		else
		{
			return "Not Required";
		}
	}
	
	@Transactional(readOnly = false)
	public int getLastResetId()
	{
		return userRegDao.getLastResetId();
	}
	
	@Transactional(readOnly = false)
	public String saveResetDetails(UserResetPasswordModel resetModel)
	{
		return userRegDao.saveResetDetails(resetModel);
	}
	

    @Transactional(readOnly = false)
	public String getValidateUser(String emailId,String password)
	{
		PasswordGenerator pwUtil = new PasswordGenerator();
		String encryptPassword = "";
		String validateStatus = "";
		
		//String existPassword = userRegDao.getPasswordViaEmailId(emailId);
			
		try {
			 encryptPassword = pwUtil.encrypt(password);
			 
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		validateStatus = userRegDao.getValidateUser(emailId,encryptPassword);		 
		 
		return validateStatus;
	}
	
    @Transactional(readOnly = false)
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		return userRegDao.getEmailIdFromForgetPassword(uniqueId);
	}
	
    @Transactional(readOnly = false)
	public String getUpdatePasswordViaEmailId(String emailId,String password)
	{
		return userRegDao.getUpdatePasswordViaEmailId(emailId,password);
	}
	
    @Transactional(readOnly = false)
	public int getResetIdViaUniqueId(String reqUniqueId)
	{
		return userRegDao.getResetIdViaUniqueId(reqUniqueId);
	}
	
    @Transactional(readOnly = false)
	public String getDeleteResetDetails(String emailId)
	{
		return userRegDao.getDeleteResetDetails(emailId);      
	}
	
    @Transactional(readOnly = false)
	public List<BusinessPlanFeatureModel> getPlanFeaturesViaPlanId(int plan_id)
	{
		return userRegDao.getPlanFeaturesViaPlanId(plan_id);
	}
		
    @Transactional(readOnly = false)
	public List<UserProductInterestModel> getInterestedProduct(int regUserId)
	{
		return userRegDao.getInterestedProduct(regUserId);
	}
	
    @Transactional(readOnly = false)
    public int getLastProductInterestId(int regUserId)
    {
    	return userRegDao.getLastProductInterestId(regUserId);
    }
    
    @Transactional(readOnly = false)
    public String saveUserIntProAsperOrder(int productInterestId,String orderId)
    {
    	return userRegDao.saveUserIntProAsperOrder(productInterestId,orderId);
    }
	
    @Transactional(readOnly = false)
    public String asignOrderIdToTranId(String transactionId,String orderId)
    {
    	return userRegDao.asignOrderIdToTranId(transactionId,orderId);
    }
    
    
	// SMS BLock
	
    @Transactional(readOnly = false)
	@SuppressWarnings("static-access")
	public String welcomeSMS(String fullName,String mobileNo)
	{
		String smsStatus = "";
		ResourceBundle resource = ResourceBundle.getBundle("resources/wellcomeRegistration");		 
		String message=resource.getString("smsMessage");
		
		String smsMessage = "Dear "+fullName+"   "+message;
		
		SendSms smsObject = new SendSms();
		smsStatus = smsObject.smsAPI(smsMessage, mobileNo);
		
		return smsStatus;
	}
	
	
	
	
	// ENd of SMS Block
	
	
	
	
	
	
	
	//  Email Block
	
	
	
    @Transactional(readOnly = false)
	public String welcomMail(int userRegId,String fullName,String emailId,String propertiFileName)
	{		
		    
		         // Send Email to User that register in the block
		 
				 ResourceBundle resource = ResourceBundle.getBundle("resources/wellcomeRegistration");
					 
				 String mailSubject=resource.getString("mailSubject");
				 
				 String headerText=resource.getString("headerText");
			   	 String headerLogo=resource.getString("headerLogo");
			   	 String contactNo=resource.getString("contactNo");
			   	 String contactEmailId=resource.getString("contactEmailId");
			   	 
			   	 String bodyHeading=resource.getString("bodyHeading");
			   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
			   	 String bodyMessage2=resource.getString("bodyMessage2");
			   	 
			   	 String actionLink=resource.getString("actionLink");
			   	 String belowImage=resource.getString("belowImage");
			   	 
			   	 String footerMessage=resource.getString("footerMessage");
			   	 
			   	 String copyright=resource.getString("copyright");
				 String termscondition=resource.getString("termscondition");		
				 String privacyPolicy = resource.getString("privacyPolicy");
					 
			   	 String facebookicon=resource.getString("facebookicon");
			   	 String twittericon=resource.getString("twittericon");
			   	 String googleicon=resource.getString("googleicon");
			   	 String linkedicon=resource.getString("linkedicon");
			   	 
			   	 String facebookLink=resource.getString("facebookLink");
			   	 String twitterLink=resource.getString("twitterLink");
			   	 String googlePlus=resource.getString("googlePlus");
			   	 String linkedIn=resource.getString("linkedIn");
			   	 
			   	 
			     StringBuilder text = new StringBuilder();
				        			        
				        
					    /* Sending Emails to Register user */
					       
				        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
				        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
				       
				        text.append("<body style='background-color: #ece8e5;'>");
				       
				        text.append("<table width='600' cellspacing='0' align='center' cellpadding='5' style='background: #fff; font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ccc;'>");
				        
						      
				                /* Header */
						        				        
						        text.append("<tr><td><table width='100%' bgcolor='#00b6f5' cellspacing='0' cellpadding='0px' border='0px' height='45'>");
						        text.append("<tbody><tr><td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>"+headerText+"</td></tr>");
						        text.append("</tbody></table></td></tr>");
						       
						        /* End of Header */
						        
						        /* Header-2 */
						        
						        text.append("<tr><td><table width='100%' style='color: #000;'>");
						        text.append("<tr>");
						        text.append("<td style='float: left;'><a href='index.html'><img width='250' height='50' src='"+headerLogo+"' /></a></td>");
						        text.append("<td style='float: right; color: #777; font-size: 12px; width: 250px;'>");
						        text.append("<span> <b style='color: #000;'>Conatct Number :</b>"+contactNo+"</span>");
						        text.append("<br> <span> <b style='color: #000;'>E-Mail : </b>"+contactEmailId+"</span> ");
						        text.append("</td></tr>");
						        text.append("</table></td></tr>");
						        
						        /* End of Header-2 */
						        
						        
						        /* Body Heading-1 */
						        
						        text.append("<tr><td><h1 style='background-color: #00b6f5; border: 1px solid #ccc; color: #fff; font-size: 20px; margin: 0; text-align: left; width: 100%;'>"+bodyHeading+"</h1></td>");
						        text.append("</tr>");
						       
						        /* End of Body Heading-1 */
						        
						        
						        /* BODY PART */
						        
						        
	                                /* Body Part-1 */
							        
							        text.append("<tr><td style='text-align: justify;'>Dear <b>"+fullName+"</b>,</td></tr>");
							       
							        /* End of Body Part-1 */
							        
                                    /* Body Part-2 */
							        
							        text.append("<tr><td style='text-align: justify;'>"+bodyMessage1+"</td></tr>");
							       
							        /* End of Body Part-2 */
							        
							        
                                    /* Body Part-3 */
							        
							        text.append("<tr><td style='text-align: justify;'>"+bodyMessage2+"</td></tr>");
							       
							        /* End of Body Part-3 */
							        
							        
                                    /* Body link */
							        
							        text.append("<tr><td><a href='http://localhost:8080/ProjectNo788/userLogin.html' target='_blank'>Click here & enjoy the service</a></td></tr>");
							       
							        /* End of Body link */
							        
                                    /* Body Down image */
							        
							        text.append("<tr><td width='300'><img width='600' height='177' src='"+belowImage+"'></td></tr>");
							       
							        /* End of Body Down image */
							        
                                    /* footer -1  */
							        
							        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px; background-color: #d3d3d3'>"+footerMessage+"</td></tr>");
							       
							        /* End of footer - 1 */
							        
                                    /* footer -2  */
							        
							        text.append("<tr><td style='border: medium none; text-align: right; width: 150px; height: 23px;'>");
							       
							        text.append("<table width='600' cellspacing='0' cellpadding='0px' border='0px' bgcolor='#00b6f5' bordercolordark='#fff' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
							        text.append("<tbody>");
							        text.append("<tr><td>");
							        text.append("<table width='285' cellspacing='0' cellpadding='0px' align='left' class='column' style='color: #a3c9e1;'>");
							        text.append("<tbody>");
							        text.append("<tr><td><h5 style='color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0; width: 155px;'>Copyright &copy; 2015 biosys.</h5></td></tr>");
							        text.append("<tr style='float: left; margin-left: 12px;'><td><a style='color: #ffffff;' href='"+termscondition+"'>Terms &amp;Conditions </a> | <a style='color: #ffffff;' href='"+privacyPolicy+"'>Privacy Policy</a></td></tr>");
							        text.append("</tbody></table>");
							        
							        text.append("<table cellspacing='0' cellpadding='0' align='right' style='color: #a3c9e1; width: 150px; text-align: right;'>");
							        text.append("<tbody>");
							        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
							        text.append("<tr><td><a href='"+facebookLink+"'><img src='"+facebookicon+"'></a></td>");
							        text.append("<td><a href='"+twitterLink+"'><img src='"+twittericon+"'></a></td>");
							        text.append("<td><a href='"+googlePlus+"'><img src='"+googleicon+"'></a></td>");
							        text.append("<td><a href='"+linkedIn+"'><img src='"+linkedicon+"'></a></td>");
							        text.append("</tr></tbody></table>");
							        text.append("</td></tr></tbody></table>");
							        text.append("</td></tr>");
							        
							        
							        /* End of footer - 2 */
							       
						        
				        text.append("</table>");
				        text.append("</body>");
				        text.append("</html>");
				       
				        /* END OF BODY PART */
				        
				        
				      /* End of Sending Emails to Register user */
				    
				    EmailSentUtil emailStatus = new EmailSentUtil();
	     	        
	     			String resetStatus = "";
	     			
					try {
						resetStatus = emailStatus.getEmailSent(emailId,mailSubject,text.toString());
					} 
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					EmailSentModel emailModel = new EmailSentModel();
					
					if(resetStatus.equals("success"))
					{
						emailModel.setSent_email_id(emailId);
						emailModel.setEmail_subject(mailSubject);
						emailModel.setEmail_message_content(text.toString());
						emailModel.setEmail_sent_status("success");
						emailModel.setEmail_sent_date(new Date());
					}
					else
					{
						emailModel.setSent_email_id(emailId);
						emailModel.setEmail_subject(mailSubject);
						emailModel.setEmail_message_content(text.toString());
						emailModel.setEmail_sent_status("failed");
						emailModel.setEmail_sent_date(new Date());
					}
					
					userRegDao.saveEmailDetails(emailModel);
					
					return resetStatus;
	}
	
	
    @Transactional(readOnly = false)
	public String orderMail(int userRegId,String fullName,String emailId,String mobileNo,String propertiFileName,String address,String orderId,HttpSession session)
	{
		// Send Email to User that register in the block
		 
		 ResourceBundle resource = ResourceBundle.getBundle("resources/orderDetailsMail");
			 
		 String mailSubject=resource.getString("mailSubject");
		 
		 String headerText=resource.getString("headerText");
	   	 String headerLogo=resource.getString("headerLogo");
	   	 String contactNo=resource.getString("contactNo");
	   	 String contactEmailId=resource.getString("contactEmailId");
	   	 
	   	 String bodyHeading=resource.getString("bodyHeading");
	   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
	   	 String bodyMessage2=resource.getString("bodyMessage2");
	   	 
	   	 String actionLink=resource.getString("actionLink");
	   	 String belowImage=resource.getString("belowImage");
	   	 
	   	 String footerMessage=resource.getString("footerMessage");
	   	 
	   	 String copyright=resource.getString("copyright");
		 String termscondition=resource.getString("termscondition");		
		 String privacyPolicy = resource.getString("privacyPolicy");
			 
	   	 String facebookicon=resource.getString("facebookicon");
	   	 String twittericon=resource.getString("twittericon");
	   	 String googleicon=resource.getString("googleicon");
	   	 String linkedicon=resource.getString("linkedicon");
	   	 
	   	 String facebookLink=resource.getString("facebookLink");
	   	 String twitterLink=resource.getString("twitterLink");
	   	 String googlePlus=resource.getString("googlePlus");
	   	 String linkedIn=resource.getString("linkedIn");
	   	 
	   	 
	     StringBuilder text = new StringBuilder();
		        			        
		        
			    /* Sending Emails to Register user */
			       
		        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
		        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
		       
		        text.append("<body style='background-color: #ece8e5;'>");
		       
		        text.append("<table width='600' cellspacing='0' align='center' cellpadding='5' style='background: #fff; font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ccc;'>");
		        
				      
		                /* Header */
				        				        
				        text.append("<tr><td><table width='100%' bgcolor='#00b6f5' cellspacing='0' cellpadding='0px' border='0px' height='45'>");
				        text.append("<tbody><tr><td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>"+headerText+"</td></tr>");
				        text.append("</tbody></table></td></tr>");
				       
				        /* End of Header */
				        
				        /* Header-2 */
				        
				        text.append("<tr><td><table width='100%' style='color: #000;'>");
				        text.append("<tr>");
				        text.append("<td style='float: left;'><a href='index.html'><img width='250' height='50' src='"+headerLogo+"' /></a></td>");
				        text.append("<td style='float: right; color: #777; font-size: 12px; width: 250px;'>");
				        text.append("<span> <b style='color: #000;'>Conatct Number :</b>"+contactNo+"</span>");
				        text.append("<br> <span> <b style='color: #000;'>E-Mail : </b>"+contactEmailId+"</span> ");
				        text.append("</td></tr>");
				        text.append("</table></td></tr>");
				        
				        /* End of Header-2 */
				        
				        
				        /* Body Heading-1 */
				        
				        text.append("<tr><td><h1 style='background-color: #00b6f5; border: 1px solid #ccc; color: #fff; font-size: 20px; margin: 0; text-align: left; width: 100%;'>"+bodyHeading+"</h1></td>");
				        text.append("</tr>");
				       
				        /* End of Body Heading-1 */
				        
				        
				        /* BODY PART */
				        
				        
                           /* Body Part-1 */
					        
					        text.append("<tr><td style='text-align: justify;'>Dear <b>"+fullName+"</b>,</td></tr>");
					       
					        /* End of Body Part-1 */
					        
                           /* Body Part-2 */
					        
					        text.append("<tr><td style='text-align: justify;'>"+bodyMessage1+"</td></tr>");
					       
					        /* End of Body Part-2 */
					        
					        
					         //  Order detail Table
					        
			                           /* Body Part-3 */
								        
								        
					                      text.append("<tr><td><table width='100%' cellpadding='5' cellspacing='0px'>");
					                      text.append("<tr><td colspan='6' style='border: 1px solid #ccc; background-color: #00b6f5; color: #fff;'><strong>Order Details of 1 KIT</strong></td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Name : </strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+fullName+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Email Id :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+emailId+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Contact No :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>+91 &nbsp; "+mobileNo+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Shipping Address :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+address+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Order Id :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>"+orderId+"</td></tr>");
					                      text.append("<tr><td style='border: 1px solid #ccc; width:130px;'><strong>Currency :</strong></td>");
					                      text.append("<td colspan='5' style='border: 0px solid #ccc;'>INR</td></tr>");
					                      text.append("</table>");
					                      text.append("</td></tr>");
					                      
					                      text.append("<tr><td><p style='border-top:1px solid #ccc;margin-top: -10px;'></p></td></tr>");
					                      
					                      text.append("<tr><td>"); 
					                      text.append("<table width='100%' cellspacing='0px' cellpadding='5' style='margin-top: -22px;'>");
					                      text.append("<tbody>");
					                    
					                      text.append("<tr style='float: right; text-align: right;'>");
					                      text.append("<td style='border: 0px solid #ccc;width:130px;'><strong>KIT Amount :</strong></td>");
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+session.getAttribute("kitCharge")+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("<tr><td></td></tr>");
					                      
					                      text.append("<tr style='float: right; text-align: right;'>"); 
					                      text.append("<td style='border: 0px solid #ccc;width:130px;'><strong>VAT ("+session.getAttribute("vat")+"%) :</strong></td>");
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+session.getAttribute("vatPersentageVal")+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("<tr><td></td></tr>");
					                      
					                      text.append("<tr style='float: right; text-align: right;'>");
					                      text.append("<td style='border: 0px solid #ccc;width:130px;'><strong>Subtotal :</strong></td>");
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+session.getAttribute("subtotal")+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("<tr><td></td></tr>");
					                      
					                      text.append("<tr style='float: right; text-align: right;'>");
					                      text.append("<td style='border: 0px solid #ccc;width:200px;'><strong>Registration Charge :</strong></td>"); 
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+session.getAttribute("regCharge")+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("<tr><td><p style='border-top:1px solid #ccc;margin-top: -10px;'></p></td></tr>");
					                      
					                      text.append("<tr style='float: right; text-align: right;'>");
					                      text.append("<td style='border: 0px solid #ccc;width:200px;'><strong>Total :</strong></td>"); 
					                      text.append("<td style='border: 0px solid rgb(204, 204, 204); width: 100px;'>"+session.getAttribute("total")+"</td>");
					                      text.append("</tr>");
					                      
					                      text.append("</tbody>");			                      
					                      text.append("</table>");
					                      
					                      text.append("</td></tr>");
					                    
								        
								        /* End of Body Part-3 */
							
								        
						     //  End of Order detail Table			        
								        
                           
                           /* footer -1  */
					        
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px; background-color: #d3d3d3'>"+footerMessage+"</td></tr>");
					       
					        /* End of footer - 1 */
					        
                           /* footer -2  */
					        
					        text.append("<tr><td style='border: medium none; text-align: right; width: 150px; height: 23px;'>");
					       
					        text.append("<table width='600' cellspacing='0' cellpadding='0px' border='0px' bgcolor='#00b6f5' bordercolordark='#fff' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tbody>");
					        text.append("<tr><td>");
					        text.append("<table width='285' cellspacing='0' cellpadding='0px' align='left' class='column' style='color: #a3c9e1;'>");
					        text.append("<tbody>");
					        text.append("<tr><td><h5 style='color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0; width: 155px;'>Copyright &copy; 2015 biosys.</h5></td></tr>");
					        text.append("<tr style='float: left; margin-left: 12px;'><td><a style='color: #ffffff;' href='"+termscondition+"'>Terms &amp;Conditions </a> | <a style='color: #ffffff;' href='"+privacyPolicy+"'>Privacy Policy</a></td></tr>");
					        text.append("</tbody></table>");
					        
					        text.append("<table cellspacing='0' cellpadding='0' align='right' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tbody>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href='"+facebookLink+"'><img src='"+facebookicon+"'></a></td>");
					        text.append("<td><a href='"+twitterLink+"'><img src='"+twittericon+"'></a></td>");
					        text.append("<td><a href='"+googlePlus+"'><img src='"+googleicon+"'></a></td>");
					        text.append("<td><a href='"+linkedIn+"'><img src='"+linkedicon+"'></a></td>");
					        text.append("</tr></tbody></table>");
					        text.append("</td></tr></tbody></table>");
					        text.append("</td></tr>");
					        
					        
					        /* End of footer - 2 */
					       
				        
		        text.append("</table>");
		        text.append("</body>");
		        text.append("</html>");
		       
		        /* END OF BODY PART */
		        
		        
		      /* End of Sending Emails to Register user */
		    
		    EmailSentUtil emailStatus = new EmailSentUtil();
	        
			String resetStatus = "";
			
			try 
			{
				resetStatus = emailStatus.getEmailSent(emailId,mailSubject,text.toString());
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			EmailSentModel emailModel = new EmailSentModel();
			
			if(resetStatus.equals("success"))
			{
				emailModel.setSent_email_id(emailId);
				emailModel.setEmail_subject(mailSubject);
				emailModel.setEmail_message_content(text.toString());
				emailModel.setEmail_sent_status("success");
				emailModel.setEmail_sent_date(new Date());
			}
			else
			{
				emailModel.setSent_email_id(emailId);
				emailModel.setEmail_subject(mailSubject);
				emailModel.setEmail_message_content(text.toString());
				emailModel.setEmail_sent_status("failed");
				emailModel.setEmail_sent_date(new Date());
			}
			
			userRegDao.saveEmailDetails(emailModel);
			
			return resetStatus;
	}
	
    @Transactional(readOnly = false)
	public String referFriendMail(String emailId,String fullName)
	{
		// Send Email to User that register in the block
		 
		 ResourceBundle resource = ResourceBundle.getBundle("resources/referFriend");
			 
		 String mailSubject=resource.getString("mailSubject");
		 
		 String headerText=resource.getString("headerText");
	   	 String headerLogo=resource.getString("headerLogo");
	   	 String contactNo=resource.getString("contactNo");
	   	 String contactEmailId=resource.getString("contactEmailId");
	   	 
	   	 String bodyHeading=resource.getString("bodyHeading");
	   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
	   	 String bodyMessage2=resource.getString("bodyMessage2");
	   	 
	   	 String commonURL = resource.getString("commonURL");
	   	 String actionLink=resource.getString("actionLink");
	   	 String belowImage=resource.getString("belowImage");
	   	 
	   	 String footerMessage=resource.getString("footerMessage");
	   	 
	   	 String copyright=resource.getString("copyright");
		 String termscondition=resource.getString("termscondition");		
		 String privacyPolicy = resource.getString("privacyPolicy");
			 
	   	 String facebookicon=resource.getString("facebookicon");
	   	 String twittericon=resource.getString("twittericon");
	   	 String googleicon=resource.getString("googleicon");
	   	 String linkedicon=resource.getString("linkedicon");
	   	 
	   	 String facebookLink=resource.getString("facebookLink");
	   	 String twitterLink=resource.getString("twitterLink");
	   	 String googlePlus=resource.getString("googlePlus");
	   	 String linkedIn=resource.getString("linkedIn");
	   	 
	   	 
	     StringBuilder text = new StringBuilder();
		        			        
		        
			    /* Sending Emails to Register user */
			       
		        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
		        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
		       
		        text.append("<body style='background-color: #ece8e5;'>");
		       
		        text.append("<table width='600' cellspacing='0' align='center' cellpadding='5' style='background: #fff; font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ccc;'>");
		        
				      
		                /* Header */
				        				        
				        text.append("<tr><td><table width='100%' bgcolor='#00b6f5' cellspacing='0' cellpadding='0px' border='0px' height='45'>");
				        text.append("<tbody><tr><td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>"+headerText+"</td></tr>");
				        text.append("</tbody></table></td></tr>");
				       
				        /* End of Header */
				        
				        /* Header-2 */
				        
				        text.append("<tr><td><table width='100%' style='color: #000;'>");
				        text.append("<tr>");
				        text.append("<td style='float: left;'><a href='index.html'><img width='250' height='50' src='"+headerLogo+"' /></a></td>");
				        text.append("<td style='float: right; color: #777; font-size: 12px; width: 250px;'>");
				        text.append("<span> <b style='color: #000;'>Conatct Number :</b>"+contactNo+"</span>");
				        text.append("<br> <span> <b style='color: #000;'>E-Mail : </b>"+contactEmailId+"</span> ");
				        text.append("</td></tr>");
				        text.append("</table></td></tr>");
				        
				        /* End of Header-2 */
				        
				        
				        /* Body Heading-1 */
				        
				        text.append("<tr><td><h1 style='background-color: #00b6f5; border: 1px solid #ccc; color: #fff; font-size: 20px; margin: 0; text-align: left; width: 100%;'>"+bodyHeading+"</h1></td>");
				        text.append("</tr>");
				       
				        /* End of Body Heading-1 */
				        
				        
				        /* BODY PART */
				        
				        
                           /* Body Part-1 */
					        
					        text.append("<tr><td style='text-align: justify;'>Dear <b>"+fullName+"</b>,</td></tr>");
					       
					        /* End of Body Part-1 */
					        
                           /* Body Part-2 */
					        
					        text.append("<tr><td style='text-align: justify;'>"+bodyMessage1+"</td></tr>");
					       
					        /* End of Body Part-2 */
					        
					        
					        /* Body link */
					        
					        text.append("<tr><td><a href='"+commonURL+"userRegister.html' target='_blank'>Click here to be a member.</a></td></tr>");
					       
					        /* End of Body link */         
								        
                           
                           /* footer -1  */
					        
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px; background-color: #d3d3d3'>"+footerMessage+"</td></tr>");
					       
					        /* End of footer - 1 */
					        
                           /* footer -2  */
					        
					        text.append("<tr><td style='border: medium none; text-align: right; width: 150px; height: 23px;'>");
					       
					        text.append("<table width='600' cellspacing='0' cellpadding='0px' border='0px' bgcolor='#00b6f5' bordercolordark='#fff' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tbody>");
					        text.append("<tr><td>");
					        text.append("<table width='285' cellspacing='0' cellpadding='0px' align='left' class='column' style='color: #a3c9e1;'>");
					        text.append("<tbody>");
					        text.append("<tr><td><h5 style='color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0; width: 155px;'>Copyright &copy; 2015 biosys.</h5></td></tr>");
					        text.append("<tr style='float: left; margin-left: 12px;'><td><a style='color: #ffffff;' href='"+termscondition+"'>Terms &amp;Conditions </a> | <a style='color: #ffffff;' href='"+privacyPolicy+"'>Privacy Policy</a></td></tr>");
					        text.append("</tbody></table>");
					        
					        text.append("<table cellspacing='0' cellpadding='0' align='right' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tbody>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href='"+facebookLink+"'><img src='"+facebookicon+"'></a></td>");
					        text.append("<td><a href='"+twitterLink+"'><img src='"+twittericon+"'></a></td>");
					        text.append("<td><a href='"+googlePlus+"'><img src='"+googleicon+"'></a></td>");
					        text.append("<td><a href='"+linkedIn+"'><img src='"+linkedicon+"'></a></td>");
					        text.append("</tr></tbody></table>");
					        text.append("</td></tr></tbody></table>");
					        text.append("</td></tr>");
					        
					        
					        /* End of footer - 2 */
					       
				           
		        text.append("</table>");
		        text.append("</body>");
		        text.append("</html>");
		       
		        /* END OF BODY PART */
		        
		        
		      /* End of Sending Emails to Register user */
		    
		    EmailSentUtil emailStatus = new EmailSentUtil();
	        
			String resetStatus = "";
			
			try 
			{
				resetStatus = emailStatus.getEmailSent(emailId,mailSubject,text.toString());
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			EmailSentModel emailModel = new EmailSentModel();
			
			if(resetStatus.equals("success"))
			{
				emailModel.setSent_email_id(emailId);
				emailModel.setEmail_subject(mailSubject);
				emailModel.setEmail_message_content(text.toString());
				emailModel.setEmail_sent_status("success");
				emailModel.setEmail_sent_date(new Date());
			}
			else
			{
				emailModel.setSent_email_id(emailId);
				emailModel.setEmail_subject(mailSubject);
				emailModel.setEmail_message_content(text.toString());
				emailModel.setEmail_sent_status("failed");
				emailModel.setEmail_sent_date(new Date());
			}
			
			userRegDao.saveEmailDetails(emailModel);
			
			return resetStatus;
	}
	
    @Transactional(readOnly = false)
	public String resetPasswordMail(String emailId,String resetId)
	{
		// Send Email to User that register in the block
		 
		 ResourceBundle resource = ResourceBundle.getBundle("resources/forgetPasswordMail");
			 
		 String mailSubject=resource.getString("mailSubject");
		 
		 String headerText=resource.getString("headerText");
	   	 String headerLogo=resource.getString("headerLogo");
	   	 String contactNo=resource.getString("contactNo");
	   	 String contactEmailId=resource.getString("contactEmailId");
	   	 
	   	 String bodyHeading=resource.getString("bodyHeading");
	   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
	   	 String bodyMessage2=resource.getString("bodyMessage2");
	   	 
	   	 String commonURL = resource.getString("commonURL");
	   	 String actionLink=resource.getString("actionLink");
	   	 String belowImage=resource.getString("belowImage");
	   	 
	   	 String footerMessage=resource.getString("footerMessage");
	   	 
	   	 String copyright=resource.getString("copyright");
		 String termscondition=resource.getString("termscondition");		
		 String privacyPolicy = resource.getString("privacyPolicy");
			 
	   	 String facebookicon=resource.getString("facebookicon");
	   	 String twittericon=resource.getString("twittericon");
	   	 String googleicon=resource.getString("googleicon");
	   	 String linkedicon=resource.getString("linkedicon");
	   	 
	   	 String facebookLink=resource.getString("facebookLink");
	   	 String twitterLink=resource.getString("twitterLink");
	   	 String googlePlus=resource.getString("googlePlus");
	   	 String linkedIn=resource.getString("linkedIn");
	   	 
	   	 
	     StringBuilder text = new StringBuilder();
		        			        
		        
			    /* Sending Emails to Register user */
			       
		        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
		        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
		       
		        text.append("<body style='background-color: #ece8e5;'>");
		       
		        text.append("<table width='600' cellspacing='0' align='center' cellpadding='5' style='background: #fff; font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ccc;'>");
		        
				      
		                /* Header */
				        				        
				        text.append("<tr><td><table width='100%' bgcolor='#00b6f5' cellspacing='0' cellpadding='0px' border='0px' height='45'>");
				        text.append("<tbody><tr><td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>"+headerText+"</td></tr>");
				        text.append("</tbody></table></td></tr>");
				       
				        /* End of Header */
				        
				        /* Header-2 */
				        
				        text.append("<tr><td><table width='100%' style='color: #000;'>");
				        text.append("<tr>");
				        text.append("<td style='float: left;'><a href='index.html'><img width='250' height='50' src='"+headerLogo+"' /></a></td>");
				        text.append("<td style='float: right; color: #777; font-size: 12px; width: 250px;'>");
				        text.append("<span> <b style='color: #000;'>Conatct Number :</b>"+contactNo+"</span>");
				        text.append("<br> <span> <b style='color: #000;'>E-Mail : </b>"+contactEmailId+"</span> ");
				        text.append("</td></tr>");
				        text.append("</table></td></tr>");
				        
				        /* End of Header-2 */
				        
				        
				        /* Body Heading-1 */
				        
				        text.append("<tr><td><h1 style='background-color: #00b6f5; border: 1px solid #ccc; color: #fff; font-size: 20px; margin: 0; text-align: left; width: 100%;'>"+bodyHeading+"</h1></td>");
				        text.append("</tr>");
				       
				        /* End of Body Heading-1 */
				        
				        
				        /* BODY PART */
				        
				        
                           /* Body Part-1 */
					        
					        text.append("<tr><td style='text-align: justify;'>Dear <b>User</b>,</td></tr>");
					       
					        /* End of Body Part-1 */
					        
                           /* Body Part-2 */
					        
					        text.append("<tr><td style='text-align: justify;'>"+bodyMessage1+"</td></tr>");
					       
					        /* End of Body Part-2 */
					        
					        
					        /* Body link */
					        
					        text.append("<tr><td><a href='"+commonURL+"resetPassword.html?requnIkedij="+resetId+"' target='_blank'>Click here to reset your password.</a></td></tr>");
					       
					        /* End of Body link */         
								        
                           
                           /* footer -1  */
					        
					        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px; background-color: #d3d3d3'>"+footerMessage+"</td></tr>");
					       
					        /* End of footer - 1 */
					        
                           /* footer -2  */
					        
					        text.append("<tr><td style='border: medium none; text-align: right; width: 150px; height: 23px;'>");
					       
					        text.append("<table width='600' cellspacing='0' cellpadding='0px' border='0px' bgcolor='#00b6f5' bordercolordark='#fff' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
					        text.append("<tbody>");
					        text.append("<tr><td>");
					        text.append("<table width='285' cellspacing='0' cellpadding='0px' align='left' class='column' style='color: #a3c9e1;'>");
					        text.append("<tbody>");
					        text.append("<tr><td><h5 style='color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0; width: 155px;'>Copyright &copy; 2015 biosys.</h5></td></tr>");
					        text.append("<tr style='float: left; margin-left: 12px;'><td><a style='color: #ffffff;' href='"+termscondition+"'>Terms &amp;Conditions </a> | <a style='color: #ffffff;' href='"+privacyPolicy+"'>Privacy Policy</a></td></tr>");
					        text.append("</tbody></table>");
					        
					        text.append("<table cellspacing='0' cellpadding='0' align='right' style='color: #a3c9e1; width: 150px; text-align: right;'>");
					        text.append("<tbody>");
					        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
					        text.append("<tr><td><a href='"+facebookLink+"'><img src='"+facebookicon+"'></a></td>");
					        text.append("<td><a href='"+twitterLink+"'><img src='"+twittericon+"'></a></td>");
					        text.append("<td><a href='"+googlePlus+"'><img src='"+googleicon+"'></a></td>");
					        text.append("<td><a href='"+linkedIn+"'><img src='"+linkedicon+"'></a></td>");
					        text.append("</tr></tbody></table>");
					        text.append("</td></tr></tbody></table>");
					        text.append("</td></tr>");
					        
					        
					        /* End of footer - 2 */
					       
				        
		        text.append("</table>");
		        text.append("</body>");
		        text.append("</html>");
		       
		        /* END OF BODY PART */
		        
		        
		      /* End of Sending Emails to Register user */
		    
		    EmailSentUtil emailStatus = new EmailSentUtil();
	        
			String resetStatus = "";
			
			try 
			{
				resetStatus = emailStatus.getEmailSent(emailId,mailSubject,text.toString());
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			EmailSentModel emailModel = new EmailSentModel();
			
			if(resetStatus.equals("success"))
			{
				emailModel.setSent_email_id(emailId);
				emailModel.setEmail_subject(mailSubject);
				emailModel.setEmail_message_content(text.toString());
				emailModel.setEmail_sent_status("success");
				emailModel.setEmail_sent_date(new Date());
			}
			else
			{
				emailModel.setSent_email_id(emailId);
				emailModel.setEmail_subject(mailSubject);
				emailModel.setEmail_message_content(text.toString());
				emailModel.setEmail_sent_status("failed");
				emailModel.setEmail_sent_date(new Date());
			}
			
			userRegDao.saveEmailDetails(emailModel);

			return resetStatus;
	}
	
	
	
	
    @Transactional(readOnly = false)
	public String contactUsMail(String name,String emailId,String mobileNo,String modeOfContact,String description)
	{
		
		// Send Email to User that register in the block
		 
				 ResourceBundle resource = ResourceBundle.getBundle("resources/contactUs");
					 
				 String mailSubject=resource.getString("mailSubject");
				 
				 String headerText=resource.getString("headerText");
			   	 String headerLogo=resource.getString("headerLogo");
			   	 String contactNo=resource.getString("contactNo");
			   	 String contactEmailId=resource.getString("contactEmailId");
			   	 
			   	 String bodyHeading=resource.getString("bodyHeading");
			   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
			   	 String bodyMessage2=resource.getString("bodyMessage2");
			   	 			   	 
			   	 String actionLink=resource.getString("actionLink");
			   	 String belowImage=resource.getString("belowImage");
			   	 
			   	 String footerMessage=resource.getString("footerMessage");
			   	 
			   	 String copyright=resource.getString("copyright");
				 String termscondition=resource.getString("termscondition");		
				 String privacyPolicy = resource.getString("privacyPolicy");
					 
			   	 String facebookicon=resource.getString("facebookicon");
			   	 String twittericon=resource.getString("twittericon");
			   	 String googleicon=resource.getString("googleicon");
			   	 String linkedicon=resource.getString("linkedicon");
			   	 
			   	 String facebookLink=resource.getString("facebookLink");
			   	 String twitterLink=resource.getString("twitterLink");
			   	 String googlePlus=resource.getString("googlePlus");
			   	 String linkedIn=resource.getString("linkedIn");
			   	 
			   	 
			     StringBuilder text = new StringBuilder();
				        			        
				        
					    /* Sending Emails to Register user */
					       
				        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
				        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
				       
				        text.append("<body style='background-color: #ece8e5;'>");
				       
				        text.append("<table width='600' cellspacing='0' align='center' cellpadding='5' style='background: #fff; font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ccc;'>");
				        
						      
				                /* Header */
						        				        
						        text.append("<tr><td><table width='100%' bgcolor='#00b6f5' cellspacing='0' cellpadding='0px' border='0px' height='45'>");
						        text.append("<tbody><tr><td align='left' style='color: #fff; font-weight: bold; font-size: 18px; text-shadow: 1px 2px 0 #004879; padding-left: 15px;'>"+headerText+"</td></tr>");
						        text.append("</tbody></table></td></tr>");
						       
						        /* End of Header */
						        
						        /* Header-2 */
						        
						        text.append("<tr><td><table width='100%' style='color: #000;'>");
						        text.append("<tr>");
						        text.append("<td style='float: left;'><a href='index.html'><img width='250' height='50' src='"+headerLogo+"' /></a></td>");
						        text.append("<td style='float: right; color: #777; font-size: 12px; width: 250px;'>");
						        text.append("<span> <b style='color: #000;'>Conatct Number :</b>"+contactNo+"</span>");
						        text.append("<br> <span> <b style='color: #000;'>E-Mail : </b>"+contactEmailId+"</span> ");
						        text.append("</td></tr>");
						        text.append("</table></td></tr>");
						        
						        /* End of Header-2 */
						        
						        
						        /* Body Heading-1 */
						        
						        text.append("<tr><td><h1 style='background-color: #00b6f5; border: 1px solid #ccc; color: #fff; font-size: 20px; margin: 0; text-align: left; width: 100%;'>"+bodyHeading+"</h1></td>");
						        text.append("</tr>");
						       
						        /* End of Body Heading-1 */
					
						        
						        /* BODY PART */
						        						        
		                           /* Body Part-1 */
							        
							        text.append("<tr><td style='text-align: justify;'>Dear <b>"+name+"</b>,</td></tr>");
							       
							        /* End of Body Part-1 */
							        
		                           /* Body Part-2 */
							        
							        text.append("<tr><td style='text-align: justify;'>"+bodyMessage1+"</td></tr>");
							       
							        /* End of Body Part-2 */
							        
							        
		                           /* footer -1  */
							        
							        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px; background-color: #d3d3d3'>"+footerMessage+"</td></tr>");
							       
							        /* End of footer - 1 */
							        
		                           /* footer -2  */
							        
							        text.append("<tr><td style='border: medium none; text-align: right; width: 150px; height: 23px;'>");
							       
							        text.append("<table width='600' cellspacing='0' cellpadding='0px' border='0px' bgcolor='#00b6f5' bordercolordark='#fff' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
							        text.append("<tbody>");
							        text.append("<tr><td>");
							        text.append("<table width='285' cellspacing='0' cellpadding='0px' align='left' class='column' style='color: #a3c9e1;'>");
							        text.append("<tbody>");
							        text.append("<tr><td><h5 style='color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0; width: 155px;'>Copyright &copy; 2015 biosys.</h5></td></tr>");
							        text.append("<tr style='float: left; margin-left: 12px;'><td><a style='color: #ffffff;' href='"+termscondition+"'>Terms &amp;Conditions </a> | <a style='color: #ffffff;' href='"+privacyPolicy+"'>Privacy Policy</a></td></tr>");
							        text.append("</tbody></table>");
							        
							        text.append("<table cellspacing='0' cellpadding='0' align='right' style='color: #a3c9e1; width: 150px; text-align: right;'>");
							        text.append("<tbody>");
							        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
							        text.append("<tr><td><a href='"+facebookLink+"'><img src='"+facebookicon+"'></a></td>");
							        text.append("<td><a href='"+twitterLink+"'><img src='"+twittericon+"'></a></td>");
							        text.append("<td><a href='"+googlePlus+"'><img src='"+googleicon+"'></a></td>");
							        text.append("<td><a href='"+linkedIn+"'><img src='"+linkedicon+"'></a></td>");
							        text.append("</tr></tbody></table>");
							        text.append("</td></tr></tbody></table>");
							        text.append("</td></tr>");
							        							        
							    /* End of footer - 2 */							        
						            
				        text.append("</table>");
				        text.append("</body>");
				        text.append("</html>");
				       
				        /* END OF BODY PART */
				        
				        
				      /* End of Sending Emails to Register user */
				    
				    EmailSentUtil emailStatus = new EmailSentUtil();
			        
					String resetStatus = "";
					
					try 
					{
						resetStatus = emailStatus.getEmailSent(emailId,mailSubject,text.toString());
					} 
					catch (Exception e) {
						
						e.printStackTrace();
					}
					
					EmailSentModel emailModel = new EmailSentModel();
					
					if(resetStatus.equals("success"))
					{
						emailModel.setSent_email_id(emailId);
						emailModel.setEmail_subject(mailSubject);
						emailModel.setEmail_message_content(text.toString());
						emailModel.setEmail_sent_status("success");
						emailModel.setEmail_sent_date(new Date());
					}
					else
					{
						emailModel.setSent_email_id(emailId);
						emailModel.setEmail_subject(mailSubject);
						emailModel.setEmail_message_content(text.toString());
						emailModel.setEmail_sent_status("failed");
						emailModel.setEmail_sent_date(new Date());
					}
					
				 userRegDao.saveEmailDetails(emailModel);
					
			return resetStatus;
	}
	
	
	
}
