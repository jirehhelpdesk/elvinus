package com.biosys.service;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.biosys.bean.BusinessPlanBean;
import com.biosys.bean.BusinessPlanFeatureBean;
import com.biosys.bean.EmailSentBean;
import com.biosys.bean.PlanDetailsBean;
import com.biosys.bean.ProductDetailsBean;
import com.biosys.bean.SMSSentBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.dao.SuperAdminDao;
import com.biosys.domain.AdminRegistrationModel;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.PlanDetailsModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.util.EmailSentUtil;

@Service("superAdminService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class SuperAdminServiceImpl implements SuperAdminService {

	@Autowired
	private SuperAdminDao supAdminDao;
	
	@Transactional(readOnly = false)
	public String saveAdminDetails(AdminRegistrationModel adminDetails,String password)
	{
		String saveStatus = supAdminDao.saveAdminDetails(adminDetails);
		
		if(saveStatus.equals("success"))
		{
			  
			 // Send Email to User that register in the block
			 
			 ResourceBundle resource = ResourceBundle.getBundle("resources/subAdminSignUpMail");
				 
			 String mailSubject=resource.getString("mailSubject");
			 
		   	 String headerLogo=resource.getString("headerLogo");
		   	 String contactNo=resource.getString("contactNo");
		   	 
		   	 String bodyHeading=resource.getString("bodyHeading");
		   	 String bodyMessage1=resource.getString("bodyMessage1");			   	 
		   	 
		   	 String actionLink=resource.getString("actionLink");
		   	 String belowImage=resource.getString("belowImage");
		   	 
		   	 String footerMessage=resource.getString("footerMessage");
		   	 
		   	 String copyright=resource.getString("copyright");
			 String termscondition=resource.getString("termscondition");		
			 String privacyPolicy = resource.getString("privacyPolicy");
				 
		   	 String facebookicon=resource.getString("facebookicon");
		   	 String twittericon=resource.getString("twittericon");
		   	 String googleicon=resource.getString("googleicon");
		   	 String linkedicon=resource.getString("linkedicon");
		   	 
		   	 String facebookLink=resource.getString("facebookLink");
		   	 String twitterLink=resource.getString("twitterLink");
		   	 String googlePlus=resource.getString("googlePlus");
		   	 String linkedIn=resource.getString("linkedIn");
		   	 
		   	 
		     StringBuilder text = new StringBuilder();
			        			        
			        
				    /* Sending Emails to Register user */
				       
			        text.append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
			        text.append("<html xmlns='http://www.w3.org/1999/xhtml'><head><meta name='viewport' content='width=device-width' /><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /></head>");
			       
			        text.append("<body style='background-color: #ece8e5;'>");
			       
			        text.append("<table width='600' cellspacing='0' align='center' cellpadding='5' style='background: #fff; font-weight: normal; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border: 1px solid #ccc;'>");
			        
					      
					        
					        /* Header-2 */
					        
					        text.append("<tr><td><table width='100%' style='color: #000;'>");
					        text.append("<tr>");
					        text.append("<td style='float: left;'><a href='index.html'><img width='250' height='50' src='"+headerLogo+"' /></a></td>");
					        text.append("<td style='float: right; color: #777; font-size: 12px; width: 250px;'>");
					        text.append("</td></tr>");
					        text.append("</table></td></tr>");
					        
					        /* End of Header-2 */
					        
					        
					        /* Body Heading-1 */
					        
					        text.append("<tr><td><h1 style='background-color: #00b6f5; border: 1px solid #ccc; color: #fff; font-size: 20px; margin: 0; text-align: left; width: 100%;'>"+bodyHeading+"</h1></td>");
					        text.append("</tr>");
					       
					        /* End of Body Heading-1 */
					        
					        
					        /* BODY PART */
					        
					        
                               /* Body Part-1 */
						        
						        text.append("<tr><td style='text-align: justify;'>Dear <b>"+adminDetails.getAdmin_full_name()+"</b>,</td></tr>");
						       
						        /* End of Body Part-1 */
						        
                               /* Body Part-2 */
						        
						        text.append("<br><tr><td style='text-align: justify;'>"+bodyMessage1+"</td></tr>");
						       
						        /* End of Body Part-2 */
						        
                                 
						        
						        /* Body Part-3 */
						        
						        text.append("<tr><td style='text-align: justify;margin-left:50px;padding: 11px 54px 14px;'>");
						        text.append("<span><b>Role :</b> "+adminDetails.getAdmin_type()+"</span><br><br>");
						        text.append("<span><b>UserId :</b> "+adminDetails.getAdmin_email_id()+"</span><br><br>");
						        text.append("<span><b>Password :</b> "+password+"</span><br>");
						        text.append("</td></tr>");
						        /* End of Body Part-3 */
						        
						      
                               /* Body link */
						        
						        text.append("<tr><td height='45' align='center'  style='padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(255, 255, 255); text-transform: uppercase; background-color: rgb(29, 200, 233);' >"+
										"<span style='font-family: 'proxima_novablack', Helvetica; font-weight: normal;'>"+
										"<a href='"+actionLink+"' target='_blank' style='color: rgb(255, 255, 255); font-size: 18px; text-decoration: none; line-height: 34px; width: 100%;' t-style='whiteText' object='link-editable'>Access the admin</a></td></tr>");

						        /*text.append("<tr><td width='265' bgcolor='#1dc8e9' height='45' align='center'  style='padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica,Arial,sans-serif; color: rgb(255, 255, 255); text-transform: uppercase; background-color: rgb(29, 200, 233);' t-style='whiteText' mc:edit='7'>"+
																		"<span style='font-family: 'proxima_novablack', Helvetica; font-weight: normal;'>"+
																		"<a href='"+actionLink+"' target='_blank' style='color: rgb(255, 255, 255); font-size: 18px; text-decoration: none; line-height: 34px; width: 100%;' t-style='whiteText' object='link-editable'>Go for it!</a></td></tr>");
						        */
						        
						       /* End of Body link */
						        
						        
                               /* Body Down image */
						        
						        text.append("<tr><td width='300'><img width='600' height='177' src='"+belowImage+"'></td></tr>");
						       
						        /* End of Body Down image */
						        
                               /* footer -1  */
						        
						        text.append("<tr><td align='center' style='color: #31576f; font-size: 12px; padding: 5px; background-color: #d3d3d3'>"+footerMessage+"</td></tr>");
						       
						        /* End of footer - 1 */
						        
                               /* footer -2  */
						        
						        text.append("<tr><td style='border: medium none; text-align: right; width: 150px; height: 23px;'>");
						       
						        text.append("<table width='600' cellspacing='0' cellpadding='0px' border='0px' bgcolor='#00b6f5' bordercolordark='#fff' style='padding-left: 15px; padding-right: 15px; margin: 0;'>");
						        text.append("<tbody>");
						        text.append("<tr><td>");
						        text.append("<table width='285' cellspacing='0' cellpadding='0px' align='left' class='column' style='color: #a3c9e1;'>");
						        text.append("<tbody>");
						        text.append("<tr><td><h5 style='color: #ffffff; font-size: 12px; margin: 10px -47px 5px 0; width: 155px;'>Copyright &copy; 2015 biosys.</h5></td></tr>");
						        text.append("<tr style='float: left; margin-left: 12px;'><td><a style='color: #ffffff;' href='"+termscondition+"'>Terms &amp;Conditions </a> | <a style='color: #ffffff;' href='"+privacyPolicy+"'>Privacy Policy</a></td></tr>");
						        text.append("</tbody></table>");
						        
						        text.append("<table cellspacing='0' cellpadding='0' align='right' style='color: #a3c9e1; width: 150px; text-align: right;'>");
						        text.append("<tbody>");
						        text.append("<tr><td><h5 style='margin: 10px -47px 0px 0; color: #a3c9e1; padding-left: 15px; width: 150px; font-size: 16px; font-weight: bold;'>Follow Us</h5></td></tr>");
						        text.append("<tr><td><a href='"+facebookLink+"'><img src='"+facebookicon+"'></a></td>");
						        text.append("<td><a href='"+twitterLink+"'><img src='"+twittericon+"'></a></td>");
						        text.append("<td><a href='"+googlePlus+"'><img src='"+googleicon+"'></a></td>");
						        text.append("<td><a href='"+linkedIn+"'><img src='"+linkedicon+"'></a></td>");
						        text.append("</tr></tbody></table>");
						        text.append("</td></tr></tbody></table>");
						        text.append("</td></tr>");
						        
						        
						        /* End of footer - 2 */
						       
					        
			        text.append("</table>");
			        text.append("</body>");
			        text.append("</html>");
			       
			        /* END OF BODY PART */
			        
			        
			      /* End of Sending Emails to Register user */
			    
			    EmailSentUtil emailStatus = new EmailSentUtil();
    	        
    			String resetStatus = "";
    			String subject  = mailSubject +adminDetails.getAdmin_full_name();
    			
				try {
					resetStatus = emailStatus.getEmailSent(adminDetails.getAdmin_email_id(),subject,text.toString());
				} 
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				EmailSentModel emailModel = new EmailSentModel();
				
				if(resetStatus.equals("success"))
				{
					emailModel.setSent_email_id(adminDetails.getAdmin_email_id());
					emailModel.setEmail_subject(subject);
					emailModel.setEmail_message_content(text.toString());
					emailModel.setEmail_sent_status("success");
					emailModel.setEmail_sent_date(new Date());
				}
				else
				{
					emailModel.setSent_email_id(adminDetails.getAdmin_email_id());
					emailModel.setEmail_subject(subject);
					emailModel.setEmail_message_content(text.toString());
					emailModel.setEmail_sent_status("failed");
					emailModel.setEmail_sent_date(new Date());
				}
				
				//supAdminDao.saveEmailDetails(emailModel);
						
		}
		
		return saveStatus;
	}
	
	@Transactional(readOnly = false)
	public String getValidateSuperAdmin(String uid,String pwd)
	{
		return supAdminDao.getValidateSuperAdmin(uid,pwd);
	}
	
	@Transactional(readOnly = false)
	public List<AdminRegistrationModel> getAllAdminDetails()
	{
		return supAdminDao.getAllAdminDetails();
	}
	
	@Transactional(readOnly = false)
	public String removeAdminById(int adminId)
	{
		return supAdminDao.removeAdminById(adminId);
	}
	
	@Transactional(readOnly = false)
	public int getLastProductId()
	{
		return supAdminDao.getLastProductId();
	}
	
	@Transactional(readOnly = false)
	public String saveProductDetails(ProductDetailsModel productDetails)
	{
		return supAdminDao.saveProductDetails(productDetails);
	}
	
	@Transactional(readOnly = false)
	public String updateProductDetails(ProductDetailsModel productDetails)
	{
		return supAdminDao.updateProductDetails(productDetails);
	}
	
	@Transactional(readOnly = false)
	public List<ProductDetailsBean> getAllProductDetailsDetails()
	{
		return supAdminDao.getAllProductDetailsDetails();
	}
	
	@Transactional(readOnly = false)
	public String removeProductById(int productId)
	{
		return supAdminDao.removeProductById(productId);
	}
	
	@Transactional(readOnly = false)
	public List<ProductDetailsBean> getAllProductDetailsByCriteria(String searchValue,String searchType)
	{
		return supAdminDao.getAllProductDetailsByCriteria(searchValue,searchType);
	}
	
	@Transactional(readOnly = false)
	public String saveCostValues(CostManageModel costModel)
	{
		return supAdminDao.saveCostValues(costModel);
	}
	
	@Transactional(readOnly = false)
	public int getCostExistOrNot(String costName)
	{
		return supAdminDao.getCostExistOrNot(costName);
	}
	
	@Transactional(readOnly = false)
	public List<CostManageModel> getAllCostDetails()
	{
		return supAdminDao.getAllCostDetails();
	}
	
	@Transactional(readOnly = false)
	public List<UserBasicInfoBean> getVendorBasicDetails(String searchValue,String searchType)
	{
		return supAdminDao.getVendorBasicDetails(searchValue,searchType);
	}
	
	@Transactional(readOnly = false)
    public List<EmailSentBean> getEmailSentDetails()
    {
    	return supAdminDao.getEmailSentDetails();
    }
	
	@Transactional(readOnly = false)
	public List<SMSSentBean> getSMSDetails()
	{
		return supAdminDao.getSMSDetails();
	}
	
	@Transactional(readOnly = false)
	public String savePlanDetails(PlanDetailsModel planModel)
	{
		return supAdminDao.savePlanDetails(planModel);
	}
	
	@Transactional(readOnly = false)
	public List<PlanDetailsBean> getPlandetails()
	{
		return supAdminDao.getPlandetails();
	}
    
	@Transactional(readOnly = false)
	public int getStatusPlanExistOrNot(String planName)
	{
		return supAdminDao.getStatusPlanExistOrNot(planName);
	}
	
	@Transactional(readOnly = false)
	public List<UserProfileDetailsModel> getVendorOtherDetailsById(int vendorId)
    {
    	return supAdminDao.getVendorOtherDetailsById(vendorId);
    }
	
	@Transactional(readOnly = false)
	public List<UserBasicInfoBean> getVendorBasicInfoDetailsById(int vendorId)
	{
		return supAdminDao.getVendorBasicInfoDetailsById(vendorId);
	}
	
	@Transactional(readOnly = false)
	public List<BusinessPlanBean> getBusinessPlandetails()
	{
		return supAdminDao.getBusinessPlandetails();
	}
	
	@Transactional(readOnly = false)
	public List<BusinessPlanFeatureBean> getBusinessPlanFeatures()
	{
		return supAdminDao.getBusinessPlanFeatures();
	}
	
	@Transactional(readOnly = false)
	public String saveBusinessPlanDetails(BusinessPlanModel planModel)
	{
		return supAdminDao.saveBusinessPlanDetails(planModel);
	}
	
	@Transactional(readOnly = false)
	public String saveBusinessPlanFeatures(BusinessPlanFeatureModel planFeature)
	{
		return supAdminDao.saveBusinessPlanFeatures(planFeature);
	}
	
	@Transactional(readOnly = false)
	public String deleteFeature(int featureId)
	{
		return supAdminDao.deleteFeature(featureId);
	}
	
	@Transactional(readOnly = false)
	public List<ProductDetailsModel> getProductDetailsByProductId(int productId)
	{
		return supAdminDao.getProductDetailsByProductId(productId);
	}
	
	@Transactional(readOnly = false)
	public long getBusinessStatus(String type)
	{
		long total = 0;
		
		if(type.equals("totalUsers"))
		{
			total += supAdminDao.getRecords("totalUsers");
		}
		else if(type.equals("totalOrders"))
		{
			total += supAdminDao.getRecords("totalOrders");
		}
		else
		{
			
		}
		
		
		return total;
	}
	
	@Transactional(readOnly = false)
	public List<UserReferenceDetailsModel> getAllReferenceDetails(String criteria)
	{
		return supAdminDao.getAllReferenceDetails(criteria);		
	}
	
}
