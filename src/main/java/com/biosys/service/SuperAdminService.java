package com.biosys.service;

import java.util.List;

import com.biosys.bean.BusinessPlanBean;
import com.biosys.bean.BusinessPlanFeatureBean;
import com.biosys.bean.EmailSentBean;
import com.biosys.bean.PlanDetailsBean;
import com.biosys.bean.ProductDetailsBean;
import com.biosys.bean.SMSSentBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.AdminRegistrationModel;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.PlanDetailsModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;

public interface SuperAdminService {

	public String saveAdminDetails(AdminRegistrationModel adminDetails,String password);
	
	public String getValidateSuperAdmin(String uid,String pwd);
	
	public List<AdminRegistrationModel> getAllAdminDetails();
	
	public String removeAdminById(int adminId);
	
	public int getLastProductId();
	
	public String saveProductDetails(ProductDetailsModel productDetails);
	
	public String updateProductDetails(ProductDetailsModel productDetails);
	
	public List<ProductDetailsBean> getAllProductDetailsDetails();
	
	public String removeProductById(int productId);
	
	public List<ProductDetailsBean> getAllProductDetailsByCriteria(String searchValue,String searchType);
	
	public String saveCostValues(CostManageModel costModel);
	
	public int getCostExistOrNot(String costName);
	
	public List<CostManageModel> getAllCostDetails();
	
	public List<UserBasicInfoBean> getVendorBasicDetails(String searchValue,String searchType);
	
	public List<EmailSentBean> getEmailSentDetails();
	
	public List<SMSSentBean> getSMSDetails();
	
	public String savePlanDetails(PlanDetailsModel planModel);
	
	public List<PlanDetailsBean> getPlandetails();
	
	public int getStatusPlanExistOrNot(String planName);
	
	public List<UserProfileDetailsModel> getVendorOtherDetailsById(int vendorId);
	
	public List<UserBasicInfoBean> getVendorBasicInfoDetailsById(int vendorId);

	public List<BusinessPlanBean> getBusinessPlandetails();
	
	public List<BusinessPlanFeatureBean> getBusinessPlanFeatures();
	
	public String saveBusinessPlanDetails(BusinessPlanModel planModel);
	
	public String saveBusinessPlanFeatures(BusinessPlanFeatureModel planFeature);
	
	public String deleteFeature(int featureId);
	
	public List<ProductDetailsModel> getProductDetailsByProductId(int productId);
	
	public long getBusinessStatus(String type);
	
	public List<UserReferenceDetailsModel> getAllReferenceDetails(String criteria);		

}
