package com.biosys.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserPlanDetailsModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.domain.UserRegistrationStageModel;
import com.biosys.domain.UserResetPasswordModel;
import com.biosys.domain.UserReturnProductModel;

public interface userRegistrationService {

	public String saveUserBasicDetails(UserBasicInfoModel userModel);
	
	public int getLastUserId();
	
	public String checkEmailIdExistOrNot(String emailId);
	
	public int getUserIdViaEmailId(String emailId);
	
	public String saveUserStepDetails(UserRegistrationStageModel regStage);
	
	public String saveProductInterestOfUser(UserProductInterestModel intModel);
	
	public int getUserRegStageId(int regUserId);
	
	public String saveReferenceDetails(UserReferenceDetailsModel refModel);
	
	public String saveUserPlanDetails(UserPlanDetailsModel planModel);
	
	public List<UserBasicInfoBean> getUserBasicInfoViaUserId(int regUserId);
	
	public String saveTransactionDetails(TransactionDetailModel tranModel);
	
	public String saveOrderDetails(OrderDetailsModel ordModel);
	
	public int getLastTransactionId();
	
	public int getLastOrderId();
	
	public String saveReturnProducDetails(UserReturnProductModel returnModel);
	
	public String getRegistrationStage(int regUserId);
	
	public String welcomMail(int userRegId,String fullName,String emailId,String propertiFileName);
	
	public String referFriendMail(String emailId,String fullName);
	
	public String orderMail(int userRegId,String fullName,String emailId,String mobileNo,String propertiFileName,String address,String orderId,HttpSession session);
	
	public String resetPasswordMail(String emailId,String resetId);
	
	public int getLastResetId();
	
	public String saveResetDetails(UserResetPasswordModel resetModel);
	
	public String getValidateUser(String emailId,String password);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getUpdatePasswordViaEmailId(String emailId,String password);
	
	public int getResetIdViaUniqueId(String reqUniqueId);
	
	public String getDeleteResetDetails(String emailId);      
	
	public String contactUsMail(String name,String emailId,String contactNo,String modeOfContact,String description);
	
	public String welcomeSMS(String fullName,String mobileNo);
	
	public List<BusinessPlanModel> getPlanDetailsViaPlanName(String planType);
	
	public List<BusinessPlanFeatureModel> getPlanFeaturesViaPlanId(int plan_id);
	
	public List<UserProductInterestModel> getInterestedProduct(int regUserId);
	
	public int getLastProductInterestId(int regUserId);
	
	public String saveUserIntProAsperOrder(int productInterestId,String orderId);
	
	public String asignOrderIdToTranId(String transactionId,String orderId);
	
 }
