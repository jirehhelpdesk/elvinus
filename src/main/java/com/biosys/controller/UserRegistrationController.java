package com.biosys.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserPlanDetailsModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.domain.UserRegistrationStageModel;
import com.biosys.domain.UserResetPasswordModel;
import com.biosys.domain.UserReturnProductModel;
import com.biosys.service.SuperAdminService;
import com.biosys.service.userRegistrationService;
import com.biosys.service.userService;
import com.biosys.util.AccessControl;
import com.biosys.util.EmailSentUtil;
import com.biosys.util.PasswordGenerator;

@Controller
public class UserRegistrationController {

	@Autowired
	userRegistrationService userRegService;
	
	@Autowired
	SuperAdminService superAdminService;
	
	@Autowired
	userService userService;
	
	
	@RequestMapping(value = "/progressBar", method = RequestMethod.GET)
	public String progressBar(HttpSession session,HttpServletRequest request) {
						
		return "aaaaProgressBar";				
	}
	
	@RequestMapping(value = "/popUp", method = RequestMethod.GET)
	public String popUp(HttpSession session,HttpServletRequest request) {
						
		return "aaaaInfoPopUp";				
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String showHome(HttpSession session,HttpServletRequest request) {
						
		return "index";				
	}
	
    public String Hello() {
		
		return "index";				
	}
	
	@RequestMapping(value = "/aboutUs", method = RequestMethod.GET)
	public String aboutUs(HttpSession session,HttpServletRequest request) {
						
		return "AboutUs";				
	}
	
	@RequestMapping(value = "/productDetails", method = RequestMethod.GET)
	public String productDetails(HttpSession session,HttpServletRequest request) {
						
		return "BusinessProductDetails";				
	}
	
	@RequestMapping(value = "/userLogin", method = RequestMethod.GET)
	public String userLogin(HttpSession session,HttpServletRequest request) {
		
		
		if (session.getAttribute("authenticate") == null)
		{							
			request.setAttribute("actionMessage", "Not Required");
		    return "userlogin";	
		}		
		else 
		{	
			return "redirect:/checkRegistrationStage.html";				
		   // return "redirect:/myProfile.html";	
		}
	}
		
	
	@RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
	public String forgetPassword(HttpSession session,HttpServletRequest request) {
					
		request.setAttribute("requestMesage", "Not Required");
		
		return "userforgetPassword";				
	}
	
	@RequestMapping(value = "/userRegister", method = RequestMethod.GET)
	public String userRegister(HttpSession session,HttpServletRequest request) {
					
		return "userRegistrationForm";				
	}
	
	@RequestMapping(value = "/regValidateEmailId", method = RequestMethod.POST)
	public @ResponseBody String regValidateEmailId(HttpSession session,HttpServletRequest request) {
		
		String checkStatus = "";
		String emailId = request.getParameter("emailId");
		checkStatus = userRegService.checkEmailIdExistOrNot(emailId);	
		return checkStatus;				
	}
		
	
	
	@RequestMapping(value = "/saveUserRegister", method = RequestMethod.POST)
	public @ResponseBody String saveUserRegister(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
						
		// Encrypt Password
		
		PasswordGenerator cryptoUtil=new PasswordGenerator();
		String encryptPassword = "";
		
		try 
		{
			encryptPassword = cryptoUtil.encrypt(request.getParameter("pw"));
		} 
		catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e)
		{
			e.printStackTrace();
		}
		
		// End of Encrypt password.
		
		
		// create a random Vendor Id
		   
		   Date today = new Date();
		   Calendar cal = Calendar.getInstance();
	       cal.setTime(today);
	       int year = cal.get(Calendar.YEAR);
	       int month = cal.get(Calendar.MONTH);
	       month = month + 1;
	       int day = cal.get(Calendar.DAY_OF_MONTH);
	       int currentTime = cal.get(Calendar.HOUR);
	       
	       String name = request.getParameter("fullName");
	       int lastUserId = userRegService.getLastUserId() + 1;
		   String VendorId = currentTime+name.substring(0,3)+year+day+month+lastUserId;
		
		   
		// End of creating vendor random id
		
		   
		// Save user basic details
		   
		UserBasicInfoModel userModel = new UserBasicInfoModel();
		userModel.setVendor_id(VendorId.toUpperCase());
		userModel.setUser_name(request.getParameter("fullName"));
		userModel.setUser_emailid(request.getParameter("emailId"));
		userModel.setUser_mobile_no(request.getParameter("mobileNo"));
		userModel.setUser_shipping_address(request.getParameter("address"));
		userModel.setPassword(encryptPassword);
		userModel.setUser_cr_date(new Date());

		saveStatus = userRegService.saveUserBasicDetails(userModel);
		
		// End of save user basic details
		
		
		// Save user steps to register
		
		int userId = userRegService.getUserIdViaEmailId(request.getParameter("emailId"));
		session.setAttribute("regUserId", userId);
		
		UserRegistrationStageModel regStage = new UserRegistrationStageModel();
		regStage.setUser_id(userId);
		regStage.setUser_reg_steps("1-Basic");
		regStage.setCost_reg_date(new Date());
		userRegService.saveUserStepDetails(regStage);
		
		// end of user step to register.
			
		session.setAttribute("regSession", "exist");
		
		String mailStatus = userRegService.welcomMail(userId,request.getParameter("fullName"),request.getParameter("emailId"),"wellcomeRegistration");
		
		String smsStatus = userRegService.welcomeSMS(request.getParameter("fullName"),request.getParameter("mobileNo"));
		
		return saveStatus;	
		
	}
	
	
	
	@RequestMapping(value = "/productSelection", method = RequestMethod.GET)
	public String productSelection(HttpSession session,HttpServletRequest request) {
					
	  if(session.getAttribute("regUserId")!=null)
		{
		    
			return "userRegProductSelection";		
		}
		else
		{			
			return "redirect:/userLogin.html";		
		}
		
		
		//return "userRegProductSelection";	
	}
	
		
	@RequestMapping(value = "/getProductAsPerType", method = RequestMethod.POST)
	public ModelAndView getProductAsPerType(HttpSession session,HttpServletRequest request) {
			
		String searchValue = request.getParameter("type");
		String productType = request.getParameter("type");
		
		String existValue = request.getParameter("existValue");
		if(existValue!=null)
		{
			request.setAttribute("existValue", existValue);
		}
		else
		{
			request.setAttribute("existValue", "Not There");
		}
		
		Map<String, Object> model = new HashMap<String, Object>();			
		model.put("productDetails",superAdminService.getAllProductDetailsByCriteria(searchValue,productType));
		
		return new ModelAndView("userProductList",model);				
	}
	
	
	
	@RequestMapping(value = "/getProductDetailsById", method = RequestMethod.POST)
	public ModelAndView getProductDetailsById(HttpSession session,HttpServletRequest request) {
	
		String agriProId = request.getParameter("agriProId");
		String herBalId = request.getParameter("herBalId");
		String hmCareId = request.getParameter("hmCareId");
				
		String agriProIdArray[] = agriProId.split(",");
		String herBalIdArray[] = herBalId.split(",");
		String hmCareIdArray[] = hmCareId.split(",");
		
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		List<ProductDetailsModel> agriProductDetails = new ArrayList<ProductDetailsModel>();
		for(int i=0;i<agriProIdArray.length;i++)
		{
			if(!agriProIdArray[i].equals(""))
			{
				int productId = Integer.parseInt(agriProIdArray[i]);
				agriProductDetails.addAll(superAdminService.getProductDetailsByProductId(productId));
			}
			
		}
		
		List<ProductDetailsModel> herbalProductDetails = new ArrayList<ProductDetailsModel>();
		for(int i=0;i<herBalIdArray.length;i++)
		{
			if(!herBalIdArray[i].equals(""))
			{
				int productId = Integer.parseInt(herBalIdArray[i]);
				herbalProductDetails.addAll(superAdminService.getProductDetailsByProductId(productId));
			}
		}
		
		List<ProductDetailsModel> hmCareProductDetails = new ArrayList<ProductDetailsModel>();
		for(int i=0;i<hmCareIdArray.length;i++)
		{
			if(!hmCareIdArray[i].equals(""))
			{
				int productId = Integer.parseInt(hmCareIdArray[i]);
				hmCareProductDetails.addAll(superAdminService.getProductDetailsByProductId(productId));
			}
		}
		
		model.put("agriProductDetails",agriProductDetails);
		model.put("herbalProductDetails",herbalProductDetails);
		model.put("hmCareProductDetails",hmCareProductDetails);
		
		return new ModelAndView("userCartDetailsPopUp",model);		
	}
	
	
	
	@RequestMapping(value = "/showMoreAboutProduct", method = RequestMethod.POST)
	public ModelAndView showMoreAboutProduct(HttpSession session,HttpServletRequest request) {
	
		int productId = Integer.parseInt(request.getParameter("productId"));
		Map<String, Object> model = new HashMap<String, Object>();	
		
		model.put("productDetailsUseInPopUp",superAdminService.getProductDetailsByProductId(productId));
		
		return new ModelAndView("productPopUp",model);				
	}	
	
	
	@RequestMapping(value = "/saveProductSelection", method = RequestMethod.POST)
	public @ResponseBody String saveProductSelection(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		
		UserProductInterestModel intModel = new UserProductInterestModel();
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		intModel.setUser_id(regUserId);
		intModel.setUser_interest_agriculture(request.getParameter("agricultureProduct"));
		intModel.setUser_interest_herbal(request.getParameter("herbalProduct"));
		intModel.setUser_interest_homecare(request.getParameter("homecareProduct"));
		intModel.setUpdate_date(new Date());
		
		saveStatus = userRegService.saveProductInterestOfUser(intModel);
		
		// Save user steps to register
		
		int useRegStageId = userRegService.getUserRegStageId(regUserId);
				
				UserRegistrationStageModel regStage = new UserRegistrationStageModel();
				regStage.setUser_reg(useRegStageId);
				regStage.setUser_id(regUserId);
				regStage.setUser_reg_steps("1-Basic+2-Product");
				regStage.setCost_reg_date(new Date());
				userRegService.saveUserStepDetails(regStage);
				
		// end of user step to register.
								
		return saveStatus;
	}
	
	
	@RequestMapping(value = "/email2", method = RequestMethod.GET)
	public String email2(HttpSession session,HttpServletRequest request) {
		
		//String mailStatus = userRegService.orderMail(10,"Abinash Raula","abi@gmail.com","9036007662","orderDetailsMail","dfg dfgd fgdfgdfgdfg","ORD12345232","15638.00","862.18","16500.00","500.00","17000.00");
		//String mailStatus = userRegService.welcomMail(10,"Abinash Raula","abinash.raula@jirehsol.com","wellcomeRegistration");
		
		return "e-MailNewsLetter";						
	}
	
	
	@RequestMapping(value = "/referFriend", method = RequestMethod.GET)
	public String referFriend(HttpSession session,HttpServletRequest request) {
		
		if(session.getAttribute("regUserId")!=null)
		{
			int regUserId = (int) session.getAttribute("regUserId");
			
			int totalProduct = 0;
			String agriId = "";
			String herbalId = "";
			String hmCareId = "";
			
			List<UserProductInterestModel> interestProduct = userRegService.getInterestedProduct(regUserId);
				
			if(interestProduct.size()>0)
			{
				agriId = interestProduct.get(0).getUser_interest_agriculture();
				herbalId = interestProduct.get(0).getUser_interest_herbal();
				hmCareId = interestProduct.get(0).getUser_interest_homecare();
				
				if(agriId.length()>0)
				{
					totalProduct += (agriId.split(",").length);
				}
				if(herbalId.length()>0)
				{
					totalProduct += (herbalId.split(",").length);
				}
				if(hmCareId.length()>0)
				{
					totalProduct += (hmCareId.split(",").length);
				}
				
				
			}
			
			request.setAttribute("agriId", agriId);
			request.setAttribute("herbalId", herbalId);
			request.setAttribute("hmCareId", hmCareId);
			
			request.setAttribute("totalProduct", totalProduct);
			
			return "userRegAddReference";		
		}
		else
		{		    
			return "redirect:/index.html";		
		}		
					
		//return "userRegAddReference";				
	}
	
		
	@RequestMapping(value = "/saveReferenceDetails", method = RequestMethod.POST)
	public @ResponseBody String saveReferenceDetails(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		
		UserReferenceDetailsModel refModel = new UserReferenceDetailsModel();
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		refModel.setReference_given_user_id(regUserId);
		refModel.setReferee_name(request.getParameter("referName"));
		refModel.setReferee_emailid(request.getParameter("referEmail"));
		refModel.setReferee_mobile_no(request.getParameter("referMobileNo"));
		refModel.setReference_given_date(new Date());
		refModel.setReference_status("Pending");
		refModel.setReference_amount(0);
		
		saveStatus = userRegService.saveReferenceDetails(refModel);
		
		// Save user steps to register
		
			int useRegStageId = userRegService.getUserRegStageId(regUserId);
					
			UserRegistrationStageModel regStage = new UserRegistrationStageModel();
			regStage.setUser_reg(useRegStageId);
			regStage.setUser_id(regUserId);
			regStage.setUser_reg_steps("1-Basic+2-Product+3-Reference");
			regStage.setCost_reg_date(new Date());
			userRegService.saveUserStepDetails(regStage);
						
		// end of user step to register.
			
		userRegService.referFriendMail(request.getParameter("referEmail"),request.getParameter("referName"));				
						
		return saveStatus;
	}
	
		
	@RequestMapping(value = "/planSelection", method = RequestMethod.GET)
	public String planSelection(HttpSession session,HttpServletRequest request) {
		

		if(session.getAttribute("regUserId")!=null)
		{
            int regUserId = (int) session.getAttribute("regUserId");
			
			int totalProduct = 0;
			String agriId = "";
			String herbalId = "";
			String hmCareId = "";
			
			List<UserProductInterestModel> interestProduct = userRegService.getInterestedProduct(regUserId);
				
			if(interestProduct.size()>0)
			{
				agriId = interestProduct.get(0).getUser_interest_agriculture();
				herbalId = interestProduct.get(0).getUser_interest_herbal();
				hmCareId = interestProduct.get(0).getUser_interest_homecare();
				
				if(agriId.length()>0)
				{
					totalProduct += (agriId.split(",").length);
				}
				if(herbalId.length()>0)
				{
					totalProduct += (herbalId.split(",").length);
				}
				if(hmCareId.length()>0)
				{
					totalProduct += (hmCareId.split(",").length);
				}
				
				
			}
			
			request.setAttribute("agriId", agriId);
			request.setAttribute("herbalId", herbalId);
			request.setAttribute("hmCareId", hmCareId);
			
			request.setAttribute("totalProduct", totalProduct);
			
			return "userRegPlanSelection";		
		}
		else
		{
			return "redirect:/index.html";		
		}	
		
		//return "userRegPlanSelection";				
	}
	
	
	@RequestMapping(value = "/showPlanDetails", method = RequestMethod.POST)
	public ModelAndView showPlanDetails(HttpSession session,HttpServletRequest request) {
			
		String planType = request.getParameter("planType");
		
		List<BusinessPlanModel> planDetails = userRegService.getPlanDetailsViaPlanName(planType);
		List<BusinessPlanFeatureModel> planFeatures = userRegService.getPlanFeaturesViaPlanId(planDetails.get(0).getPlan_id());
		
		Map<String, Object> model = new HashMap<String, Object>();			
		model.put("planDetails",planDetails);
		model.put("planFeatures",planFeatures);
		
		return new ModelAndView("planTable",model);				
	}
	
	
	@RequestMapping(value = "/table", method = RequestMethod.GET)
	public String table(HttpSession session,HttpServletRequest request) {
		
		return "PlanADesign";
	}
	
		
	@RequestMapping(value = "/saveUserPlanDetails", method = RequestMethod.POST)
	public @ResponseBody String savePlanDetails(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		int regUserId = (int) session.getAttribute("regUserId");
		
		UserPlanDetailsModel planModel = new UserPlanDetailsModel();
		
		planModel.setUser_id(regUserId);
		planModel.setUser_plan_name(request.getParameter("selectPlanName"));
		planModel.setUser_plan_select_date(new Date());
		
		saveStatus = userRegService.saveUserPlanDetails(planModel);
		
		// Save user steps to register
		
			int useRegStageId = userRegService.getUserRegStageId(regUserId);
					
			UserRegistrationStageModel regStage = new UserRegistrationStageModel();
			regStage.setUser_reg(useRegStageId);
			regStage.setUser_id(regUserId);
			regStage.setUser_reg_steps("1-Basic+2-Product+3-Reference+4-Plan");
			regStage.setCost_reg_date(new Date());
			userRegService.saveUserStepDetails(regStage);
						
		// end of user step to register.
								
					
		return saveStatus;
	}
	
	
	@RequestMapping(value = "/paymentMode", method = RequestMethod.GET)
	public String paymentMode(HttpSession session,HttpServletRequest request) {
	

	    int regUserId = (int) session.getAttribute("regUserId");
		
		int totalProduct = 0;
		String agriId = "";
		String herbalId = "";
		String hmCareId = "";
		
		List<UserProductInterestModel> interestProduct = userRegService.getInterestedProduct(regUserId);
			
		if(interestProduct.size()>0)
		{
			agriId = interestProduct.get(0).getUser_interest_agriculture();
			herbalId = interestProduct.get(0).getUser_interest_herbal();
			hmCareId = interestProduct.get(0).getUser_interest_homecare();
			
			if(agriId.length()>0)
			{
				totalProduct += (agriId.split(",").length);
			}
			if(herbalId.length()>0)
			{
				totalProduct += (herbalId.split(",").length);
			}
			if(hmCareId.length()>0)
			{
				totalProduct += (hmCareId.split(",").length);
			}
			
			
		}
		
		request.setAttribute("agriId", agriId);
		request.setAttribute("herbalId", herbalId);
		request.setAttribute("hmCareId", hmCareId);
		
		request.setAttribute("totalProduct", totalProduct);
	
	
		return "userRegPaymentSelection";
	}
	
	
	@RequestMapping(value = "/capturePaymentMode", method = RequestMethod.POST)
	public @ResponseBody String capturePaymentMode(HttpSession session,HttpServletRequest request) {
	     
		String paymentMode = request.getParameter("mode");
		
		if(paymentMode==null)
		{
			return "failure";
		}
		else
		{
			session.setAttribute("paymentMode", paymentMode);
			session.setAttribute("regUserId", Integer.parseInt(request.getParameter("userId")));
			
			return "success";
		}
	}
	
	
	@RequestMapping(value = "/registrationCheckOut", method = RequestMethod.GET)
	public ModelAndView registrationCheckOut(HttpSession session,HttpServletRequest request) {
		
		if(session.getAttribute("regUserId")!=null)
		{			
			
			   // Cart Details
			
			
			    int regUserId = (int) session.getAttribute("regUserId");
				
				int totalProduct = 0;
				String agriId = "";
				String herbalId = "";
				String hmCareId = "";
				
				List<UserProductInterestModel> interestProduct = userRegService.getInterestedProduct(regUserId);
					
				if(interestProduct.size()>0)
				{
					agriId = interestProduct.get(0).getUser_interest_agriculture();
					herbalId = interestProduct.get(0).getUser_interest_herbal();
					hmCareId = interestProduct.get(0).getUser_interest_homecare();
					
					if(agriId.length()>0)
					{
						totalProduct += (agriId.split(",").length);
					}
					if(herbalId.length()>0)
					{
						totalProduct += (herbalId.split(",").length);
					}
					if(hmCareId.length()>0)
					{
						totalProduct += (hmCareId.split(",").length);
					}
					
					
				}
				
				request.setAttribute("agriId", agriId);
				request.setAttribute("herbalId", herbalId);
				request.setAttribute("hmCareId", hmCareId);
				
				request.setAttribute("totalProduct", totalProduct);
			
			
			
			  // End of Cart Details  
			
				String saveStatus = "";
						
				Map<String, Object> model = new HashMap<String, Object>();			
				model.put("userBasicInformation",userRegService.getUserBasicInfoViaUserId(regUserId));
										
				List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
				
				float kitCharge = 0;
				float vat = 0;
				float regCharge = 0;
				
				float subtotal = 0;				
				float total = 0;
				
				try{
					
					 for(int i=0;i<costDetails.size();i++)
						{
							if(costDetails.get(i).getCost_name().equals("Registration Charge"))
							{
								regCharge = Float.parseFloat(costDetails.get(i).getCost_value());
							}
							else if(costDetails.get(i).getCost_name().equals("Per Kit Cost"))
							{
								kitCharge = Float.parseFloat(costDetails.get(i).getCost_value());
							}
							else if(costDetails.get(i).getCost_name().equals("VAT"))
							{
								vat = Float.parseFloat(costDetails.get(i).getCost_value());
							}
						}
					
					float vatPersentageVal = (vat/100) * kitCharge;
					subtotal = kitCharge + vatPersentageVal;
					total = subtotal + regCharge; 
					
					request.setAttribute("kitCharge", kitCharge);
					request.setAttribute("vat", vat);
					request.setAttribute("vatPersentageVal", vatPersentageVal);
					request.setAttribute("subtotal", subtotal);
					request.setAttribute("regCharge", regCharge);
					request.setAttribute("total", total);
					
					session.setAttribute("kitCharge", kitCharge);
					session.setAttribute("vat", vat);
					session.setAttribute("vatPersentageVal", vatPersentageVal);
					session.setAttribute("subtotal", subtotal);
					session.setAttribute("regCharge", regCharge);
					session.setAttribute("total", total);
							
				}
				catch(Exception e)
				{
					request.setAttribute("kitCharge", "NA");
					request.setAttribute("vat", "NA");
					request.setAttribute("vatPersentageVal", "NA");
					request.setAttribute("subtotal", "NA");
					request.setAttribute("regCharge", "NA");
					request.setAttribute("total", "NA");
					
					session.setAttribute("kitCharge", "NA");
					session.setAttribute("vat", "NA");
					session.setAttribute("vatPersentageVal", "NA");
					session.setAttribute("subtotal", "NA");
					session.setAttribute("regCharge", "NA");
					session.setAttribute("total", "NA");
				}
				
				String paymentMode = (String)session.getAttribute("paymentMode");
				
				//String paymentMode = "Online";
				
				String orderId = "";
				Date today = new Date();
				
			    Calendar cal = Calendar.getInstance();
		        cal.setTime(today);
		        int day = cal.get(Calendar.DAY_OF_MONTH);
		        int currentTime = cal.get(Calendar.HOUR);
		        int lastTranId = userRegService.getLastTransactionId();
				   
				int lastOrderId = userRegService.getLastOrderId();
				lastOrderId = lastOrderId + 1;
				orderId = "ORD"+regUserId+day+currentTime+lastTranId+lastOrderId;
				   
				session.setAttribute("orderId", orderId);   
				   
				if(paymentMode.equals("Offline"))
				{
					
					return new ModelAndView("userRegCheckOut",model);					
				}
				else
				{					   
					return new ModelAndView("userRegOnlineCheckOut",model);					
				}					
		}
		else
		{		
			request.setAttribute("actionMessage", "Not Required");
			return new ModelAndView("userlogin");		
		}
	}
	
	
	@RequestMapping(value = "/payOnline", method = RequestMethod.POST)
	public ModelAndView submitOnlinePayment(HttpSession session,HttpServletRequest request) {
	
		session.setAttribute("total", request.getParameter("total"));
		session.setAttribute("paymentMode", request.getParameter("paymentMode"));
		session.setAttribute("vat", request.getParameter("vat"));
		
		session.setAttribute("fullname", request.getParameter("fullname"));
		session.setAttribute("emailId", request.getParameter("emailId"));
		session.setAttribute("mobileNo", request.getParameter("mobileNo"));		
		session.setAttribute("shAddress", request.getParameter("shAddress"));
		
		return new ModelAndView("userPayOnline");				
	}
	
	
	@RequestMapping(value = "/onlineRedirect", method = RequestMethod.GET)
	public String  onlineRedirect(HttpSession session,HttpServletRequest request) {
		
		session.setAttribute("PaymentID", request.getParameter("PaymentID"));
		
		return "redirect:/saveAndCompleteRegistration.html";
	}
	
		
	@RequestMapping(value = "/saveAndCompleteRegistration", method = RequestMethod.GET)
	public String saveAndCompleteRegistrationGET(HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		Date today = new Date();
		
	    Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int currentTime = cal.get(Calendar.HOUR);
         
        String total = (String) session.getAttribute("total");
		String vat = (String) session.getAttribute("vat");
		String fullname = (String) session.getAttribute("fullname");
		String emailId = (String) session.getAttribute("emailId");
		String mobileNo = (String) session.getAttribute("mobileNo");
		String shAddress = (String) session.getAttribute("shAddress");		
		String paymentMode = (String) session.getAttribute("paymentMode");
		
		
		// Save the Transaction details of the registration 
		
           int lastTranId = userRegService.getLastTransactionId();
           lastTranId = lastTranId+1;
		   String transactionId = "TRAN"+regUserId+day+currentTime+lastTranId;
		
		   TransactionDetailModel tranModel = new TransactionDetailModel();
		   
		   tranModel.setTransaction_unique_id(transactionId);
		   tranModel.setTransaction_id_from_EBS_gateway((String)session.getAttribute("PaymentID"));
		   tranModel.setTransaction_amount(total);
		   tranModel.setTransaction_mode(paymentMode);
		   //tranModel.setTransaction_type_for_organization(request.getParameter(""));
		   tranModel.setTransaction_against_user_id(regUserId);
		   tranModel.setTransaction_status("Success");
		   tranModel.setTransaction_date(today);
		   tranModel.setTransaction_TDS_amount(request.getParameter("tds"));
		   tranModel.setTransaction_VAT_amount(vat);
		   tranModel.setTransaction_ServiceTax_amount(request.getParameter("serTax"));
		   
		   userRegService.saveTransactionDetails(tranModel);
		   
		// End of Save the Transaction details of the registration 
		
		// Save the Order details of the registration 
		   
		   String orderId = "";
		   
		   int lastOrderId = userRegService.getLastOrderId();
		   lastOrderId = lastOrderId + 1;
		   orderId = "ORD"+regUserId+day+currentTime+lastTranId+lastOrderId;
		   
		   OrderDetailsModel ordModel = new OrderDetailsModel();
		   
		   ordModel.setOrder_unique_id(orderId);
		   ordModel.setOrder_status("Pending");
		   ordModel.setOrder_status_details("Will be update you soon");
		   ordModel.setOrder_for_user_id(regUserId+"");
		   ordModel.setOrder_for_name(fullname);
		   ordModel.setOrder_for_email_id(emailId);
		   ordModel.setOrder_for_mobile_no(mobileNo);
		   ordModel.setOrder_amount(total);
		   ordModel.setOrder_shipping_address(shAddress);		   
		   ordModel.setOrder_date(today);
		   ordModel.setOrder_expected_date("Date will notify to your contact details.");
		   
		   userRegService.saveOrderDetails(ordModel);
		   
		   
		// End of Save the Order details of the registration 
		
		   
					// Assign order id to transaction Id
					   
			           userRegService.asignOrderIdToTranId(transactionId,orderId);
			
			        // Assign order id to transaction Id
			          
		   
				// Update interested product details with the order Id
				   
		   			int productInterestId =  userRegService.getLastProductInterestId(regUserId);
		   			userRegService.saveUserIntProAsperOrder(productInterestId,orderId);
				   
				// End of the code for Update interested product details with the order Id
				   
		   
		   
		//  Start save Policy Amount details
		   
		   String userPlan = userService.getUserPlanViaUserId(regUserId);
		   int policyAmountperPlan = userService.getPolicyAmountAsPerPlan(userPlan);
		   		
		   // Calculate policy year duration year 
		   
		   Calendar now = Calendar.getInstance();
		   String today_date =  now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
		   now.add(Calendar.DATE, 1825);
		   String afterPolicyYearDate = now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
		   
		   DateFormat formatter = null;
	        
		   Date startingDate = null;
		   Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(today_date);
		        endingDate = (Date) formatter.parse(afterPolicyYearDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }		   
		   
		   
		   
		   // end of calculate policy year duration date
		   
		   PolicyAmountModel policyAmount  = new PolicyAmountModel();
		   
		   policyAmount.setUser_id(regUserId);
		   policyAmount.setUser_order_id(orderId);
		   policyAmount.setPaid_amount(total);
		   policyAmount.setPolicy_amount(policyAmountperPlan);
		   policyAmount.setPolicy_start_date(startingDate);
		   policyAmount.setPolicy_end_date(endingDate);
		   policyAmount.setPolicy_status("Pending");	
		   policyAmount.setPolicy_status_details("Will be update by support team.");
		   policyAmount.setPolicy_entry_date(new Date());
		   
		   userService.saveUserPolicyAmountDetails(policyAmount);
		   
		//  End of plocy Amount details  
		   
		   
		// Save user steps to register
			
				int useRegStageId = userRegService.getUserRegStageId(regUserId);
						
				UserRegistrationStageModel regStage = new UserRegistrationStageModel();
				regStage.setUser_reg(useRegStageId);
				regStage.setUser_id(regUserId);
				regStage.setUser_reg_steps("1-Basic+2-Product+3-Reference+4-Plan+5-Payment");
				regStage.setCost_reg_date(new Date());
				saveStatus = userRegService.saveUserStepDetails(regStage);
							
		// end of user step to register.
									
		
		//  Check and update reference status update mode		
				
				String referUpdate =  userService.getUpdateReferDetailsIfExist(emailId);
				
		//  End of Check and update reference status update mode	
		
		
	    // update user register Date
				
				userService.getUpdateUserRegDate(regUserId);	
				
		// End of user Update register date
			
				
		String mailStatus = userRegService.orderMail(regUserId,fullname,emailId,mobileNo,"orderDetailsMail",shAddress,orderId,session);		
				
		return "redirect:/userReturnProduct.html";
	}
	
	
	@RequestMapping(value = "/saveAndCompleteRegistration", method = RequestMethod.POST)
	public @ResponseBody String saveAndCompleteRegistration(HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		Date today = new Date();
		
	    Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int currentTime = cal.get(Calendar.HOUR);
         
		
		// Save the Transaction details of the registration 
		
           int lastTranId = userRegService.getLastTransactionId();
           lastTranId = lastTranId+1;
		   String transactionId = "TRAN"+regUserId+day+currentTime+lastTranId;
		
		
		   TransactionDetailModel tranModel = new TransactionDetailModel();
		   
		   tranModel.setTransaction_unique_id(transactionId);
		   tranModel.setTransaction_amount(request.getParameter("total"));
		   tranModel.setTransaction_mode(request.getParameter("paymentMode"));
		   //tranModel.setTransaction_type_for_organization(request.getParameter(""));
		   tranModel.setTransaction_against_user_id(regUserId);
		   tranModel.setTransaction_status("Pending");
		   tranModel.setTransaction_date(today);
		   tranModel.setTransaction_TDS_amount(request.getParameter("tds"));
		   tranModel.setTransaction_VAT_amount(request.getParameter("vat"));
		   tranModel.setTransaction_ServiceTax_amount(request.getParameter("serTax"));
		   
		   userRegService.saveTransactionDetails(tranModel);
		   
		   
		// End of Save the Transaction details of the registration 
		
		   
		// Save the Order details of the registration 
		   
		   String orderId = "";
		   
		   int lastOrderId = userRegService.getLastOrderId();
		   lastOrderId = lastOrderId + 1;
		   orderId = "ORD"+regUserId+day+currentTime+lastTranId+lastOrderId;
		   
		   OrderDetailsModel ordModel = new OrderDetailsModel();
		   
		   ordModel.setOrder_unique_id(orderId);
		   ordModel.setOrder_status("Pending");
		   ordModel.setOrder_status_details("Will be update you soon");
		   ordModel.setOrder_for_user_id(regUserId+"");
		   ordModel.setOrder_for_name(request.getParameter("fullname"));
		   ordModel.setOrder_for_email_id(request.getParameter("emailId"));
		   ordModel.setOrder_for_mobile_no(request.getParameter("mobileNo"));
		   ordModel.setOrder_amount(request.getParameter("total"));
		   ordModel.setOrder_shipping_address(request.getParameter("shAddress"));		   
		   ordModel.setOrder_date(today);
		   ordModel.setOrder_expected_date("Date will notify to your contact details.");
		   
		   userRegService.saveOrderDetails(ordModel);
		   
		   
		// End of Save the Order details of the registration 
		
					   
					// Assign order id to transaction Id
					   
			           userRegService.asignOrderIdToTranId(transactionId,orderId);
			
			        // Assign order id to transaction Id
			          
			           	
			        // Update interested product details with the order Id
					   
			   			int productInterestId =  userRegService.getLastProductInterestId(regUserId);
			   			userRegService.saveUserIntProAsperOrder(productInterestId,orderId);
					   
					// End of the code for Update interested product details with the order Id
					   
			   			
		//  Start save Policy Amount details
		   
		   String userPlan = userService.getUserPlanViaUserId(regUserId);
		   int policyAmountperPlan = userService.getPolicyAmountAsPerPlan(userPlan);
		   		
		   // Calculate policy year duration year 
		   
		   Calendar now = Calendar.getInstance();
		   String today_date =  now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
		   now.add(Calendar.DATE, 1825);
		   String afterPolicyYearDate = now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
		   
		   
		   
		   
		   DateFormat formatter = null;
	        
		   Date startingDate = null;
		   Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(today_date);
		        endingDate = (Date) formatter.parse(afterPolicyYearDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }		   
		   
		   
		    
		   // end of calculate policy year duration date
		   
		   PolicyAmountModel policyAmount  = new PolicyAmountModel();
		   
		   policyAmount.setUser_id(regUserId);
		   policyAmount.setUser_order_id(orderId);
		   policyAmount.setPaid_amount(request.getParameter("total"));
		   policyAmount.setPolicy_amount(policyAmountperPlan);
		   policyAmount.setPolicy_start_date(startingDate);
		   policyAmount.setPolicy_end_date(endingDate);
		   policyAmount.setPolicy_status("Pending");	
		   policyAmount.setPolicy_status_details("Will be update by support team.");
		   policyAmount.setPolicy_entry_date(new Date());
		   
		   userService.saveUserPolicyAmountDetails(policyAmount);
		   
		//  End of plocy Amount details  
		   
		   
		// Save user steps to register
			
				int useRegStageId = userRegService.getUserRegStageId(regUserId);
						
				UserRegistrationStageModel regStage = new UserRegistrationStageModel();
				regStage.setUser_reg(useRegStageId);
				regStage.setUser_id(regUserId);
				regStage.setUser_reg_steps("1-Basic+2-Product+3-Reference+4-Plan+5-Payment");
				regStage.setCost_reg_date(new Date());
				saveStatus = userRegService.saveUserStepDetails(regStage);
							
		// end of user step to register.
									
		
		//  Check and update reference status update mode		
				
				String referUpdate =  userService.getUpdateReferDetailsIfExist(request.getParameter("emailId"));
				
		//  End of Check and update reference status update mode	
		
		
	    // update user register Date
				
				userService.getUpdateUserRegDate(regUserId);	
				
		// End of user Update register date
			
				
		String mailStatus = userRegService.orderMail(regUserId,
							request.getParameter("fullname"),
							request.getParameter("emailId"),
							request.getParameter("mobileNo"), "orderDetailsMail",
							request.getParameter("shAddress"), orderId, session);		
				
		return saveStatus;
	}
	
	
	
	
	@RequestMapping(value = "/userReturnProduct", method = RequestMethod.GET)
	public String userReturnProduct(HttpSession session,HttpServletRequest request) {
			
		return "userReturnProductAfterFiveYear";
	}
	
	
	@RequestMapping(value = "/saveReturnBackProduct", method = RequestMethod.POST)
	public @ResponseBody String saveReturnBackProduct(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		
		UserReturnProductModel returnModel = new UserReturnProductModel();
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		returnModel.setUser_id(regUserId);
		returnModel.setUser_return_product_name(request.getParameter("returnProduct"));
		returnModel.setReturn_product_quantity_kg(request.getParameter("amtReturnProduct"));
		returnModel.setAgreed_date(new Date());
		
		saveStatus = userRegService.saveReturnProducDetails(returnModel);
		
		return saveStatus;
	}
	
	

	
	
	@RequestMapping(value = "/forgetPasswordRequest", method = RequestMethod.POST)
	public @ResponseBody String forgetPasswordRequest(HttpSession session,HttpServletRequest request) {
		
		String requestStatus = "";
		
		String emailId = request.getParameter("emailId");
		
		Date today = new Date();
		
	    Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int currentTime = cal.get(Calendar.HOUR);
        
        int lastResetId = userRegService.getLastResetId();
        lastResetId = lastResetId+1;
		String reset_unique_id = emailId.substring(3,8)+emailId.substring(0, 5)+"kfgs"+day+currentTime+lastResetId;
		
		String encryptUniqueKey = "";
		PasswordGenerator cryptoUtil=new PasswordGenerator();
		
		try 
		{
			encryptUniqueKey = cryptoUtil.encrypt(encryptUniqueKey);
		} 
		catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e)
		{
			e.printStackTrace();
		}
		
		
		encryptUniqueKey = encryptUniqueKey.replaceAll("&", "a");
		encryptUniqueKey = encryptUniqueKey.replaceAll("\\+", "b");
		encryptUniqueKey = encryptUniqueKey.replaceAll("\\=", "i");
		encryptUniqueKey = encryptUniqueKey.replaceAll("\\-", "n");
		
		
		UserResetPasswordModel resetModel = new UserResetPasswordModel();
		
		resetModel.setReset_unique_id(encryptUniqueKey);
		resetModel.setReset_email_id(emailId);
		resetModel.setReset_status("Pending");
		resetModel.setReset_date(new Date());
		
		requestStatus = userRegService.saveResetDetails(resetModel);
		
		if(requestStatus.equals("success"))
		{
			String mailStatus = userRegService.resetPasswordMail(emailId, encryptUniqueKey);
			
			if(!mailStatus.equals("success"))
			{
				requestStatus = "failed";
			}
		}
		
	    return requestStatus;
	}
		
	@RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
	public String resetPassword(HttpSession session,HttpServletRequest request) {
	
		String uniqueId = request.getParameter("requnIkedij");
		
		String requestedEmailID = userRegService.getEmailIdFromForgetPassword(uniqueId);
		
		if(requestedEmailID.equals("Not Exist"))
		{
			request.setAttribute("requestMesage", "Given Link is expired please try again.");
			
			return "userforgetPassword";
		}
		else
		{
			request.setAttribute("requestedEmailID", requestedEmailID);
			request.setAttribute("uniqueId", uniqueId);		
			return "userResetPassword";
		}
		
	}
	
	@RequestMapping(value = "/saveResetPassword", method = RequestMethod.POST)
	public @ResponseBody String saveResetPassword(HttpSession session,HttpServletRequest request) {
	
		String saveRequest = "";
		
		String emailId = request.getParameter("regEmailId");
		String password = request.getParameter("changePassword");
		
		String encryptPassword = "";
		PasswordGenerator cryptoUtil = new PasswordGenerator();
		
		try 
		{
			encryptPassword = cryptoUtil.encrypt(password);
		} 
		catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e)
		{
			e.printStackTrace();
		}
		
		
		saveRequest = userRegService.getUpdatePasswordViaEmailId(emailId,encryptPassword);
		
        //int reserId = userRegService.getResetIdViaUniqueId(request.getParameter("reqUniqueId"));
		
		userRegService.getDeleteResetDetails(emailId);      
		 
		return saveRequest;
	}
	
	public String checkSessionUser(HttpServletRequest request,HttpSession session) {
		
	    session = request.getSession(false);
		
		if (session.getAttribute("vendorId") == null)
		{							
			return "Not Exist";		
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			return "Not Exist";		
		} 
		else 
		{				
			return "Exist";									
		}
 }
 
	
	
	@RequestMapping(value = "/userLogOut", method = RequestMethod.GET)
	public String userLogOut(HttpSession session,HttpServletRequest request) {
	
		session.removeAttribute("vendorId");		
		session.removeAttribute("profilePicture");
		
		session.removeAttribute("authenticate");
		
		request.setAttribute("actionMessage", "You have successfully logged out.");
		return "userlogin";
	}
	
	
	@RequestMapping(value = "/authenticationUser", method = RequestMethod.POST)
	public @ResponseBody String authenticationUser(HttpSession session,HttpServletRequest request) {
	
		System.out.println("asdasd");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		String validateUser = userRegService.getValidateUser(emailId,password);
		
		if(!validateUser.equals("false"))
		{
			session.setAttribute("emailId",emailId);		
			session.setAttribute("vendorId",validateUser);			
			
			
			return "true";
		}
		else
		{
			return "false";
		}
		
	}
	
	@RequestMapping(value = "/checkRegistrationStage", method = RequestMethod.GET)
	public String checkRegistrationStage(HttpSession session,HttpServletRequest request) 
	{			
		
		String emailId = (String)session.getAttribute("emailId");
		int regUserId = userRegService.getUserIdViaEmailId(emailId);
		
		session.setAttribute("regUserId",regUserId);
		
		String registrationStage = userRegService.getRegistrationStage(regUserId);
		
		if(registrationStage.equals("Product"))
		{
			session.setAttribute("completeStatus", "Pending");
			
			return "redirect:/productSelection.html";		
		}
		else if(registrationStage.equals("Reference"))
		{
			session.setAttribute("completeStatus", "Pending");
			
			return "redirect:/referFriend.html";		
		}
		else if(registrationStage.equals("Plan"))
		{
			session.setAttribute("completeStatus", "Pending");
			
			return "redirect:/planSelection.html";		
		}
		else if(registrationStage.equals("Payment"))
		{
			session.setAttribute("completeStatus", "Pending");
			
			return "redirect:/paymentMode.html";		
		}		
		else
		{		
			session.setAttribute("authenticate",regUserId);
			
			return "redirect:/myProfile.html";		
		}		
		
	}
	 
	
	
	@RequestMapping(value = "/contactUsbiosys", method = RequestMethod.POST)
	public @ResponseBody String contactUsbiosys(HttpSession session,HttpServletRequest request) {
	
		String contactStatus = "";
		
		String name = request.getParameter("fullName");
		String emailId = request.getParameter("email");
		String contactNo = request.getParameter("mobileNo");
		String modeOfContact = request.getParameter("category");
		String description = request.getParameter("description");
		
		contactStatus = userRegService.contactUsMail(name,emailId,contactNo,modeOfContact,description);
		
		return contactStatus;
	}
	
	
	
	
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewProductsForuser", method = RequestMethod.GET)
	@ResponseBody public byte[] previewProducts(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String productType = request.getParameter("productType");
		String fileName = request.getParameter("fileName");
		
		// Required file Config for entire Controller 
		
		 ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
		 
		 String fileDir = "";
		 if(productType.equals("Agriculture"))
		 {
			 fileDir = fileResource.getString("agricultureProdcutFile");		
		 }
		 else if(productType.equals("Herbal"))
		 {
			 fileDir = fileResource.getString("herbalProdcutFile");	
		 }
		 else
		 {
			 fileDir = fileResource.getString("homecareProdcutFile");	
		 }
		 
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		if(StringUtils.isNotBlank(docCategory))
		{				
			/* Checks that files are exist or not  */
			
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
					fPath.append(targetFile.getPath());
				}	
				
			/* Checks that files are exist or not  */
				
		}
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}
			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
	
	
	
	
	
	
	
	
}
