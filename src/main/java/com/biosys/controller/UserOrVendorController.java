package com.biosys.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserProfileDetailsBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.domain.UserRegistrationStageModel;
import com.biosys.service.SuperAdminService;
import com.biosys.service.userRegistrationService;
import com.biosys.service.userService;

@Controller
public class UserOrVendorController {

	@Autowired
	private userRegistrationService userRegService;
	
	@Autowired
	private SuperAdminService superAdminService;
	
	@Autowired
	private userService userService;
	
	

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String contact(HttpSession session,HttpServletRequest request) {
	
		
		return "contactUs";
	}
	
	@RequestMapping(value = "/privacyAndPolicy", method = RequestMethod.GET)
	public String privacyAndPolicy(HttpSession session,HttpServletRequest request) {
	
		
		return "privacyPolicy";
	}
	
	@RequestMapping(value = "/termConditions", method = RequestMethod.GET)
	public String termConditions(HttpSession session,HttpServletRequest request) {
	
		
		return "termsAndConditions";
	}
	
	
	
	@RequestMapping(value = "/validateEmailIdOfUser", method = RequestMethod.POST)
	public @ResponseBody String validateEmailIdOfUser(HttpSession session,HttpServletRequest request) {
	
		String validateStatus = "";
		String vendorId = (String)session.getAttribute("vendorId");
		int userId = userService.getUserIdViaVendorId(vendorId);
		
		String emailId = request.getParameter("emailId");
		
		validateStatus = userService.validateUserEmailIdExistOrNot(userId,emailId);
		
		return validateStatus;
	}
	
	
	@RequestMapping(value = "/myProfile", method = RequestMethod.GET)
	public ModelAndView myProfile(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{		
			Map<String, Object> model = new HashMap<String, Object>();					
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
			
			session.setAttribute("fullName",userService.getFullNameViaVenId(vendorId));
			List<UserBasicInfoBean> userBasicInfo = userService.getUserBasicDetailsViaVendorId(vendorId);
			List<UserProfileDetailsBean> userOtherProfileInfo = userService.getUserOtherProfileDetailsViaUserId(userId);
			List<OrderDetailsBean> oderDetails = userService.getLastOrderDetailsViaUserId(userId);
			
			model.put("userBasicInfo",userBasicInfo);
			model.put("userOtherInfo",userOtherProfileInfo);
			model.put("userLastOrder",oderDetails);
			
			String profilePicture = userService.getProfilePhoto(userId);
			session.setAttribute("profilePicture",profilePicture);
			
			return new ModelAndView("userMyProfile",model);						
		}
		
	}
	
	///  Add reference Controller 
	
	@RequestMapping(value = "/userReferFriends", method = RequestMethod.GET)
	public ModelAndView userReferFriends(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{	
			Map<String, Object> model = new HashMap<String, Object>();	
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
			
			List<UserReferenceDetailsBean> referenceDetails = userService.GetReferenceDetailsViaUserId(userId);
			
			model.put("referenceDetails",referenceDetails);
			
		    return new ModelAndView("userAddMoreReference",model);	
		}
	}
	
		
	@RequestMapping(value = "/saveAddMoreReference", method = RequestMethod.POST)
	public @ResponseBody String saveReferenceDetails(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		
		UserReferenceDetailsModel refModel = new UserReferenceDetailsModel();
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		refModel.setReference_given_user_id(regUserId);
		refModel.setReferee_name(request.getParameter("referName"));
		refModel.setReferee_emailid(request.getParameter("emailId"));
		refModel.setReferee_mobile_no(request.getParameter("referMobileNo"));
		refModel.setReference_given_date(new Date());
		refModel.setReference_status("Pending");
		refModel.setReference_amount(0);
		
		saveStatus = userRegService.saveReferenceDetails(refModel);
		
		userRegService.referFriendMail(request.getParameter("emailId"),request.getParameter("referName"));				
						
		return saveStatus;
	}
	
	
	
	// End of Add reference from user side
	
	
	// buy More Packets by User
	
	@RequestMapping(value = "/buyMorePackets", method = RequestMethod.GET)
	public ModelAndView buyMorePackets(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{	
		    return new ModelAndView("userBuyPackets");	
		}
	}
	
	
	@RequestMapping(value = "/getInnerProductDetailsById", method = RequestMethod.POST)
	public ModelAndView getProductDetailsById(HttpSession session,HttpServletRequest request) {
	
		String agriProId = request.getParameter("agriProId");
		String herBalId = request.getParameter("herBalId");
		String hmCareId = request.getParameter("hmCareId");
				
		String agriProIdArray[] = agriProId.split(",");
		String herBalIdArray[] = herBalId.split(",");
		String hmCareIdArray[] = hmCareId.split(",");
		
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		List<ProductDetailsModel> agriProductDetails = new ArrayList<ProductDetailsModel>();
		for(int i=0;i<agriProIdArray.length;i++)
		{
			if(!agriProIdArray[i].equals(""))
			{
				int productId = Integer.parseInt(agriProIdArray[i]);
				agriProductDetails.addAll(superAdminService.getProductDetailsByProductId(productId));
			}
			
		}
		
		List<ProductDetailsModel> herbalProductDetails = new ArrayList<ProductDetailsModel>();
		for(int i=0;i<herBalIdArray.length;i++)
		{
			if(!herBalIdArray[i].equals(""))
			{
				int productId = Integer.parseInt(herBalIdArray[i]);
				herbalProductDetails.addAll(superAdminService.getProductDetailsByProductId(productId));
			}
		}
		
		List<ProductDetailsModel> hmCareProductDetails = new ArrayList<ProductDetailsModel>();
		for(int i=0;i<hmCareIdArray.length;i++)
		{
			if(!hmCareIdArray[i].equals(""))
			{
				int productId = Integer.parseInt(hmCareIdArray[i]);
				hmCareProductDetails.addAll(superAdminService.getProductDetailsByProductId(productId));
			}
		}
		
		model.put("agriProductDetails",agriProductDetails);
		model.put("herbalProductDetails",herbalProductDetails);
		model.put("hmCareProductDetails",hmCareProductDetails);
		
		return new ModelAndView("userInnerCartDetailsPopUp",model);		
	}
	
	
	@RequestMapping(value = "/captureProductSelection", method = RequestMethod.POST)
	public @ResponseBody String captureProductSelection(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		
		UserProductInterestModel intModel = new UserProductInterestModel();
		
		int regUserId = (int) session.getAttribute("regUserId");
		
		intModel.setUser_id(regUserId);
		intModel.setUser_interest_agriculture(request.getParameter("agricultureProduct"));
		intModel.setUser_interest_herbal(request.getParameter("herbalProduct"));
		intModel.setUser_interest_homecare(request.getParameter("homecareProduct"));
		intModel.setUpdate_date(new Date());		
		
		saveStatus = userRegService.saveProductInterestOfUser(intModel);
						
		return saveStatus;
	}
	
	
	@RequestMapping(value = "/managePaymentMode", method = RequestMethod.POST)
	public @ResponseBody String gotoCheckOutPage(HttpSession session,HttpServletRequest request) {
			
       String paymentMode = request.getParameter("mode");		
		
       if(paymentMode==null)
		{
			return "failure";
		}
		else
		{
			session.setAttribute("paymentMode", paymentMode);			
			return "success";
		}		
	}
	
	
	
	
	@RequestMapping(value = "/gotoCheckOutPage", method = RequestMethod.GET)
	public ModelAndView registrationCheckOut(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{	
			String saveStatus = "";
			
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
					
			Map<String, Object> model = new HashMap<String, Object>();			
			model.put("userBasicInformation",userRegService.getUserBasicInfoViaUserId(userId));
					
			
			List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
			
			float kitCharge = 0;
			float vat = 0;
			float regCharge = 0;
			
			float subtotal = 0;				
			float total = 0;
			
			try{
				
				for(int i=0;i<costDetails.size();i++)
				{
					if(costDetails.get(i).getCost_name().equals("Registration Charge"))
					{
						regCharge = Float.parseFloat(costDetails.get(i).getCost_value());
					}
					else if(costDetails.get(i).getCost_name().equals("Per Kit Cost"))
					{
						kitCharge = Float.parseFloat(costDetails.get(i).getCost_value());
					}
					else if(costDetails.get(i).getCost_name().equals("VAT"))
					{
						vat = Float.parseFloat(costDetails.get(i).getCost_value());
					}
				}
				
				float vatPersentageVal = (vat/100) * kitCharge;
				subtotal = kitCharge + vatPersentageVal;
				total = subtotal + regCharge; 
				
				request.setAttribute("kitCharge", kitCharge);
				request.setAttribute("vat", vat);
				request.setAttribute("vatPersentageVal", vatPersentageVal);
				request.setAttribute("subtotal", subtotal);
				request.setAttribute("regCharge", regCharge);
				request.setAttribute("total", total);
				
				session.setAttribute("kitCharge", kitCharge);
				session.setAttribute("vat", vat);
				session.setAttribute("vatPersentageVal", vatPersentageVal);
				session.setAttribute("subtotal", subtotal);
				session.setAttribute("regCharge", regCharge);
				session.setAttribute("total", total);
						
			}
			catch(Exception e)
			{
				request.setAttribute("kitCharge", "NA");
				request.setAttribute("vat", "NA");
				request.setAttribute("vatPersentageVal", "NA");
				request.setAttribute("subtotal", "NA");
				request.setAttribute("regCharge", "NA");
				request.setAttribute("total", "NA");
				
				session.setAttribute("kitCharge", "NA");
				session.setAttribute("vat", "NA");
				session.setAttribute("vatPersentageVal", "NA");
				session.setAttribute("subtotal", "NA");
				session.setAttribute("regCharge", "NA");
				session.setAttribute("total", "NA");
			}
			
			String paymentMode = (String)session.getAttribute("paymentMode");	
			
			
			String orderId = "";
			Date today = new Date();
			
		    Calendar cal = Calendar.getInstance();
	        cal.setTime(today);
	        int day = cal.get(Calendar.DAY_OF_MONTH);
	        int currentTime = cal.get(Calendar.HOUR);
	        int lastTranId = userRegService.getLastTransactionId();
			   
			int lastOrderId = userRegService.getLastOrderId();
			lastOrderId = lastOrderId + 1;
			orderId = "ORD"+userId+day+currentTime+lastTranId+lastOrderId;
			   
			session.setAttribute("orderId", orderId);   
			
			
			
			// Cart Details
			
			int regUserId = userId;
			
			int totalProduct = 0;
			String agriId = "";
			String herbalId = "";
			String hmCareId = "";
			
			List<UserProductInterestModel> interestProduct = userRegService.getInterestedProduct(regUserId);
				
			if(interestProduct.size()>0)
			{
				agriId = interestProduct.get(0).getUser_interest_agriculture();
				herbalId = interestProduct.get(0).getUser_interest_herbal();
				hmCareId = interestProduct.get(0).getUser_interest_homecare();
				
				if(agriId.length()>0)
				{
					totalProduct += (agriId.split(",").length);
				}
				if(herbalId.length()>0)
				{
					totalProduct += (herbalId.split(",").length);
				}
				if(hmCareId.length()>0)
				{
					totalProduct += (hmCareId.split(",").length);
				}
				
				
			}
			
			request.setAttribute("agriId", agriId);
			request.setAttribute("herbalId", herbalId);
			request.setAttribute("hmCareId", hmCareId);
			
			request.setAttribute("totalProduct", totalProduct);
			
			
			// End Of Cart Details
			
			
			
			if(paymentMode.equals("Online"))
			{
				return new ModelAndView("userBuyPacketsOnlineCheckOut",model);
			}
			else
			{
				return new ModelAndView("userBuyPacketsCheckOut",model);
			}
		    		
		}
		
	}
	
	
	@RequestMapping(value = "/buypayOnline", method = RequestMethod.POST)
	public ModelAndView submitOnlinePayment(HttpSession session,HttpServletRequest request) {
	
		session.setAttribute("total", request.getParameter("total"));
		session.setAttribute("paymentMode", request.getParameter("paymentMode"));
		session.setAttribute("vat", request.getParameter("vat"));
		
		session.setAttribute("fullname", request.getParameter("fullname"));
		session.setAttribute("emailId", request.getParameter("emailId"));
		session.setAttribute("mobileNo", request.getParameter("mobileNo"));		
		session.setAttribute("shAddress", request.getParameter("shAddress"));
						
		return new ModelAndView("userBuyPacketPayOnline");				
	}
	
	
	@RequestMapping(value = "/userBuyPacketOnlineRedirect", method = RequestMethod.GET)
	public String  userBuyPacketOnlineRedirect(HttpSession session,HttpServletRequest request) {
				
		session.setAttribute("PaymentID", request.getParameter("PaymentID"));
			
		return "redirect:/saveUserPurchageDetails.html";
	}
	
	
	@RequestMapping(value = "/saveUserPurchageDetails", method = RequestMethod.GET)
	public String saveUserPurchageDetailsGET(HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		String vendorId = (String)session.getAttribute("vendorId");
		int userId = userService.getUserIdViaVendorId(vendorId);
			
		Date today = new Date();
		
	    Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int currentTime = cal.get(Calendar.HOUR);
         

        String total = (String) session.getAttribute("total");
		String vat = (String) session.getAttribute("vat");
		String fullname = (String) session.getAttribute("fullname");
		String emailId = (String) session.getAttribute("emailId");
		String mobileNo = (String) session.getAttribute("mobileNo");
		String shAddress = (String) session.getAttribute("shAddress");		
		String paymentMode = (String) session.getAttribute("paymentMode");
		
		
		
		// Save the Transaction details of the registration 
		
           int lastTranId = userRegService.getLastTransactionId();
           lastTranId = lastTranId+1;
		   String transactionId = "TRAN"+userId+day+currentTime+lastTranId;
		
		
		   TransactionDetailModel tranModel = new TransactionDetailModel();
		   
		   tranModel.setTransaction_unique_id(transactionId);
		   tranModel.setTransaction_id_from_EBS_gateway((String)session.getAttribute("PaymentID"));
		   tranModel.setTransaction_amount(total);
		   tranModel.setTransaction_mode(paymentMode);
		   //tranModel.setTransaction_type_for_organization(request.getParameter(""));
		   tranModel.setTransaction_against_user_id(userId);
		   tranModel.setTransaction_status("Success");
		   tranModel.setTransaction_date(today);
		   tranModel.setTransaction_TDS_amount(request.getParameter("tds"));
		   tranModel.setTransaction_VAT_amount(vat);
		   tranModel.setTransaction_ServiceTax_amount(request.getParameter("serTax"));
		   
		   userRegService.saveTransactionDetails(tranModel);
		   
		// End of Save the Transaction details of the registration 
		
		   
		// Save the Order details of the registration 
		   
		   String orderId = "";
		   
		   int lastOrderId = userRegService.getLastOrderId();
		   lastOrderId = lastOrderId + 1;
		   orderId = "ORD"+userId+day+currentTime+lastTranId+lastOrderId;
		   
		   OrderDetailsModel ordModel = new OrderDetailsModel();
		   
		   ordModel.setOrder_unique_id(orderId);
		   ordModel.setOrder_status("Pending");
		   ordModel.setOrder_status_details("Will be update you soon");
		   ordModel.setOrder_for_user_id(userId+"");
		   ordModel.setOrder_for_name(fullname);
		   ordModel.setOrder_for_email_id(emailId);
		   ordModel.setOrder_for_mobile_no(mobileNo);
		   ordModel.setOrder_amount(total);
		   ordModel.setOrder_shipping_address(shAddress);		   
		   ordModel.setOrder_date(today);
		   ordModel.setOrder_expected_date("Date will notify to your contact details.");
		   
		   userRegService.saveOrderDetails(ordModel);
		   
		   
		   // End of Save the Order details of the registration 
		
					// Assign order id to transaction Id
					   
			           userRegService.asignOrderIdToTranId(transactionId,orderId);
			
			        // Assign order id to transaction Id
			          
		   		
				// Update interested product details with the order Id
				   
		  			int productInterestId =  userRegService.getLastProductInterestId(userId);
		  			System.out.println("Id="+userId);
		  			userRegService.saveUserIntProAsperOrder(productInterestId,orderId);
				   
				// End of the code for Update interested product details with the order Id
				   
  			
  			
		   // Save policy Amount details 
		   
			   String userPlan = userService.getUserPlanViaUserId(userId);
			   int policyAmountperPlan = userService.getPolicyAmountAsPerPlan(userPlan);
			   		
			// Calculate policy year duration year 
			   
			   Calendar now = Calendar.getInstance();
			   String today_date =  now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
			   now.add(Calendar.DATE, 1825);
			   String afterPolicyYearDate = now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
			   
			   
			   
			   
			   DateFormat formatter = null;
		        
			   Date startingDate = null;
			   Date endingDate = null;
			    
			    try {
			    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(today_date);
			        endingDate = (Date) formatter.parse(afterPolicyYearDate);
			        
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }		   
			   			   
			   // end of calculate policy year duration date
			    
			    
			   PolicyAmountModel policyAmount  = new PolicyAmountModel();
			   
			   policyAmount.setUser_id(userId);
			   policyAmount.setUser_order_id(orderId);
			   policyAmount.setPaid_amount(total);
			   policyAmount.setPolicy_amount(policyAmountperPlan);
			   policyAmount.setPolicy_start_date(startingDate);
			   policyAmount.setPolicy_end_date(endingDate);
			   policyAmount.setPolicy_status("Pending");	
			   policyAmount.setPolicy_status_details("Will be update by support team.");
			   policyAmount.setPolicy_entry_date(new Date());
			   
			   userService.saveUserPolicyAmountDetails(policyAmount);
			   
		   // End of Save policy Amount details
		   
		  String mailStatus = userRegService.orderMail(userId,fullname,emailId,mobileNo,"orderDetailsMail",shAddress,orderId,session);		
			
		  session.setAttribute("fromEBS", "True");
		  
		  
		  return "redirect:/buyMorePackets.html";
	}
	
	
	@RequestMapping(value = "/saveUserPurchageDetails", method = RequestMethod.POST)
	public @ResponseBody String saveUserPurchageDetails(HttpSession session,HttpServletRequest request) {
		
		String saveStatus = "";
		
		String vendorId = (String)session.getAttribute("vendorId");
		int userId = userService.getUserIdViaVendorId(vendorId);
			
		Date today = new Date();
		
	    Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int currentTime = cal.get(Calendar.HOUR);
         
		
		// Save the Transaction details of the registration 
		
           int lastTranId = userRegService.getLastTransactionId();
           lastTranId = lastTranId+1;
		   String transactionId = "TRAN"+userId+day+currentTime+lastTranId;
		
		
		   TransactionDetailModel tranModel = new TransactionDetailModel();
		   
		   tranModel.setTransaction_unique_id(transactionId);
		   
		   tranModel.setTransaction_amount(request.getParameter("total"));
		   tranModel.setTransaction_mode(request.getParameter("paymentMode"));
		   //tranModel.setTransaction_type_for_organization(request.getParameter(""));
		   tranModel.setTransaction_against_user_id(userId);
		   tranModel.setTransaction_status("Pending");
		   tranModel.setTransaction_date(today);
		   tranModel.setTransaction_TDS_amount(request.getParameter("tds"));
		   tranModel.setTransaction_VAT_amount(request.getParameter("vat"));
		   tranModel.setTransaction_ServiceTax_amount(request.getParameter("serTax"));
		   
		   userRegService.saveTransactionDetails(tranModel);
		   
		// End of Save the Transaction details of the registration 
		
		   
		// Save the Order details of the registration 
		   
		   String orderId = "";
		   
		   int lastOrderId = userRegService.getLastOrderId();
		   lastOrderId = lastOrderId + 1;
		   orderId = "ORD"+userId+day+currentTime+lastTranId+lastOrderId;
		   
		   OrderDetailsModel ordModel = new OrderDetailsModel();
		   
		   ordModel.setOrder_unique_id(orderId);
		   ordModel.setOrder_status("Pending");
		   ordModel.setOrder_status_details("Will be update you soon");
		   ordModel.setOrder_for_user_id(userId+"");
		   ordModel.setOrder_for_name(request.getParameter("fullname"));
		   ordModel.setOrder_for_email_id(request.getParameter("emailId"));
		   ordModel.setOrder_for_mobile_no(request.getParameter("mobileNo"));
		   ordModel.setOrder_amount(request.getParameter("total"));
		   ordModel.setOrder_shipping_address(request.getParameter("shAddress"));		   
		   ordModel.setOrder_date(today);
		   ordModel.setOrder_expected_date("Date will notify to your contact details.");
		   
		   userRegService.saveOrderDetails(ordModel);
		   
		   
		// End of Save the Order details of the registration 
		
		            
		             // Assign order id to transaction Id
		   
		                userRegService.asignOrderIdToTranId(transactionId,orderId);
		   
		             // Assign order id to transaction Id
		                
		   
					// Update interested product details with the order Id
				    
			 			int productInterestId =  userRegService.getLastProductInterestId(userId);
			 			System.out.println("Id="+userId);
			 			userRegService.saveUserIntProAsperOrder(productInterestId,orderId);
					   
					// End of the code for Update interested product details with the order Id
					   			   
		   
		   // Save policy Amount details 
		   
			   String userPlan = userService.getUserPlanViaUserId(userId);
			   int policyAmountperPlan = userService.getPolicyAmountAsPerPlan(userPlan);
			   		
// Calculate policy year duration year 
			   
			   Calendar now = Calendar.getInstance();
			   String today_date =  now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
			   now.add(Calendar.DATE, 1825);
			   String afterPolicyYearDate = now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);
			   
			   
			   
			   
			   DateFormat formatter = null;
		        
			   Date startingDate = null;
			   Date endingDate = null;
			    
			    try {
			    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(today_date);
			        endingDate = (Date) formatter.parse(afterPolicyYearDate);
			        
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }		   
			   
			   // end of calculate policy year duration date
			    
			   PolicyAmountModel policyAmount  = new PolicyAmountModel();
			   
			   policyAmount.setUser_id(userId);
			   policyAmount.setUser_order_id(orderId);
			   policyAmount.setPaid_amount(request.getParameter("total"));
			   policyAmount.setPolicy_amount(policyAmountperPlan);
			   policyAmount.setPolicy_start_date(startingDate);
			   policyAmount.setPolicy_end_date(endingDate);
			   policyAmount.setPolicy_status("Pending");	
			   policyAmount.setPolicy_status_details("Will be update by support team.");
			   policyAmount.setPolicy_entry_date(new Date());
			   
			   userService.saveUserPolicyAmountDetails(policyAmount);
			   
		   // End of Save policy Amount details
		   
		  String mailStatus = userRegService.orderMail(userId,
							request.getParameter("fullname"),
							request.getParameter("emailId"),
							request.getParameter("mobileNo"), "orderDetailsMail",
							request.getParameter("shAddress"), orderId, session);		
				
		  return mailStatus;
	}
	// End of buy more packets controller
	
	
	
	
	// Track your last order details
		
	@RequestMapping(value = "/trackOrder", method = RequestMethod.GET)
	public ModelAndView trackOrder(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{				
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
			
			Map<String, Object> model = new HashMap<String, Object>();			
			
			List<OrderDetailsBean> orderDetails = userService.getAllOrderDetails(userId);
			
			model.put("orderDetails",orderDetails);
			
			
		    return new ModelAndView("userTrackOrder",model);		
		}
	}
	
	
	
	
	// End of your tracking order 
	
	
	
	
	// Profile Settings 
	
	@RequestMapping(value = "/profileSetting", method = RequestMethod.GET)
	public ModelAndView profileSetting(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{	
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
						
			Map<String, Object> model = new HashMap<String, Object>();				
			
			List<UserProfileDetailsBean> userOthDetails = userService.getUserOtherProfileDetailsViaUserId(userId);  
			List<UserBasicInfoBean> userBasicInfo = userService.getUserBasicDetailsViaVendorId(vendorId);
			
			model.put("userOthDetails",userOthDetails);
			model.put("userBasicInfo",userBasicInfo);
			
		    return new ModelAndView("userSetting",model);		
		}
	}
	
	
	@RequestMapping(value = "/showPasswordSetting", method = RequestMethod.GET)
	public  ModelAndView showPasswordSetting(HttpSession session,HttpServletRequest request) {
	
		 
		 return new ModelAndView("userPasswordSetting");		
	}
	
	@RequestMapping(value = "/checkCurrentPassword", method = RequestMethod.POST)
	public  @ResponseBody String checkCurrentPassword(HttpSession session,HttpServletRequest request) {
	
		 String checkStatus = "";
		 String vendorId = (String)session.getAttribute("vendorId");
			
		 String password = request.getParameter("curPassword");
		 checkStatus = userService.checkCurrentPassword(password,vendorId);
		 
		 return checkStatus;
	}
	
	@RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
	public  @ResponseBody String updatePassword(HttpSession session,HttpServletRequest request) {
	
		 String updateStatus = "";
		 String vendorId = (String)session.getAttribute("vendorId");
			
		 String password = request.getParameter("nwpassword");
		 updateStatus = userService.updatePassword(password,vendorId);
		 
		 return updateStatus;
	}
	
	@RequestMapping(value = "/updateBasicInfo", method = RequestMethod.POST)
	public @ResponseBody String updateBasicInfo(HttpSession session,HttpServletRequest request) {
	
		String saveStatus = "";
		String vendorId = (String)session.getAttribute("vendorId");
		int userId = userService.getUserIdViaVendorId(vendorId);
		
		UserBasicInfoModel basicInfo = new UserBasicInfoModel();
		
		basicInfo.setUser_emailid(request.getParameter("basicEmail"));
		basicInfo.setUser_mobile_no(request.getParameter("mobileNo"));
		basicInfo.setUser_shipping_address(request.getParameter("shippingAddress"));
		basicInfo.setUser_id(userId);
		
		saveStatus = userService.getUpdateUserBasicInfo(basicInfo);
		
		return saveStatus;
	}
	
	
	
	
	@RequestMapping(value = "/saveProfileOtherDetails", method = RequestMethod.POST)
	public @ResponseBody String saveProfileOtherDetails(HttpSession session,HttpServletRequest request,@RequestParam CommonsMultipartFile[] profilePhoto) {
	
		String saveStatus = "";
		String fileName = "";
		
		String vendorId = (String)session.getAttribute("vendorId");
		int userId = userService.getUserIdViaVendorId(vendorId);
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
 	    String profileDirectory = fileResource.getString("profileDirectory")+"/"+userId;
 	   		
 	    File ourPath = new File(profileDirectory);
 	    String storePath = ourPath.getPath();
 	    
 	   UserProfileDetailsModel userOthModel = new UserProfileDetailsModel();
 	    
 	   
 	    // Create directory as per the category and 
 	    
 	   
 	    // upload the file as per the directory 
 	    
    		File fld = new File(storePath);
        	if(!fld.exists())
        	{
    		    fld.mkdirs();
        	}
    		  			     		 
    		if (profilePhoto != null && profilePhoto.length > 0)
            {
                for (CommonsMultipartFile aFile : profilePhoto)            
                {    
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {       
                        try {
							aFile.transferTo(new File(storePath + "/" + aFile.getOriginalFilename()));
						} catch (IllegalStateException | IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}  
                        fileName = aFile.getOriginalFilename();
                    }
                }
            }  
		
    		
    		
    		int userProfileId = userService.getUserProfileIdViaUserId(userId);
    		List<UserProfileDetailsBean> userOthDetails = userService.getUserOtherProfileDetailsViaUserId(userId);
    		
    		if(userProfileId!=0)
    		{
    			userOthModel.setUser_profile_id(userProfileId);
    		}
    		
    		
    		userOthModel.setUser_id(userId);
    		userOthModel.setUser_alternate_emailid(request.getParameter("alterEmailid"));
    		userOthModel.setUser_dob(request.getParameter("dateOfBirth"));
    		
    		if(fileName.equals(""))
    		{
    			String profilePicture = userService.getProfilePhoto(userId);
    			if(!profilePicture.equals("No Photo"))
    			{
    				userOthModel.setUser_profile_image_name(profilePicture);
    			}
    			else
    			{
    				userOthModel.setUser_profile_image_name(fileName);
    			}			
    		}
    		else
    		{
    			userOthModel.setUser_profile_image_name(fileName);
    		}
    		
    		userOthModel.setUser_permanent_address(request.getParameter("permanentAddress"));
    		userOthModel.setUser_id_type(request.getParameter("poiTypeName"));
    		userOthModel.setUser_proof_of_id(request.getParameter("poiIdName").toUpperCase());
    		userOthModel.setUser_profile_cr_date(new Date());
    		
    		saveStatus = userService.saveUserOtherProfileDetails(userOthModel);
    		
    		if(userOthModel.getUser_profile_image_name()!=null)
    		{
    			userService.updateProfileImageInBasicInfo(userId,userOthModel.getUser_profile_image_name());
    		}
    		
		    return saveStatus;
	}
	
	
	
	// End of Profile Setting  
	
	
	
	
	// Bonus amount Details
	
	@RequestMapping(value = "/earcnedReferenceAmt", method = RequestMethod.GET)
	public ModelAndView bonusAmount(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{	
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
			
			String userPlan = userService.getUserPlanViaUserId(userId);
			request.setAttribute("userPlan", userPlan);	
					
			List<UserReferenceDetailsModel> referDetails = userService.getUserReferDetails(userId);
			List<ReferBonusAmountModel> bonusDetails = userService.getReferalBonusDetails(userId);
			
			List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
			float tds = 0;
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("TDS"))
				{
					tds = Float.parseFloat(costDetails.get(i).getCost_value());
				}				
			}	
			
			float totalCreditAmount = 0 ;
			
			for(int i=0;i<referDetails.size();i++)
			{
				String checkStatus = referDetails.get(i).getReference_check_status();
				if(checkStatus==null)
				{					
					totalCreditAmount += 0;
				}	
				else if(checkStatus.equals("Pending"))
				{
					float tdsAmount = (tds/100) * referDetails.get(i).getReference_amount();					
					totalCreditAmount += referDetails.get(i).getReference_amount() - tdsAmount;
				}	
				else
				{
					totalCreditAmount += 0;
				}
			}
			
			for(int i=0;i<bonusDetails.size();i++)
			{
				String checkStatus = bonusDetails.get(i).getBonus_check_status();
				if(checkStatus==null)
				{					
					totalCreditAmount += 0;
				}	
				else if(checkStatus.equals("Pending"))
				{
					float tdsAmount = (tds/100) * bonusDetails.get(i).getBonus_amount();
					
					totalCreditAmount += bonusDetails.get(i).getBonus_amount() - tdsAmount;
				}
				else
				{
					totalCreditAmount += 0;
				}
			}
			
			request.setAttribute("totalCreditAmount", totalCreditAmount);
			
			Map<String, Object> model = new HashMap<String, Object>();										
            model.put("referDetails",referDetails);
			
		    return new ModelAndView("userBonusAmount",model);	
		}
	}
	
	@RequestMapping(value = "/showReferenceStatus", method = RequestMethod.POST)
	public ModelAndView showReferenceStatus(HttpSession session,HttpServletRequest request) {
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		int referenceId = Integer.parseInt(request.getParameter("referenceId"));
		
		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
		float tds = 0;
		for(int i=0;i<costDetails.size();i++)
		{
			if(costDetails.get(i).getCost_name().equals("TDS"))
			{
				tds = Float.parseFloat(costDetails.get(i).getCost_value());
			}				
		}			
		
		List<UserReferenceDetailsModel> referDetail = userService.getReferDetailViaReferId(referenceId);		
		int referAmount = referDetail.get(0).getReference_amount();
		float tdsAmount = (tds/100) * referAmount;		
		float total = 	referAmount	- tdsAmount;							
      
		model.put("eachreferDetails",referDetail);       
		request.setAttribute("referAmount",referAmount);	
		request.setAttribute("tds",tds);	
		request.setAttribute("tdsAmount",tdsAmount);	
		request.setAttribute("total",total);	
		
		return new ModelAndView("userReferenceStatus",model);
	}
	
	// reference bonus policy status
	
	@RequestMapping(value = "/refBonusPolicyStatus", method = RequestMethod.POST)
	public ModelAndView refBonusPolicyStatus(HttpSession session,HttpServletRequest request) {
	
		Map<String, Object> model = new HashMap<String, Object>();	
		
		String vendorId = (String)session.getAttribute("vendorId");
		int userId = userService.getUserIdViaVendorId(vendorId);
		
		String userPlan = userService.getUserPlanViaUserId(userId);
			
		model.put("referalBonusDetails",userService.getReferalBonusDetails(userId));     
		
		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
		float tds = 0;	
		try{
			
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("TDS"))
				{
					tds = Float.parseFloat(costDetails.get(i).getCost_value());
				}				
			}						
		}
		catch(Exception e)
		{
			request.setAttribute("tds", "NA");				
		}
					 
		request.setAttribute("tds",tds);	
		 
		return new ModelAndView("userReferBonusPolicy",model);
	}
	
	
	
	@RequestMapping(value = "/policyAmount", method = RequestMethod.GET)
	public ModelAndView policyAmount(HttpSession session,HttpServletRequest request) {
	
		if (session.getAttribute("vendorId") == null)
		{							
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");
		}
		else if(((String)session.getAttribute("vendorId")).equals(null))
		{
			session.removeAttribute("vendorId");			 
			request.setAttribute("actionMessage", "Session had expired please login.");
			return new ModelAndView("userlogin");	
		} 
		else 
		{	
			
			String vendorId = (String)session.getAttribute("vendorId");
			int userId = userService.getUserIdViaVendorId(vendorId);
			
			String userPlan = userService.getUserPlanViaUserId(userId);
			
			int policyAmount = userService.getPolicyAmountAsPerPlan(userPlan);
			List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
			
			
			request.setAttribute("userPlan", userPlan);	
			
            Map<String, Object> model = new HashMap<String, Object>();							
			
            model.put("policyDetails",userService.getUserAllPolicyDetails(userId));
			
			float tds = 0;
			float tdsAmount = 0;					
			
			try{
				
				for(int i=0;i<costDetails.size();i++)
				{
					if(costDetails.get(i).getCost_name().equals("TDS"))
					{
						tds = Float.parseFloat(costDetails.get(i).getCost_value());
					}				
				}						
			}
			catch(Exception e)
			{
				request.setAttribute("tds", "NA");				
			}
						 
			request.setAttribute("tds",tds);	
			 
		    return new ModelAndView("userPolicyAmount",model);	
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// Preview Profile Photo
	
	
	@SuppressWarnings({ "resource", "unused" })
	@RequestMapping(value = "/previewProfilePicture", method = RequestMethod.GET)
	@ResponseBody public byte[] previewProducts(HttpServletRequest request,HttpSession session) throws IOException {
		
		
		String fileName = request.getParameter("fileName");
		
		// Required file Config for entire Controller 
		 
		 ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
		 String vendorId = (String)session.getAttribute("vendorId");
		 int userId = userService.getUserIdViaVendorId(vendorId);
			
		 	
		 String fileDir = fileResource.getString("profileDirectory")+"/"+userId;
		 
		// End of Required file Config for entire Controller 
		 
		ServletContext servletContext = request.getSession().getServletContext();
		
		File ourPath = new File(fileDir);
		
		StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
		String docCategory = fPath.toString();
		
		if(StringUtils.isNotBlank(docCategory))
		{				
			/* Checks that files are exist or not  */
			
				File exeFile=new File(fPath.toString());
				if(!exeFile.exists())
				{
					fPath.delete(0, fPath.length());
					File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
					fPath.append(targetFile.getPath());
				}	
				
			/* Checks that files are exist or not  */
				
		}
		
		InputStream in;
		
		try {					
				FileInputStream docdir = new FileInputStream(fPath.toString());													
				if (docdir != null) 
				{						
					return IOUtils.toByteArray(docdir);					
				} 
				else 
				{							
					return null;
				}
			
		} 
		catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return null; // todo: return safe photo instead
		}

	}
	
}
