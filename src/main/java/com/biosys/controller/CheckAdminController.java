package com.biosys.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.service.SubAdminService;
import com.biosys.service.SuperAdminService;
import com.biosys.service.userRegistrationService;
import com.biosys.service.userService;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserReferenceDetailsBean;

@Controller
public class CheckAdminController {

	@Autowired
	private SubAdminService subAdminService;
	
	@Autowired
	private userRegistrationService userRegService;
	
	@Autowired
	private SuperAdminService superAdminService;
	
	@Autowired
	private userService userService;
	
	
	@RequestMapping(value = "/cheadminsignout", method = RequestMethod.GET)
	public String supAdminLogOut(HttpSession session,HttpServletRequest request) {
				
		session.removeAttribute("cheAdminId");			 
		request.setAttribute("actionMessage", "success");
        return "subAdminLogin";				
	}
	
	
	@RequestMapping(value = "/checkAdmin", method = RequestMethod.GET)
	public ModelAndView authSubAdminLogin(HttpSession session,HttpServletRequest request) {
				        
		if (session.getAttribute("cheAdminId") == null)
		{									
			request.setAttribute("actionMessage", "notRquired");
			return new ModelAndView("subAdminLogin");	
        }
        else
        {
            Map<String, Object> model = new HashMap<String, Object>();		
			List<Integer> userIdList = new ArrayList<Integer>();
            
        	List<TransactionDetailModel> tranDetails = subAdminService.getOffLineTransacitonCheckDetails();
        	for(int i=0;i<tranDetails.size();i++)
        	{
        		userIdList.add(tranDetails.get(i).getTransaction_against_user_id());
        	}
        	
        	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
        	
        	model.put("tranDetails",tranDetails);
        	model.put("userBasicDetails",userBasicDetails);
        	
        	return new ModelAndView("checkAdmin",model);
        }						
	}
	
		
	@RequestMapping(value = "/updateTransactionCheckStatus", method = RequestMethod.POST)
	public @ResponseBody String updateTransactionCheckStatus(HttpSession session,HttpServletRequest request) {
	
		String updateStatus = "";
		
		String checkStatus = request.getParameter("status");
		int uniqueId =  Integer.parseInt(request.getParameter("uniqueId"));
		
		updateStatus = subAdminService.updateCheckStatusByCriteria(uniqueId,checkStatus,"Transaction");
		
		return updateStatus;
	}
		
	@RequestMapping(value = "/searchTranCheckStatus", method = RequestMethod.POST)
	public ModelAndView searchTranCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		
		Map<String, Object> model = new HashMap<String, Object>();		
		List<Integer> userIdList = new ArrayList<Integer>();
		
		List<TransactionDetailModel> tranDetails = subAdminService.getOffLineTransacitonCheckDetails(searchType,name,startDate,endDate);
    	for(int i=0;i<tranDetails.size();i++)
    	{
    		userIdList.add(tranDetails.get(i).getTransaction_against_user_id());
    	}
    	
    	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
    	
    	model.put("tranDetails",tranDetails);
    	model.put("userBasicDetails",userBasicDetails);
    			
		return new ModelAndView("checkSearchOffLineUserCheck",model);
	}
	
	
	// End of Offline Check Functionality 
	
	
	
	
	// Reference Check Details 
	
	
	@RequestMapping(value = "/referFriendCheck", method = RequestMethod.GET)
	public ModelAndView referFriendCheck(HttpSession session,HttpServletRequest request) {
				
        
		if (session.getAttribute("cheAdminId") == null)
		{			
			request.setAttribute("actionMessage", "notRquired");
        	return new ModelAndView("subAdminLogin");
        }
        else
        {
        	Map<String, Object> model = new HashMap<String, Object>();		
			
        	List<UserReferenceDetailsModel> referenceDetails = subAdminService.getReferenceCheckDetails();
        	
        	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsViaRefBean(referenceDetails);
        	
        	model.put("referenceDetails",referenceDetails);
        	model.put("userBasicDetails",userBasicDetails);
        	
        	return new ModelAndView("checkForReferFriend",model);
        }						
	}
	
	
	
	@RequestMapping(value = "/updateRefCheckStatus", method = RequestMethod.POST)
	public @ResponseBody String updateRefCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String updateStatus = "";
		
		String checkStatus = request.getParameter("status");
		int referId =  Integer.parseInt(request.getParameter("referId"));
		
		updateStatus = subAdminService.updateReferCheckStatus(referId,checkStatus);
		
		return updateStatus;
	}
	
	
	@RequestMapping(value = "/searchRefCheckStatus", method = RequestMethod.POST)
	public ModelAndView searchRefCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		
		Map<String, Object> model = new HashMap<String, Object>();		
		
    	List<UserReferenceDetailsModel> referenceDetails = subAdminService.getReferenceCheckDetailsByCriteria(searchType,name,startDate,endDate);
    	
    	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsViaRefBean(referenceDetails);
    	
    	model.put("referenceDetails",referenceDetails);
    	model.put("userBasicDetails",userBasicDetails);
    			
		return new ModelAndView("checkSearchUserCheck",model);
	}
	
	
	// End of reference Check Details
	
	
	
	
	
	// Reference Bonus Check Details
	
		
	@RequestMapping(value = "/referFriendBonusCheck", method = RequestMethod.GET)
	public ModelAndView referFriendBonusCheck(HttpSession session,HttpServletRequest request) {
				       
		if (session.getAttribute("cheAdminId") == null)
		{									
			request.setAttribute("actionMessage", "notRquired");
			
			return new ModelAndView("subAdminLogin");	
        }
        else
        {   
        	String searchType = request.getParameter("searchType");
    		String name = request.getParameter("name");
    		String startDate = request.getParameter("startDate");
    		String endDate = request.getParameter("endDate");
    		List<Integer> userIdList = new ArrayList<Integer>();
    		
        	Map<String, Object> model = new HashMap<String, Object>();		
			
        	List<ReferBonusAmountModel> bonusDetail = subAdminService.getReferenceBonusChecksWithCriteria(searchType,name,startDate,endDate);
        	for(int i=0;i<bonusDetail.size();i++)
        	{
        		userIdList.add(bonusDetail.get(i).getUser_id());
        	}
        	
        	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
        	
        	model.put("bonusDetail",bonusDetail);
        	model.put("userBasicDetails",userBasicDetails);
        	       	
        	return new ModelAndView("checkForReferFriendBonus",model);
        }	 					
	}
	
	
	@RequestMapping(value = "/updateRefBonusCheckStatus", method = RequestMethod.POST)
	public @ResponseBody String updateRefBonusCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String updateStatus = "";
		
		String checkStatus = request.getParameter("status");
		int uniqueId =  Integer.parseInt(request.getParameter("uniqueId"));
		
		updateStatus = subAdminService.updateCheckStatusByCriteria(uniqueId,checkStatus,"Bonus");
		
		return updateStatus;
	}
	
	
	@RequestMapping(value = "/searchRefBonusCheckStatus", method = RequestMethod.POST)
	public ModelAndView searchRefBonusCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		
		Map<String, Object> model = new HashMap<String, Object>();		
		List<Integer> userIdList = new ArrayList<Integer>();
		
		List<ReferBonusAmountModel> bonusDetail = subAdminService.getReferenceBonusChecksWithCriteria(searchType,name,startDate,endDate);
    	for(int i=0;i<bonusDetail.size();i++)
    	{
    		userIdList.add(bonusDetail.get(i).getUser_id());
    	}
    	
    	
    	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
    	
    	model.put("bonusDetail",bonusDetail);
    	model.put("userBasicDetails",userBasicDetails);
    				
		return new ModelAndView("checkSearchBonusUserCheck",model);
	}
	
	
	
	// End of Reference Bonus Check Details
	
	
	
	
	
	
	// Start Policy Check Management 
	
	@RequestMapping(value = "/userPlanPolicyCheck", method = RequestMethod.GET)
	public ModelAndView userPlanPolicyCheck(HttpSession session,HttpServletRequest request) {
				        
		if (session.getAttribute("cheAdminId") == null)
		{									
			request.setAttribute("actionMessage", "notRquired");
			return new ModelAndView("subAdminLogin");	
        }
        else
        {
        	String searchType = request.getParameter("searchType");
    		String name = request.getParameter("name");
    		String startDate = request.getParameter("startDate");
    		String endDate = request.getParameter("endDate");
    		List<Integer> userIdList = new ArrayList<Integer>();
    		
        	Map<String, Object> model = new HashMap<String, Object>();		
			
        	List<PolicyAmountModel> bonusDetail = subAdminService.getPolicyBonusChecksWithCriteria(searchType,name,startDate,endDate);
        	
        	for(int i=0;i<bonusDetail.size();i++)
        	{
        		userIdList.add(bonusDetail.get(i).getUser_id());
        	}
        	
        	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
        	
        	model.put("bonusDetail",bonusDetail);
        	model.put("userBasicDetails",userBasicDetails);
        	 
        	
        	
        	return new ModelAndView("checkForPlanPolicy",model);
        }						
	}
	
	
	@RequestMapping(value = "/updatePolicyBonusCheckStatus", method = RequestMethod.POST)
	public @ResponseBody String updatePolicyBonusCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String updateStatus = "";
		
		String checkStatus = request.getParameter("status");
		int uniqueId =  Integer.parseInt(request.getParameter("uniqueId"));
		
		updateStatus = subAdminService.updateCheckStatusByCriteria(uniqueId,checkStatus,"Policy");
		
		return updateStatus;
	}
	
	
	@RequestMapping(value = "/searchPolicyBonusCheckStatus", method = RequestMethod.POST)
	public ModelAndView searchPolicyBonusCheckStatus(HttpSession session,HttpServletRequest request) {
		
		String searchType = request.getParameter("searchType");
		String name = request.getParameter("name");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
				
		Map<String, Object> model = new HashMap<String, Object>();		
		List<Integer> userIdList = new ArrayList<Integer>();
		
		List<PolicyAmountModel> bonusDetail = subAdminService.getPolicyBonusChecksWithCriteria(searchType,name,startDate,endDate);
    	for(int i=0;i<bonusDetail.size();i++)
    	{
    		userIdList.add(bonusDetail.get(i).getUser_id());
    	}
    	    	
    	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
    	
    	model.put("bonusDetail",bonusDetail);
    	model.put("userBasicDetails",userBasicDetails);
    				
		return new ModelAndView("checkSearchPolicyBonusUserCheck",model);
	}
	
	
	// End of Policy Check Functionality 
	
	
	
	
	
	
	@RequestMapping(value = "/checkReports", method = RequestMethod.GET)
	public ModelAndView checkReports(HttpSession session,HttpServletRequest request) {
				       
		if (session.getAttribute("cheAdminId") == null)
		{									
			request.setAttribute("actionMessage", "notRquired");
			return new ModelAndView("subAdminLogin");	
        }
        else
        {
        	Map<String, Object> model = new HashMap<String, Object>();	
        	
        	model.put("checkReport",subAdminService.getCheckReportDetails("Monthly"));
        	
        	return new ModelAndView("checkReports",model);
        }
						
	}
	
	
	
	@RequestMapping(value = "/generateCheckReport", method = RequestMethod.POST)
	public @ResponseBody String generateCheckReport(HttpSession session,HttpServletRequest request) {
				       
		String checkReportStatus	= "";
		
		String reportType = request.getParameter("reportType");
		String reportFor = request.getParameter("reportFor");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
				
    	checkReportStatus = subAdminService.createCheckReport(reportType,reportFor,fromDate,toDate,request);
    	
		return checkReportStatus;		
	}
	
	
	
	@RequestMapping(value = "/downloadCheckReport", method = RequestMethod.GET)
	public String  downloadCheckReport(HttpSession session,HttpServletRequest request) {
						
		String referCategory = request.getParameter("referCategory");
		
		if(referCategory.equals("Offline transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String offLineTransationReportDirectory = fileResource.getString("offLineTransationReportDirectory");		
			
			request.setAttribute("rootDirectory",offLineTransationReportDirectory);
		}
		else if(referCategory.equals("Refer Friend Check"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String referFriendReportDirectory = fileResource.getString("referFriendReportDirectory");		
			
			request.setAttribute("rootDirectory",referFriendReportDirectory);
		}
		else if(referCategory.equals("Refer Friend Bonus"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String referFriendBonusReportDirectory = fileResource.getString("referFriendBonusReportDirectory");		
			
			request.setAttribute("rootDirectory",referFriendBonusReportDirectory);
		}
		else
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String userPlanPolicyReportDirectory = fileResource.getString("userPlanPolicyReportDirectory");		
			
			request.setAttribute("rootDirectory",userPlanPolicyReportDirectory);
		}
		
        request.setAttribute("reportName",request.getParameter("reportName"));
		
		return "downloadReport";			
	}
	
	
	@RequestMapping(value = "/deleteCheckReport", method = RequestMethod.POST)
	public @ResponseBody String deleteCheckReport(HttpSession session,HttpServletRequest request) {
				
		String checkReportStatus	= "";
		
		String reportName = request.getParameter("reportName");
		String reportCategory = request.getParameter("ReportCategory");
		int reportId = Integer.parseInt(request.getParameter("reportId"));
		
		if(reportCategory.equals("Offline transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String reportCategoryResource = fileResource.getString("offLineTransationReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(reportCategoryResource,reportName,reportId,request);			
		}
		else if(reportCategory.equals("Refer Friend Check"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String reportCategoryResource = fileResource.getString("referFriendReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(reportCategoryResource,reportName,reportId,request);
		}
		else if(reportCategory.equals("Refer Friend Bonus"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String reportCategoryResource = fileResource.getString("referFriendBonusReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(reportCategoryResource,reportName,reportId,request);
		}
		else
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String reportCategoryResource = fileResource.getString("userPlanPolicyReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(reportCategoryResource,reportName,reportId,request);
		}
				
		return checkReportStatus;		
	}
	
	
	
	
	
	
}
