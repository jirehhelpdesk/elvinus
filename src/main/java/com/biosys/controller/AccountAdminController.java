package com.biosys.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.TDSReportModel;
import com.biosys.domain.TaxReportModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.service.SubAdminService;
import com.biosys.service.SuperAdminService;

@Controller
public class AccountAdminController {

	@Autowired
	private SubAdminService subAdminService;
	
	@Autowired
	private SuperAdminService superAdminService;
	
	
	@RequestMapping(value = "/actadminsignout", method = RequestMethod.GET)
	public String supAdminLogOut(HttpSession session,HttpServletRequest request) {
				
		session.removeAttribute("actAdminId");			 
		request.setAttribute("actionMessage", "success");
        return "subAdminLogin";				
	}
	
		
	@RequestMapping(value = "/accountAdmin", method = RequestMethod.GET)
	public ModelAndView authSubAdminLogin(HttpSession session,HttpServletRequest request) {
				        
		if (session.getAttribute("actAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {       	
        	return new ModelAndView("accountAdmin");
        }
						
	}	
	
	
	@RequestMapping(value = "/searchTransactionDetails", method = RequestMethod.POST)
	public ModelAndView searchTransactionDetails(HttpSession session,HttpServletRequest request) {
				
		String tranId = request.getParameter("tranId");
		String tranType = request.getParameter("tranType");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("tranDetails",subAdminService.getListOfTransaction(tranId,tranType));		
		return new ModelAndView("accountTransactionList",model);			
	}
	
	
	@RequestMapping(value = "/showTransDetails", method = RequestMethod.POST)
	public ModelAndView showTransDetails(HttpSession session,HttpServletRequest request) {
				
		String tranId = request.getParameter("transactionId");
		String tranType = request.getParameter("tranType");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("tranDetails",subAdminService.getListOfTransaction(tranId,tranType));		
		return new ModelAndView("accountTransactionUpdate",model);				
	}
	
	
	@RequestMapping(value = "/amountReceived", method = RequestMethod.GET)
	public ModelAndView amountReceived(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("actAdminId") == null)
		{									
			return new ModelAndView("subAdminLogin");
        }
        else
        {
        	List<TransactionDetailModel> tranObject = subAdminService.getListOfTodaysTransaction();
        	Map<String, Object> model = new HashMap<String, Object>();  		
    		model.put("todaysTranDetails",tranObject);	
    		
    		// Calculate Total turn over on today's transaction 
    		
    		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		

    		float vat = 0;
    		
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("VAT"))
				{
					vat = Float.parseFloat(costDetails.get(i).getCost_value());
				}
			}
    			
    		long totalAmount = 0;
    		
    		for(int i=0;i<tranObject.size();i++)
    		{
    			String amount = tranObject.get(i).getTransaction_amount();   
    			if(amount!=null)
    			{
    				totalAmount += Integer.parseInt(amount.substring(0, amount.lastIndexOf(".")));
    			}
    		}
    		    		
    		float vatPersentageVal = (vat/100) * totalAmount;
			double totalAmountWithOutVat = totalAmount - vatPersentageVal;
			
			request.setAttribute("vat", vat);	
    		request.setAttribute("totalTurnOver", totalAmount);    
    		request.setAttribute("totalAmountWithOutVat", totalAmountWithOutVat);   
    		   		
        	return new ModelAndView("accountRecivedAmount",model);				  	
        }	
	}
	
	
	@RequestMapping(value = "/amountReceivedWeekly", method = RequestMethod.POST)
	public ModelAndView amountReceivedWeekly(HttpSession session,HttpServletRequest request) {
				
        	List<TransactionDetailModel> tranObject = subAdminService.getListOfWeeklyTransaction();
        	Map<String, Object> model = new HashMap<String, Object>();  		
    		model.put("weeklyTranDetails",tranObject);	
    		
    		// Calculate Total turn over on today's transaction 
    		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		

    		float vat = 0;
    		
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("VAT"))
				{
					vat = Float.parseFloat(costDetails.get(i).getCost_value());
				}
			}
    			
    		
    		long totalAmount = 0;
    		for(int i=0;i<tranObject.size();i++)
    		{
    			String amount = tranObject.get(i).getTransaction_amount();
    			if(amount!=null)
    			{
    				totalAmount += Integer.parseInt(amount.substring(0, amount.lastIndexOf(".")));
    			}    			
    		}
    		
    		
    		float vatPersentageVal = (vat/100) * totalAmount;
			double totalAmountWithOutVat = totalAmount - vatPersentageVal;
			
			request.setAttribute("vat", vat);	
    		request.setAttribute("totalTurnOver", totalAmount);    
    		request.setAttribute("totalAmountWithOutVat", totalAmountWithOutVat);   
    		
    		
    		Date today = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		Date sevenDayBefore = new Date(today.getTime() - 7 * 24 * 3600 * 1000);
    		
    		request.setAttribute("today", sdf.format(today));
    		request.setAttribute("sevenDayBefore", sdf.format(sevenDayBefore));
    		
        	return new ModelAndView("accountRecivedAmountWeekly",model);				  	      
	}
	
	
	//  download the weekly report
		
	@RequestMapping(value = "/downloadAmtReceivedRecord", method = RequestMethod.POST)
	public void downloadAmtReceivedRecord(HttpSession session,HttpServletRequest request) {
				
        	List<TransactionDetailModel> tranObject = subAdminService.getListOfWeeklyTransaction();
    		long totalAmount = 0;
    		
    		Date today = new Date();
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    		Date sevenDayBefore = new Date(today.getTime() - 7 * 24 * 3600 * 1000);
    		
    		request.setAttribute("today", sdf.format(today));
    		request.setAttribute("sevenDayBefore", sdf.format(sevenDayBefore));
    		
    		HSSFWorkbook workbook = new HSSFWorkbook();
        	HSSFSheet sheet = workbook.createSheet("Java Books");
            
            Object[][] bookData = {
            		            		
                    {"Head First Java", "Kathy Serria", 79},
                    {"Effective Java", "Joshua Bloch", 36},
                    {"Clean Code", "Robert martin", 42},
                    {"Thinking in Java", "Bruce Eckel", 35},
                    
            };
     
            int rowCount = 0;
             
            for (Object[] aBook : bookData) {
                Row row = sheet.createRow(++rowCount);
                 
                int columnCount = 0;
                 
                for (Object field : aBook) {
                    Cell cell = row.createCell(++columnCount);
                    if (field instanceof String) {
                        cell.setCellValue((String) field);
                    } else if (field instanceof Integer) {
                        cell.setCellValue((Integer) field);
                    }
                }
                 
            }
             
           
            try (FileOutputStream outputStream = new FileOutputStream("D:\\JavaBooks.xls")) {
                try {
					workbook.write(outputStream);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            } catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	
            
	}
	
	
	@RequestMapping(value = "/taxAmountToGovt", method = RequestMethod.GET)
	public ModelAndView taxAmountToGovt(HttpSession session,HttpServletRequest request) {
				
		if (session.getAttribute("actAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {      	
        	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
   		    int fromVATreportDate = Integer.parseInt(fileResource.getString("fromVATreportDate"));	
   		    int toVATreportDate = Integer.parseInt(fileResource.getString("toVATreportDate"));	
   		      		    
        	List<TransactionDetailModel> tranObject = subAdminService.getTaxTransaction();  //   getSuccessTransactionDetails();
        	
        	List<Integer> useIds = new ArrayList<Integer>();
	    	for(int i=0;i<tranObject.size();i++)
	    	{
	    		useIds.add(tranObject.get(i).getTransaction_against_user_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetail = subAdminService.getUserDetailModelByUserId(useIds);  // As per User Details 
	    		    	
        	Map<String, Object> model = new HashMap<String, Object>();  		
    		model.put("tranObject",tranObject);	
    		
    		// Calculate Total turn over on today's transaction 
    		
    		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
    		
    		float vat = 0;
    		
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("VAT"))
				{
					vat = Float.parseFloat(costDetails.get(i).getCost_value());
				}
			}
    			
    		long totalAmount = 0;
    		
    		for(int i=0;i<tranObject.size();i++)
    		{
    			String amount = tranObject.get(i).getTransaction_amount();    		
    			if(amount!=null)
    			{
    				totalAmount += Integer.parseInt(amount.substring(0, amount.lastIndexOf(".")));
    			}  			
    		}
    		   		
    		float vatPersentageVal = (vat/100) * totalAmount;
			double totalAmountWithOutVat = totalAmount - vatPersentageVal;
			
			request.setAttribute("vat", vat);	
    		request.setAttribute("totalTurnOver", totalAmount);    
    		request.setAttribute("vatPersentageVal", vatPersentageVal);   
    		
    		Date today = new Date();
    		Calendar cal = Calendar.getInstance();
    	    cal.setTime(today);
    	    int year = cal.get(Calendar.YEAR);
    	    int month = cal.get(Calendar.MONTH);
    	    int currentMonth = month + 1;
    	    int day = cal.get(Calendar.DAY_OF_MONTH);
    	    
    	    int lastDay = day + 1;
    	    
    	    String reportName = "VAT Report_"+lastDay+"-"+month+" to "+day+"-"+currentMonth+" "+year; 
    	    
    	    TaxReportModel taxModel = new TaxReportModel();
    	    
    	    if(day==toVATreportDate)
    	    {
    	    	int taxId = subAdminService.getTodaysTaxId(reportName);
    	    	
    	    	if(taxId!=0)
    	    	{
    	    		taxModel.setTax_id(taxId);
    	    	}
    	    	
    	    	taxModel.setTax_duration_details(lastDay+"-"+month+" to "+day+"-"+currentMonth);
    	    	taxModel.setTax_report_date(new Date());
    	    	taxModel.setTax_report_name(reportName);
    	    	taxModel.setTax_status("Document Ready");
    	    	taxModel.setTax_amount(vatPersentageVal);  
    	    	
    	    	subAdminService.saveTaxReportDetails(taxModel);  
    	    	   	    	
    	    	// Report Generator
    	    	
    	    	String vatTAXReportDirectory = fileResource.getString("vatTAXReportDirectory");
    	    	
    	    	String fileDirectory = vatTAXReportDirectory;
    	    	   	    			
    	    	try 
    	    	{    	    		 
    	             File exlDirectory = new File(fileDirectory);
    	             if(!exlDirectory.exists())
    	         	 {
    	            	 exlDirectory.mkdirs();
    	         	 }
    	             
    	             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
    	             
    	             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
    	             
    	             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
    	        	 
    	             //Add the created Cells to the heading of the sheet
    	             
    	             writableSheet.addCell(new Label(0, 0, "Serial No"));
    	             writableSheet.addCell(new Label(1, 0, "Full Name"));
    	             writableSheet.addCell(new Label(2, 0, "Email Id"));
    	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
    	             writableSheet.addCell(new Label(4, 0, "Shipping Address")); 
    	             writableSheet.addCell(new Label(5, 0, "Payment Mode")); 
    	             writableSheet.addCell(new Label(6, 0, "Payment Date")); 
    	             writableSheet.addCell(new Label(7, 0, "Payment Amount")); 
    	             writableSheet.addCell(new Label(8, 0, "VAT")); 
    	             writableSheet.addCell(new Label(9, 0, "VAT Amount")); 
    	             
    	             int j = 1;
    	             
    	             for(int i=0;i<userBasicDetail.size();i++)
   	                 {  	
    	            	 writableSheet.addCell(new Number(0, j, j));
    	            	 writableSheet.addCell(new Label(1, j, userBasicDetail.get(i).getUser_name()));
    	            	 writableSheet.addCell(new Label(2, j, userBasicDetail.get(i).getUser_emailid()));
    	            	 writableSheet.addCell(new Label(3, j, userBasicDetail.get(i).getUser_mobile_no()));
    	            	 writableSheet.addCell(new Label(4, j, userBasicDetail.get(i).getUser_shipping_address()));
    	            	 writableSheet.addCell(new Label(5, j, tranObject.get(i).getTransaction_mode()));
    	            	 
    	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	            	 
    	            	 writableSheet.addCell(new Label(6, j, sdf.format(tranObject.get(i).getTransaction_date())));
    	            	 
    	            	 String tranAmount = tranObject.get(i).getTransaction_amount();
    	            	 
    	            	 int amount = Integer.parseInt(tranAmount.substring(0, tranAmount.lastIndexOf(".")));
    	            	 float vatAmout = (vat / 100 ) * amount;
    	            	 
    	            	 writableSheet.addCell(new Number(7, j, amount));
    	            	 writableSheet.addCell(new Number(8, j, vat));
    	            	 writableSheet.addCell(new Number(9, j, vatAmout));    	
    	            	 
    	            	 j = j + 1;
    	             }
    	                	             
    	             writableWorkbook.write();
    	             writableWorkbook.close();
    	  
	    	         } catch (IOException e) {
	    	             e.printStackTrace();
	    	         } catch (RowsExceededException e) {
	    	             e.printStackTrace();
	    	         } catch (WriteException e) {
	    	             e.printStackTrace();
	    	         }
    	    	   	    	
    	           // End of Report Generator 	    	    	     	    	 
    	    }
    	    else if(day>fromVATreportDate)
    	    {
    	    	
    	    }
    	    else
    	    {
    	    	
    	    }    	
    	        	    
    	    model.put("taxDetails",subAdminService.getAllTaxReport("allReport"));	
        	    	    
        	return new ModelAndView("accountTaxToGovt",model);		  	
        }  				
	}
	
	
	@RequestMapping(value = "/downloadVatReport", method = RequestMethod.GET)
	public String downloadVatReport(HttpSession session,HttpServletRequest request) {
	
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
		String vatTAXReportDirectory = fileResource.getString("vatTAXReportDirectory");		
		
		request.setAttribute("rootDirectory",vatTAXReportDirectory);
		request.setAttribute("reportName",request.getParameter("reportName"));
		
		return "downloadReport";			
	}
	
	
	@RequestMapping(value = "/payAmtToCustomer", method = RequestMethod.GET)
	public ModelAndView payAmtToCustomer(HttpSession session,HttpServletRequest request) {
						
		if (session.getAttribute("actAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {
        	List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		

    		float tds = 0;
    		float dailyReferAmount = 0;
    		float dailyReferBonusAmount = 0;
    		float dailyPolicyBonusAmount = 0;
    		
    		float totalAmountToPay = 0;
    		
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("TDS"))
				{
					tds = Float.parseFloat(costDetails.get(i).getCost_value());
				}
			}
				
			// Each Reference Amount
			
			List<Integer> userIdList = new ArrayList<Integer>();			
			List<UserReferenceDetailsModel> refAmount = subAdminService.getReferAmountDetails("DailyBasis");		
			
			for(int i=0;i<refAmount.size();i++)
			{
				float amount = refAmount.get(i).getReference_amount();
				float tdsCharge = (tds / 100) * amount;
				float finalAmount = amount - tdsCharge;
				
				dailyReferAmount += finalAmount;
			}
			
			for(int i=0;i<refAmount.size();i++)
	    	{
	    		userIdList.add(refAmount.get(i).getReference_given_user_id());
	    	}
	    	
	    	List<UserBasicInfoBean> userBasicDetails = subAdminService.getUserDetailsByUserId(userIdList);
	    	
	    	
			
			// Reference Bonus Amount 
	    	List<Integer> useIdforReferBonus = new ArrayList<Integer>();
	    	
			List<ReferBonusAmountModel> refBonusAmount = subAdminService.getReferBonusDetails("DailyBasis");		
			
			for(int i=0;i<refBonusAmount.size();i++)
			{
				float amount = refBonusAmount.get(i).getBonus_amount();
				float tdsCharge = (tds / 100) * amount;
				float finalAmount = amount - tdsCharge;
				
				dailyReferBonusAmount += finalAmount;
			}
			
			for(int i=0;i<refBonusAmount.size();i++)
	    	{
				useIdforReferBonus.add(refBonusAmount.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoBean> userBasicDetailForReferBonus = subAdminService.getUserDetailsByUserId(useIdforReferBonus);
	    	
	    	
	    	
	    	
			// Policy Bonus Amount 
	    	List<Integer> useIdforPolicyBonus = new ArrayList<Integer>();		
			List<PolicyAmountModel> policyAmount = subAdminService.getPolicyBonusDetails("DailyBasis");		
			
			for(int i=0;i<policyAmount.size();i++)
			{
				float amount = policyAmount.get(i).getPolicy_amount();
				float tdsCharge = (tds / 100) * amount;
				float finalAmount = amount - tdsCharge;
				
				dailyPolicyBonusAmount += finalAmount;
			}
			
			for(int i=0;i<policyAmount.size();i++)
	    	{
				useIdforPolicyBonus.add(policyAmount.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoBean> userBasicDetailForPolicyBonus = subAdminService.getUserDetailsByUserId(useIdforPolicyBonus);
	    	
	    	
	    	// Total Calculation 
	    	
	    	
			totalAmountToPay = dailyReferAmount + dailyReferBonusAmount + dailyPolicyBonusAmount;
			
			request.setAttribute("tds", tds);
			request.setAttribute("dailyReferAmount", dailyReferAmount);
			request.setAttribute("dailyReferBonusAmount", dailyReferBonusAmount);
			request.setAttribute("dailyPolicyBonusAmount", dailyPolicyBonusAmount);
			request.setAttribute("totalAmountToPay", totalAmountToPay);
			
			
			Map<String, Object> model = new HashMap<String, Object>();  		
    		
			model.put("refAmount",refAmount);
    		model.put("refBonusAmount",refBonusAmount);
    		model.put("policyAmount",policyAmount);
    		
    		
    		model.put("userBasicDetails",userBasicDetails);
    		model.put("userBasicDetailForReferBonus",userBasicDetailForReferBonus);
    		model.put("userBasicDetailForPolicyBonus",userBasicDetailForPolicyBonus);
    		
    		
        	return new ModelAndView("accountpayToCustomer",model);		  	
        }  				
	}
	
	
	@RequestMapping(value = "/TDSamountReceived", method = RequestMethod.GET)
	public ModelAndView TDSamountReceived(HttpSession session,HttpServletRequest request) {
				
		if(session.getAttribute("actAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {
        	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
    		int fromVATreportDate = Integer.parseInt(fileResource.getString("fromTDSReportDate"));	
    		int toVATreportDate = Integer.parseInt(fileResource.getString("toTDSReportDate"));	
    		  
        	List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		

    		float tds = 0;
    		
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("TDS"))
				{
					tds = Float.parseFloat(costDetails.get(i).getCost_value());
				}
			}
			
			List<UserReferenceDetailsModel> referenceDetails = superAdminService.getAllReferenceDetails("monthly");	
			
			List<ReferBonusAmountModel> referenceBonusDetails = subAdminService.getAllReferenceBonusDetails();	
			
			List<PolicyAmountModel> policyBonusDetails = subAdminService.getAllPolicyBonusDetails();	

			float totalTDSAmount = 0;
			
			// Calculate total TDS Amount of reference details 
			
			for(int i=0;i<referenceDetails.size();i++)
			{
				float tdsAmount = (tds/100) * referenceDetails.get(i).getReference_amount();
				totalTDSAmount += tdsAmount;
			}
			
			// Calculate total TDS Amount of reference bonus details 
			
			for(int i=0;i<referenceBonusDetails.size();i++)
			{
				float tdsAmount = (tds/100) * referenceBonusDetails.get(i).getBonus_amount();
				totalTDSAmount += tdsAmount;
			}
			
			// Calculate total TDS Amount of Policy bonus details 
			
			for(int i=0;i<policyBonusDetails.size();i++)
			{
				float tdsAmount = (tds/100) * policyBonusDetails.get(i).getPolicy_amount();
				totalTDSAmount += tdsAmount;
			}
				
			
			// user details for user reference 
			
			
			List<Integer> useIds = new ArrayList<Integer>();
	    	for(int i=0;i<referenceDetails.size();i++)
	    	{
	    		useIds.add(referenceDetails.get(i).getReference_given_user_id());
	    	}
	    	
	    	
	    	List<UserBasicInfoModel> userBasicDetail = subAdminService.getUserDetailModelByUserId(useIds);  // As per User Details 
	    	
	    	// End of user details for user
	    	
	    	
	    	// user details for user reference Bonus
			
			List<Integer> useIdsRB = new ArrayList<Integer>();
	    	for(int i=0;i<referenceBonusDetails.size();i++)
	    	{
	    		useIdsRB.add(referenceBonusDetails.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetailReferenceBonus = subAdminService.getUserDetailModelByUserId(useIdsRB);  // As per User Details 
	    	
	    	// End of user details for user reference Bonus
	    	
	    	
	    	// user details for user Policy Bonus 
			
			
			List<Integer> useIdsPB = new ArrayList<Integer>();
	    	for(int i=0;i<policyBonusDetails.size();i++)
	    	{
	    		useIdsPB.add(policyBonusDetails.get(i).getUser_id());
	    	}
	    	
	    	List<UserBasicInfoModel> userBasicDetailPolicyBonus = subAdminService.getUserDetailModelByUserId(useIdsPB);  // As per User Details 
	    	
	    	// End of user details for user Policy Bonus 
	    		    		    	
			request.setAttribute("tds", tds);	
    		request.setAttribute("totalTDS", totalTDSAmount);    
    		
    		Date today = new Date();
    		Calendar cal = Calendar.getInstance();
    	    cal.setTime(today);
    	    int year = cal.get(Calendar.YEAR);
    	    int month = cal.get(Calendar.MONTH);
    	    int currentMonth = month + 1;
    	    int day = cal.get(Calendar.DAY_OF_MONTH);
    	    
    	    int lastDay = day + 1;
    	    
    	    String reportName = "TDS Report_"+lastDay+"-"+month+" to "+day+"-"+currentMonth+" "+year; 
    	        			
			if(day==toVATreportDate)
    	    {
				TDSReportModel tdsReport =  new TDSReportModel();
				
    	    	int tdsId = subAdminService.getTodaysTDSId(reportName);
    	    	
    	    	if(tdsId!=0)
    	    	{
    	    		tdsReport.setTds_id(tdsId);
    	    	}
    	    	
    	    	tdsReport.setTds_month(reportName);
    			tdsReport.setTds_amount(totalTDSAmount);
    			tdsReport.setTds_report_name(reportName);
    			tdsReport.setTds_generated_date(new Date());
    			
    	    	subAdminService.saveTDSReportDetails(tdsReport);  
    	    	   	    	
    	    	// Report Generator
    	    	
    	    	String tdsReportDirectory = fileResource.getString("tdsReportDirectory");
    	    	
    	    	String fileDirectory = tdsReportDirectory;
    	    	   	    			
    	    	try 
    	    	{    	    		 
		             File exlDirectory = new File(fileDirectory);
		             if(!exlDirectory.exists())
		         	 {
		            	 exlDirectory.mkdirs();
		         	 }
		             
		             File excelReport = new File(fileDirectory + "/" + reportName+".xls");
		             
		             WritableWorkbook writableWorkbook = Workbook.createWorkbook(excelReport);
		             
		             WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
		        	 
		             //Add the created Cells to the heading of the sheet
		             
		             
		             // Start of reference details
		             
    	             writableSheet.addCell(new Label(0, 0, "Serial No"));
    	             writableSheet.addCell(new Label(1, 0, "Full Name"));
    	             writableSheet.addCell(new Label(2, 0, "Email Id"));
    	             writableSheet.addCell(new Label(3, 0, "Mobile No"));
    	             writableSheet.addCell(new Label(4, 0, "Address")); 
    	             writableSheet.addCell(new Label(5, 0, "Generated Date")); 
    	             writableSheet.addCell(new Label(6, 0, "Reference Amount")); 
    	             writableSheet.addCell(new Label(7, 0, "TDS")); 
    	             writableSheet.addCell(new Label(8, 0, "TDS Amount")); 
    	             
    	             int j = 1;
    	             
    	             
    	             for(int i=0;i<userBasicDetail.size();i++)
   	                 {  	
    	            	
    	            	 writableSheet.addCell(new Number(0, j, j));
    	            	 writableSheet.addCell(new Label(1, j, userBasicDetail.get(i).getUser_name()));
    	            	 writableSheet.addCell(new Label(2, j, userBasicDetail.get(i).getUser_emailid()));
    	            	 writableSheet.addCell(new Label(3, j, userBasicDetail.get(i).getUser_mobile_no()));
    	            	 writableSheet.addCell(new Label(4, j, userBasicDetail.get(i).getUser_shipping_address()));
    	            	 
    	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	            	 
    	            	 writableSheet.addCell(new Label(5, j, sdf.format(referenceDetails.get(i).getReference_accept_date())));
    	            	 
    	            	 int amount = referenceDetails.get(i).getReference_amount();
    	            	     	            	
    	            	 float tdsAmout = (tds / 100 ) * amount;
    	            	 
    	            	 writableSheet.addCell(new Number(6, j, amount));
    	            	 writableSheet.addCell(new Number(7, j, tds));
    	            	 writableSheet.addCell(new Number(8, j, tdsAmout));    	
    	            	 
    	            	 j = j + 1;
    	            	 
    	             }
    	             
    	             
    	            // End of reference details
    	             
    	             
    	            // Start to append reference Bonus Report
    	            
    	             
    	             int k = j + 2;
    	             
    	             writableSheet.addCell(new Label(0, k, "Serial No"));
    	             writableSheet.addCell(new Label(1, k, "Full Name"));
    	             writableSheet.addCell(new Label(2, k, "Email Id"));
    	             writableSheet.addCell(new Label(3, k, "Mobile No"));
    	             writableSheet.addCell(new Label(4, k, "Address")); 
    	             writableSheet.addCell(new Label(5, k, "Generated Date")); 
    	             writableSheet.addCell(new Label(6, k, "Reference Bonus Amount")); 
    	             writableSheet.addCell(new Label(7, k, "TDS")); 
    	             writableSheet.addCell(new Label(8, k, "TDS Amount")); 
    	             
    	             k = k + 1;
    	             
    	             int slNo = 1;
    	             for(int i=0;i<userBasicDetailReferenceBonus.size();i++)
   	                 {  	
    	            	 
    	            	 writableSheet.addCell(new Number(0, k, slNo));
    	            	 writableSheet.addCell(new Label(1, k, userBasicDetailReferenceBonus.get(i).getUser_name()));
    	            	 writableSheet.addCell(new Label(2, k, userBasicDetailReferenceBonus.get(i).getUser_emailid()));
    	            	 writableSheet.addCell(new Label(3, k, userBasicDetailReferenceBonus.get(i).getUser_mobile_no()));
    	            	 writableSheet.addCell(new Label(4, k, userBasicDetailReferenceBonus.get(i).getUser_shipping_address()));
    	            	 
    	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	            	 
    	            	 writableSheet.addCell(new Label(5, k, sdf.format(referenceBonusDetails.get(i).getUpdate_date())));
    	            	 
    	            	 int amount = referenceBonusDetails.get(i).getBonus_amount();
    	            	     	            	
    	            	 float tdsAmout = (tds / 100 ) * amount;
    	            	 
    	            	 writableSheet.addCell(new Number(6, k, amount));
    	            	 writableSheet.addCell(new Number(7, k, tds));
    	            	 writableSheet.addCell(new Number(8, k, tdsAmout));    	
    	            	 
    	            	 k = k + 1;
    	            	 slNo = slNo +1;
    	             }
    	             
    	             
    	             // End of reference Bonus Report
    	             
    	             
    	             // Start to append Policy Bonus Report
    	                	             
    	             int l = k + 2;
    	             
    	             writableSheet.addCell(new Label(0, l, "Serial No"));
    	             writableSheet.addCell(new Label(1, l, "Full Name"));
    	             writableSheet.addCell(new Label(2, l, "Email Id"));
    	             writableSheet.addCell(new Label(3, l, "Mobile No"));
    	             writableSheet.addCell(new Label(4, l, "Address"));  
    	             writableSheet.addCell(new Label(5, l, "Policy End Date")); 
    	             writableSheet.addCell(new Label(6, l, "Policy Amount")); 
    	             writableSheet.addCell(new Label(7, l, "TDS")); 
    	             writableSheet.addCell(new Label(8, l, "TDS Amount")); 
    	             
    	             l = l + 1;
    	             
    	             int slNo1 = 1;
    	             
    	             for(int i=0;i<userBasicDetailPolicyBonus.size();i++)
   	                 {  	
    	            	
    	            	 writableSheet.addCell(new Number(0, l, slNo1));
    	            	 writableSheet.addCell(new Label(1, l, userBasicDetailPolicyBonus.get(i).getUser_name()));
    	            	 writableSheet.addCell(new Label(2, l, userBasicDetailPolicyBonus.get(i).getUser_emailid()));
    	            	 writableSheet.addCell(new Label(3, l, userBasicDetailPolicyBonus.get(i).getUser_mobile_no()));
    	            	 writableSheet.addCell(new Label(4, l, userBasicDetailPolicyBonus.get(i).getUser_shipping_address()));
    	            	
    	            	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	            	 
    	            	 writableSheet.addCell(new Label(5, l, sdf.format(policyBonusDetails.get(i).getPolicy_end_date())));
    	            	 
    	            	 int amount = policyBonusDetails.get(i).getPolicy_amount();
    	            	     	            	
    	            	 float tdsAmout = (tds / 100 ) * amount;
    	            	 
    	            	 writableSheet.addCell(new Number(6, l, amount));
    	            	 writableSheet.addCell(new Number(7, l, tds));
    	            	 writableSheet.addCell(new Number(8, l, tdsAmout));    	
    	            	 
    	            	 l = l + 1;
    	            	 
    	            	 slNo1 = slNo1 + 1;
    	             }
    	             
    	             // End of Policy Bonus Report
    	             
    	             
    	             writableWorkbook.write();
    	             writableWorkbook.close();
    	  
	    	         } catch (IOException e) {
	    	             e.printStackTrace();
	    	         } catch (RowsExceededException e) {
	    	             e.printStackTrace();
	    	         } catch (WriteException e) {
	    	             e.printStackTrace();
	    	         }
    	    	   	    	
    	           // End of Report Generator 	    	    	      	    	 
    	    }
    	    else if(day>fromVATreportDate)
    	    {
    	    	
    	    }
    	    else
    	    {
    	    	
    	    }    	
			
			Map<String, Object> model = new HashMap<String, Object>();  		
			
			model.put("tdsDetails",subAdminService.getAllTdsReport("allReport"));	
        	 
        	return new ModelAndView("accountTDStoBusiness",model);		  	  
        }  				
	} 
	
	
	@RequestMapping(value = "/downloadTDSReport", method = RequestMethod.GET)
	public String downloadTDSReport(HttpSession session,HttpServletRequest request) {
			
		request.setAttribute("reportName",request.getParameter("reportName"));				
		return "downloadReport";			
	}
	
	
	
	
	@RequestMapping(value = "/accountReports", method = RequestMethod.GET)
	public ModelAndView accountReports(HttpSession session,HttpServletRequest request) {
		
		if (session.getAttribute("actAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {
            Map<String, Object> model = new HashMap<String, Object>();  		
			
            model.put("existReportDetails",subAdminService.getReportDetails("Monthly","Account Admin"));
			
        	return new ModelAndView("accountReportRecords",model);		  	
        }  				
	}
	
	
	
	@RequestMapping(value = "/generateAccountReport", method = RequestMethod.POST)
	public @ResponseBody String generateAccountReport(HttpSession session,HttpServletRequest request) {
		
		String reportStatus = "";
		
		String reportType = request.getParameter("reportType");
		String reportFor = request.getParameter("reportFor");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		
		reportStatus = subAdminService.createAccountReport(reportType,reportFor,fromDate,toDate,request);
		
		//String abst = subAdminService.createAccountReport(reportType, reportFor, fromDate, toDate, request);
		
		return reportStatus;
	}
	
	
	
	@RequestMapping(value = "/downloadAccountReport", method = RequestMethod.GET)
	public String  downloadCheckReport(HttpSession session,HttpServletRequest request) 
	{						
		String referCategory = request.getParameter("referCategory");
		
		if(referCategory.equals("Purchased Packet Transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("purchaseTransationReportDirectory");		
			
			request.setAttribute("rootDirectory",rootDirectory);
		}
		else if(referCategory.equals("Refer Friend Transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("referFriendReportDirectory");		
			
			request.setAttribute("rootDirectory",rootDirectory);
		}
		else if(referCategory.equals("Refer Friend Bonus Transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("referFriendBonusReportDirectory");		
			
			request.setAttribute("rootDirectory",rootDirectory);
		}
		else
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("userPlanPolicyReportDirectory");		
			
			request.setAttribute("rootDirectory",rootDirectory);
		}
		
        request.setAttribute("reportName",request.getParameter("reportName"));
		
		return "downloadReport";			
	}
	
	
	@RequestMapping(value = "/deleteAccountReport", method = RequestMethod.POST)
	public @ResponseBody String deleteCheckReport(HttpSession session,HttpServletRequest request) 
	{		
		String checkReportStatus	= "";
		
		String reportName = request.getParameter("reportName");
		String reportCategory = request.getParameter("ReportCategory");
		int reportId = Integer.parseInt(request.getParameter("reportId"));
				
		if(reportCategory.equals("Purchased Packet Transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("purchaseTransationReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(rootDirectory,reportName,reportId,request);					
		}
		else if(reportCategory.equals("Refer Friend Transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("referFriendReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(rootDirectory,reportName,reportId,request);
		}
		else if(reportCategory.equals("Refer Friend Bonus Transaction"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("referFriendBonusReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(rootDirectory,reportName,reportId,request);
		}
		else
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			String rootDirectory = fileResource.getString("userPlanPolicyReportDirectory");		
			
			checkReportStatus = subAdminService.deleteReports(rootDirectory,reportName,reportId,request);
		}
				
		return checkReportStatus;		
	}
		
	public int result()
	{
		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();
		return costDetails.size();
	}
			
}
