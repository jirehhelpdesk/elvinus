package com.biosys.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;








import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.biosys.bean.ProductDetailsBean;
import com.biosys.domain.AdminRegistrationModel;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.PlanDetailsModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.service.SubAdminService;
import com.biosys.service.SuperAdminService;
import com.biosys.util.AccessControl;
import com.biosys.util.PasswordGenerator;

import org.apache.commons.lang.StringUtils;

@Controller
public class SuperAdminController {

	@Autowired
	private SuperAdminService superAdminService;
	
	
	@Autowired
	private SubAdminService subAdminService;
	
	
	@RequestMapping(value = "/superAuthurl", method = RequestMethod.GET)
	public String showHome(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		return "superAdminLogin";				
	}
	
	
	@RequestMapping(value = "/general-error", method = RequestMethod.GET)
	public String generalError(HttpSession session,HttpServletRequest request) 
	{			
		session = request.getSession(false);			
		return "ErrorPage";
	}
	
	
	
	@RequestMapping(value = "/adminLogin", method = RequestMethod.GET)
	public String getAdminLogin(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		return "subAdminLogin";				
	}
	
	
	@RequestMapping(value = "/supAdminLogOut", method = RequestMethod.GET)
	public String supAdminLogOut(HttpSession session,HttpServletRequest request) {
				
		session.removeAttribute("supAdminId");			 
		request.setAttribute("actionMessage", "success");
        return "superAdminLogin";				
	}
	
	@RequestMapping(value = "/authSupadmin", method = RequestMethod.GET)
	public String getValidateSuperAdminWithGet(HttpSession session,HttpServletRequest request) {
				
        
		if (session.getAttribute("supAdminId") == null)
		{									
        	return "superAdminLogin";
        }
		if (session.getAttribute("supAdminId") == null)
		{									
        	return "superAdminLogin";
        }
		
		
		return "superAdminLogin";					
	}

	
	@RequestMapping(value = "/authSupadmin", method = RequestMethod.POST)
	public String getValidateSuperAdmin(HttpSession session,HttpServletRequest request) {
				
		String uid = request.getParameter("adminid");
        String pwd = request.getParameter("adminPassword");
       
        String authenticateStatus = superAdminService.getValidateSuperAdmin(uid,pwd);
        if(authenticateStatus.equals("success"))
        {
        	session.setAttribute("supAdminId", uid);
        	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
        	return "supMyAdmin";	
        }
        else
        {
        	request.setAttribute("actionMessage", "failed");
        	return "superAdminLogin";
        }					
	}
	
	
	@RequestMapping(value = "/authSubAdminLogin", method = RequestMethod.POST)
	public String getAuthSubAdminLoginn(HttpSession session,HttpServletRequest request) {
				
		String admincategory = request.getParameter("admincategory");
		String uid = request.getParameter("adminid");
        String pwd = request.getParameter("adminPassword");
        
        String authenticateStatus = subAdminService.getValidateSubAdmin(admincategory,uid,pwd);
        if(authenticateStatus.equals("success"))
        {        	
        	if(admincategory.equals("Account"))
        	{
        		session.setAttribute("actAdminId", uid);
            	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
            	
        		return "redirect:/accountAdmin.html";	
        	}
        	else if(admincategory.equals("Product"))
        	{
        		session.setAttribute("proAdminId", uid);
            	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
            	
        		return "redirect:/productAdmin.html";	
        	}
        	else
        	{
        		session.setAttribute("cheAdminId", uid);
            	session.setAttribute("USER_SESSION_ID", "JIR"+session.getId()+"K");
            	
        		return "redirect:/checkAdmin.html";	
        	}         	
        }
        else
        {
        	request.setAttribute("actionMessage", "failed");
        	return "subAdminLogin";
        }					
	}
	
	
	@RequestMapping(value = "/checkThesession", method = RequestMethod.GET)
	public String checkThesession(HttpSession session,HttpServletRequest request) {		
		
		if (session.getAttribute("supAdminId") != null)
		{									
			return "supMyAdmin";
        }
		else if (session.getAttribute("actAdminId") != null)
		{									
			return "redirect:/accountAdmin.html";	
        }
		else if (session.getAttribute("proAdminId") != null)
		{									
			return "redirect:/productAdmin.html";
        }
		else if(session.getAttribute("cheAdminId") != null)
		{
			return "redirect:/checkAdmin.html";	
		}
		else if(session.getAttribute("userId") != null)
		{
			return "redirect:/index.html";
		}	
		else
		{
			return "redirect:/index.html";
		}		
	}
	
	
	@RequestMapping(value = "/myAdmin", method = RequestMethod.GET)
	public String myAdmin(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		
		if (session.getAttribute("supAdminId") == null)
		{	 				
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";	
		}
		else
		{	
		     return "supMyAdmin";		
		}
	}
	
	
	@RequestMapping(value = "/myProduct", method = RequestMethod.GET)
	public String myProduct(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		
		if (session.getAttribute("supAdminId") == null)
		{					
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";	
		}
		else
		{						   
		    return "superMyProduct";		
		}
	}
	
	
	@RequestMapping(value = "/myvendor", method = RequestMethod.GET)
	public String myVendor(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		
		if (session.getAttribute("supAdminId") == null)
		{					
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";	
		}
		else
		{	
			
		    return "supMyVendor";		
		}
	}
	
	@RequestMapping(value = "/costmanage", method = RequestMethod.GET)
	public ModelAndView costManage(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		
		if (session.getAttribute("supAdminId") == null)
		{					
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return new ModelAndView("superAdminLogin");
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return new ModelAndView("superAdminLogin");	
		}
		else
		{	
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("costDetails",superAdminService.getAllCostDetails());
			
		    return new ModelAndView("supCostManage",model);		
		}
		
	}
	
	
	
	
	@RequestMapping(value = "/planmanage", method = RequestMethod.GET)
	public ModelAndView planManage(HttpSession session,HttpServletRequest request) 
	{				
		request.setAttribute("actionMessage", "notRequired");
		
		if (session.getAttribute("supAdminId") == null)
		{					
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return new ModelAndView("superAdminLogin");
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return new ModelAndView("superAdminLogin");	
		}
		else
		{	
			Map<String, Object> model = new HashMap<String, Object>();			
			//model.put("planDetails",superAdminService.getPlandetails());
			
			model.put("businessPlan",superAdminService.getBusinessPlandetails());
			model.put("planFeatures",superAdminService.getBusinessPlanFeatures());
			
		    //return new ModelAndView("supPlanManager",model);	
		    return new ModelAndView("CopyofsupPlanManager",model);	
		}
	}
	
	
	
	@RequestMapping(value = "/supadminsettings", method = RequestMethod.GET)
	public ModelAndView supAdminSettings(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		
		if (session.getAttribute("supAdminId") == null)
		{					
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return new ModelAndView("superAdminLogin");
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return new ModelAndView("superAdminLogin");	
		}
		else
		{	
            Map<String, Object> model = new HashMap<String, Object>();			
			model.put("emailDetails",superAdminService.getEmailSentDetails());
			
		    return new ModelAndView("supSettings",model);		
		}
	}
	
	
	@RequestMapping(value = "/viewVendorprofile", method = RequestMethod.GET)
	public String viewVendorprofile(HttpSession session,HttpServletRequest request) {
				
		request.setAttribute("actionMessage", "notRquired");
		
		if (session.getAttribute("supAdminId") == null)
		{					
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";
		}
		else if(((String)session.getAttribute("supAdminId")).equals(null))
		{			
			 session.removeAttribute("Ind_id");			 
			 request.setAttribute("actionMessage", "failed");
	         return "superAdminLogin";	
		}
		else
		{	
		    return "supViewMyVendor";		
		}
	}
	
	
	// Creating admin through Ajax Request.
	
	
	@RequestMapping(value = "/createBusinessAdmin", method = RequestMethod.POST)
	@ResponseBody public String createBusinessAdmin(HttpSession session,HttpServletRequest request) {
				
		AdminRegistrationModel adminModel = new AdminRegistrationModel();
		
		adminModel.setAdmin_type(request.getParameter("admincategory"));
		adminModel.setAdmin_full_name(request.getParameter("fullName"));
		adminModel.setAdmin_email_id(request.getParameter("emailId"));
		adminModel.setAdmin_mobile_no(request.getParameter("contactNo"));
		
		PasswordGenerator cryptoUtil=new PasswordGenerator();
		String encryptPassword = "";
		
		String password = request.getParameter("password");
		
		try 
		{
			encryptPassword = cryptoUtil.encrypt(request.getParameter("password"));
		} 
		catch (InvalidKeyException | NoSuchAlgorithmException
				| InvalidKeySpecException | NoSuchPaddingException
				| InvalidAlgorithmParameterException
				| UnsupportedEncodingException | IllegalBlockSizeException
				| BadPaddingException e)
		{
			e.printStackTrace();
		}
		
		adminModel.setAdmin_password(encryptPassword);		
		adminModel.setAdmin_cr_date(new Date());
				
		String adminStatus = superAdminService.saveAdminDetails(adminModel,password);
				
		return adminStatus;
	
	}                      
		
	// List of  Admin admin details 
	
	@RequestMapping(value = "/manageBusinessAdmin", method = RequestMethod.POST)
	public ModelAndView manageBusinessAdmin(HttpSession session,HttpServletRequest request) {
		
		Map<String, Object> model = new HashMap<String, Object>();
				
		model.put("adminDetails",superAdminService.getAllAdminDetails());
		
		return new ModelAndView("superManageAdmin",model);
	}
	
	
	// Remove admin from the business
	
	@RequestMapping(value = "/removeBusinessAdmin", method = RequestMethod.POST)
	@ResponseBody public String removeBusinessAdmin(HttpSession session,HttpServletRequest request) {
		
		int adminId = Integer.parseInt(request.getParameter("adminId"));
		String removeStatus = superAdminService.removeAdminById(adminId);
		return removeStatus;
	}
	
	
	
	// Enter new Products in the business
	
	
	@RequestMapping(value="/saveProductDetails", method=RequestMethod.POST)
	@ResponseBody public String saveProductDetails(HttpServletRequest request,HttpSession session,
 			@RequestParam CommonsMultipartFile[] product_image_file_name) throws Exception {
 		 
		
	   // Creating unique id of each product 
		
		   Date date = new Date(); // your date
	       Calendar cal = Calendar.getInstance();
	       cal.setTime(date);
	       int year = cal.get(Calendar.YEAR);
	       int month = cal.get(Calendar.MONTH);
	       int day = cal.get(Calendar.DAY_OF_MONTH);
	       int currentTime = cal.get(Calendar.MINUTE);
	      			     	       					     	       
	       int lastInvoiceNumber = superAdminService.getLastProductId();
	       int randomNumber = lastInvoiceNumber+1;
	       
	       String uniqueProductId = "PRO"+year+month+day+currentTime+randomNumber;
	    
       // Creating unique id of each product    
	       
 		String docName = "";
 		
 		String finName = request.getParameter("product_name");
 			
 	    ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
 	    String productFilePath = "";
 	    
 	    if(request.getParameter("product_category").equals("Agriculture"))
 	    {
 	    	productFilePath += fileResource.getString("agricultureProdcutFile");
 	    }
 	    else if(request.getParameter("product_category").equals("Herbal"))
	    {
 	    	productFilePath += fileResource.getString("herbalProdcutFile");
	    }
 	    else
 	    {
 	    	productFilePath += fileResource.getString("homecareProdcutFile");
 	    }
 	    
 			 		
 	    File ourPath = new File(productFilePath);
 	    String storePath = ourPath.getPath();
 	    
 	    
 	    // Create directory as per the category and 
 	    
 	    // upload the file as per the directory 
 	    
    		File fld = new File(storePath);
        	if(!fld.exists())
        	{
    		    fld.mkdirs();
        	}
    		
    			     		 
    		if (product_image_file_name != null && product_image_file_name.length > 0)
            {
                for (CommonsMultipartFile aFile : product_image_file_name)            
                {                  	
                    if (!aFile.getOriginalFilename().equals(""))                 
                    {       
                        aFile.transferTo(new File(storePath + "/" + aFile.getOriginalFilename()));  
                        docName = aFile.getOriginalFilename();
                    }
                }
            }  
			    	
    		ProductDetailsModel productDetails = new ProductDetailsModel();
    		productDetails.setProduct_unique_id(uniqueProductId);
    		productDetails.setProduct_category(request.getParameter("product_category"));
    		productDetails.setProduct_name(request.getParameter("product_name"));
    		productDetails.setProduct_price(request.getParameter("product_price"));
    		productDetails.setProduct_description(request.getParameter("product_description"));
    		productDetails.setProduct_image_file_name(docName);
    		productDetails.setProduct_cr_date(new Date());
    		
    		
    		if ("success".equals(superAdminService.saveProductDetails(productDetails))) {
                
    			return "success";
    			
    		} else {

    			return "failure";
    		}   
			    		   				
 	}
	
	//  Product Update block
	
	@RequestMapping(value="/updateProductDetails", method=RequestMethod.POST)
	@ResponseBody public String updateProductDetails(HttpServletRequest request,HttpSession session,
 			@RequestParam CommonsMultipartFile[] upd_product_image_file_name) throws Exception {
 		 
		
		String uniqueProductId = request.getParameter("product_uni_id");
		int product_id = Integer.parseInt(request.getParameter("product_id"));		
		String existFile = request.getParameter("existfileName");
		
		String docName = "";
		String productFilePath = "";
				
	 	    ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
	 	   
	 	    
	 	    if(request.getParameter("upd_product_category").equals("Agriculture"))
	 	    {
	 	    	productFilePath += fileResource.getString("agricultureProdcutFile");
	 	    }
	 	    else if(request.getParameter("upd_product_category").equals("Herbal"))
		    {
	 	    	productFilePath += fileResource.getString("herbalProdcutFile");
		    }
	 	    else
	 	    {
	 	    	productFilePath += fileResource.getString("homecareProdcutFile");
	 	    }
	 		 		
	 	    File ourPath = new File(productFilePath);
	 	    String storePath = ourPath.getPath();
	 	    
	 	    
	 	    // Create directory as per the category and 
	 	    
	 	    // upload the file as per the directory 
	 	   
	    		File fld = new File(storePath);
	        	if(!fld.exists())
	        	{
	    		    fld.mkdirs();
	        	}
	    		
	    			     		 
	    		if (upd_product_image_file_name != null && upd_product_image_file_name.length > 0)
	            {
	                for (CommonsMultipartFile aFile : upd_product_image_file_name)            
	                {                    	
	                    if (!aFile.getOriginalFilename().equals(""))                 
	                    {                          	
	                        aFile.transferTo(new File(storePath + "/" + aFile.getOriginalFilename()));  
	                        docName = aFile.getOriginalFilename();
	                    }
	                }
	                
	                /*
	                File deleteFileDir = new File(storePath+"/"+existFile);
	                boolean a = deleteFileDir.delete();
	                System.out.println("Delete Status="+a+"==="+deleteFileDir.toString());*/
	            }  
					
    		ProductDetailsModel productDetails = new ProductDetailsModel();
    		productDetails.setProduct_id(product_id);
    		productDetails.setProduct_unique_id(uniqueProductId);
    		productDetails.setProduct_category(request.getParameter("upd_product_category"));
    		productDetails.setProduct_name(request.getParameter("upd_product_name"));
    		productDetails.setProduct_price(request.getParameter("upd_product_price"));
    		productDetails.setProduct_description(request.getParameter("upd_product_description"));
    		if(!docName.equals(""))
    		{
    			productDetails.setProduct_image_file_name(docName);
    		}
    		else
    		{
    			productDetails.setProduct_image_file_name(existFile);
    		}
    		
    		productDetails.setProduct_cr_date(new Date());
    		
    		
    		if ("success".equals(superAdminService.updateProductDetails(productDetails))) {
                
    			return "success";
    			
    		} else {

    			return "failure";
    		}   
			    		   				
 	}
	
	
	
	// List of  products in the business
	
	
		@RequestMapping(value = "/getProducts", method = RequestMethod.POST)
		public ModelAndView getProducts(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
					
			model.put("productDetails",superAdminService.getAllProductDetailsDetails());
			
			return new ModelAndView("supManageProduct",model);
		}
		
		
		// Search Products 
		
				
		@RequestMapping(value = "/searchProducts", method = RequestMethod.POST)
		public ModelAndView searchProducts(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			String searchType = request.getParameter("type");
			String searchValue = request.getParameter("value");
			
			model.put("productDetails",superAdminService.getAllProductDetailsByCriteria(searchValue,searchType));
			
			return new ModelAndView("supManageProduct",model);
		}
		
		
		// edit Products details from the business
		
	
		@RequestMapping(value = "/editProducts", method = RequestMethod.POST)
        public ModelAndView editProducts(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			String searchType = "Product Auto Id";
			String searchValue = request.getParameter("productId");
			
			model.put("productDetails",superAdminService.getAllProductDetailsByCriteria(searchValue,searchType));
			
			return new ModelAndView("supEditProduct",model);
		}
		
		
		// Remove Products from the business
		
		@RequestMapping(value = "/removeProducts", method = RequestMethod.POST)
		@ResponseBody public String removeProducts(HttpSession session,HttpServletRequest request) {
			
			int productId = Integer.parseInt(request.getParameter("productId"));
			String removeStatus = superAdminService.removeProductById(productId);
			return removeStatus;		
		}
		
		
		
		@SuppressWarnings({ "resource", "unused" })
		@RequestMapping(value = "/previewProducts", method = RequestMethod.GET)
		@ResponseBody public byte[] previewProducts(HttpServletRequest request,HttpSession session) throws IOException {
			
			
			String productType = request.getParameter("productType");
			String fileName = request.getParameter("fileName");
			
			// Required file Config for entire Controller 
			
			 ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
			 
			 String fileDir = "";
			 if(productType.equals("Agriculture"))
			 {
				 fileDir = fileResource.getString("agricultureProdcutFile");		
			 }
			 else if(productType.equals("Herbal"))
			 {
				 fileDir = fileResource.getString("herbalProdcutFile");	
			 }
			 else
			 {
				 fileDir = fileResource.getString("homecareProdcutFile");	
			 }
			 
			 
			// End of Required file Config for entire Controller 
			 
			ServletContext servletContext = request.getSession().getServletContext();
			
			File ourPath = new File(fileDir);
			
			StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
			String docCategory = fPath.toString();
			
			if(StringUtils.isNotBlank(docCategory))
			{				
				/* Checks that files are exist or not  */
				
					File exeFile=new File(fPath.toString());
					if(!exeFile.exists())
					{
						fPath.delete(0, fPath.length());
						File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
						fPath.append(targetFile.getPath());
					}	
				/* Checks that files are exist or not  */
					
			}
			
			InputStream in;
			
			try {
				
				if (AccessControl.isValidSession(request)) {
						
					FileInputStream docdir = new FileInputStream(fPath.toString());													
					if (docdir != null) 
					{						
						return IOUtils.toByteArray(docdir);					
					} 
					else 
					{							
						return null;
					}
				} 
				else {
					
					in = servletContext.getResourceAsStream("/resources/images/Access_Denied.jpg");
					if (in != null) 
					{
						return IOUtils.toByteArray(in);
					} else
					{
						return null;
					}
				}
			} 
			catch (FileNotFoundException e) {
				
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
			} catch (IOException e) {
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
			}

		}
		
		
		@RequestMapping(value = "/saveCostDetails", method = RequestMethod.POST)
		@ResponseBody public String saveCostProducts(HttpSession session,HttpServletRequest request) {
			
			String costName = request.getParameter("costName");
			
			CostManageModel costModel = new CostManageModel();
			
			int cost_id = superAdminService.getCostExistOrNot(costName);
			
			if(cost_id!=0)
			{
				costModel.setCost_id(cost_id);
			}			
			costModel.setCost_name(costName);
			costModel.setCost_value(request.getParameter("costValue"));
			costModel.setCost_cr_date(new Date());
			
			String saveStatus = superAdminService.saveCostValues(costModel);
			
			return saveStatus;		
		}
		
		
		// Search vendor by the criteria 
		
		@RequestMapping(value = "/searchVendor", method = RequestMethod.POST)
        public ModelAndView searchVendor(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			String searchType = request.getParameter("searchType");
			String searchValue = request.getParameter("searchValue");
			
			model.put("vendorDetails",superAdminService.getVendorBasicDetails(searchValue,searchType));
			
			return new ModelAndView("supVendorList",model);
		}
		
		
		// get all the details of vendor and display
		
		
		@RequestMapping(value = "/showAllvendorDetails", method = RequestMethod.POST)
        public ModelAndView showAllvendorDetails(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			int userId = Integer.parseInt(request.getParameter("vendorId"));
			
			model.put("vendorOtherDetails",superAdminService.getVendorOtherDetailsById(userId));
			model.put("vendorBasicDetails",superAdminService.getVendorBasicInfoDetailsById(userId));
			
			request.setAttribute("userId", userId);
			return new ModelAndView("supViewMyVendor",model);
		}
						
		// get SMS details report in setting bar
		
		@RequestMapping(value = "/smsStatus", method = RequestMethod.POST)
        public ModelAndView smsStatus(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("smsDetails",superAdminService.getSMSDetails());
			
			return new ModelAndView("supSmsStatus",model);
		}
		
		
		// Save Business Plan details
		
		
		@RequestMapping(value = "/saveBusinessPlanDetails", method = RequestMethod.POST)
		@ResponseBody public String saveBusinessPlanDetails(HttpSession session,HttpServletRequest request) {
			
			String saveStatus = "";
		    
			BusinessPlanModel planModel = new BusinessPlanModel();
			
			planModel.setPlan_id(Integer.parseInt(request.getParameter("plan_id")));
			planModel.setPlan_name(request.getParameter("planName"));
			planModel.setPlan_policy_amount(Integer.parseInt(request.getParameter("planVal1")));
			planModel.setPlan_amount_refer_per_friend(Integer.parseInt(request.getParameter("planVal2")));
			
			saveStatus = superAdminService.saveBusinessPlanDetails(planModel);
			
			return saveStatus;
		}
		
		// End of save Business Plan Details
		
		
		// Save pLan feature details
		

		@RequestMapping(value = "/saveBusinessPlanfeature", method = RequestMethod.POST)
		@ResponseBody public String saveBusinessPlanfeature(HttpSession session,HttpServletRequest request) {
			
			String saveStatus = "";
		    
			BusinessPlanFeatureModel planFeature = new BusinessPlanFeatureModel();
			
			planFeature.setPlan_id(Integer.parseInt(request.getParameter("plan_id")));
			planFeature.setFeature_restrict_day(Integer.parseInt(request.getParameter("noofDays")));
			planFeature.setFeature_restrict_friend(Integer.parseInt(request.getParameter("noofFriend")));
			planFeature.setFeature_restrict_bonus_amount(Integer.parseInt(request.getParameter("bonusAmount")));
			planFeature.setFeature_restrict_compliment_amount(Integer.parseInt(request.getParameter("compAmount")));
			
			saveStatus = superAdminService.saveBusinessPlanFeatures(planFeature);
			
			return saveStatus;
		}
		
		
		@RequestMapping(value = "/deleteFeature", method = RequestMethod.POST)
		public @ResponseBody  String deleteFeature(HttpSession session,HttpServletRequest request) {
					
			String deleteStatus = "";
			
			int featureId = Integer.parseInt(request.getParameter("featureId"));
			
			deleteStatus=superAdminService.deleteFeature(featureId);
			
			return deleteStatus;
		}
		
		
		// End of Save business feature details
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Save Plan details
		
		
		@RequestMapping(value = "/savePlanDetails", method = RequestMethod.POST)
		@ResponseBody public String savePlanDetails(HttpSession session,HttpServletRequest request) {
			
			String saveStatus = "";
			
			PlanDetailsModel planModel = new PlanDetailsModel();
			
			int planStatus = superAdminService.getStatusPlanExistOrNot(request.getParameter("planName"));
			
			if(planStatus!=0)
			{
				planModel.setPlan_id(planStatus);
			}
			
			planModel.setPlan_name(request.getParameter("planName"));
			planModel.setBonus_amt_after_five_year(Integer.parseInt(request.getParameter("planVal1")));
			planModel.setBonus_amt_per_friend(Integer.parseInt(request.getParameter("planVal2")));
		
			if(!request.getParameter("planVal3").equals(""))
			{
				planModel.setBonus_amt_after_25_days(Integer.parseInt(request.getParameter("planVal3")));
			}
			
			if(!request.getParameter("planVal4").equals(""))
			{
				planModel.setBonus_amt_within_25_days(Integer.parseInt(request.getParameter("planVal4")));			
			}
			
			if(!request.getParameter("planVal5").equals(""))
			{
				planModel.setBonus_amt_after_50_days(Integer.parseInt(request.getParameter("planVal5")));
			}
			
			if(!request.getParameter("planVal6").equals(""))
			{
				planModel.setBonus_amt_within_50_days(Integer.parseInt(request.getParameter("planVal6")));
			}
			
			if(!request.getParameter("planVal7").equals(""))
			{
				planModel.setBonus_amt_after_75_days(Integer.parseInt(request.getParameter("planVal7")));
			}
			
			if(!request.getParameter("planVal8").equals(""))
			{
				planModel.setBonus_amt_within_75_days(Integer.parseInt(request.getParameter("planVal8")));
			}
			
			if(!request.getParameter("planVal9").equals(""))
			{
				planModel.setBonus_amt_within_100_days(Integer.parseInt(request.getParameter("planVal9")));
			}
			
			if(!request.getParameter("planVal10").equals(""))
			{
				planModel.setBonus_amt_after_100_days(Integer.parseInt(request.getParameter("planVal10")));
			}
			
			if(!request.getParameter("planVal11").equals(""))
			{
				planModel.setAdditional_bonus(Integer.parseInt(request.getParameter("planVal11")));
			}
			
			saveStatus = superAdminService.savePlanDetails(planModel);

			return saveStatus;		
		}
				
		
		//  show plan B details
		
		@RequestMapping(value = "/showPlanB", method = RequestMethod.POST)
        public ModelAndView showPlanB(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("businessPlan",superAdminService.getBusinessPlandetails());
			model.put("planFeatures",superAdminService.getBusinessPlanFeatures());
			
			return new ModelAndView("supPlanBManage",model);
		}
			
		
	    //  show plan C details
		
		@RequestMapping(value = "/showPlanC", method = RequestMethod.POST)
        public ModelAndView showPlanC(HttpSession session,HttpServletRequest request) {
			
			Map<String, Object> model = new HashMap<String, Object>();
			
			model.put("businessPlan",superAdminService.getBusinessPlandetails());
			model.put("planFeatures",superAdminService.getBusinessPlanFeatures());
			
			return new ModelAndView("supPlanCManage",model);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// Business Status Records		
		
		
		@RequestMapping(value = "/businessStatus", method = RequestMethod.GET)
        public ModelAndView businessStatus(HttpSession session,HttpServletRequest request) {
				   
			request.setAttribute("actionMessage", "notRquired");
			
			if (session.getAttribute("supAdminId") == null)
			{					
				 session.removeAttribute("Ind_id");			 
				 request.setAttribute("actionMessage", "failed");
		         return new ModelAndView("superAdminLogin");
			}
			else if(((String)session.getAttribute("supAdminId")).equals(null))
			{			
				 session.removeAttribute("Ind_id");			 
				 request.setAttribute("actionMessage", "failed");
		         return new ModelAndView("superAdminLogin");	
			}
			else
			{					
				long noOfUsers = superAdminService.getBusinessStatus("totalUsers");
				long noOfOrders = superAdminService.getBusinessStatus("totalOrders");
												
				request.setAttribute("noOfUsers", noOfUsers);
				request.setAttribute("noOfOrders", noOfOrders);
				
				return new ModelAndView("supBusinessStatus");	
			}			
		}
		
		
		
		// End of Business Status Records
		
		
		
		
		
		
		
		
		
		
		// Preview Profile Photo
		
		
		@SuppressWarnings({ "resource", "unused" })
		@RequestMapping(value = "/previewProfilePictureForAdmin", method = RequestMethod.GET)
		@ResponseBody public byte[] previewProfilePictureForAdmin(HttpServletRequest request,HttpSession session) throws IOException {
			
			
			String fileName = request.getParameter("fileName");
			
			// Required file Config for entire Controller 
			
			 ResourceBundle fileResource = ResourceBundle.getBundle("resources/BiosysVersion");
			 
			 String fileDir = fileResource.getString("profileDirectory")+"/"+request.getParameter("userId");
			 
			// End of Required file Config for entire Controller 
			 
			ServletContext servletContext = request.getSession().getServletContext();
			
			File ourPath = new File(fileDir);
			
			StringBuilder fPath = new StringBuilder(ourPath.getPath()+"/"+fileName);
			String docCategory = fPath.toString();
			
			if(StringUtils.isNotBlank(docCategory))
			{				
				/* Checks that files are exist or not  */
				
					File exeFile=new File(fPath.toString());
					if(!exeFile.exists())
					{
						fPath.delete(0, fPath.length());
						File targetFile = new File(request.getServletContext().getRealPath("/Back_End_Files/image-not-found.png"));					
						fPath.append(targetFile.getPath());
					}	
					
				/* Checks that files are exist or not  */
					
			}
			
			InputStream in;
			
			try {					
					FileInputStream docdir = new FileInputStream(fPath.toString());													
					if (docdir != null) 
					{						
						return IOUtils.toByteArray(docdir);					
					} 
					else 
					{							
						return null;
					}
				
			} 
			catch (FileNotFoundException e) {
				
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
			} catch (IOException e) {
				System.out.println(e.getMessage());
				return null; // todo: return safe photo instead
			}

		}
		
}
