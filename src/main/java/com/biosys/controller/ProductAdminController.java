package com.biosys.controller;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.biosys.domain.AdminRegistrationModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.service.SubAdminService;
import com.biosys.service.SuperAdminService;
import com.biosys.util.PasswordGenerator;

@Controller
public class ProductAdminController {

	@Autowired
	private SuperAdminService superAdminService;
	
	
	@Autowired
	private SubAdminService subAdminService;
	
	@RequestMapping(value = "/proadminsignout", method = RequestMethod.GET)
	public String supAdminLogOut(HttpSession session,HttpServletRequest request) {
				
		session.removeAttribute("proAdminId");			 
		request.setAttribute("actionMessage", "success");
        return "subAdminLogin";				
	}
		
	
	@RequestMapping(value = "/productAdmin", method = RequestMethod.GET)
	public ModelAndView authSubAdminLogin(HttpSession session,HttpServletRequest request) {
				        
		if (session.getAttribute("proAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {
        	List<OrderDetailsModel> orderDetails = subAdminService.getOrderDetailsWithCriteria("duration");
        	Map<String, Object> model = new HashMap<String, Object>();
    		
    		model.put("orderDetails",orderDetails);	
        	return new ModelAndView("productAdmin",model);
        }						
	}			
	
		
	@RequestMapping(value = "/getorderdetails", method = RequestMethod.POST)
	public ModelAndView getorderdetails(HttpSession session,HttpServletRequest request) {
				
		String OrderId = request.getParameter("searchValue");
		String searchType = request.getParameter("searchType");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("orderDetails",subAdminService.getOrderDetails(OrderId,searchType,startDate,endDate));		
		return new ModelAndView("productOrderList",model);		
	 
	}
	
	
	@RequestMapping(value = "/getorderdetailsForUpdate", method = RequestMethod.POST)
	public ModelAndView gerorderdetailsForUpdate(HttpSession session,HttpServletRequest request) {
				
		String OrderId = request.getParameter("ordUnqId");
		String OrderType = request.getParameter("OrderType");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
				
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("orderDetails",subAdminService.getOrderDetails(OrderId,OrderType,startDate,endDate));		
		
		if(OrderId.length()>1)
		{
			List<UserProductInterestModel> userInterestProduct = subAdminService.getInterestProductDetails(OrderId);
			List<ProductDetailsModel> productDetails = 	subAdminService.getProductDetails(userInterestProduct);
			model.put("productDetails",productDetails);					
		}
		
		return new ModelAndView("productOrderUpdate",model);		
	}	
	
	
	@RequestMapping(value = "/updateOrderDetails", method = RequestMethod.POST)
	@ResponseBody public String updateOrderDetails(HttpSession session,HttpServletRequest request) {
		
        String orderTrack = "";		
        
		OrderDetailsModel ordModel = new OrderDetailsModel();
		ordModel.setOrder_id(Integer.parseInt(request.getParameter("updorder_id")));
		ordModel.setOrder_status(request.getParameter("ord_status"));
		ordModel.setOrder_status_details(request.getParameter("order_description"));
		ordModel.setOrder_expected_date(request.getParameter("exp_date"));		
		
		orderTrack = subAdminService.getUpdateOrderDetails(ordModel);				
		
		return orderTrack;		
	}                      
	
	
	@RequestMapping(value = "/manageOrder", method = RequestMethod.POST)
	public String manageOrder(HttpSession session,HttpServletRequest request) {
				
		String orderTrack = "";		
			
		return orderTrack;			
	}
	
	
	@RequestMapping(value = "/getorderdetailsForPrint", method = RequestMethod.POST)
	public ModelAndView getorderdetailsForPrint(HttpSession session,HttpServletRequest request) {
				
		String OrderId = request.getParameter("ordUnqId");
		String OrderType = request.getParameter("OrderType");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
				
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("orderDetails",subAdminService.getOrderDetails(OrderId,OrderType,startDate,endDate));		
				
		List<CostManageModel> costDetails = superAdminService.getAllCostDetails();		
		
		float kitCharge = 0;
		float vat = 0;
		float regCharge = 0;
		
		float subtotal = 0;				
		float total = 0;
		
			for(int i=0;i<costDetails.size();i++)
			{
				if(costDetails.get(i).getCost_name().equals("Registration Charge"))
				{
					regCharge = Float.parseFloat(costDetails.get(i).getCost_value());
				}
				else if(costDetails.get(i).getCost_name().equals("Per Kit Cost"))
				{
					kitCharge = Float.parseFloat(costDetails.get(i).getCost_value());
				}
				else if(costDetails.get(i).getCost_name().equals("VAT"))
				{
					vat = Float.parseFloat(costDetails.get(i).getCost_value());
				}
			}
			
			float vatPersentageVal = (vat/100) * kitCharge;
			subtotal = kitCharge + vatPersentageVal;
			total = subtotal + regCharge; 
			
			request.setAttribute("kitCharge", kitCharge);
			request.setAttribute("vat", vat);
			request.setAttribute("vatPersentageVal", vatPersentageVal);
			request.setAttribute("subtotal", subtotal);
			request.setAttribute("regCharge", regCharge);
			request.setAttribute("total", total);
				
		    return new ModelAndView("productOrderPrint",model);		
	}
	
	
	@RequestMapping(value = "/orderReport", method = RequestMethod.GET)
	public ModelAndView orderReport(HttpSession session,HttpServletRequest request) {
				        
		if (session.getAttribute("proAdminId") == null)
		{									
        	return new ModelAndView("subAdminLogin");
        }
        else
        {              	
            Map<String, Object> model = new HashMap<String, Object>();	
        	
        	model.put("existReportDetails",subAdminService.getReportDetails("Monthly","Product Admin"));
        	 
        	return new ModelAndView("productAdminReport",model);
        }						
	}			
		
	
	@RequestMapping(value = "/generateOrderReport", method = RequestMethod.POST)
	public @ResponseBody String generateOrderReport(HttpSession session,HttpServletRequest request) {
				        
		String reportStatus = "";
		
		String reportType = request.getParameter("reportType");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
				
		reportStatus = subAdminService.createOrderReport(reportType,fromDate,toDate,request);
    	
		return reportStatus;
	}
	
	
	@RequestMapping(value = "/downloadOrderReport", method = RequestMethod.GET)
	public String  downloadCheckReport(HttpSession session,HttpServletRequest request) {
	
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
		String orderReportDirectory = fileResource.getString("orderReportDirectory");		
		
		request.setAttribute("rootDirectory",orderReportDirectory);
        
		request.setAttribute("reportName",request.getParameter("reportName"));
		
		return "downloadReport";	
	}
		
	
	@RequestMapping(value = "/deleteOrderReport", method = RequestMethod.POST)
	public @ResponseBody String deleteCheckReport(HttpSession session,HttpServletRequest request) {
	
		String checkReportStatus	= "";
			
		String reportName = request.getParameter("reportName");
		int reportId = Integer.parseInt(request.getParameter("reportId"));
		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
		String orderReportDirectory = fileResource.getString("orderReportDirectory");		
		
		checkReportStatus = subAdminService.deleteReports(orderReportDirectory,reportName,reportId,request);
		
		return checkReportStatus;
	}
	
	
	
}


