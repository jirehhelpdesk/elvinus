package com.biosys.dao;

import java.util.Date;
import java.util.List;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserProfileDetailsBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;

public interface userDao {

	public String validateUserEmailIdExistOrNot(int userId,String emailId);
	
	public List<UserBasicInfoBean> getUserBasicDetailsViaVendorId(String vendorId);
	
	public String getFullNameViaVenId(String vendorId);
	
	public int getUserIdViaVendorId(String vendorId);
	
	public List<UserProfileDetailsBean> getUserOtherProfileDetailsViaUserId(int userId);
	
	public List<OrderDetailsBean> getLastOrderDetailsViaUserId(int userId);
	
	public List<UserReferenceDetailsBean> GetReferenceDetailsViaUserId(int userId);
	
	public String saveUserOtherProfileDetails(UserProfileDetailsModel userOthModel);
	
	public int getUserProfileIdViaUserId(int userId);
	
	public String updateProfileImageInBasicInfo(int userId,String imageName);
	
	public String getProfilePhoto(int userId);
	
	public String getUpdateUserBasicInfo(UserBasicInfoModel basicInfo);
	
	public String getCurrentPassword(String vendorId);
	
	public String updatePassword(String encpwd,String vendorId);
	
	public List<OrderDetailsBean> getAllOrderDetails(int userId);
	
	public String getUserPlanViaUserId(int userId);
	
	public int getPolicyAmountAsPerPlan(String userPlan);
	
	public List<OrderDetailsModel> getUserOrderDetails(int userId);
	
	public String saveUserPolicyAmountDetails(PolicyAmountModel policyAmount);
	
	public List<PolicyAmountModel> getUserAllPolicyDetails(int userId);
	
	public List<UserReferenceDetailsModel> getReferDetailsWhoRefer(String emailId);
	
	public int getReferAmountAsPerPlan(String userPlan);
	
	public String getUpdateUserReferStatus(int referId,int referAmount);
	
	public List<UserReferenceDetailsModel> getUserReferDetails(int userId);
	
	public List<UserReferenceDetailsModel> getReferDetailViaReferId(int referenceId);
	
	public String getUpdateUserRegDate(int regUserId);	
	
	public List<UserReferenceDetailsModel> getNoOfUserReferJoined(int userId);
	
	public int getPlanIdViaPlanName(String userPlan);
	
	public List<BusinessPlanFeatureModel> getAllPlanFeature(int plan_Id);
	
	public Date getUserRegDate(int userId);
	
	public int checkPlanBonusExistOrNot(int noOfPlanDays);
	
	public String saveUserPlanBonus(ReferBonusAmountModel planBonus);
	
	public List<ReferBonusAmountModel> getReferalBonusDetails(int userId);
	
	
}
