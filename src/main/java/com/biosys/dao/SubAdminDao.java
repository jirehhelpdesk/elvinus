package com.biosys.dao;

import java.util.List;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.ProductDetailsBean;
import com.biosys.bean.TransactionDetailBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.domain.AdminRegistrationModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.ReportDetailsModel;
import com.biosys.domain.TDSReportModel;
import com.biosys.domain.TaxReportModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;

public interface SubAdminDao {

	public String getValidateSubAdmin(String admincategory,String uid,String pwd);
	
	public List<OrderDetailsBean> getOrderDetails(String OrderId,String searchType,String startDate,String endDate);
	
	public String getUpdateOrderDetails(OrderDetailsModel ordModel);		
	
	public List<TransactionDetailBean> getListOfTransaction(String tranId,String tranType);
	
	public List<TransactionDetailModel> getListOfTodaysTransaction();
	
	public List<TransactionDetailModel> getListOfWeeklyTransaction();
	
	public List<UserReferenceDetailsModel> getReferenceCheckDetails();
	
	public List<UserBasicInfoBean> getUserDetailsViaRefBean(List<UserReferenceDetailsModel> referenceDetails);
	
	public List<UserReferenceDetailsModel> getReferenceCheckDetailsByCriteria(String searchType,String name,String startDate,String endDate);
	
	public String updateReferCheckStatus(int referId,String checkStatus);
	
	public List<TransactionDetailModel> getOffLineTransacitonCheckDetails();
	
	public List<UserBasicInfoBean> getUserDetailsByUserId(List<Integer> userIdList);
	
	public String updateCheckStatusByCriteria(int uniqueId,String checkStatus,String updateType);
	
	public List<TransactionDetailModel> getOffLineTransacitonCheckDetails(String searchType,String name,String startDate,String endDate);

	public List<ReferBonusAmountModel> getReferenceBonusChecksWithCriteria(String searchType,String name,String startDate,String endDate);
	
	public List<PolicyAmountModel> getPolicyBonusChecksWithCriteria(String searchType,String name,String startDate,String endDate);
	
	
	public List<OrderDetailsModel> getOrderDetailsWithCriteria(String duration);
	
	public List<TransactionDetailModel> getTaxTransaction();
	
	 public List<UserReferenceDetailsModel> getReferAmountDetails(String criteria);		
		
	 public List<ReferBonusAmountModel> getReferBonusDetails(String criteria);		
		
	 public List<PolicyAmountModel> getPolicyBonusDetails(String criteria);		
		
	 public List<TransactionDetailModel> getSuccessTransactionDetails();
	 
	 public String saveTaxReportDetails(TaxReportModel taxModel);
	 	 
	 public int getTodaysTaxId(String reportName);
	 
	 public List<TaxReportModel> getAllTaxReport(String criteria);
	 
	 public List<UserBasicInfoModel> getUserDetailModelByUserId(List<Integer> useIds);
	 
	 public List<TDSReportModel> getAllTdsReport(String restriction);
	 
	 public String saveTDSReportDetails(TDSReportModel tdsReport);  
	 
	 public List<ReferBonusAmountModel> getAllReferenceBonusDetails();
	  
	 public List<PolicyAmountModel> getAllPolicyBonusDetails();
	

	 public int getTodaysTDSId(String reportName);
	 
	 public List<TransactionDetailModel> getOffLineTransacitonCheckReports(String reportType,String reportFor,String fromDate,String toDate);
	 
	 public List<UserReferenceDetailsModel> getReferenceCheckReports(String reportType,String reportFor,String fromDate,String toDate);
	 
	 public List<ReferBonusAmountModel> getReferenceBonusCheckReports(String reportType,String reportFor,String fromDate,String toDate);
 	
	 public List<PolicyAmountModel> getPolicyBonusCheckReports(String reportType,String reportFor,String fromDate,String toDate);
 	
	 
	 public String saveReportDetails(ReportDetailsModel reportModel);
	 
	 public List<ReportDetailsModel> getCheckReportDetails(String restriction);
	 
	 public String getUpdateReportStatus(int reportId,String ipAddress);
	 
	 public List<OrderDetailsModel> getOrderDetailsForReport(String reportType,String fromDate,String toDate);

	 public List<ReportDetailsModel> getReportDetails(String restriction,String reportGenerateFrom);
	 
	 public List<TransactionDetailModel> getPurchaseTransationForReport(String reportType,String fromDate,String toDate);

	 public List<UserProductInterestModel> getInterestProductDetails(String OrderId);
	 
	 public List<ProductDetailsModel> getProductDetails(List<UserProductInterestModel> userInterestProduct);
	 
}
