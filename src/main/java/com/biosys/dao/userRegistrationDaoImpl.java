package com.biosys.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.AdminRegistrationModel;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserPlanDetailsModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.domain.UserRegistrationStageModel;
import com.biosys.domain.UserResetPasswordModel;
import com.biosys.domain.UserReturnProductModel;

@Repository("userRegDao")
public class userRegistrationDaoImpl implements userRegistrationDao{

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public String saveUserBasicDetails(UserBasicInfoModel userModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(userModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@Override
	public String saveEmailDetails(EmailSentModel emailModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(emailModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getLastUserId()
	{
		String hql = "select user_id from UserBasicInfoModel where user_cr_date = (select MAX(user_cr_date) from UserBasicInfoModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0)+1;
		}
		else
		{
			return 0;
		}	
	}
	
	@SuppressWarnings("unchecked")
	public String checkEmailIdExistOrNot(String emailId)
	{
		String hql = "select user_emailid from UserBasicInfoModel where user_emailid = '"+emailId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return "exist";
		}
		else
		{
			return "not exist";
		}	
	}
	
	@SuppressWarnings("unchecked")
	public List<BusinessPlanModel> getPlanDetailsViaPlanName(String planType)
	{
		String hql = "from BusinessPlanModel where plan_name = '"+planType+"'";
		List<BusinessPlanModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BusinessPlanFeatureModel> getPlanFeaturesViaPlanId(int plan_id)
	{
		String hql = "from BusinessPlanFeatureModel where plan_id = "+plan_id+"";
		List<BusinessPlanFeatureModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public int getUserIdViaEmailId(String emailId)
	{
		String hql = "select user_id from UserBasicInfoModel where user_emailid = '"+emailId+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
	}
	
	@Override	 
	public String saveUserStepDetails(UserRegistrationStageModel regStage)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(regStage);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@Override	 
	public String saveProductInterestOfUser(UserProductInterestModel intModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(intModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public int getUserRegStageId(int regUserId)
	{
		String hql = "select user_reg from UserRegistrationStageModel where user_id = "+regUserId+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
	}
	
	@Override	 
	public String saveReferenceDetails(UserReferenceDetailsModel refModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(refModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@Override	 
	public String saveUserPlanDetails(UserPlanDetailsModel planModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(planModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserBasicInfoBean> getUserBasicInfoViaUserId(int regUserId)
	{
		String hql = "from UserBasicInfoModel where user_id = "+regUserId+"";
		List<UserBasicInfoBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	@Override	 
	public String saveTransactionDetails(TransactionDetailModel tranModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(tranModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	@Override	 
	public String saveOrderDetails(OrderDetailsModel ordModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(ordModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
    @SuppressWarnings("unchecked")
	public int getLastTransactionId()
    {
    	String hql = "select transaction_id from TransactionDetailModel where transaction_date=(select MAX(transaction_date) from TransactionDetailModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
    }
	
	@SuppressWarnings("unchecked")
	public int getLastOrderId()
	{
		String hql = "select order_id from OrderDetailsModel where order_date=(select MAX(order_date) from OrderDetailsModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
	}
	
	@Override	 
	public String saveReturnProducDetails(UserReturnProductModel returnModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(returnModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getRegistrationStage(int regUserId)
	{
		String hql = "select user_reg_steps from UserRegistrationStageModel where user_id="+regUserId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}
	}
	
	@SuppressWarnings("unchecked")
	public int getLastResetId()
	{
		String hql = "select reset_id from UserResetPasswordModel where reset_date=(select MAX(reset_date) from UserResetPasswordModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	
	@Override	 
	public String saveResetDetails(UserResetPasswordModel resetModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(resetModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public String getEmailIdFromForgetPassword(String uniqueId)
	{
		String hql = "select reset_email_id from UserResetPasswordModel where reset_unique_id='"+uniqueId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "Not Exist";
		}
	}
	
	
	public String getUpdatePasswordViaEmailId(String emailId,String password)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserBasicInfoModel set password = :setPassword where user_emailid = :setEmailId");
		hql.setParameter("setPassword", password);
		
		hql.setParameter("setEmailId",emailId);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public int getResetIdViaUniqueId(String reqUniqueId)
	{
		String hql = "select reset_id from UserResetPasswordModel where reset_unique_id ='"+reqUniqueId+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	
	public String getDeleteResetDetails(String emailId)
	{		
		  String hql = "DELETE FROM UserResetPasswordModel WHERE reset_email_id = :emailId";	
		  org.hibernate.Query hql1 = sessionFactory.getCurrentSession().createQuery(hql);
		  hql1.setParameter("emailId", emailId);
		  int result  = hql1.executeUpdate();
		  
		  if(result==1)
		  {
			  return "success";
		  }
		  else
		  {
			  return "failed";
		  }
	}
	
	
    @SuppressWarnings("unchecked")
	public String getValidateUser(String emailId,String encryptPassword)
    {
    	String hql = "select vendor_id from UserBasicInfoModel where user_emailid = '"+emailId+"' and password='"+encryptPassword+"'";
		System.out.println("dsfsdf");		
    	List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "false";
		}		
    }
	
	@SuppressWarnings("unchecked")
	public String getPasswordViaEmailId(String emailId)
	{
		String hql = "select password from UserBasicInfoModel where user_emailid = '"+emailId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No User";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<UserProductInterestModel> getInterestedProduct(int regUserId)
	{
		String hql = "from UserProductInterestModel where user_id = "+regUserId+" and update_date=(select MAX(update_date) from UserProductInterestModel where user_id = "+regUserId+")";     
		List<UserProductInterestModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public int getLastProductInterestId(int regUserId)
	{
		String hql = "select user_product_int_id from UserProductInterestModel where user_id = "+regUserId+" and update_date=(select MAX(update_date) from UserProductInterestModel where user_id = "+regUserId+")";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	public String saveUserIntProAsperOrder(int productInterestId,String orderId)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserProductInterestModel set order_unique_id = :orderId where user_product_int_id = :productInterestId");
		hql.setParameter("productInterestId", productInterestId);		
		hql.setParameter("orderId",orderId);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	
	 public String asignOrderIdToTranId(String transactionId,String orderId)
	 {
		 org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update TransactionDetailModel set order_unique_id = :orderId where transaction_unique_id = :transactionId");
			hql.setParameter("transactionId", transactionId);		
			hql.setParameter("orderId",orderId);
			
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failed";
			}
	 }
}
