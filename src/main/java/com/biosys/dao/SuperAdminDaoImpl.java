package com.biosys.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.biosys.bean.BusinessPlanBean;
import com.biosys.bean.BusinessPlanFeatureBean;
import com.biosys.bean.EmailSentBean;
import com.biosys.bean.PlanDetailsBean;
import com.biosys.bean.ProductDetailsBean;
import com.biosys.bean.SMSSentBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PlanDetailsModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biosys.domain.AdminRegistrationModel;

@Repository("supAdminDao")
public class SuperAdminDaoImpl implements SuperAdminDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public String saveAdminDetails(AdminRegistrationModel adminDetails)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(adminDetails);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public String getValidateSuperAdmin(String uid,String pwd)
	{
		String hql = "select super_admin_password from SuperAdminModel where super_admin_unique_id='"+uid+"' and super_admin_password='"+pwd+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			if(lsdata.get(0).contains(pwd))
			{
				return "success";
			}
			else
			{
				return "failure";
			}
			
		}
		else
		{
			return "failure";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AdminRegistrationModel> getAllAdminDetails()
	{
		String hql = "from AdminRegistrationModel";
		List<AdminRegistrationModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return lsdata;		
	}
	
	public String removeAdminById(int adminId)
	{
		 String hql = "DELETE FROM AdminRegistrationModel WHERE admin_id = :adminId";	
		  org.hibernate.Query hql1 = sessionFactory.getCurrentSession().createQuery(hql);
		  hql1.setParameter("adminId", adminId);
		  int result  = hql1.executeUpdate();
		  
		  if(result==1)
		  {
			  return "success";
		  }
		  else
		  {
			  return "failed";
		  }
	}
	
	@SuppressWarnings("unchecked")
	public int getLastProductId()
	{
		String hql = "select product_id from ProductDetailsModel where product_cr_date=(select MAX(product_cr_date) from ProductDetailsModel)";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
		
	}
	
	public String saveProductDetails(ProductDetailsModel productDetails)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.save(productDetails);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	public String updateProductDetails(ProductDetailsModel productDetails)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update ProductDetailsModel set product_category = :setProductCategory , product_name = :setProductName , product_price = :setProductPrice , product_description = :setProductDesc , product_image_file_name = :setFileName , product_cr_date = :setCrDate where product_id = :setProductId");
		hql.setParameter("setProductCategory", productDetails.getProduct_category());
		hql.setParameter("setProductName", productDetails.getProduct_name());
		hql.setParameter("setProductPrice",productDetails.getProduct_price());
		hql.setParameter("setProductDesc",productDetails.getProduct_description());
		hql.setParameter("setFileName",productDetails.getProduct_image_file_name());
		hql.setParameter("setCrDate",productDetails.getProduct_cr_date());
		
		hql.setParameter("setProductId",productDetails.getProduct_id());
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ProductDetailsBean> getAllProductDetailsDetails()
	{
		// Restrict for last 20 datas 
		
		String hql = "from ProductDetailsModel";
		List<ProductDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return lsdata;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ProductDetailsModel> getProductDetailsByProductId(int productId)
	{
		String hql = "from ProductDetailsModel where product_id="+productId+"";
		List<ProductDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return lsdata;	
	}
	
	
	
	public String removeProductById(int productId)
	{
		  String hql = "DELETE FROM ProductDetailsModel WHERE product_id = :productId";	
		  org.hibernate.Query hql1 = sessionFactory.getCurrentSession().createQuery(hql);
		  hql1.setParameter("productId", productId);
		  int result  = hql1.executeUpdate();
		  
		  if(result==1)
		  {
			  return "success";
		  }
		  else
		  {
			  return "failed";
		  }
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ProductDetailsBean> getAllProductDetailsByCriteria(String searchValue,String searchType)
	{
		String hql = "";
		if(searchType.equals("Product Auto Id"))
		{
			hql = "from ProductDetailsModel where product_id='"+searchValue+"'";
		}
		else if(searchType.equals("Product Id"))
		{
			hql = "from ProductDetailsModel where product_unique_id='"+searchValue+"'";
		}
		else if(searchType.equals("Product Name"))
		{
			hql = "from ProductDetailsModel where product_name='"+searchValue+"'";
		}
		else
		{
			hql = "from ProductDetailsModel where product_category='"+searchValue+"'";
		}		
		
		List<ProductDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return lsdata;	
	}
	
	
	public String saveCostValues(CostManageModel costModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(costModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public int getCostExistOrNot(String costName)
	{
		String hql = "select cost_id from CostManageModel where cost_name='"+costName+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);	
		}
		else
		{
			return 0;	
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<CostManageModel> getAllCostDetails()
	{
		String hql = "from CostManageModel";
		List<CostManageModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserBasicInfoBean> getVendorBasicDetails(String searchValue,String searchType)
	{
		List<UserBasicInfoBean> lsdata = new ArrayList<UserBasicInfoBean>();
		String hql = "";
		if(searchType.equals("Id"))
		{
			hql = "from UserBasicInfoModel where vendor_id='"+searchValue+"'";
		}
		else if(searchType.equals("Name"))
		{
			hql = "from UserBasicInfoModel where user_name LIKE '%"+searchValue+"%'";
		}
		else
		{
			hql = "from UserBasicInfoModel where user_emailid = '"+searchValue+"'";
		}
		lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmailSentBean> getEmailSentDetails()
	{
		String hql = "from EmailSentModel";
		List<EmailSentBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SMSSentBean> getSMSDetails()
	{
		String hql = "from SMSSentModel";
		List<SMSSentBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	public String savePlanDetails(PlanDetailsModel planModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(planModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PlanDetailsBean> getPlandetails()
	{
		String hql = "from PlanDetailsModel";
		List<PlanDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public List<BusinessPlanBean> getBusinessPlandetails()
	{
		String hql = "from BusinessPlanModel";
		List<BusinessPlanBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BusinessPlanFeatureBean> getBusinessPlanFeatures()
	{
		String hql = "from BusinessPlanFeatureModel";
		List<BusinessPlanFeatureBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public int getStatusPlanExistOrNot(String planName)
	{
		String hql = "select plan_id from PlanDetailsModel where plan_name='"+planName+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
		
	}
		
    @SuppressWarnings("unchecked")
    public List<UserProfileDetailsModel> getVendorOtherDetailsById(int vendorId)
    {
    	
		String hql = "from UserProfileDetailsModel where user_id="+vendorId+"";			
		List<UserProfileDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
    }
	
	@SuppressWarnings("unchecked")
	public List<UserBasicInfoBean> getVendorBasicInfoDetailsById(int vendorId)
	{
		List<UserBasicInfoBean> lsdata = new ArrayList<UserBasicInfoBean>();
		String hql = "";		
		hql = "from UserBasicInfoModel where user_id="+vendorId+"";		
		lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata;
	}
	
    
	public String saveBusinessPlanDetails(BusinessPlanModel planModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(planModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	public String saveBusinessPlanFeatures(BusinessPlanFeatureModel planFeature)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(planFeature);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	public String deleteFeature(int featureId)
	{
		 String hql = "DELETE FROM BusinessPlanFeatureModel WHERE feature_id = :featureId";	
		  org.hibernate.Query hql1 = sessionFactory.getCurrentSession().createQuery(hql);
		  hql1.setParameter("featureId", featureId);
		  int result  = hql1.executeUpdate();
		  
		  if(result==1)
		  {
			  return "success";
		  }
		  else
		  {
			  return "failed";
		  }
	}
	
	
	
	@SuppressWarnings("unchecked")
	public long getRecords(String criteria)
	{
		long total = 0;
		
		if(criteria.equals("totalUsers"))
		{			
			long i =   (long) (sessionFactory.getCurrentSession().createCriteria(UserBasicInfoModel.class).setProjection(Projections.rowCount()).uniqueResult());			
			total = i;
		}
		else if(criteria.equals("totalOrders"))
		{
			long i =  (long) (sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class).setProjection(Projections.rowCount()).uniqueResult());			
			total = i;
		}
		else
		{
			
		}
		
		return total;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getAllReferenceDetails(String restriction)
	{		
		List<UserReferenceDetailsModel> lsdata = new ArrayList<UserReferenceDetailsModel>();
		
		if(restriction.equals("monthly"))
		{
			ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
			int fromTDSReportDate = Integer.parseInt(fileResource.getString("fromTDSReportDate"));	
			int toTDSReportDate = Integer.parseInt(fileResource.getString("toTDSReportDate"));	
			     		    
			Date today = new Date();
			Calendar cal = Calendar.getInstance();
		    cal.setTime(today);
		    int year = cal.get(Calendar.YEAR);
		    int month = cal.get(Calendar.MONTH);	   
		    int day = fromTDSReportDate;
		           
		    String startDate = toTDSReportDate+"/"+month+"/"+year;
			String endDate = day+"/"+(month+1)+"/"+year;
						
			DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try 
		    {	    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(startDate);
		        endingDate = (Date) formatter.parse(endDate);	        
		    } 
		    catch (ParseException e) 
		    {
		        e.printStackTrace();
		    }		   
		   
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserReferenceDetailsModel.class);
			criteria.add(Restrictions.eq("reference_status","Success"));
			criteria.add(Restrictions.eq("reference_check_status","Pending"));
			criteria.add(Restrictions.ge("reference_accept_date",startingDate));
			criteria.add(Restrictions.le("reference_accept_date",endingDate));		
			criteria.addOrder(Order.desc("reference_accept_date"));
		 
			return criteria.list();	
			
		}
		else
		{
			return lsdata;
		}
		
	}
	
	
}
