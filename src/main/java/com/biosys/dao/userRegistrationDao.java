package com.biosys.dao;

import java.util.List;

import com.biosys.bean.UserBasicInfoBean;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.BusinessPlanModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserPlanDetailsModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;
import com.biosys.domain.UserRegistrationStageModel;
import com.biosys.domain.UserResetPasswordModel;
import com.biosys.domain.UserReturnProductModel;

public interface userRegistrationDao {

	public String saveUserBasicDetails(UserBasicInfoModel userModel);
	
	public int getLastUserId();
	
	public String checkEmailIdExistOrNot(String emailId);
	
	public int getUserIdViaEmailId(String emailId);
	
	public String saveUserStepDetails(UserRegistrationStageModel regStage);
	
	public String saveProductInterestOfUser(UserProductInterestModel intModel);
	
	public int getUserRegStageId(int regUserId);
	
	public String saveReferenceDetails(UserReferenceDetailsModel refModel);
	
	public String saveUserPlanDetails(UserPlanDetailsModel planModel);
	
	public List<UserBasicInfoBean> getUserBasicInfoViaUserId(int regUserId);
	
	public String saveTransactionDetails(TransactionDetailModel tranModel);
	
	public String saveOrderDetails(OrderDetailsModel ordModel);
	
    public int getLastTransactionId();
	
	public int getLastOrderId();
	
	public String saveReturnProducDetails(UserReturnProductModel returnModel);
	
	public List<BusinessPlanModel> getPlanDetailsViaPlanName(String planType);
	
	public String getRegistrationStage(int regUserId);
	
	public int getLastResetId();
	
	public String saveResetDetails(UserResetPasswordModel resetModel);
	
	public String getEmailIdFromForgetPassword(String uniqueId);
	
	public String getUpdatePasswordViaEmailId(String emailId,String password);
	
	public int getResetIdViaUniqueId(String reqUniqueId);
	
	public String getDeleteResetDetails(String emailId);      
	
	public String getValidateUser(String emailId,String encryptPassword);
	
	public String getPasswordViaEmailId(String emailId);
	
	public List<BusinessPlanFeatureModel> getPlanFeaturesViaPlanId(int plan_id);
	
	public List<UserProductInterestModel> getInterestedProduct(int regUserId);
	
	public String saveEmailDetails(EmailSentModel emailModel);
	
	public int getLastProductInterestId(int regUserId);
	
	public String saveUserIntProAsperOrder(int productInterestId,String orderId);
	
	 public String asignOrderIdToTranId(String transactionId,String orderId);
}
