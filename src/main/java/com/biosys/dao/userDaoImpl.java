package com.biosys.dao;


import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserProfileDetailsBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.domain.BusinessPlanFeatureModel;
import com.biosys.domain.EmailSentModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProfileDetailsModel;
import com.biosys.domain.UserReferenceDetailsModel;

@Repository("userDao")
public class userDaoImpl implements userDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public String validateUserEmailIdExistOrNot(int userId,String emailId)
	{
		String hql = "select user_emailid from UserBasicInfoModel where user_id!="+userId+" and user_emailid = '"+emailId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return "exist";
		}
		else
		{
			return "Not exist";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<UserBasicInfoBean> getUserBasicDetailsViaVendorId(String vendorId)
	{
		String hql = "from UserBasicInfoModel where vendor_id='"+vendorId+"'";
		List<UserBasicInfoBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String getFullNameViaVenId(String vendorId)
	{
		String hql = "select user_name from UserBasicInfoModel where vendor_id='"+vendorId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public int getUserIdViaVendorId(String vendorId)
	{
		String hql = "select user_id from UserBasicInfoModel where vendor_id='"+vendorId+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserProfileDetailsBean> getUserOtherProfileDetailsViaUserId(int userId)
	{
		String hql = "from UserProfileDetailsModel where user_id="+userId+"";
		List<UserProfileDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<OrderDetailsBean> getLastOrderDetailsViaUserId(int userId)
	{
		String hql = "from OrderDetailsModel where order_for_user_id="+userId+" and order_date=(select MAX(order_date) from OrderDetailsModel where order_for_user_id="+userId+")";
		List<OrderDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsBean> GetReferenceDetailsViaUserId(int userId)
	{
		String hql = "from UserReferenceDetailsModel where reference_given_user_id="+userId+" ORDER BY reference_given_date DESC";
		List<UserReferenceDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	public String saveUserOtherProfileDetails(UserProfileDetailsModel userOthModel)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(userOthModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public int getUserProfileIdViaUserId(int userId)
	{
		String hql = "select user_profile_id from UserProfileDetailsModel where user_id="+userId+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	
	public String getUpdateUserRegDate(int regUserId)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserBasicInfoModel set user_cr_date = :setDate where user_id = :setuserId");
		hql.setParameter("setDate", new Date());
		hql.setParameter("setuserId",regUserId);
		
	    int result  = hql.executeUpdate();		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	
	public String updateProfileImageInBasicInfo(int userId,String imageName)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserBasicInfoModel set user_profile_image_name = :setImage where user_id = :setuserId");
		hql.setParameter("setImage", imageName);
		
		hql.setParameter("setuserId",userId);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getProfilePhoto(int userId)
	{
		String hql = "select user_profile_image_name from UserProfileDetailsModel where user_id="+userId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Photo";
		}
	}
	
	public String getUpdateUserBasicInfo(UserBasicInfoModel basicInfo)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserBasicInfoModel set user_emailid = :setEmailId , user_mobile_no = :setMobileNo , user_shipping_address = :setShippingAddress  where user_id = :setuserId");
		hql.setParameter("setEmailId", basicInfo.getUser_emailid());
		hql.setParameter("setMobileNo", basicInfo.getUser_mobile_no());
		hql.setParameter("setShippingAddress", basicInfo.getUser_shipping_address());
		
		hql.setParameter("setuserId",basicInfo.getUser_id());
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	@SuppressWarnings("unchecked")
	public String getCurrentPassword(String vendorId)
	{
		String hql = "select password from UserBasicInfoModel where vendor_id='"+vendorId+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Data";
		}
	}
	
	public String updatePassword(String encpwd,String vendorId)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserBasicInfoModel set password = :setPassword where vendor_id = :setvendorId");
		hql.setParameter("setPassword", encpwd);	
		hql.setParameter("setvendorId",vendorId);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<OrderDetailsBean> getAllOrderDetails(int userId)
	{
		String hql = "from OrderDetailsModel where order_for_user_id="+userId+" ORDER BY order_date DESC";
		List<OrderDetailsBean> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public String getUserPlanViaUserId(int userId)
	{
		String hql = "select user_plan_name from UserPlanDetailsModel where user_id="+userId+"";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return "No Plan";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public int getPolicyAmountAsPerPlan(String userPlan)
	{
		String hql = "select plan_policy_amount from BusinessPlanModel where plan_name='"+userPlan+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}		
	}
	
	public List<OrderDetailsModel> getUserOrderDetails(int userId)
	{
		String hql = "from OrderDetailsModel where order_for_user_id="+userId+" ORDER BY order_date DESC";
		@SuppressWarnings("unchecked")
		List<OrderDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	public String saveUserPolicyAmountDetails(PolicyAmountModel policyAmount)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(policyAmount);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PolicyAmountModel> getUserAllPolicyDetails(int userId)
	{
		String hql = "from PolicyAmountModel where user_id="+userId+" ORDER BY policy_entry_date DESC";
		List<PolicyAmountModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getReferDetailsWhoRefer(String emailId)
	{
		String hql = "from UserReferenceDetailsModel where referee_emailid='"+emailId+"'";
		List<UserReferenceDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;		
	}
	
	@SuppressWarnings("unchecked")
	public int getReferAmountAsPerPlan(String userPlan)
	{
		String hql = "select plan_amount_refer_per_friend from BusinessPlanModel where plan_name='"+userPlan+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}	
	}
	
	
	public String getUpdateUserReferStatus(int referId,int referAmount)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserReferenceDetailsModel set reference_amount = :setReferAmount , reference_status = :setReferenceStatus , reference_accept_date = :setreferAcceptdate , reference_check_status = :setCheckStatus where reference_id = :setReferId");
		hql.setParameter("setReferAmount", referAmount);	
		hql.setParameter("setreferAcceptdate", new Date());	
		hql.setParameter("setReferenceStatus", "Success");
		hql.setParameter("setCheckStatus", "Pending");	
		hql.setParameter("setReferId",referId);
		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getUserReferDetails(int userId)
	{
		String hql = "from UserReferenceDetailsModel where reference_given_user_id="+userId+"";
		List<UserReferenceDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;			
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getReferDetailViaReferId(int referenceId)
	{
		String hql = "from UserReferenceDetailsModel where reference_id="+referenceId+"";
		List<UserReferenceDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getNoOfUserReferJoined(int userId)
	{
		String hql = "from UserReferenceDetailsModel where reference_given_user_id="+userId+" and reference_status='Success'";
		List<UserReferenceDetailsModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
	@SuppressWarnings("unchecked")
	public int getPlanIdViaPlanName(String userPlan)
	{
		String hql = "select plan_id from BusinessPlanModel where plan_name='"+userPlan+"'";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<BusinessPlanFeatureModel> getAllPlanFeature(int plan_Id)
	{
		String hql = "from BusinessPlanFeatureModel where plan_id="+plan_Id+"";
		List<BusinessPlanFeatureModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	@SuppressWarnings("unchecked")
	public Date getUserRegDate(int userId)
	{
		String hql = "select user_cr_date from UserBasicInfoModel where user_id="+userId+"";
		List<Date> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		return lsdata.get(0);		
	}
	
	
	@SuppressWarnings("unchecked")
	public int checkPlanBonusExistOrNot(int noOfPlanDays)
	{
		String hql = "select no_of_refers_user from ReferBonusAmountModel where user_plan_no_of_days="+noOfPlanDays+"";
		List<Integer> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(lsdata.size()>0)
		{
			return lsdata.get(0);
		}
		else
		{
			return 0;
		}			
	}
	
	public String saveUserPlanBonus(ReferBonusAmountModel planBonus)
	{
		String txStatus="";
		Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(planBonus);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		return txStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<ReferBonusAmountModel> getReferalBonusDetails(int userId)
	{
		String hql = "from ReferBonusAmountModel where user_id="+userId+"";
		List<ReferBonusAmountModel> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();	
		return lsdata;
	}
	
	
}
