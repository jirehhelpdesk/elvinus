package com.biosys.dao;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.biosys.bean.OrderDetailsBean;
import com.biosys.bean.ProductDetailsBean;
import com.biosys.bean.TransactionDetailBean;
import com.biosys.bean.UserBasicInfoBean;
import com.biosys.bean.UserReferenceDetailsBean;
import com.biosys.domain.CostManageModel;
import com.biosys.domain.OrderDetailsModel;
import com.biosys.domain.PolicyAmountModel;
import com.biosys.domain.ProductDetailsModel;
import com.biosys.domain.ReferBonusAmountModel;
import com.biosys.domain.ReportDetailsModel;
import com.biosys.domain.TDSReportModel;
import com.biosys.domain.TaxReportModel;
import com.biosys.domain.TransactionDetailModel;
import com.biosys.domain.UserBasicInfoModel;
import com.biosys.domain.UserProductInterestModel;
import com.biosys.domain.UserReferenceDetailsModel;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biosys.domain.AdminRegistrationModel;
import com.biosys.util.PasswordGenerator;

@Repository("subAdminDao")
public class SubAdminDaoImpl implements SubAdminDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getValidateSubAdmin(String admincategory,String uid,String pwd)
	{
		String validateStatus = "";
		
		String hql = "select admin_password from AdminRegistrationModel where admin_type='"+admincategory+"' and admin_email_id='"+uid+"'";
		List<String> lsdata = sessionFactory.getCurrentSession().createQuery(hql).list();		
		if(lsdata.size()>0)
		{
			PasswordGenerator cryptoUtil=new PasswordGenerator();
			String decryptPassword = "";
			
		    try {
		    	
		    	decryptPassword = cryptoUtil.decrypt(lsdata.get(0));
				
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| InvalidKeySpecException | NoSuchPaddingException
					| InvalidAlgorithmParameterException
					| IllegalBlockSizeException | BadPaddingException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    if(decryptPassword.contains(pwd))
		    {
		    	validateStatus = "success";
		    }
		    else
		    {
		    	validateStatus = "failure";
		    }
		}
		else
		{
			validateStatus = "failure";
		}		
		return validateStatus;
	}
	
	@SuppressWarnings("unchecked")
	public List<OrderDetailsBean> getOrderDetails(String OrderId,String searchType,String startDate,String endDate)
	{
		if(searchType==null)
		{
			String hql = "from OrderDetailsModel where order_unique_id='"+OrderId+"'";
			List<OrderDetailsBean> orderDetails = sessionFactory.getCurrentSession().createQuery(hql).list();		
			return orderDetails;
		}
		else if(searchType.equals("OrderId"))
		{
			String hql = "from OrderDetailsModel where order_unique_id='"+OrderId+"'";
			List<OrderDetailsBean> orderDetails = sessionFactory.getCurrentSession().createQuery(hql).list();		
			return orderDetails;	
		}	
		else if(searchType.equals("Duration"))
		{
			    DateFormat formatter = null;		        
			    Date startingDate = null;
			    Date endingDate = null;
			    
			    try {
			    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(startDate);
			        endingDate = (Date) formatter.parse(endDate);
			        
			    } catch (ParseException e) {
			        e.printStackTrace();
			    }		   
			   
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class);
				criteria.add(Restrictions.ge("order_date",startingDate));
				criteria.add(Restrictions.le("order_date",endingDate));		
				criteria.addOrder(Order.desc("order_date"));
			 
				return criteria.list();		
		}	
		else
		{
			String hql = "from OrderDetailsModel where order_unique_id='"+OrderId+"'";
			List<OrderDetailsBean> orderDetails = sessionFactory.getCurrentSession().createQuery(hql).list();		
			return orderDetails;
		}
		
	}
		
	public String getUpdateOrderDetails(OrderDetailsModel ordModel)
	{	
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update OrderDetailsModel set order_status = :setOrderStatus , order_status_details = :setOrderStatusDetails , order_expected_date = :setExpDate where order_id = :setOrderId");
		hql.setParameter("setOrderStatus", ordModel.getOrder_status());
		hql.setParameter("setOrderStatusDetails",ordModel.getOrder_status_details());
		hql.setParameter("setExpDate",ordModel.getOrder_expected_date());
		
		hql.setParameter("setOrderId",ordModel.getOrder_id());		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}		
	}
	
	@SuppressWarnings("unchecked")
	public List<TransactionDetailBean> getListOfTransaction(String tranId,String tranType)
	{
		String hql = "from TransactionDetailModel where transaction_unique_id='"+tranId+"'";
		List<TransactionDetailBean> tranDetails = sessionFactory.getCurrentSession().createQuery(hql).list();		
		return tranDetails;
	}
			
	@SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getListOfTodaysTransaction()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		StringBuilder query = new StringBuilder("from TransactionDetailModel ");
		query.append(" where transaction_status = 'Success' and transaction_date LIKE '" + sdf.format(new Date()) + "%' ORDER BY transaction_date DESC");	
		List<TransactionDetailModel> tranDetails = sessionFactory.getCurrentSession().createQuery(query.toString()).list();		
		return tranDetails;		
	}
	
	@SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getListOfWeeklyTransaction()
	{		
		Date today = new Date();
		Date sevenDayBefore = new Date(today.getTime() - 7 * 24 * 3600 * 1000);
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
		criteria.add(Restrictions.eq("transaction_status","Success"));
		criteria.add(Restrictions.ge("transaction_date",sevenDayBefore));
		criteria.add(Restrictions.le("transaction_date",today));		
		criteria.addOrder(Order.desc("transaction_date"));
	 
		return criteria.list();	
	}
			
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getReferenceCheckDetails()
	{
		String hql = "from UserReferenceDetailsModel where reference_status='Success' and reference_check_status='Pending'";
		List<UserReferenceDetailsModel> referDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
				
		/*String anthql = "from UserReferenceDetailsModel p inner join p.UserBasicInfoModel";
		 
		Query query = sessionFactory.getCurrentSession().createQuery(anthql);
		List<Object[]> listResult = query.list();
		 
		for (Object[] aRow : listResult) {
			UserReferenceDetailsModel ref = (UserReferenceDetailsModel) aRow[0];
			UserBasicInfoModel basic = (UserBasicInfoModel) aRow[1];
		    System.out.println(ref.getReference_given_user_id() + " - " + basic.getUser_name());
		}
		*/
		
		return referDetails;		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserBasicInfoBean> getUserDetailsViaRefBean(List<UserReferenceDetailsModel> referenceDetails)
	{
		List<UserBasicInfoBean> basicInfo = new ArrayList<UserBasicInfoBean>();
		
		for(int i=0;i<referenceDetails.size();i++)
		{
			String hql = "from UserBasicInfoModel where user_id="+referenceDetails.get(i).getReference_given_user_id()+"";
			List<UserBasicInfoBean> basicInfoHql = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(!basicInfo.containsAll(basicInfoHql))
			{
				basicInfo.addAll(basicInfoHql);
			}			
		}
		
		return basicInfo;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getReferenceCheckDetailsByCriteria(String searchType,String name,String startDate,String endDate)
	{
		List<UserReferenceDetailsModel> referDetails = new ArrayList<UserReferenceDetailsModel>();
		
		if(searchType.equals("Name"))
		{
			String userIdHql = "select user_id from UserBasicInfoModel where user_name LIKE '%"+name+"%'";
			List<Integer> userIdList = sessionFactory.getCurrentSession().createQuery(userIdHql).list();
			
			for(int i=0;i<userIdList.size();i++)
			{
				String hql = "from UserReferenceDetailsModel where reference_status='Success' and reference_given_user_id="+userIdList.get(i)+"";
				List<UserReferenceDetailsModel> eachReferDetails = sessionFactory.getCurrentSession().createQuery(hql).list();	
				
				referDetails.addAll(eachReferDetails);
			}
			
			return referDetails;
		}
		else
		{								    
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(startDate);
		        endingDate = (Date) formatter.parse(endDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }		   
		   
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserReferenceDetailsModel.class);
			criteria.add(Restrictions.eq("reference_status","Success"));
			criteria.add(Restrictions.ge("reference_accept_date",startingDate));
			criteria.add(Restrictions.le("reference_accept_date",endingDate));		
			criteria.addOrder(Order.desc("reference_accept_date"));
		 
			return criteria.list();		
		}	
	}
	
	
	public String updateReferCheckStatus(int referId,String checkStatus)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update UserReferenceDetailsModel set reference_check_status = :setCheckStatus where reference_id = :setReferId");
		hql.setParameter("setCheckStatus", checkStatus);
		
		hql.setParameter("setReferId",referId);		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getOffLineTransacitonCheckDetails()
	{
		String hql = "from TransactionDetailModel where transaction_mode='Offline' and transaction_status='Pending'";
		List<TransactionDetailModel> tranDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
		
		return tranDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserBasicInfoBean> getUserDetailsByUserId(List<Integer> userIdList)
	{
		List<UserBasicInfoBean> basicInfo = new ArrayList<UserBasicInfoBean>();
		
		for(int i=0;i<userIdList.size();i++)
		{
			String hql = "from UserBasicInfoModel where user_id="+userIdList.get(i)+"";
			List<UserBasicInfoBean> basicInfoHql = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(!basicInfo.containsAll(basicInfoHql))
			{
				basicInfo.addAll(basicInfoHql);
			}			
		}
		
		return basicInfo;
	}
	
	
	public String updateCheckStatusByCriteria(int uniqueId,String checkStatus,String updateType)
	{
		if(updateType.equals("Transaction"))
		{
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update TransactionDetailModel set transaction_status = :setCheckStatus where transaction_id = :setTranId");
			hql.setParameter("setCheckStatus", checkStatus);
			
			hql.setParameter("setTranId",uniqueId);		
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failed";
			}	
		}
		else if(updateType.equals("Bonus"))
		{
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update ReferBonusAmountModel set bonus_check_status = :setCheckStatus where plan_bonus_id = :setUniqueId");
			hql.setParameter("setCheckStatus", checkStatus);
			
			hql.setParameter("setUniqueId",uniqueId);		
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failed";
			}	
		}
		else if(updateType.equals("Policy"))
		{
			org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update PolicyAmountModel set policy_status = :setCheckStatus where policy_id = :setUniqueId");
			hql.setParameter("setCheckStatus", checkStatus);
			
			hql.setParameter("setUniqueId",uniqueId);		
		    int result  = hql.executeUpdate();
			
			if(result==1)
			{			
			    return "success";
			}
			else
			{			
				return "failed";
			}	
		}
		else
		{
			return "success";
		}		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getOffLineTransacitonCheckDetails(String searchType,String name,String startDate,String endDate)
	{
        List<TransactionDetailModel> referDetails = new ArrayList<TransactionDetailModel>();
		
		if(searchType.equals("Name"))
		{
			String userIdHql = "select user_id from UserBasicInfoModel where user_name LIKE '%"+name+"%'";
			List<Integer> userIdList = sessionFactory.getCurrentSession().createQuery(userIdHql).list();
			
			for(int i=0;i<userIdList.size();i++)
			{
				String hql = "from TransactionDetailModel where transaction_mode='Offline' and transaction_against_user_id="+userIdList.get(i)+"";
				List<TransactionDetailModel> eachReferDetails = sessionFactory.getCurrentSession().createQuery(hql).list();	
				
				referDetails.addAll(eachReferDetails);
			}
			
			return referDetails;
		}
		else
		{								    
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(startDate);
		        endingDate = (Date) formatter.parse(endDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }		   
		   
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
			criteria.add(Restrictions.eq("transaction_mode","Offline"));
			criteria.add(Restrictions.ge("transaction_date",startingDate));
			criteria.add(Restrictions.le("transaction_date",endingDate));		
			criteria.addOrder(Order.desc("transaction_date"));
		 
			return criteria.list();		
		}	
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<ReferBonusAmountModel> getReferenceBonusChecksWithCriteria(String searchType,String name,String startDate,String endDate)
	{
		List<ReferBonusAmountModel> referDetails = new ArrayList<ReferBonusAmountModel>();
		
		if(searchType==null)
		{
			String hql = "from ReferBonusAmountModel where bonus_check_status='Pending'";
			referDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
			return referDetails;
		}
		else if(searchType.equals("Name"))
		{
			String userIdHql = "select user_id from UserBasicInfoModel where user_name LIKE '%"+name+"%'";
			List<Integer> userIdList = sessionFactory.getCurrentSession().createQuery(userIdHql).list();
			
			for(int i=0;i<userIdList.size();i++)
			{
				String hql = "from ReferBonusAmountModel where user_id="+userIdList.get(i)+"";
				List<ReferBonusAmountModel> eachReferDetails = sessionFactory.getCurrentSession().createQuery(hql).list();	
				
				referDetails.addAll(eachReferDetails);
			}
			
			return referDetails;
		}
		else if(searchType.equals("Duration"))
		{								    
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(startDate);
		        endingDate = (Date) formatter.parse(endDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }		   
		   
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
			criteria.add(Restrictions.ge("update_date",startingDate));
			criteria.add(Restrictions.le("update_date",endingDate));		
			criteria.addOrder(Order.desc("update_date"));
		 
			return criteria.list();		
		}
		else
		{
			String hql = "from ReferBonusAmountModel where bonus_check_status='Pending'";
			referDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
			return referDetails;
		}
		
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<PolicyAmountModel> getPolicyBonusChecksWithCriteria(String searchType,String name,String startDate,String endDate)
	{
		List<PolicyAmountModel> policyDetails = new ArrayList<PolicyAmountModel>();
		
		if(searchType==null)
		{
			String hql = "from PolicyAmountModel where policy_status='Pending'";
			policyDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
			return policyDetails;
		}
		else if(searchType.equals("Name"))
		{
			String userIdHql = "select user_id from UserBasicInfoModel where user_name LIKE '%"+name+"%'";
			List<Integer> userIdList = sessionFactory.getCurrentSession().createQuery(userIdHql).list();
			
			for(int i=0;i<userIdList.size();i++)
			{
				String hql = "from PolicyAmountModel where user_id="+userIdList.get(i)+"";
				List<PolicyAmountModel> eachPolicyDetails = sessionFactory.getCurrentSession().createQuery(hql).list();	
				
				policyDetails.addAll(eachPolicyDetails);
			}
			
			return policyDetails;
		}
		else if(searchType.equals("Duration"))
		{								    
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try
		    {		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(startDate);
		        endingDate = (Date) formatter.parse(endDate);		        
		    } 
		    catch (ParseException e) 
		    {
		        e.printStackTrace();
		    }		
		    
		    
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PolicyAmountModel.class);
			criteria.add(Restrictions.ge("policy_end_date",startingDate));
			criteria.add(Restrictions.le("policy_end_date",endingDate));		
			criteria.addOrder(Order.desc("policy_end_date"));
		 
			return criteria.list();		
		}
		else
		{
			String hql = "from PolicyAmountModel where policy_status='Pending'";
			policyDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
			return policyDetails;
		}
	}
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<OrderDetailsModel> getOrderDetailsWithCriteria(String duration)
	{
		Date today = new Date();
		Date sevenDayBefore = new Date(today.getTime() - 7 * 24 * 3600 * 1000);
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class);
		criteria.add(Restrictions.ge("order_date",sevenDayBefore));
		criteria.add(Restrictions.le("order_date",today));		
		criteria.addOrder(Order.desc("order_date"));
	    
		return criteria.list();	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getTaxTransaction()
	{		
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
		int fromVATreportDate = Integer.parseInt(fileResource.getString("fromVATreportDate"));	
		int toVATreportDate = Integer.parseInt(fileResource.getString("toVATreportDate"));	
		     		    
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(today);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);	   
	    int day = fromVATreportDate;
	           
	    String startDate = toVATreportDate+"/"+month+"/"+year;
		String endDate = day+"/"+(month+1)+"/"+year;
		
		DateFormat formatter = null;
        
	    Date startingDate = null;
	    Date endingDate = null;
	    
	    try {	    	
	        formatter = new SimpleDateFormat("dd/MM/yyyy");
	        startingDate = (Date) formatter.parse(startDate);
	        endingDate = (Date) formatter.parse(endDate);	        
	    } 
	    catch (ParseException e) 
	    {
	        e.printStackTrace();
	    }		   
	   
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
		criteria.add(Restrictions.eq("transaction_status","Success"));
		criteria.add(Restrictions.ge("transaction_date",startingDate));
		criteria.add(Restrictions.le("transaction_date",endingDate));		
		criteria.addOrder(Order.desc("transaction_date"));
	 
		return criteria.list();	
	}
	
	
	
	 @SuppressWarnings("unchecked")
	 public List<UserReferenceDetailsModel> getReferAmountDetails(String criteriaVal)
	 {
		 if(criteriaVal.equals("DailyBasis"))
		 {
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			StringBuilder query = new StringBuilder("from UserReferenceDetailsModel");
			query.append(" where reference_status = 'Success' and reference_accept_date LIKE '" + sdf.format(new Date()) + "%' ORDER BY reference_accept_date DESC");	
			List<UserReferenceDetailsModel> tranDetails = sessionFactory.getCurrentSession().createQuery(query.toString()).list();		
			return tranDetails;	
		 }
		 else
		 {		
			    ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
	   		    int fromVATreportDate = Integer.parseInt(fileResource.getString("fromVATreportDate"));	
	   		    int toVATreportDate = Integer.parseInt(fileResource.getString("toVATreportDate"));	
	   		    	   		    			 					
			    Date today = new Date();
				Calendar cal = Calendar.getInstance();
			    cal.setTime(today);
			    int year = cal.get(Calendar.YEAR);
			    int month = cal.get(Calendar.MONTH);
			    month = month + 1;
			    int day = fromVATreportDate;
			           
			    String startDate = day+"/"+(month-1)+"/"+year;
				String endDate = (day-1)+"/"+month+"/"+year;
							
				DateFormat formatter = null;
		        
			    Date startingDate = null;
			    Date endingDate = null;
			    
			    try 
			    {		    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(startDate);
			        endingDate = (Date) formatter.parse(endDate);		        
			    } 
			    catch (ParseException e) 
			    {
			        e.printStackTrace();
			    }		   
			    
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.eq("transaction_status","Success"));
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			    
				return criteria.list();	
		 }    
	 }
		
	 
	 
    @SuppressWarnings("unchecked")
	public List<ReferBonusAmountModel> getReferBonusDetails(String criteriaVal)
    {
    	if(criteriaVal.equals("DailyBasis"))
		 {
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			StringBuilder query = new StringBuilder("from ReferBonusAmountModel");
			query.append(" where update_date LIKE '" + sdf.format(new Date()) + "%' ORDER BY update_date DESC");	
			List<ReferBonusAmountModel> refBonDetails = sessionFactory.getCurrentSession().createQuery(query.toString()).list();		
			return refBonDetails;	
		}
    	else if(criteriaVal.equals("WeeklyBasis"))
    	{
    		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
    		criteria.add(Restrictions.eq("transaction_status","Success"));
    		criteria.add(Restrictions.ge("transaction_date",""));
    		criteria.add(Restrictions.le("transaction_date",""));		
    		criteria.addOrder(Order.desc("transaction_date"));
    	 
    		return criteria.list();	
    	}
    	else
    	{
    		List<ReferBonusAmountModel> refBonDetails = new ArrayList<ReferBonusAmountModel>();
    		return refBonDetails;
    		
    	}
    }
    
    
		
    @SuppressWarnings("unchecked")
	public List<PolicyAmountModel> getPolicyBonusDetails(String criteriaVal)
    {
    	if(criteriaVal.equals("DailyBasis"))
		{
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			StringBuilder query = new StringBuilder("from PolicyAmountModel");
			query.append(" where policy_end_date LIKE '" + sdf.format(new Date()) + "%' ORDER BY policy_end_date DESC");	
			List<PolicyAmountModel> policyDetails = sessionFactory.getCurrentSession().createQuery(query.toString()).list();		
			return policyDetails;	
		}
	   	else
	   	{
	   		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
	   		criteria.add(Restrictions.eq("transaction_status","Success"));
	   		criteria.add(Restrictions.ge("transaction_date",""));
	   		criteria.add(Restrictions.le("transaction_date",""));		
	   		criteria.addOrder(Order.desc("transaction_date"));
	   	    
	   		return criteria.list();	
	   	}
    }
    
    @SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getSuccessTransactionDetails()
    {
    	String hql = "from TransactionDetailModel where transaction_status='Success'";
		List<TransactionDetailModel> transactionDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
		return transactionDetails;
    }
    
    public String saveTaxReportDetails(TaxReportModel taxModel)
    {
    	String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(taxModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
    }
    
    
    @SuppressWarnings("unchecked")
	public List<TaxReportModel> getAllTaxReport(String restriction)
    {
    	List<TaxReportModel> taxDetails = new ArrayList<TaxReportModel>();
    	
    	if(restriction.equals("allReport"))
    	{
    		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TaxReportModel.class);		
    		criteria.addOrder(Order.desc("tax_report_date"));
    		return criteria.list();
    	}
    	else
    	{
    		
    	}    	
    	return taxDetails;
    }
    
    
    @SuppressWarnings("unchecked")
	public int getTodaysTaxId(String reportName)
    {
    	String hql = "select tax_id from TaxReportModel where tax_report_name='"+reportName+"'";
		List<Integer> transactionDetails = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(transactionDetails.size()>0)
		{
			return transactionDetails.get(0);
		}
		else
		{
			return 0;
		}
    }
    
    @SuppressWarnings("unchecked")
	public String HelloWorld()
    {
    	String txStatus="Success100";
    	
    	/*String hql = "from TransactionDetailModel where transaction_status='Success'";
		List<TransactionDetailModel> eachPolicyDetails = sessionFactory.getCurrentSession().createQuery(hql).list();	
		
		String costHql = "from CostManageModel";
		List<CostManageModel> costDetails = sessionFactory.getCurrentSession().createQuery(costHql).list();	
		
		float vat = 0;
		
		for(int i=0;i<costDetails.size();i++)
		{
			if(costDetails.get(i).getCost_name().equals("VAT"))
			{ 
				vat = Float.parseFloat(costDetails.get(i).getCost_value());
			}
		}
			
		long totalAmount = 0;
		
		for(int i=0;i<eachPolicyDetails.size();i++)
		{
			String amount = eachPolicyDetails.get(i).getTransaction_amount();    		
			if(amount!=null)
			{
				totalAmount += Integer.parseInt(amount.substring(0, amount.lastIndexOf(".")));
			}  			
		}
		   		
		float vatPersentageVal = (vat/100) * totalAmount;
		double totalAmountWithOutVat = totalAmount - vatPersentageVal;
		
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(today);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);
	    month = month + 1;
	    int day = cal.get(Calendar.DAY_OF_MONTH);;
	    
	    System.out.println("Today's Date ="+day+"/"+month+"/"+year);
	    
	    if(day==19)
	    {
	    	TaxReportModel taxModel = new TaxReportModel();
	    	taxModel.setTax_duration_details("20-Jul to 19-Aug 2015");
	    	taxModel.setTax_report_date(new Date());
	    	taxModel.setTax_report_name("tax_report");
	    	taxModel.setTax_status("Document Ready");
	    	taxModel.setTax_amount(totalAmount);    	    	
	    	
	    	// Save Tax Details 
	    	
			Session signUpSession=sessionFactory.openSession();
			Transaction tx=null;
			try {
			tx=signUpSession.beginTransaction();
			signUpSession.saveOrUpdate(taxModel);
			tx.commit();
			if(tx.wasCommitted()){
				txStatus="success";
			} else {
				txStatus="failure";
			}
			} catch (Exception he) {
				System.out.println(he.getMessage());
			} finally {
				signUpSession.close();
			}			 
	    }
	   */	       	
    	
    	return txStatus;
    }
    
    @SuppressWarnings("unchecked")
	public List<UserBasicInfoModel> getUserDetailModelByUserId(List<Integer> useIds)
    {
        List<UserBasicInfoModel> basicInfo = new ArrayList<UserBasicInfoModel>();
		
		for(int i=0;i<useIds.size();i++)
		{
			String hql = "from UserBasicInfoModel where user_id="+useIds.get(i)+"";
			List<UserBasicInfoModel> basicInfoHql = sessionFactory.getCurrentSession().createQuery(hql).list();
			
			if(basicInfoHql.size()>0)
			{
				basicInfo.addAll(basicInfoHql);
			}			
		}
		
		return basicInfo;
    }
    
    
    
    @SuppressWarnings("unchecked")
	public List<TDSReportModel> getAllTdsReport(String restriction)
    {
    	String hql = "from TDSReportModel";
		List<TDSReportModel> tdsReport = sessionFactory.getCurrentSession().createQuery(hql).list();
		return tdsReport;
    }
   
    
    public String saveTDSReportDetails(TDSReportModel tdsReport)
    {
    	String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(tdsReport);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
    }
    
    
    @SuppressWarnings("unchecked")
	public int getTodaysTDSId(String reportName)
    {
    	String hql = "select tds_id from TDSReportModel where tds_report_name='"+reportName+"'";
		List<Integer> tdsId = sessionFactory.getCurrentSession().createQuery(hql).list();
		if(tdsId.size()>0)
		{
			return tdsId.get(0);
		}
		else
		{
			return 0;
		}		
    }
    
    
    @SuppressWarnings("unchecked")
	public List<ReferBonusAmountModel> getAllReferenceBonusDetails()
    {
    	ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
		int fromTDSReportDate = Integer.parseInt(fileResource.getString("fromTDSReportDate"));	
		int toTDSReportDate = Integer.parseInt(fileResource.getString("toTDSReportDate"));	
		     		    
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(today);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);	   
	    int day = fromTDSReportDate;
	           
	    String startDate = toTDSReportDate+"/"+month+"/"+year;
		String endDate = day+"/"+(month+1)+"/"+year;
		
		DateFormat formatter = null;
        
	    Date startingDate = null;
	    Date endingDate = null;
	    
	    try {	    	
	        formatter = new SimpleDateFormat("dd/MM/yyyy");
	        startingDate = (Date) formatter.parse(startDate);
	        endingDate = (Date) formatter.parse(endDate);	        
	    } 
	    catch (ParseException e) 
	    {
	        e.printStackTrace();
	    }		   
	   
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
		criteria.add(Restrictions.eq("bonus_check_status","Pending"));
		criteria.add(Restrictions.ge("update_date",startingDate));
		criteria.add(Restrictions.le("update_date",endingDate));		
		criteria.addOrder(Order.desc("update_date"));
	 
		return criteria.list();	
    }
	  
    
	public List<PolicyAmountModel> getAllPolicyBonusDetails()
	{
		ResourceBundle fileResource = ResourceBundle.getBundle("resources/reportDate");
		int fromTDSReportDate = Integer.parseInt(fileResource.getString("fromTDSReportDate"));	
		int toTDSReportDate = Integer.parseInt(fileResource.getString("toTDSReportDate"));	
		     		    
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(today);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);	   
	    int day = fromTDSReportDate;
	           
	    String startDate = toTDSReportDate+"/"+month+"/"+year;
		String endDate = day+"/"+(month+1)+"/"+year;
		
		DateFormat formatter = null;
        
	    Date startingDate = null;
	    Date endingDate = null;
	    
	    try 
	    {	    	
	        formatter = new SimpleDateFormat("dd/MM/yyyy");
	        startingDate = (Date) formatter.parse(startDate);
	        endingDate = (Date) formatter.parse(endDate);	        
	    } 
	    catch (ParseException e) 
	    {
	        e.printStackTrace();
	    }		   
	   
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PolicyAmountModel.class);
		criteria.add(Restrictions.eq("policy_status","Pending"));
		criteria.add(Restrictions.ge("policy_end_date",startingDate));
		criteria.add(Restrictions.le("policy_end_date",endingDate));		
		criteria.addOrder(Order.desc("policy_end_date"));
	 
		return criteria.list();	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ReportDetailsModel> getCheckReportDetails(String restriction)
	{
		List<ReportDetailsModel> reports = new ArrayList<ReportDetailsModel>();
		if(restriction.equals("Monthly"))
		{			
		   Date today = new Date();
		   Calendar cal = Calendar.getInstance();
	       cal.setTime(today);
	       int year = cal.get(Calendar.YEAR);
	       int month = cal.get(Calendar.MONTH);
	       month = month + 1;
		   
		   String todays = "1/"+month+"/"+year;
		   
		   System.out.println("asdf"+todays);
		   
			Date startingDate = null;
			DateFormat formatter = null;
			
			try {					    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(todays);		        
			    } 
			catch (ParseException e)
			    {
			        e.printStackTrace();
			    }	

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReportDetailsModel.class);
			criteria.add(Restrictions.ge("generate_from","Check Admin"));
			criteria.add(Restrictions.ge("report_exist_status","Exist"));	
			criteria.add(Restrictions.ge("report_generate_date",startingDate));	
			criteria.addOrder(Order.desc("report_generate_date"));
			
			return criteria.list();		
		}
		else
		{
			return reports;
		}
				
	}
	
	public String saveReportDetails(ReportDetailsModel reportModel)
	{
		String txStatus="";
    	
    	Session signUpSession=sessionFactory.openSession();
		Transaction tx=null;
		try {
		tx=signUpSession.beginTransaction();
		signUpSession.saveOrUpdate(reportModel);
		tx.commit();
		if(tx.wasCommitted()){
			txStatus="success";
		} else {
			txStatus="failure";
		}
		} catch (Exception he) {
			System.out.println(he.getMessage());
		} finally {
			signUpSession.close();
		}
		
		return txStatus;
	}
	
	
	
	// Check Report Details Query Details
	
	@SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getOffLineTransacitonCheckReports(String reportType,String reportFor,String fromDate,String toDate)
	 {
		   
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(fromDate);
		        endingDate = (Date) formatter.parse(toDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    
			if(reportType.equals("Pending"))
			{										    			   	
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.eq("transaction_mode","Offline"));
				criteria.add(Restrictions.eq("transaction_status","Pending"));
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			 
				return criteria.list();		
			}
			else if(reportType.equals("Success"))
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.eq("transaction_mode","Offline"));
				criteria.add(Restrictions.eq("transaction_status","Success"));
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			 
				return criteria.list();		
			}
			else 
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.eq("transaction_mode","Offline"));
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			 
				return criteria.list();		
			}
	 }
	
	
	@SuppressWarnings("unchecked")
	public List<UserReferenceDetailsModel> getReferenceCheckReports(String reportType,String reportFor,String fromDate,String toDate)
	{
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(fromDate);
		        endingDate = (Date) formatter.parse(toDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    
			if(reportType.equals("Pending"))
			{										    			   	
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserReferenceDetailsModel.class);
				criteria.add(Restrictions.eq("reference_check_status","Pending"));
				criteria.add(Restrictions.ge("reference_accept_date",startingDate));
				criteria.add(Restrictions.le("reference_accept_date",endingDate));		
				criteria.addOrder(Order.desc("reference_accept_date"));
			 
				return criteria.list();		
			}
			else if(reportType.equals("Success"))
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserReferenceDetailsModel.class);
				criteria.add(Restrictions.eq("reference_check_status","Success"));
				criteria.add(Restrictions.ge("reference_accept_date",startingDate));
				criteria.add(Restrictions.le("reference_accept_date",endingDate));		
				criteria.addOrder(Order.desc("reference_accept_date"));
			 
				return criteria.list();		
			}
			else 
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(UserReferenceDetailsModel.class);
				criteria.add(Restrictions.ge("reference_accept_date",startingDate));
				criteria.add(Restrictions.le("reference_accept_date",endingDate));		
				criteria.addOrder(Order.desc("reference_accept_date"));
			     
				return criteria.list();		
			}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<ReferBonusAmountModel> getReferenceBonusCheckReports(String reportType,String reportFor,String fromDate,String toDate)
	{
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(fromDate);
		        endingDate = (Date) formatter.parse(toDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    
			if(reportType.equals("Pending"))
			{										    			   	
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
				criteria.add(Restrictions.eq("bonus_check_status","Pending"));
				criteria.add(Restrictions.ge("update_date",startingDate));
				criteria.add(Restrictions.le("update_date",endingDate));		
				criteria.addOrder(Order.desc("update_date"));
			 
				return criteria.list();		
			}
			else if(reportType.equals("Success"))
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
				criteria.add(Restrictions.eq("bonus_check_status","Success"));
				criteria.add(Restrictions.ge("update_date",startingDate));
				criteria.add(Restrictions.le("update_date",endingDate));		
				criteria.addOrder(Order.desc("update_date"));
			 
				return criteria.list();		
			}
			else 
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReferBonusAmountModel.class);
				criteria.add(Restrictions.ge("update_date",startingDate));
				criteria.add(Restrictions.le("update_date",endingDate));		
				criteria.addOrder(Order.desc("update_date"));
			     
				return criteria.list();		
			}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PolicyAmountModel> getPolicyBonusCheckReports(String reportType,String reportFor,String fromDate,String toDate)
	{
		DateFormat formatter = null;
        
	    Date startingDate = null;
	    Date endingDate = null;
	    
	    try {
	    	
	        formatter = new SimpleDateFormat("dd/MM/yyyy");
	        startingDate = (Date) formatter.parse(fromDate);
	        endingDate = (Date) formatter.parse(toDate);
	        
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }	
	    
	    
		if(reportType.equals("Pending"))
		{										    			   	
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PolicyAmountModel.class);
			criteria.add(Restrictions.eq("policy_status","Pending"));
			criteria.add(Restrictions.ge("policy_end_date",startingDate));
			criteria.add(Restrictions.le("policy_end_date",endingDate));		
			criteria.addOrder(Order.desc("policy_end_date"));
		 
			return criteria.list();		
		}
		else if(reportType.equals("Success"))
		{
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PolicyAmountModel.class);
			criteria.add(Restrictions.eq("policy_status","Success"));
			criteria.add(Restrictions.ge("policy_end_date",startingDate));
			criteria.add(Restrictions.le("policy_end_date",endingDate));		
			criteria.addOrder(Order.desc("policy_end_date"));
		 
			return criteria.list();		
		}
		else 
		{
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(PolicyAmountModel.class);
			criteria.add(Restrictions.ge("policy_end_date",startingDate));
			criteria.add(Restrictions.le("policy_end_date",endingDate));		
			criteria.addOrder(Order.desc("policy_end_date"));
		     
			return criteria.list();		
		}
	}
 	
	
	public String getUpdateReportStatus(int reportId,String ipAddress)
	{
		org.hibernate.Query hql = sessionFactory.getCurrentSession().createQuery("update ReportDetailsModel set report_generate_system_ip = :setIpAddress , report_exist_status = :setReportStatus where report_id = :setreportId");
		hql.setParameter("setIpAddress", ipAddress);
		hql.setParameter("setReportStatus", "Deleted");
		
		hql.setParameter("setreportId",reportId);		
	    int result  = hql.executeUpdate();
		
		if(result==1)
		{			
		    return "success";
		}
		else
		{			
			return "failed";
		}
	}
	
	
	// End of Check Report Query Details
	
	
	
	
	
	
	// Start of Order Report Details///////
	
	
	@SuppressWarnings("unchecked")
	public List<OrderDetailsModel> getOrderDetailsForReport(String reportType,String fromDate,String toDate)
	{
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(fromDate);
		        endingDate = (Date) formatter.parse(toDate);
		        
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }	
		    
		    
			if(reportType.equals("Pending"))
			{										    			   	
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class);
				criteria.add(Restrictions.eq("order_status","Pending"));
				criteria.add(Restrictions.ge("order_date",startingDate));
				criteria.add(Restrictions.le("order_date",endingDate));		
				criteria.addOrder(Order.desc("order_date"));
			 
				return criteria.list();		
			}
			else if(reportType.equals("Success"))
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class);
				criteria.add(Restrictions.eq("order_status","Success"));
				criteria.add(Restrictions.ge("order_date",startingDate));
				criteria.add(Restrictions.le("order_date",endingDate));		
				criteria.addOrder(Order.desc("order_date"));
			 
				return criteria.list();		
			}
			else if(reportType.equals("Released"))
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class);
				criteria.add(Restrictions.eq("order_status","Released"));
				criteria.add(Restrictions.ge("order_date",startingDate));
				criteria.add(Restrictions.le("order_date",endingDate));		
				criteria.addOrder(Order.desc("order_date"));
			 
				return criteria.list();		
			}
			else 
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(OrderDetailsModel.class);
				criteria.add(Restrictions.ge("order_date",startingDate));
				criteria.add(Restrictions.le("order_date",endingDate));		
				criteria.addOrder(Order.desc("order_date"));
			     
				return criteria.list();		
			}
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ReportDetailsModel> getReportDetails(String restriction,String reportGenerateFrom)
	{
		List<ReportDetailsModel> reports = new ArrayList<ReportDetailsModel>();
		if(restriction.equals("Monthly"))
		{			
		   Date today = new Date();
		   Calendar cal = Calendar.getInstance();
	       cal.setTime(today);
	       int year = cal.get(Calendar.YEAR);
	       int month = cal.get(Calendar.MONTH);
	       month = month + 1;
		   
		   String todays = "1/"+month+"/"+year;
		  
			Date startingDate = null;
			DateFormat formatter = null;
			
			try {					    	
			        formatter = new SimpleDateFormat("dd/MM/yyyy");
			        startingDate = (Date) formatter.parse(todays);		        
			    } 
			catch (ParseException e)
			    {
			        e.printStackTrace();
			    }	

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ReportDetailsModel.class);
			criteria.add(Restrictions.ge("generate_from",reportGenerateFrom));	
			criteria.add(Restrictions.ge("report_exist_status","Exist"));	
			criteria.add(Restrictions.ge("report_generate_date",startingDate));	
			criteria.addOrder(Order.desc("report_generate_date"));
			
			return criteria.list();		
		}
		else
		{
			return reports;
		}
	}
	
	
	
	
	
	//  Account Reports
	
	 @SuppressWarnings("unchecked")
	public List<TransactionDetailModel> getPurchaseTransationForReport(String reportType,String fromDate,String toDate)
	 {
		    DateFormat formatter = null;
	        
		    Date startingDate = null;
		    Date endingDate = null;
		    
		    try 
		    {
		    	
		        formatter = new SimpleDateFormat("dd/MM/yyyy");
		        startingDate = (Date) formatter.parse(fromDate);
		        endingDate = (Date) formatter.parse(toDate);
		        
		    } 
		    catch (ParseException e) 
		    {
		        e.printStackTrace();
		    }			    
		    
			if(reportType.equals("Pending"))
			{										    			   	
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.eq("transaction_status","Pending"));
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			 
				return criteria.list();		
			}
			else if(reportType.equals("Success"))
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.eq("transaction_status","Success"));
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			 
				return criteria.list();		
			}
			else 
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TransactionDetailModel.class);
				criteria.add(Restrictions.ge("transaction_date",startingDate));
				criteria.add(Restrictions.le("transaction_date",endingDate));		
				criteria.addOrder(Order.desc("transaction_date"));
			    
				return criteria.list();		
			}
	 }
	
	// End of Account Reports
	
	 
	 
	 @SuppressWarnings("unchecked")
	 public List<UserProductInterestModel> getInterestProductDetails(String OrderId)
	 {
		 String hql = "from UserProductInterestModel where order_unique_id='"+OrderId+"'";
		 List<UserProductInterestModel> productInterestId = sessionFactory.getCurrentSession().createQuery(hql).list();		
		 return productInterestId;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public List<ProductDetailsModel> getProductDetails(List<UserProductInterestModel> userInterestProduct)
	 {
		 List<ProductDetailsModel> productDetails = new ArrayList<ProductDetailsModel>();
		 
				 for(int i=0;i<userInterestProduct.size();i++)
				 {
					 // Agriculture 
					 					 
					 String agriIds = userInterestProduct.get(i).getUser_interest_agriculture();
					 if(agriIds.length()>0)
					 {
						 agriIds = agriIds.substring(0,agriIds.length()-1);
						 String productIdArray[] = agriIds.split(",");
						 
						 for(int j=0;j<productIdArray.length;j++)
						 {
							 String hql = "from ProductDetailsModel where product_id="+productIdArray[j]+"";
							 productDetails.addAll(sessionFactory.getCurrentSession().createQuery(hql).list());
						 }
					 }
					 
					 
					 // For Herbal
					 
					 String herbalIds = userInterestProduct.get(i).getUser_interest_herbal();
					 if(herbalIds.length()>0)
					 {
						 herbalIds = herbalIds.substring(0,herbalIds.length()-1);
						 String productIdArray[] = herbalIds.split(",");
						 
						 for(int j=0;j<productIdArray.length;j++)
						 {
							 String hql = "from ProductDetailsModel where product_id="+productIdArray[j]+"";
							 productDetails.addAll(sessionFactory.getCurrentSession().createQuery(hql).list());
						 }
					 }
					 
					 //For Home Care
					 
					 String hmCareIds = userInterestProduct.get(i).getUser_interest_homecare();
					 if(hmCareIds.length()>0)
					 {
						 hmCareIds = hmCareIds.substring(0,hmCareIds.length()-1);
						 String productIdArray[] = hmCareIds.split(",");
						 
						 for(int j=0;j<productIdArray.length;j++)
						 {
							 String hql = "from ProductDetailsModel where product_id="+productIdArray[j]+"";
							 productDetails.addAll(sessionFactory.getCurrentSession().createQuery(hql).list());
						 }
					 }
					 
				 }
		
		 return productDetails;
	 }
	 
}
