package com.biosys.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.biosys.service.SuperAdminService;
import com.biosys.service.SuperAdminServiceImpl;

@Configuration
public class AppConfig {
	
	@Bean
	public SuperAdminService getSampleService() {
		return new SuperAdminServiceImpl();
	}
	
	
}