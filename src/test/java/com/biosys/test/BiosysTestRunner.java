package com.biosys.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.biosys.config.AppConfig;
import com.biosys.service.SuperAdminService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class,loader=AnnotationConfigContextLoader.class)
public class BiosysTestRunner {
	
	
	@Autowired
	private SuperAdminService superAdminService;
		
	
	@BeforeClass
	public static void setUp() {
		System.out.println("-----> SETUP <-----");
	}
	
	@Test
	public void testSampleService() {
		assertEquals("class com.biosys.service.SuperAdminServiceImpl",this.superAdminService.getClass().toString());
	}
	
	@Test
	public void testSampleServiceGetAccountDescription() {
		//	Check if the return description has a 'Description:' string.
		assertTrue(superAdminService.getAllAdminDetails().contains("Bear"));
	}
	
	@Test
	public void testSampleServiceGetAccountCode() {
		//	Check if the return description has a 'Code:' string.
		assertTrue(superAdminService.getAllCostDetails().contains("2"));
	}
		
	
	
  /*@Test

	public void testSampleServiceCreateNewOrder() {
		
		AdminRegistrationModel subAdmin = new AdminRegistrationModel();
		subAdmin.setAdmin_full_name("sdfsdf");
		subAdmin.setAdmin_email_id("sdfsdf@gmial.com");
		
		Order newOrder = new Order();
		newOrder.setSecurityCode("XYZ");
		newOrder.setDescription("Description");
		if(newOrder != null) {
			assertThat(sampleService.createOrder(newOrder), instanceOf(Order.class));
			assertNotNull("Security isn't null", newOrder.getSecurityCode());
			assertNotNull("Description isn't not null", newOrder.getDescription());
		}
				
		assertNotNull("New Order is not null", newOrder);	
									
	}
	
	
	*/
	
	
	
	/*
	@Test
	public void testSampleServiceGetOrder() {
		
		Order existingOrder = sampleService.getOrder(0);

		if(existingOrder != null) {
			assertThat(sampleService.getOrder(0), instanceOf(Order.class));
			assertNotNull("Security isn't null", existingOrder.getSecurityCode());
			assertNotNull("Description isn't null", existingOrder.getDescription());
		}
		
		assertNotNull("Object is not null", existingOrder);
	}
	*/
	
		
	@AfterClass
	public static void afterTest() 
	{		
		System.out.println("-----> DESTROY <-----");
	}
	
	 	
	 		 	
}




